## RBO Payroll Registries Applications

In the project directory, you can run:

### `npm start:attachments`

Runs the Attachments App in the development mode.<br>
Open [http://localhost:3444](http://localhost:3444) to view it in the browser.

The page will reload if you make edits.<br>

### `npm start:detachments`

Runs the Detachments App in the development mode.<br>
Open [http://localhost:3444](http://localhost:3444) to view it in the browser.

The page will reload if you make edits.<br>

### `--@rbo/payroll-registries:stand=bss6|bss7|bss15|bss5`

You can run App in the development mode with different stand (default - bss6):

### `--@rbo/payroll-registries:port=3444`

You can run App in the development mode with different port (default - 3444):

### `--@rbo/payroll-registries:locale=ru|en`

You can run App in the development mode with different locale (default - ru):

### `--@rbo/payroll-registries:mock=true`

You can run App in the development mode with mock (default - false):

### `npm publish`

Builds the npm package for production to the `dist` folder.<br>

Your apps is ready!

#### RBO Platform FrontDev Team

If you have some question about this repository contact with RBO Platform FrontDev Team:

[LEVENETS Sergey](mailto:Sergey.LEVENETS@raiffeisen.ru)

[SEREBRYAKOV Aleksey](mailto:Aleksey.V.SEREBRYAKOV@raiffeisen.ru)

[KARGALSKAYA Olesya](mailto:Olesya.A.KARGALSKAYA@raiffeisen.ru)
