import { call } from 'redux-saga/effects';

import actions from './actions';

export function* logout() {
  yield call(actions.logout);
}

export function* notificationSuccess(params) {
  const { title, message } = params;
  yield call(actions.notification, {
    level: 'success',
    title: title || '',
    message: message || '',
  });
}

export function* notificationWarning(params) {
  const { title, message } = params;
  yield call(actions.notification, {
    level: 'warning',
    title: title || '',
    message: message || '',
  });
}

export function* notificationError(params) {
  const { name, title, message, stack } = params;
  let notificationMessage;
  if (name === 'RBO Controls Error') {
    stack.forEach(({ text }) => {
      notificationMessage += `${text}\n`;
    });
  } else {
    notificationMessage = message || '';
  }
  yield call(actions.notification, {
    level: 'error',
    title: title || '',
    message: notificationMessage,
  });
}

export function* notificationConfirm(params) {
  const { level, title, message, ok, cancel } = params;
  yield call(actions.notification, {
    type: 'confirm',
    autoDismiss: 0,
    level: level || 'success',
    title: title || '',
    message: message || '',
    success: ok,
    cancel,
  });
}

export function* sign(params) {
  const { ids, callbacks } = params;
  yield call(actions.sign, {
    docIds: ids,
    callbacks: {
      cancel: callbacks.cancel,
      success: callbacks.success,
      fail: callbacks.fail,
    },
  });
}

export function* removeSign(params) {
  const { ids, callbacks } = params;
  yield call(actions.removeSign, {
    docIds: ids,
    callbacks: {
      cancel: callbacks.cancel,
      success: callbacks.success,
      fail: callbacks.fail,
    },
  });
}

export function* saved(param) {
  yield call(actions.saved, param);
}
