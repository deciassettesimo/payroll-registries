const actions = {};

export function configureActions({ platformActions }) {
  Object.keys(platformActions).forEach(key => {
    actions[key] = platformActions[key];
  });
}

export default actions;
