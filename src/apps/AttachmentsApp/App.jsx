import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';

import Loader from '@rbo/rbo-components/lib/Loader/index';

import RboConfirmModal from 'common/components/RboConfirmModal';
import RboDocSaveAsTemplateModal from 'common/components/RboDocSaveAsTemplateModal';
import RboServerError from 'common/components/RboServerError';

import ExportModal from './ExportModal';

export default class App extends PureComponent {
  static propTypes = {
    isError: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    serverError: PropTypes.shape(),
    routes: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        path: PropTypes.string.isRequired,
        component: PropTypes.func.isRequired,
        exact: PropTypes.bool,
      }),
    ).isRequired,
  };

  static defaultProps = {
    serverError: null,
  };

  render() {
    const { isLoading, isError, serverError, routes } = this.props;

    if (isLoading) return <Loader dimension={Loader.REFS.DIMENSIONS.L} isCentered />;
    if (isError) return <RboServerError serverData={serverError} />;

    return (
      <Fragment>
        <Switch>
          {routes.map(route => (
            <Route key={route.id} path={route.path} component={route.component} exact={route.exact} />
          ))}
        </Switch>
        <RboConfirmModal />
        <RboDocSaveAsTemplateModal />
        <ExportModal />
      </Fragment>
    );
  }
}
