import { REDUCER_KEY, ACTIONS } from './constants';
import { ROUTES, PERMISSIONS } from './config';

import exportModalReducers from './ExportModal/reducers';
import itemContainerReducers from './Item/reducers';
import listContainerReducers from './List/reducers';

const initialState = {
  isInitialized: false,
  isMounted: false,
  isError: false,
  serverError: null,
  routes: ROUTES,
  routerBasename: '',
  permissions: PERMISSIONS,
};

const mountRequest = (state, { routerBasename, permissions }) => ({
  ...state,
  isMounted: false,
  isError: false,
  serverError: null,
  routerBasename,
  routes: ROUTES.map(item => ({ ...item, path: `${routerBasename}${item.path}` })),
  permissions,
});

const mountSuccess = (state, payload) => ({ ...state, isInitialized: true, isMounted: true, ...payload });

const mountFail = (state, serverError) => ({ ...state, isMounted: true, isError: true, serverError });

const unmount = state => ({ ...state, isMounted: false, isError: false, serverError: null });

const saveSettingsSuccess = (state, payload) => ({ ...state, settings: payload.settings });

function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest(state, action.payload);

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.UNMOUNT:
      return unmount(state);

    case ACTIONS.SAVE_SETTINGS_SUCCESS:
      return saveSettingsSuccess(state, action.payload);

    default:
      return state;
  }
}

export default {
  [REDUCER_KEY]: reducer,
  ...exportModalReducers,
  ...itemContainerReducers,
  ...listContainerReducers,
};
