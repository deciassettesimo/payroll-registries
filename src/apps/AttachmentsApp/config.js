import { PERMISSIONS_IDS } from 'common/constants';

import { ROUTES_IDS, ROUTES_PATHS } from './constants';
import * as containers from './containers';

export const ROUTES = [
  {
    id: ROUTES_IDS.INDEX,
    path: ROUTES_PATHS.INDEX,
    component: containers.List,
    exact: true,
  },
  {
    id: ROUTES_IDS.ARCHIVE,
    path: ROUTES_PATHS.ARCHIVE,
    component: containers.List,
    exact: true,
  },
  {
    id: ROUTES_IDS.TRASH,
    path: ROUTES_PATHS.TRASH,
    component: containers.List,
    exact: true,
  },
  {
    id: ROUTES_IDS.TEMPLATE,
    path: ROUTES_PATHS.TEMPLATE,
    component: containers.Item,
    exact: true,
  },
  {
    id: ROUTES_IDS.ITEM,
    path: ROUTES_PATHS.ITEM,
    component: containers.Item,
    exact: true,
  },
  {
    id: ROUTES_IDS.NOT_FOUND,
    path: ROUTES_PATHS.NOT_FOUND,
    component: containers.NotFound,
  },
];

export const PERMISSIONS = {
  [PERMISSIONS_IDS.CREATE]: false,
  [PERMISSIONS_IDS.IMPORT]: false,
  [PERMISSIONS_IDS.EXPORT]: false,
  [PERMISSIONS_IDS.CREATE_FROM_TEMPLATE]: false,
};
