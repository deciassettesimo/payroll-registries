import { compose } from 'redux';
import { connect } from 'react-redux';
import { lifecycle, withHandlers } from 'recompose';
import { withLastLocation } from 'react-router-last-location';

import { lifecycleDoc } from 'hocs/lifecycles';
import {
  handlersDocFields,
  handlersDocStructure,
  handlersDocOperations,
  handlersDocTemplates,
  handlersDocTop,
} from 'hocs/handlers';

import Item from './Item';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default compose(
  withLastLocation,
  connect(
    mapStateToProps,
    actions,
  ),
  lifecycle(lifecycleDoc),
  withHandlers({
    ...handlersDocFields,
    ...handlersDocStructure,
    ...handlersDocOperations,
    ...handlersDocTemplates,
    ...handlersDocTop,
  }),
)(Item);
