import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import {
  InputDateCalendar,
  InputDigital,
  InputSearch,
  InputSelect,
  InputViewMode,
} from '@rbo/rbo-components/lib/Input';
import { Required } from '@rbo/rbo-components/lib/Styles';
import Tooltip from '@rbo/rbo-components/lib/Tooltip';

import { SECTION_ID, FIELD_ID } from '../../constants';

export default class CommonInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.COMMON_INFO }}>
        <Section.Header>
          <Section.Title>{LABELS.SECTIONS.COMMON_INFO}</Section.Title>
        </Section.Header>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell width="auto">
                  <Label htmlFor={FIELD_ID.NUMBER}>
                    {LABELS.FIELDS.NUMBER}
                    {isEditable && <Required />}
                  </Label>
                  <Field width="120px">
                    {isEditable ? (
                      <InputDigital
                        id={FIELD_ID.NUMBER}
                        value={sectionData[FIELD_ID.NUMBER]}
                        isWarning={formWarnings[FIELD_ID.NUMBER]}
                        isError={formErrors[FIELD_ID.NUMBER]}
                        disabled={formDisabled[FIELD_ID.NUMBER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={11}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.NUMBER}
                        value={sectionData[FIELD_ID.NUMBER]}
                        isWarning={formWarnings[FIELD_ID.NUMBER]}
                        isError={formErrors[FIELD_ID.NUMBER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="auto">
                  <Label htmlFor={FIELD_ID.DATE}>
                    {LABELS.FIELDS.DATE}
                    {isEditable && <Required />}
                  </Label>
                  <Field width="120px">
                    {isEditable ? (
                      <InputDateCalendar
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.DATE}
                        value={sectionData[FIELD_ID.DATE]}
                        isWarning={formWarnings[FIELD_ID.DATE]}
                        isError={formErrors[FIELD_ID.DATE]}
                        disabled={formDisabled[FIELD_ID.DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.DATE}
                        value={sectionData[FIELD_ID.DATE]}
                        isWarning={formWarnings[FIELD_ID.DATE]}
                        isError={formErrors[FIELD_ID.DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CUSTOMER_NAME}>{LABELS.FIELDS.CUSTOMER_NAME}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.CUSTOMER_NAME}
                      value={sectionData[FIELD_ID.CUSTOMER_NAME]}
                      isWarning={formWarnings[FIELD_ID.CUSTOMER_NAME]}
                      isError={formErrors[FIELD_ID.CUSTOMER_NAME]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.CUSTOMER_INN}>{LABELS.FIELDS.CUSTOMER_INN}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.CUSTOMER_INN}
                      value={sectionData[FIELD_ID.CUSTOMER_INN]}
                      isWarning={formWarnings[FIELD_ID.CUSTOMER_INN]}
                      isError={formErrors[FIELD_ID.CUSTOMER_INN]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.CUSTOMER_OGRN}>{LABELS.FIELDS.CUSTOMER_OGRN}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.CUSTOMER_OGRN}
                      value={sectionData[FIELD_ID.CUSTOMER_OGRN]}
                      isWarning={formWarnings[FIELD_ID.CUSTOMER_OGRN]}
                      isError={formErrors[FIELD_ID.CUSTOMER_OGRN]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_CITY}>
                    {LABELS.FIELDS.CARD_BRANCH_CITY}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.CARD_BRANCH_CITY}
                        value={sectionData[FIELD_ID.CARD_BRANCH_CITY]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_CITY]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_CITY]}
                        disabled={formDisabled[FIELD_ID.CARD_BRANCH_CITY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.CARD_BRANCH_CITY].items}
                        isSearching={sectionDictionaries[FIELD_ID.CARD_BRANCH_CITY].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CARD_BRANCH_CITY}
                        value={sectionData[FIELD_ID.CARD_BRANCH_CITY]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_CITY]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_CITY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_NAME}>
                    {LABELS.FIELDS.CARD_BRANCH_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.CARD_BRANCH_NAME}
                        value={sectionData[FIELD_ID.CARD_BRANCH_NAME]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_NAME]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_NAME]}
                        disabled={formDisabled[FIELD_ID.CARD_BRANCH_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.CARD_BRANCH_NAME].items}
                        isSearching={sectionDictionaries[FIELD_ID.CARD_BRANCH_NAME].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CARD_BRANCH_NAME}
                        value={sectionData[FIELD_ID.CARD_BRANCH_NAME]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_NAME]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_ADDRESS}>{LABELS.FIELDS.CARD_BRANCH_ADDRESS}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.CARD_BRANCH_ADDRESS}
                      value={sectionData[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      isWarning={formWarnings[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      isError={formErrors[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.TARIFF_PLAN_NAME}>
                    {LABELS.FIELDS.TARIFF_PLAN_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.TARIFF_PLAN_NAME}
                        value={sectionData[FIELD_ID.TARIFF_PLAN_NAME]}
                        isWarning={formWarnings[FIELD_ID.TARIFF_PLAN_NAME]}
                        isError={formErrors[FIELD_ID.TARIFF_PLAN_NAME]}
                        disabled={formDisabled[FIELD_ID.TARIFF_PLAN_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.TARIFF_PLAN_NAME].items}
                        optionRenderer={optionRenderer}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.TARIFF_PLAN_NAME}
                        value={sectionData[FIELD_ID.TARIFF_PLAN_NAME]}
                        isWarning={formWarnings[FIELD_ID.TARIFF_PLAN_NAME]}
                        isError={formErrors[FIELD_ID.TARIFF_PLAN_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.LOAD_SCHEME_NAME}>
                    {LABELS.FIELDS.LOAD_SCHEME_NAME}
                    {isEditable && <Required />}
                    <Tooltip>{LABELS.OTHER.LOAD_SCHEME_NAME_TOOLTIP}</Tooltip>
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.LOAD_SCHEME_NAME}
                        value={sectionData[FIELD_ID.LOAD_SCHEME_NAME]}
                        isWarning={formWarnings[FIELD_ID.LOAD_SCHEME_NAME]}
                        isError={formErrors[FIELD_ID.LOAD_SCHEME_NAME]}
                        disabled={formDisabled[FIELD_ID.LOAD_SCHEME_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.LOAD_SCHEME_NAME].items}
                        optionRenderer={optionRenderer}
                        placeholder={
                          !sectionData[FIELD_ID.TARIFF_PLAN_NAME]
                            ? LABELS.OTHER.LOAD_SCHEME_NAME_DISABLED_PLACEHOLDER
                            : null
                        }
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.LOAD_SCHEME_NAME}
                        value={sectionData[FIELD_ID.LOAD_SCHEME_NAME]}
                        isWarning={formWarnings[FIELD_ID.LOAD_SCHEME_NAME]}
                        isError={formErrors[FIELD_ID.LOAD_SCHEME_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
