import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputSuggest } from '@rbo/rbo-components/lib/Input';
import Button from '@rbo/rbo-components/lib/Button';
import IconDictionaryAdd from '@rbo/rbo-components/lib/Icon/IconDictionaryAdd';

import { SECTION_ID, FIELD_ID } from '../../constants';

export default class OtherData extends Component {
  static propTypes = {
    isEditable: PropTypes.bool.isRequired,
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    onSaveResponsiblePersonButtonClick: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
      onSaveResponsiblePersonButtonClick,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.RESPONSIBLE_PERSON }}>
        <Section.Header>
          <Section.Title>{LABELS.SECTIONS.RESPONSIBLE_PERSON}</Section.Title>
        </Section.Header>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.OFFICIAL_NAME}>{LABELS.FIELDS.OFFICIAL_NAME}</Label>
                  <Field>
                    {isEditable ? (
                      <InputSuggest
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.OFFICIAL_NAME}
                        value={sectionData[FIELD_ID.OFFICIAL_NAME]}
                        isWarning={formWarnings[FIELD_ID.OFFICIAL_NAME]}
                        isError={formErrors[FIELD_ID.OFFICIAL_NAME]}
                        disabled={formDisabled[FIELD_ID.OFFICIAL_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.OFFICIAL_NAME].items}
                        isSearching={sectionDictionaries[FIELD_ID.OFFICIAL_NAME].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                        maxLength={60}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.OFFICIAL_NAME}
                        value={sectionData[FIELD_ID.OFFICIAL_NAME]}
                        isWarning={formWarnings[FIELD_ID.OFFICIAL_NAME]}
                        isError={formErrors[FIELD_ID.OFFICIAL_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="44%">
                  <Label htmlFor={FIELD_ID.OFFICIAL_PHONE}>{LABELS.FIELDS.OFFICIAL_PHONE}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.OFFICIAL_PHONE}
                        value={sectionData[FIELD_ID.OFFICIAL_PHONE]}
                        isWarning={formWarnings[FIELD_ID.OFFICIAL_PHONE]}
                        isError={formErrors[FIELD_ID.OFFICIAL_PHONE]}
                        disabled={formDisabled[FIELD_ID.OFFICIAL_PHONE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={40}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.OFFICIAL_PHONE}
                        value={sectionData[FIELD_ID.OFFICIAL_PHONE]}
                        isWarning={formWarnings[FIELD_ID.OFFICIAL_PHONE]}
                        isError={formErrors[FIELD_ID.OFFICIAL_PHONE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                {isEditable && (
                  <Cell width="56%">
                    <Field>
                      <Button
                        id={FIELD_ID.SAVE_RESPONSIBLE_PERSON_BUTTON}
                        disabled={formDisabled[FIELD_ID.SAVE_RESPONSIBLE_PERSON_BUTTON]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onClick={onSaveResponsiblePersonButtonClick}
                      >
                        <IconDictionaryAdd dimension={IconDictionaryAdd.REFS.DIMENSIONS.XS} />
                        <span>{LABELS.FIELDS.SAVE_RESPONSIBLE_PERSON_BUTTON}</span>
                      </Button>
                    </Field>
                  </Cell>
                )}
              </Row>
            </Block>
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
