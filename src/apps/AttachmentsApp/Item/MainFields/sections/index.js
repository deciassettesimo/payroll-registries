export { default as CommonInfo } from './CommonInfo';
export { default as OtherData } from './OtherData';
export { default as ResponsiblePerson } from './ResponsiblePerson';
export { default as TemplateName } from './TemplateName';
