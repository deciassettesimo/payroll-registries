import { escapeRegExp, getDictionaryValue } from 'utils';

import { FIELD_ID } from './constants';

export function mapDictionaryItems(dictionary, key, documentData) {
  const { items } = dictionary;
  const searchValue = escapeRegExp(dictionary.searchValue);
  let searchRegExp;
  switch (key) {
    case FIELD_ID.CARD_BRANCH_CITY:
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.name,
          }))
          .filter(item => item.title && item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.CARD_BRANCH_NAME:
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.name,
            describe: item.address,
            city: item.city,
          }))
          /* Отделение выдачи карт
           * Список подразделений должен быть отфильтрован в зависимости от выбранного города, если он выбран
           * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405812 */
          .filter(
            item => !documentData[FIELD_ID.CARD_BRANCH_CITY] || item.city === documentData[FIELD_ID.CARD_BRANCH_CITY],
          )
          .filter(item => item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.TARIFF_PLAN_NAME:
    case FIELD_ID.LOAD_SCHEME_NAME:
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.number,
            title: item.name,
          }))
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.OFFICIAL_NAME:
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.fio,
            describe: item.phone,
          }))
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case 'templates':
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item[FIELD_ID.ID],
            title: item[FIELD_ID.TEMPLATE_NAME],
            describe: item[FIELD_ID.CUSTOMER_NAME],
          }))
          .filter(item => item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    default:
      return dictionary;
  }
}

export const changeState = {
  /** Город выдачи карт */
  [FIELD_ID.CARD_BRANCH_CITY]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.CARD_BRANCH_CITY;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const cardBranchCity = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (cardBranchCity !== state.documentData[fieldId]) {
      /* Отделение выдачи карт
       * Значение в поле очищается при смене города
       * Если у организации только одно отделение, то поле должно заполняться его наименованием по умолчанию.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405812 */
      const cardBranchDictionaryItems = state.documentDictionaries[FIELD_ID.CARD_BRANCH_NAME].items.filter(
        item => item.city === cardBranchCity,
      );
      const cardBranchDictionaryItem = cardBranchDictionaryItems.length === 1 ? cardBranchDictionaryItems[0] : null;
      const cardBranchAddress = cardBranchDictionaryItem ? cardBranchDictionaryItem.address : null;
      const cardBranchBic = cardBranchDictionaryItem ? cardBranchDictionaryItem.bic : null;
      const cardBranchId = cardBranchDictionaryItem ? cardBranchDictionaryItem.id : null;
      const cardBranchName = cardBranchDictionaryItem ? cardBranchDictionaryItem.name : null;

      changedFields = {
        [fieldId]: cardBranchCity,
        [FIELD_ID.CARD_BRANCH_ADDRESS]: cardBranchAddress,
        [FIELD_ID.CARD_BRANCH_BIC]: cardBranchBic,
        [FIELD_ID.CARD_BRANCH_ID]: cardBranchId,
        [FIELD_ID.CARD_BRANCH_NAME]: cardBranchName,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },

  /** Отделение выдачи карт */
  [FIELD_ID.CARD_BRANCH_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.CARD_BRANCH_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const cardBranchName = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (cardBranchName !== state.documentData[fieldId]) {
      const cardBranchAddress = (selectedDictionaryItem && selectedDictionaryItem.address) || null;
      const cardBranchBic = (selectedDictionaryItem && selectedDictionaryItem.bic) || null;
      const cardBranchCity = fieldValue
        ? (selectedDictionaryItem && selectedDictionaryItem.city) || null
        : state.documentData[FIELD_ID.CARD_BRANCH_CITY];
      const cardBranchId = (selectedDictionaryItem && selectedDictionaryItem.id) || null;
      changedFields = {
        [fieldId]: cardBranchName,
        [FIELD_ID.CARD_BRANCH_ADDRESS]: cardBranchAddress,
        [FIELD_ID.CARD_BRANCH_BIC]: cardBranchBic,
        [FIELD_ID.CARD_BRANCH_CITY]: cardBranchCity,
        [FIELD_ID.CARD_BRANCH_ID]: cardBranchId,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },

  /** Тарифный план */
  [FIELD_ID.TARIFF_PLAN_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.TARIFF_PLAN_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'number');
    const tariffPlanName = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (tariffPlanName !== state.documentData[fieldId]) {
      const tariffPlanNumber = (selectedDictionaryItem && selectedDictionaryItem.number) || null;
      const loadSchemeName = null;
      const loadSchemeNumber = null;
      changedFields = {
        [fieldId]: tariffPlanName,
        [FIELD_ID.TARIFF_PLAN_NUMBER]: tariffPlanNumber,
        [FIELD_ID.LOAD_SCHEME_NAME]: loadSchemeName,
        [FIELD_ID.LOAD_SCHEME_NUMBER]: loadSchemeNumber,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Схема загрузки */
  [FIELD_ID.LOAD_SCHEME_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.LOAD_SCHEME_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'number');
    const loadSchemeName = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (loadSchemeName !== state.documentData[fieldId]) {
      const loadSchemeNumber = (selectedDictionaryItem && selectedDictionaryItem.number) || null;
      changedFields = {
        [fieldId]: loadSchemeName,
        [FIELD_ID.LOAD_SCHEME_NUMBER]: loadSchemeNumber,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Исполнитель */
  [FIELD_ID.OFFICIAL_NAME]: (state, { fieldValue, dictionaryValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.OFFICIAL_NAME;

    if (dictionaryValue) {
      const selectedDictionaryItem = getDictionaryValue(dictionaryValue, state.documentDictionaries[fieldId], 'id');
      const officialName = (selectedDictionaryItem && selectedDictionaryItem.fio) || null;
      const officialPhone = (selectedDictionaryItem && selectedDictionaryItem.phone) || null;
      changedFields = {
        [fieldId]: officialName,
        [FIELD_ID.OFFICIAL_PHONE]: officialPhone,
      };
    } else {
      changedFields = {
        [fieldId]: fieldValue,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },
};
