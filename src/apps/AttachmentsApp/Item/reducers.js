import { v4 } from 'uuid';

import getDocErrorsFromEmployees from 'utils/getDocErrorsFromEmployees';

import employeesReducers from './Employees/reducers';
import employeeReducers from './Employee/reducers';
import {
  REDUCER_KEY,
  VIEW_ID,
  TAB_ID,
  FIELD_ID,
  EMPLOYEES_INFO_ITEM_FIELD_ID,
  OPERATION_ID,
  ACTIONS,
} from './constants';
import { STRUCTURE, DICTIONARIES, OPERATIONS } from './config';
import { changeState } from './utils';

const initialState = {
  isMounted: false,
  isError: false,
  serverError: null,
  isSaved: true,
  isLoading: true,
  isDisabled: false,
  isEditable: false,
  isTemplate: false,
  isArchived: false,
  isChecking: false,
  organization: null,
  operations: OPERATIONS.map(item => ({ ...item, disabled: false, progress: false })),
  documentHistory: { isVisible: false, items: [] },
  documentSignatures: { isVisible: false, items: [] },
  activeField: null,
  activeError: null,
  documentStructure: STRUCTURE.map(tab => ({ ...tab, isActive: tab.id === TAB_ID.MAIN_FIELDS })),
  documentData: {},
  documentErrors: [],
  documentDictionaries: Object.keys(DICTIONARIES).reduce(
    (result, key) => ({ ...result, [key]: { items: [], searchValue: '', isSearching: false } }),
    {},
  ),
  templates: { items: [], defaultValue: null, value: null, searchValue: '', isSearching: false, isVisible: false },

  view: VIEW_ID.MAIN,
  editedEmployee: null,
};

const mountRequest = () => ({ ...initialState, isMounted: true, isLoading: true });

const unmount = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, payload) => {
  const {
    isTemplate,
    isArchived,
    isEditable,
    organization,
    documentData,
    documentErrors,
    documentDictionaries,
    templates,
  } = payload;

  return {
    ...state,
    isLoading: false,
    isTemplate,
    isArchived,
    isEditable,
    organization,
    documentData,
    documentErrors: documentErrors.concat(
      getDocErrorsFromEmployees(
        documentData[FIELD_ID.EMPLOYEES_INFO],
        EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
      ),
    ),
    documentDictionaries: {
      ...state.documentDictionaries,
      ...Object.keys(documentDictionaries).reduce(
        (result, key) => ({ ...result, [key]: { ...state.documentDictionaries[key], ...documentDictionaries[key] } }),
        {},
      ),
    },
    templates: {
      ...state.templates,
      ...templates,
    },
  };
};

const mountFail = (state, serverError) => ({ ...state, isMounted: true, isLoading: false, isError: true, serverError });

const refreshChecking = (state, { documentData }) => ({
  ...state,
  documentData: { ...state.documentData, ...documentData },
});

const refreshSuccess = (state, { isArchived, isEditable, documentData, documentErrors }) => ({
  ...state,
  isArchived,
  isEditable,
  documentData,
  documentErrors: documentErrors.concat(
    getDocErrorsFromEmployees(
      documentData[FIELD_ID.EMPLOYEES_INFO],
      EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
    ),
  ),
});

const refreshFail = (state, serverError) => ({ ...state, isError: true, serverError });

const getDataRequest = state => ({ ...state, isDisabled: true });

const getDataFinish = state => ({ ...state, isDisabled: false });

const saveDataRequest = state => ({ ...state, isDisabled: true });

const saveDataSuccess = (state, payload) => ({
  ...state,
  isDisabled: false,
  documentData: {
    ...state.documentData,
    [FIELD_ID.ID]: payload[FIELD_ID.ID],
    [FIELD_ID.STATUS]: payload[FIELD_ID.STATUS],
    [FIELD_ID.ALLOWED_SM_ACTIONS]: payload[FIELD_ID.ALLOWED_SM_ACTIONS],
  },
  documentErrors: payload.errors,
});

const saveDataFail = (state, payload) => ({
  ...state,
  isDisabled: false,
  documentErrors: payload ? payload.errors : state.documentErrors,
});

const changeTab = (state, { tabId }) => ({
  ...state,
  documentStructure: state.documentStructure.map(tab => ({ ...tab, isActive: tab.id === tabId })),
});

const closeTop = state => ({
  ...state,
  documentHistory: { isVisible: false, items: [] },
  documentSignatures: { isVisible: false, items: [] },
});

const activateError = (state, { errorId }) => ({ ...state, activeError: errorId });

const fieldFocus = (state, { fieldId }) => ({ ...state, activeField: fieldId, activeError: null });

const fieldBlur = state => ({ ...state, activeField: null });

const changeData = (state, { fieldId, fieldValue, dictionaryValue }) => {
  switch (fieldId) {
    case FIELD_ID.CARD_BRANCH_CITY:
    case FIELD_ID.CARD_BRANCH_NAME:
    case FIELD_ID.TARIFF_PLAN_NAME:
    case FIELD_ID.LOAD_SCHEME_NAME:
      return changeState[fieldId](state, { fieldValue });
    case FIELD_ID.OFFICIAL_NAME:
      return changeState[fieldId](state, { fieldValue, dictionaryValue });
    case FIELD_ID.IS_CHECK:
      return fieldValue ? state : { ...state, documentData: { ...state.documentData, [fieldId]: fieldValue } };
    default:
      return { ...state, documentData: { ...state.documentData, [fieldId]: fieldValue } };
  }
};

const isCheckConfirm = state => ({ ...state, documentData: { ...state.documentData, [FIELD_ID.IS_CHECK]: true } });

const dictionarySearch = (state, { dictionaryId, dictionarySearchValue }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      searchValue: dictionarySearchValue,
    },
  },
});

const dictionarySearchRequest = (state, { dictionaryId }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      isSearching: true,
    },
  },
});

const dictionarySearchSuccess = (state, { dictionaryId, dictionaryItems }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      items: dictionaryItems,
      isSearching: false,
    },
  },
});

const dictionarySearchCancel = (state, { dictionaryId }) =>
  dictionaryId === state.activeField
    ? state
    : {
        ...state,
        documentDictionaries: {
          ...state.documentDictionaries,
          [dictionaryId]: {
            ...state.documentDictionaries[dictionaryId],
            isSearching: false,
          },
        },
      };

const dictionarySearchFail = (state, { dictionaryId }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      isSearching: false,
    },
  },
});

const templatesSearch = (state, { searchValue }) => ({
  ...state,
  templates: {
    ...state.templates,
    searchValue,
  },
});

const templatesChoose = (state, { templateId }) => ({
  ...state,
  templates: {
    ...state.templates,
    value: templateId,
    searchValue: '',
  },
});

const operationStart = (state, operationId, disableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? true : operation.progress,
    disabled: disableOtherOperations ? true : operation.disabled,
  })),
});

const operationFinish = (state, operationId, enableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? false : operation.progress,
    disabled: enableOtherOperations ? false : operation.disabled,
  })),
});

const operationStateHistorySuccess = (state, payload) => ({
  ...state,
  documentHistory: { isVisible: true, items: payload },
  documentSignatures: { isVisible: false, items: [] },
});

const operationCheckSignSuccess = (state, payload) => ({
  ...state,
  documentSignatures: { isVisible: true, items: payload },
  documentHistory: { isVisible: false, items: [] },
});

const openEmployee = (state, { id }) => ({
  ...state,
  view: VIEW_ID.EMPLOYEE,
  editedEmployee: id,
});

const removeEmployees = (state, { ids }) => {
  const employeesInfo = state.documentData[FIELD_ID.EMPLOYEES_INFO]
    .filter(item => !ids.includes(item[EMPLOYEES_INFO_ITEM_FIELD_ID.ID]))
    .sort((a, b) => (a[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] > b[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] ? 1 : -1))
    .map((item, index) => ({ ...item, [EMPLOYEES_INFO_ITEM_FIELD_ID.NPP]: index + 1 }));

  return {
    ...state,
    documentData: {
      ...state.documentData,
      [FIELD_ID.EMPLOYEES_INFO]: employeesInfo,
    },
  };
};

const addEmployees = (state, { employeesInfo }) => ({
  ...state,
  documentData: {
    ...state.documentData,
    [FIELD_ID.EMPLOYEES_INFO]: state.documentData[FIELD_ID.EMPLOYEES_INFO]
      .map(item => item)
      .concat(employeesInfo.map(item => ({ ...item, [EMPLOYEES_INFO_ITEM_FIELD_ID.ID]: v4() })))
      .sort((a, b) => (a[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] > b[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] ? 1 : -1))
      .map((item, index) => ({ ...item, [EMPLOYEES_INFO_ITEM_FIELD_ID.NPP]: index + 1 })),
  },
});

const saveEmployee = (state, employeesInfoItemData) => {
  const id = employeesInfoItemData[EMPLOYEES_INFO_ITEM_FIELD_ID.ID] || v4();

  const employeesInfo = state.documentData[FIELD_ID.EMPLOYEES_INFO].filter(
    item => item[EMPLOYEES_INFO_ITEM_FIELD_ID.ID] !== id,
  );
  employeesInfo.push({ ...employeesInfoItemData, [EMPLOYEES_INFO_ITEM_FIELD_ID.ID]: id });

  return {
    ...state,
    documentData: {
      ...state.documentData,
      [FIELD_ID.EMPLOYEES_INFO]: employeesInfo.sort((a, b) =>
        a[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] > b[EMPLOYEES_INFO_ITEM_FIELD_ID.NPP] ? 1 : -1,
      ),
    },
    editedEmployee: id,
  };
};

const closeEmployee = state => ({
  ...state,
  view: VIEW_ID.MAIN,
  editedEmployee: null,
});

function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest();

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.REFRESH_CHECKING:
      return refreshChecking(state, action.payload);

    case ACTIONS.REFRESH_SUCCESS:
      return refreshSuccess(state, action.payload);

    case ACTIONS.REFRESH_FAIL:
      return refreshFail(state, action.error);

    case ACTIONS.GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.GET_DATA_SUCCESS:
    case ACTIONS.GET_DATA_FAIL:
      return getDataFinish(state);

    case ACTIONS.SAVE_DATA_REQUEST:
      return saveDataRequest(state);

    case ACTIONS.SAVE_DATA_SUCCESS:
      return {
        ...saveDataSuccess(state, action.payload),
        isSaved: true,
      };

    case ACTIONS.SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.CLOSE_TOP:
      return closeTop(state);

    case ACTIONS.ACTIVATE_ERROR:
      return activateError(state, action.payload);

    case ACTIONS.FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.FIELD_BLUR:
      return fieldBlur(state);

    case ACTIONS.CHANGE_DATA:
      return {
        ...changeData(state, action.payload),
        isSaved: false,
      };

    case ACTIONS.IS_CHECK_CONFIRM:
      return {
        ...isCheckConfirm(state),
        isSaved: false,
      };

    case ACTIONS.DICTIONARY_SEARCH:
      return dictionarySearch(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_REQUEST:
      return dictionarySearchRequest(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_SUCCESS:
      return dictionarySearchSuccess(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_CANCEL:
      return dictionarySearchCancel(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_FAIL:
      return dictionarySearchFail(state, action.payload);

    case ACTIONS.TEMPLATES_SEARCH:
      return templatesSearch(state, action.payload);
    case ACTIONS.TEMPLATES_CHOOSE:
      return templatesChoose(state, action.payload);

    case ACTIONS.OPERATION_ARCHIVE:
      return operationStart(state, OPERATION_ID.ARCHIVE, true);
    case ACTIONS.OPERATION_ARCHIVE_SUCCESS:
      return { ...operationFinish(state, OPERATION_ID.ARCHIVE, true), isSaved: true };
    case ACTIONS.OPERATION_ARCHIVE_FAIL:
      return operationFinish(state, OPERATION_ID.ARCHIVE, true);

    case ACTIONS.OPERATION_CHECK_SIGN:
      return operationStart(state, OPERATION_ID.CHECK_SIGN, false);
    case ACTIONS.OPERATION_CHECK_SIGN_SUCCESS:
      return operationCheckSignSuccess(operationFinish(state, OPERATION_ID.CHECK_SIGN, false), action.payload);
    case ACTIONS.OPERATION_CHECK_SIGN_FAIL:
      return operationFinish(state, OPERATION_ID.CHECK_SIGN, false);

    case ACTIONS.OPERATION_REMOVE:
      return operationStart(state, OPERATION_ID.REMOVE, true);
    case ACTIONS.OPERATION_REMOVE_SUCCESS:
      return { ...operationFinish(state, OPERATION_ID.REMOVE, true), isSaved: true };
    case ACTIONS.OPERATION_REMOVE_CANCEL:
    case ACTIONS.OPERATION_REMOVE_FAIL:
      return operationFinish(state, OPERATION_ID.REMOVE, true);

    case ACTIONS.OPERATION_SAVE:
      return operationStart(state, OPERATION_ID.SAVE, true);
    case ACTIONS.OPERATION_SAVE_SUCCESS:
    case ACTIONS.OPERATION_SAVE_FAIL:
      return operationFinish(state, OPERATION_ID.SAVE, true);

    case ACTIONS.OPERATION_SIGN:
      return operationStart(state, OPERATION_ID.SIGN, true);
    case ACTIONS.OPERATION_SIGN_SUCCESS:
    case ACTIONS.OPERATION_SIGN_CANCEL:
    case ACTIONS.OPERATION_SIGN_FAIL:
      return operationFinish(state, OPERATION_ID.SIGN, true);

    case ACTIONS.OPERATION_STATE_HISTORY:
      return operationStart(state, OPERATION_ID.STATE_HISTORY, false);
    case ACTIONS.OPERATION_STATE_HISTORY_SUCCESS:
      return operationStateHistorySuccess(operationFinish(state, OPERATION_ID.STATE_HISTORY, false), action.payload);
    case ACTIONS.OPERATION_STATE_HISTORY_FAIL:
      return operationFinish(state, OPERATION_ID.STATE_HISTORY, false);

    case ACTIONS.OPERATION_UNARCHIVE:
      return operationStart(state, OPERATION_ID.ARCHIVE, true);
    case ACTIONS.OPERATION_UNARCHIVE_SUCCESS:
    case ACTIONS.OPERATION_UNARCHIVE_FAIL:
      return operationFinish(state, OPERATION_ID.ARCHIVE, true);

    case ACTIONS.REMOVE_EMPLOYEES:
      return {
        ...removeEmployees(state, action.payload),
        isSaved: false,
      };

    case ACTIONS.ADD_EMPLOYEES:
      return addEmployees(state, action.payload);

    case ACTIONS.OPEN_EMPLOYEE:
      return openEmployee(state, action.payload);

    case ACTIONS.SAVE_EMPLOYEE:
      return {
        ...saveEmployee(state, action.payload),
        isSaved: false,
      };

    case ACTIONS.CLOSE_EMPLOYEE:
      return closeEmployee(state);

    default:
      return state;
  }
}

export default {
  [REDUCER_KEY]: reducer,
  ...employeesReducers,
  ...employeeReducers,
};
