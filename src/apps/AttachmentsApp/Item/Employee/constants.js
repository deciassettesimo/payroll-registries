import { OPERATION_ID as COMMON_OPERATION_ID, OPERATIONS_ACTIONS_TYPES } from 'common/constants';
import { EMPLOYEES_INFO_ITEM_FIELD_ID as DAL_FIELD_ID } from 'dal/payroll-registries-attachments/constants';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesAttachmentsItemEmployee';

export const TAB_ID = keyMirrorWithPrefix(
  {
    MAIN_FIELDS: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_ITEM_EMPLOYEE_',
);

export const SECTION_ID = keyMirrorWithPrefix(
  {
    COMMON_INFO: null,
    ADDITIONAL_INFO: null,
    ADDRESS_INFO: null,
    IDENTITY_DOCUMENT: null,
    NON_RESIDENT_DOCUMENTS: null,
    CONTACT_INFO: null,
    FROM_BANK_INFO: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_ITEM_EMPLOYEE_',
);

export const FIELD_ID = { ...DAL_FIELD_ID };

export const OPERATION_ID = {
  ...COMMON_OPERATION_ID,
  SAVE_AND_ADD: 'SAVE_AND_ADD',
};

export const DICTIONARIES_OPTIONS_VALUE = {
  EMPLOYEE_RESIDENT: { TRUE: '1', FALSE: '0' },
  GENDER: { MALE: 'муж', FEMALE: 'жен' },
  MARRIED: { TRUE: 'Женат/Замужем', FALSE: 'Холост/Не замужем' },
};

export const LOCALE_KEY = {
  HEADER: {
    DOC_TITLE: 'loc.registryOnAttachmentsTitle',
    TITLE: 'loc.employeeInformation',
  },
  EXTRA: {
    STRUCTURE: 'loc.documentStructure',
  },
  TABS: {
    MAIN_FIELDS: 'loc.mainFields',
  },
  SECTIONS: {
    COMMON_INFO: 'loc.commonInfo',
    ADDITIONAL_INFO: 'loc.additionalInformation',
    ADDRESS_INFO: 'loc.address',
    IDENTITY_DOCUMENT: 'loc.identityDocument',
    NON_RESIDENT_DOCUMENTS: 'loc.stayConfirmingDocuments',
    CONTACT_INFO: 'loc.contactInformation',
    FROM_BANK_INFO: 'loc.fromBankInfo',
  },
  FIELDS: {
    NPP: 'loc.orderNumber.abbr',
    EMPLOYEE_NUM: 'loc.personnelNumber.abbr',
    EMPLOYEE_PROCESSING_RESULT_STATUS: 'loc.status',
    EMPLOYEE_PROCESSING_RESULT_ACCOUNTS: {
      ACCOUNT: 'loc.account',
      CURRENCY: 'loc.currency',
    },
    EMPLOYEE_SURNAME: 'loc.surname',
    EMPLOYEE_NAME: 'loc.name',
    EMPLOYEE_PATRONYMIC: 'loc.patronymic',
    EMPLOYEE_RESIDENT: 'loc.resident',
    EMBOSSED_FAMILY: 'loc.surnameInLatin',
    EMBOSSED_NAME: 'loc.nameInLatin',
    EMPLOYEE_BIRTH_DATE: 'loc.dateOfBirth',
    GENDER: 'loc.gender',
    CITIZENSHIP_NAME: 'loc.citizenship',
    COUNTRY_NAME: 'loc.countryOfBirth',
    PLACE_OF_BIRTH: 'loc.placeOfBirth',
    CARD_BRANCH_CITY: 'loc.cardsIssuingCity',
    CARD_BRANCH_NAME: 'loc.cardsIssuingOffice',
    CARD_BRANCH_ADDRESS: 'loc.cardsIssuingOfficeAddress',
    TARIFF_PLAN_NAME: 'loc.tariffPlanOfIndividual.abbr',
    LOAD_SCHEME_NAME: 'loc.loadingSchemeOfIndividual.abbr',

    MARRIED: 'loc.familyStatus',
    EMPLOYEE_POSITION: 'loc.post',
    INN: 'loc.tin',
    SNILS: 'loc.snils',

    ADDRESS_INFO_ADDRESS_MATCH: 'loc.addressesMatch',
    ADDRESS_INFO_REG_ADDRESS_INDEX: 'loc.index',
    ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME: 'loc.country',
    ADDRESS_INFO_REG_ADDRESS_STATE: 'loc.state',
    ADDRESS_INFO_REG_ADDRESS_DISTRICT: 'loc.district',
    ADDRESS_INFO_REG_ADDRESS_CITY: 'loc.city',
    ADDRESS_INFO_REG_ADDRESS_PLACE: 'loc.locality',
    ADDRESS_INFO_REG_ADDRESS_STREET: 'loc.street',
    ADDRESS_INFO_REG_ADDRESS_BUILDING: 'loc.house',
    ADDRESS_INFO_REG_ADDRESS_BLOCK: 'loc.building.abbr',
    ADDRESS_INFO_REG_ADDRESS_FLAT: 'loc.apartment',
    ADDRESS_INFO_LIV_ADDRESS_INDEX: 'loc.index',
    ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME: 'loc.country',
    ADDRESS_INFO_LIV_ADDRESS_STATE: 'loc.state',
    ADDRESS_INFO_LIV_ADDRESS_DISTRICT: 'loc.district',
    ADDRESS_INFO_LIV_ADDRESS_CITY: 'loc.city',
    ADDRESS_INFO_LIV_ADDRESS_PLACE: 'loc.locality',
    ADDRESS_INFO_LIV_ADDRESS_STREET: 'loc.street',
    ADDRESS_INFO_LIV_ADDRESS_BUILDING: 'loc.house',
    ADDRESS_INFO_LIV_ADDRESS_BLOCK: 'loc.building.abbr',
    ADDRESS_INFO_LIV_ADDRESS_FLAT: 'loc.apartment',

    EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION: 'loc.identityDocumentType',
    EMPLOYEE_DOCUMENT_SERIES: 'loc.series',
    EMPLOYEE_DOCUMENT_NUMBER: 'loc.number',
    EMPLOYEE_DOCUMENT_ISSUED_DATE: 'loc.dateOfIssue',
    EMPLOYEE_DOCUMENT_ISSUED_BY: 'loc.issuedBy',
    EMPLOYEE_DOCUMENT_SUBDIVISION_CODE: 'loc.divisionCode',
    EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME: 'loc.countryOfIssue',

    VISA_DPP_SERIES: 'loc.series',
    VISA_DPP_NUMBER: 'loc.number',
    VISA_DPP_DATE_START: 'loc.validFrom',
    VISA_DPP_DATE_END: 'loc.validTo',
    VISA_DPP_ISSUED_BY: 'loc.issuedBy',
    VISA_DPP_ISSUED_DATE: 'loc.dateOfIssue',
    MIGRATION_CARD_DPP_SERIES: 'loc.series',
    MIGRATION_CARD_DPP_NUMBER: 'loc.number',
    MIGRATION_CARD_DPP_DATE_START: 'loc.validFrom',
    MIGRATION_CARD_DPP_DATE_END: 'loc.validTo',
    MIGRATION_CARD_DPP_ISSUED_BY: 'loc.issuedBy',
    MIGRATION_CARD_DPP_ISSUED_DATE: 'loc.dateOfIssue',
    PERMISSION_TO_STAY_DPP_SERIES: 'loc.series',
    PERMISSION_TO_STAY_DPP_NUMBER: 'loc.number',
    PERMISSION_TO_STAY_DPP_DATE_START: 'loc.validFrom',
    PERMISSION_TO_STAY_DPP_DATE_END: 'loc.validTo',
    PERMISSION_TO_STAY_DPP_ISSUED_BY: 'loc.issuedBy',
    PERMISSION_TO_STAY_DPP_ISSUED_DATE: 'loc.dateOfIssue',

    EMPLOYEE_CONTACT_INFO_HOME_PHONE: 'loc.home.adj',
    EMPLOYEE_CONTACT_INFO_WORK_PHONE: 'loc.work.adj',
    EMPLOYEE_CONTACT_INFO_MOBILE_PHONE: 'loc.mobile.adj',
    EMPLOYEE_CONTACT_INFO_EMAIL: 'loc.email',

    EMPLOYEE_PROCESSING_RESULT_CODE_1C: 'loc.code1C',
    EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK: 'loc.bankMessage',
  },
  OPERATIONS: {
    SAVE_AND_ADD: 'loc.saveAndAddNew',
  },
  DICTIONARIES_OPTIONS: {
    EMPLOYEE_RESIDENT: {
      TRUE: 'loc.resident',
      FALSE: 'loc.nonResident',
    },
    GENDER: {
      MALE: 'loc.male',
      FEMALE: 'loc.female',
    },
    MARRIED: {
      TRUE: 'loc.married',
      FALSE: 'loc.unmarried',
    },
  },
  OTHER: {
    LOAD_SCHEME_NAME_DISABLED_PLACEHOLDER: 'loc.chooseTariffPlanFirst',
    LOAD_SCHEME_NAME_TOOLTIP: 'loc.choiceOfLoadingSchemeConditions',
    ADDRESS_INFO_REG_ADDRESS_HEADER: 'loc.addressOfRegistration',
    ADDRESS_INFO_LIV_ADDRESS_HEADER: 'loc.addressOfActualResidence',
    VISA_HEADER: 'loc.visa',
    MIGRATION_CARD_HEADER: 'loc.migrationCard',
    PERMISSION_TO_STAY_HEADER: 'loc.temporaryResidencePermit',
    PAGE_LEAVE_MESSAGE: 'loc.changesNotSavedAndWillBeLoose.closeDocument.question',
    PAGE_LEAVE_CONFIRM: 'loc.goTo',
    PAGE_LEAVE_CANCEL: 'loc.stay',
  },
  NOTIFICATIONS: {
    SAVE: {
      SUCCESS: {
        TITLE: 'loc.employeeAdded',
      },
      FAIL: {
        TITLE: 'loc.unableSaveEmployee',
      },
      RBO_CONTROLS: {
        TITLE: 'loc.employeeCannotBeSaved',
        MESSAGE: 'loc.toSaveEmployeeFixErrors',
      },
      SUCCESS_WITH_WARNINGS: {
        TITLE: 'loc.employeeSavedWithWarnings',
      },
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,

    REFRESH: null,
    REFRESH_SUCCESS: null,
    REFRESH_FAIL: null,

    GET_DATA_REQUEST: null,
    GET_DATA_SUCCESS: null,
    GET_DATA_FAIL: null,
    SAVE_DATA_REQUEST: null,
    SAVE_DATA_SUCCESS: null,
    SAVE_DATA_FAIL: null,

    ACTIVATE_ERROR: null,
    FIELD_FOCUS: null,
    FIELD_BLUR: null,
    CHANGE_DATA: null,

    DICTIONARY_SEARCH: null,
    DICTIONARY_SEARCH_REQUEST: null,
    DICTIONARY_SEARCH_SUCCESS: null,
    DICTIONARY_SEARCH_CANCEL: null,
    DICTIONARY_SEARCH_FAIL: null,

    OPERATIONS: null,
    ...OPERATIONS_ACTIONS_TYPES.BACK,
    ...OPERATIONS_ACTIONS_TYPES.SAVE,
    OPERATION_SAVE_AND_ADD: null,
    OPERATION_SAVE_AND_ADD_SUCCESS: null,
    OPERATION_SAVE_AND_ADD_FAIL: null,

    SECTION_MINIMIZER_TOGGLE: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_ITEM_EMPLOYEE_',
);
