import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDateCalendar } from '@rbo/rbo-components/lib/Input';
import { Header } from '@rbo/rbo-components/lib/Styles';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class NonResidentDocuments extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, isMinimized, sectionData, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, isMinimized, sectionData, formWarnings, formErrors, formDisabled })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.NON_RESIDENT_DOCUMENTS, value: !isMinimized });
  };

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
      isMinimized,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.NON_RESIDENT_DOCUMENTS }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.NON_RESIDENT_DOCUMENTS}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Fragment>
              <Blockset>
                <Block isWide>
                  <Row vAlignItems={Row.REFS.V_ALIGN_ITEMS.CENTER}>
                    <Cell width="auto">
                      <Header size={7}>{LABELS.OTHER.VISA_HEADER}</Header>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.VISA_DPP_SERIES}>{LABELS.FIELDS.VISA_DPP_SERIES}</Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.VISA_DPP_SERIES}
                            value={sectionData[FIELD_ID.VISA_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.VISA_DPP_SERIES]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_SERIES}
                            value={sectionData[FIELD_ID.VISA_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.VISA_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.VISA_DPP_NUMBER}>{LABELS.FIELDS.VISA_DPP_NUMBER}</Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.VISA_DPP_NUMBER}
                            value={sectionData[FIELD_ID.VISA_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.VISA_DPP_NUMBER]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_NUMBER}
                            value={sectionData[FIELD_ID.VISA_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.VISA_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.VISA_DPP_DATE_START}>{LABELS.FIELDS.VISA_DPP_DATE_START}</Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.VISA_DPP_DATE_START}
                            value={sectionData[FIELD_ID.VISA_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.VISA_DPP_DATE_START]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_DATE_START}
                            value={sectionData[FIELD_ID.VISA_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.VISA_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.VISA_DPP_DATE_END}>{LABELS.FIELDS.VISA_DPP_DATE_END}</Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.VISA_DPP_DATE_END}
                            value={sectionData[FIELD_ID.VISA_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.VISA_DPP_DATE_END]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_DATE_END}
                            value={sectionData[FIELD_ID.VISA_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.VISA_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.VISA_DPP_ISSUED_BY}>{LABELS.FIELDS.VISA_DPP_ISSUED_BY}</Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.VISA_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={255}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.VISA_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.VISA_DPP_ISSUED_DATE}>{LABELS.FIELDS.VISA_DPP_ISSUED_DATE}</Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.VISA_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            disabled={formDisabled[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.VISA_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.VISA_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block isWide>
                  <Row vAlignItems={Row.REFS.V_ALIGN_ITEMS.CENTER}>
                    <Cell width="auto">
                      <Header size={7}>{LABELS.OTHER.MIGRATION_CARD_HEADER}</Header>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_SERIES}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_SERIES}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.MIGRATION_CARD_DPP_SERIES}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_SERIES}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_NUMBER}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_NUMBER}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.MIGRATION_CARD_DPP_NUMBER}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_NUMBER}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_DATE_START}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_DATE_START}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.MIGRATION_CARD_DPP_DATE_START}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_DATE_START}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_DATE_END}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_DATE_END}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.MIGRATION_CARD_DPP_DATE_END}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_DATE_END}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_ISSUED_BY}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={255}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE}>
                        {LABELS.FIELDS.MIGRATION_CARD_DPP_ISSUED_DATE}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            disabled={formDisabled[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block isWide>
                  <Row vAlignItems={Row.REFS.V_ALIGN_ITEMS.CENTER}>
                    <Cell width="auto">
                      <Header size={7}>{LABELS.OTHER.PERMISSION_TO_STAY_HEADER}</Header>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_SERIES}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_NUMBER}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={50}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_DATE_START}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_DATE_END}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_ISSUED_BY}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={255}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE}>
                        {LABELS.FIELDS.PERMISSION_TO_STAY_DPP_ISSUED_DATE}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            disabled={formDisabled[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE}
                            value={sectionData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
            </Fragment>
          )}
        </Section.Content>
      </Section>
    );
  }
}
