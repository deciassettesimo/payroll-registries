import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode } from '@rbo/rbo-components/lib/Input';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class ContactInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, isMinimized, sectionData, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, isMinimized, sectionData, formWarnings, formErrors, formDisabled })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.CONTACT_INFO, value: !isMinimized });
  };

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
      isMinimized,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.CONTACT_INFO }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.CONTACT_INFO}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Blockset>
              <Block>
                <Row>
                  <Cell width="33.3%">
                    <Label htmlFor={FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE}>
                      {LABELS.FIELDS.EMPLOYEE_CONTACT_INFO_HOME_PHONE}
                    </Label>
                    <Field>
                      {isEditable ? (
                        <InputText
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          disabled={formDisabled[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={50}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                  <Cell width="33.3%">
                    <Label htmlFor={FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE}>
                      {LABELS.FIELDS.EMPLOYEE_CONTACT_INFO_WORK_PHONE}
                    </Label>
                    <Field>
                      {isEditable ? (
                        <InputText
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          disabled={formDisabled[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={50}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                  <Cell width="33.4%">
                    <Label htmlFor={FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE}>
                      {LABELS.FIELDS.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE}
                    </Label>
                    <Field>
                      {isEditable ? (
                        <InputText
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          disabled={formDisabled[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={50}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                </Row>
              </Block>
              <Block>
                <Row>
                  <Cell>
                    <Label htmlFor={FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL}>
                      {LABELS.FIELDS.EMPLOYEE_CONTACT_INFO_EMAIL}
                    </Label>
                    <Field>
                      {isEditable ? (
                        <InputText
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          disabled={formDisabled[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={100}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL}
                          value={sectionData[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                </Row>
              </Block>
            </Blockset>
          )}
        </Section.Content>
      </Section>
    );
  }
}
