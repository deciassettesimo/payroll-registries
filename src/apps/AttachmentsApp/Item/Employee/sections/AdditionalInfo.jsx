import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDigital, InputSelect } from '@rbo/rbo-components/lib/Input';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class AdditionalInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const {
      isEditable,
      isMinimized,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
    } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !==
      JSON.stringify({
        isEditable,
        isMinimized,
        sectionData,
        sectionDictionaries,
        formWarnings,
        formErrors,
        formDisabled,
      })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.ADDITIONAL_INFO, value: !isMinimized });
  };

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      isMinimized,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.ADDITIONAL_INFO }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.ADDITIONAL_INFO}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Blockset>
              <Block>
                <Row>
                  <Cell width="50%">
                    <Label htmlFor={FIELD_ID.MARRIED}>{LABELS.FIELDS.MARRIED}</Label>
                    <Field>
                      {isEditable ? (
                        <InputSelect
                          id={FIELD_ID.MARRIED}
                          value={sectionData[FIELD_ID.MARRIED]}
                          isWarning={formWarnings[FIELD_ID.MARRIED]}
                          isError={formErrors[FIELD_ID.MARRIED]}
                          disabled={formDisabled[FIELD_ID.MARRIED]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          options={sectionDictionaries[FIELD_ID.MARRIED].items}
                          optionRenderer={optionRenderer}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.MARRIED}
                          value={sectionData[FIELD_ID.MARRIED]}
                          isWarning={formWarnings[FIELD_ID.MARRIED]}
                          isError={formErrors[FIELD_ID.MARRIED]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                  <Cell width="50%">
                    <Label htmlFor={FIELD_ID.EMPLOYEE_POSITION}>{LABELS.FIELDS.EMPLOYEE_POSITION}</Label>
                    <Field>
                      {isEditable ? (
                        <InputText
                          id={FIELD_ID.EMPLOYEE_POSITION}
                          value={sectionData[FIELD_ID.EMPLOYEE_POSITION]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_POSITION]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_POSITION]}
                          disabled={formDisabled[FIELD_ID.EMPLOYEE_POSITION]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={255}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_POSITION}
                          value={sectionData[FIELD_ID.EMPLOYEE_POSITION]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_POSITION]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_POSITION]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                </Row>
              </Block>
              <Block>
                <Row>
                  <Cell width="50%">
                    <Label htmlFor={FIELD_ID.INN}>{LABELS.FIELDS.INN}</Label>
                    <Field>
                      {isEditable ? (
                        <InputDigital
                          id={FIELD_ID.INN}
                          value={sectionData[FIELD_ID.INN]}
                          isWarning={formWarnings[FIELD_ID.INN]}
                          isError={formErrors[FIELD_ID.INN]}
                          disabled={formDisabled[FIELD_ID.INN]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={15}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.INN}
                          value={sectionData[FIELD_ID.INN]}
                          isWarning={formWarnings[FIELD_ID.INN]}
                          isError={formErrors[FIELD_ID.INN]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                  <Cell width="50%">
                    <Label htmlFor={FIELD_ID.SNILS}>{LABELS.FIELDS.SNILS}</Label>
                    <Field>
                      {isEditable ? (
                        <InputDigital
                          id={FIELD_ID.SNILS}
                          value={sectionData[FIELD_ID.SNILS]}
                          isWarning={formWarnings[FIELD_ID.SNILS]}
                          isError={formErrors[FIELD_ID.SNILS]}
                          disabled={formDisabled[FIELD_ID.SNILS]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          maxLength={11}
                        />
                      ) : (
                        <InputViewMode
                          id={FIELD_ID.SNILS}
                          value={sectionData[FIELD_ID.SNILS]}
                          isWarning={formWarnings[FIELD_ID.SNILS]}
                          isError={formErrors[FIELD_ID.SNILS]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      )}
                    </Field>
                  </Cell>
                </Row>
              </Block>
            </Blockset>
          )}
        </Section.Content>
      </Section>
    );
  }
}
