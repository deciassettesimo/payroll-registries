import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputViewMode } from '@rbo/rbo-components/lib/Input';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class FromBankInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const { isMinimized, sectionData, formWarnings, formErrors } = this.props;
    return (
      JSON.stringify({
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
      }) !== JSON.stringify({ isMinimized, sectionData, formWarnings, formErrors })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.FROM_BANK_INFO, value: !isMinimized });
  };

  render() {
    const { LABELS, sectionData, formWarnings, formErrors, handleFocus, handleBlur, isMinimized } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.FROM_BANK_INFO }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.FROM_BANK_INFO}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Fragment>
              <Blockset>
                <Block>
                  <Row>
                    <Cell width="60%">
                      <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_CODE_1C}</Label>
                      <Field>
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C}
                          value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                          rows={1}
                        />
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block />
              </Blockset>
              <Blockset>
                <Block>
                  {sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS].map(item => (
                    <Row key={item.id}>
                      <Cell width="60%">
                        <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS.ACCOUNT}</Label>
                        <Field>
                          <InputViewMode
                            id={`${FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS}_${item.id}_account`}
                            value={item.account}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        </Field>
                      </Cell>
                      <Cell width="40%">
                        <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS.CURRENCY}</Label>
                        <Field>
                          <InputViewMode
                            id={`${FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS}_${item.id}_currency`}
                            value={item.currency}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        </Field>
                      </Cell>
                    </Row>
                  ))}
                </Block>
                <Block />
              </Blockset>
              <Blockset>
                <Block isWide>
                  <Row>
                    <Cell>
                      <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK}</Label>
                      <Field>
                        <InputViewMode
                          id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK}
                          value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                          isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                          isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                          onFocus={handleFocus}
                          onBlur={handleBlur}
                        />
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
            </Fragment>
          )}
        </Section.Content>
      </Section>
    );
  }
}
