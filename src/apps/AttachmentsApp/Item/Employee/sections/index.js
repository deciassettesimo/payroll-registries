export { default as CommonInfo } from './CommonInfo';
export { default as AdditionalInfo } from './AdditionalInfo';
export { default as AddressInfo } from './AddressInfo';
export { default as IdentityDocument } from './IdentityDocument';
export { default as NonResidentDocuments } from './NonResidentDocuments';
export { default as ContactInfo } from './ContactInfo';
export { default as FromBankInfo } from './FromBankInfo';
