import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDateCalendar, InputSelect, InputSearch } from '@rbo/rbo-components/lib/Input';
import { Required } from '@rbo/rbo-components/lib/Styles';
import Tooltip from '@rbo/rbo-components/lib/Tooltip';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class CommonInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.COMMON_INFO }}>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell width="25%">
                  <Label htmlFor={FIELD_ID.NPP}>{LABELS.FIELDS.NPP}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.NPP}
                      value={sectionData[FIELD_ID.NPP]}
                      isWarning={formWarnings[FIELD_ID.NPP]}
                      isError={formErrors[FIELD_ID.NPP]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
                <Cell width="25%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_NUM}>{LABELS.FIELDS.EMPLOYEE_NUM}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_NUM}
                        value={sectionData[FIELD_ID.EMPLOYEE_NUM]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NUM]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NUM]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_NUM]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={60}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_NUM}
                        value={sectionData[FIELD_ID.EMPLOYEE_NUM]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NUM]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NUM]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS}>
                    {LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_STATUS}
                  </Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS}
                      value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_SURNAME}>
                    {LABELS.FIELDS.EMPLOYEE_SURNAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_SURNAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_SURNAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_SURNAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_SURNAME]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_SURNAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_SURNAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_SURNAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_SURNAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_SURNAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_NAME}>
                    {LABELS.FIELDS.EMPLOYEE_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_NAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NAME]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_NAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_PATRONYMIC}>{LABELS.FIELDS.EMPLOYEE_PATRONYMIC}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_PATRONYMIC}
                        value={sectionData[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_PATRONYMIC}
                        value={sectionData[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_RESIDENT}>
                    {LABELS.FIELDS.EMPLOYEE_RESIDENT}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.EMPLOYEE_RESIDENT}
                        value={sectionData[FIELD_ID.EMPLOYEE_RESIDENT]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_RESIDENT]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_RESIDENT]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_RESIDENT]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.EMPLOYEE_RESIDENT].items}
                        optionRenderer={optionRenderer}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_RESIDENT}
                        value={sectionData[FIELD_ID.EMPLOYEE_RESIDENT]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_RESIDENT]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_RESIDENT]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMBOSSED_FAMILY}>
                    {LABELS.FIELDS.EMBOSSED_FAMILY}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMBOSSED_FAMILY}
                        value={sectionData[FIELD_ID.EMBOSSED_FAMILY]}
                        isWarning={formWarnings[FIELD_ID.EMBOSSED_FAMILY]}
                        isError={formErrors[FIELD_ID.EMBOSSED_FAMILY]}
                        disabled={formDisabled[FIELD_ID.EMBOSSED_FAMILY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMBOSSED_FAMILY}
                        value={sectionData[FIELD_ID.EMBOSSED_FAMILY]}
                        isWarning={formWarnings[FIELD_ID.EMBOSSED_FAMILY]}
                        isError={formErrors[FIELD_ID.EMBOSSED_FAMILY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMBOSSED_NAME}>
                    {LABELS.FIELDS.EMBOSSED_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMBOSSED_NAME}
                        value={sectionData[FIELD_ID.EMBOSSED_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMBOSSED_NAME]}
                        isError={formErrors[FIELD_ID.EMBOSSED_NAME]}
                        disabled={formDisabled[FIELD_ID.EMBOSSED_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMBOSSED_NAME}
                        value={sectionData[FIELD_ID.EMBOSSED_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMBOSSED_NAME]}
                        isError={formErrors[FIELD_ID.EMBOSSED_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_BIRTH_DATE}>
                    {LABELS.FIELDS.EMPLOYEE_BIRTH_DATE}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputDateCalendar
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.EMPLOYEE_BIRTH_DATE}
                        value={sectionData[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_BIRTH_DATE}
                        value={sectionData[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.GENDER}>
                    {LABELS.FIELDS.GENDER}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.GENDER}
                        value={sectionData[FIELD_ID.GENDER]}
                        isWarning={formWarnings[FIELD_ID.GENDER]}
                        isError={formErrors[FIELD_ID.GENDER]}
                        disabled={formDisabled[FIELD_ID.GENDER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.GENDER].items}
                        optionRenderer={optionRenderer}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.GENDER}
                        value={sectionData[FIELD_ID.GENDER]}
                        isWarning={formWarnings[FIELD_ID.GENDER]}
                        isError={formErrors[FIELD_ID.GENDER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.CITIZENSHIP_NAME}>
                    {LABELS.FIELDS.CITIZENSHIP_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.CITIZENSHIP_NAME}
                        value={sectionData[FIELD_ID.CITIZENSHIP_NAME]}
                        isWarning={formWarnings[FIELD_ID.CITIZENSHIP_NAME]}
                        isError={formErrors[FIELD_ID.CITIZENSHIP_NAME]}
                        disabled={formDisabled[FIELD_ID.CITIZENSHIP_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.CITIZENSHIP_NAME].items}
                        isSearching={sectionDictionaries[FIELD_ID.CITIZENSHIP_NAME].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                        popupBoxStyle={{ width: 360 }}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CITIZENSHIP_NAME}
                        value={sectionData[FIELD_ID.CITIZENSHIP_NAME]}
                        isWarning={formWarnings[FIELD_ID.CITIZENSHIP_NAME]}
                        isError={formErrors[FIELD_ID.CITIZENSHIP_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.COUNTRY_NAME}>
                    {LABELS.FIELDS.COUNTRY_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.COUNTRY_NAME}
                        value={sectionData[FIELD_ID.COUNTRY_NAME]}
                        isWarning={formWarnings[FIELD_ID.COUNTRY_NAME]}
                        isError={formErrors[FIELD_ID.COUNTRY_NAME]}
                        disabled={formDisabled[FIELD_ID.COUNTRY_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.COUNTRY_NAME].items}
                        isSearching={sectionDictionaries[FIELD_ID.COUNTRY_NAME].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                        popupBoxStyle={{ width: 360 }}
                        popupBoxAlign={InputSearch.REFS.POPUP_BOX_ALIGN.END}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.COUNTRY_NAME}
                        value={sectionData[FIELD_ID.COUNTRY_NAME]}
                        isWarning={formWarnings[FIELD_ID.COUNTRY_NAME]}
                        isError={formErrors[FIELD_ID.COUNTRY_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.PLACE_OF_BIRTH}>
                    {LABELS.FIELDS.PLACE_OF_BIRTH}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.PLACE_OF_BIRTH}
                        value={sectionData[FIELD_ID.PLACE_OF_BIRTH]}
                        isWarning={formWarnings[FIELD_ID.PLACE_OF_BIRTH]}
                        isError={formErrors[FIELD_ID.PLACE_OF_BIRTH]}
                        disabled={formDisabled[FIELD_ID.PLACE_OF_BIRTH]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={150}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.PLACE_OF_BIRTH}
                        value={sectionData[FIELD_ID.PLACE_OF_BIRTH]}
                        isWarning={formWarnings[FIELD_ID.PLACE_OF_BIRTH]}
                        isError={formErrors[FIELD_ID.PLACE_OF_BIRTH]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_CITY}>{LABELS.FIELDS.CARD_BRANCH_CITY}</Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.CARD_BRANCH_CITY}
                        value={sectionData[FIELD_ID.CARD_BRANCH_CITY]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_CITY]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_CITY]}
                        disabled={formDisabled[FIELD_ID.CARD_BRANCH_CITY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.CARD_BRANCH_CITY].items}
                        isSearching={sectionDictionaries[FIELD_ID.CARD_BRANCH_CITY].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CARD_BRANCH_CITY}
                        value={sectionData[FIELD_ID.CARD_BRANCH_CITY]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_CITY]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_CITY]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_NAME}>{LABELS.FIELDS.CARD_BRANCH_NAME}</Label>
                  <Field>
                    {isEditable ? (
                      <InputSearch
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.CARD_BRANCH_NAME}
                        value={sectionData[FIELD_ID.CARD_BRANCH_NAME]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_NAME]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_NAME]}
                        disabled={formDisabled[FIELD_ID.CARD_BRANCH_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onSearch={handleSearch}
                        options={sectionDictionaries[FIELD_ID.CARD_BRANCH_NAME].items}
                        isSearching={sectionDictionaries[FIELD_ID.CARD_BRANCH_NAME].isSearching}
                        optionRenderer={optionRenderer}
                        isCleanable
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CARD_BRANCH_NAME}
                        value={sectionData[FIELD_ID.CARD_BRANCH_NAME]}
                        isWarning={formWarnings[FIELD_ID.CARD_BRANCH_NAME]}
                        isError={formErrors[FIELD_ID.CARD_BRANCH_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.CARD_BRANCH_ADDRESS}>{LABELS.FIELDS.CARD_BRANCH_ADDRESS}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.CARD_BRANCH_ADDRESS}
                      value={sectionData[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      isWarning={formWarnings[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      isError={formErrors[FIELD_ID.CARD_BRANCH_ADDRESS]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.TARIFF_PLAN_NAME}>{LABELS.FIELDS.TARIFF_PLAN_NAME}</Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.TARIFF_PLAN_NAME}
                        value={sectionData[FIELD_ID.TARIFF_PLAN_NAME]}
                        isWarning={formWarnings[FIELD_ID.TARIFF_PLAN_NAME]}
                        isError={formErrors[FIELD_ID.TARIFF_PLAN_NAME]}
                        disabled={formDisabled[FIELD_ID.TARIFF_PLAN_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.TARIFF_PLAN_NAME].items}
                        optionRenderer={optionRenderer}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.TARIFF_PLAN_NAME}
                        value={sectionData[FIELD_ID.TARIFF_PLAN_NAME]}
                        isWarning={formWarnings[FIELD_ID.TARIFF_PLAN_NAME]}
                        isError={formErrors[FIELD_ID.TARIFF_PLAN_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.LOAD_SCHEME_NAME}>
                    {LABELS.FIELDS.LOAD_SCHEME_NAME}
                    <Tooltip>{LABELS.OTHER.LOAD_SCHEME_NAME_TOOLTIP}</Tooltip>
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputSelect
                        id={FIELD_ID.LOAD_SCHEME_NAME}
                        value={sectionData[FIELD_ID.LOAD_SCHEME_NAME]}
                        isWarning={formWarnings[FIELD_ID.LOAD_SCHEME_NAME]}
                        isError={formErrors[FIELD_ID.LOAD_SCHEME_NAME]}
                        disabled={formDisabled[FIELD_ID.LOAD_SCHEME_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        options={sectionDictionaries[FIELD_ID.LOAD_SCHEME_NAME].items}
                        optionRenderer={optionRenderer}
                        placeholder={
                          !sectionData[FIELD_ID.TARIFF_PLAN_NAME]
                            ? LABELS.OTHER.LOAD_SCHEME_NAME_DISABLED_PLACEHOLDER
                            : null
                        }
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.LOAD_SCHEME_NAME}
                        value={sectionData[FIELD_ID.LOAD_SCHEME_NAME]}
                        isWarning={formWarnings[FIELD_ID.LOAD_SCHEME_NAME]}
                        isError={formErrors[FIELD_ID.LOAD_SCHEME_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
