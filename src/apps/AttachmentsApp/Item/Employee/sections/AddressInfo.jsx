import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputCheckbox, InputSearch } from '@rbo/rbo-components/lib/Input';
import { Header, Required } from '@rbo/rbo-components/lib/Styles';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class AddressInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const {
      isEditable,
      isMinimized,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
    } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !==
      JSON.stringify({
        isEditable,
        isMinimized,
        sectionData,
        sectionDictionaries,
        formWarnings,
        formErrors,
        formDisabled,
      })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.ADDRESS_INFO, value: !isMinimized });
  };

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
      isMinimized,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.ADDRESS_INFO }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.ADDRESS_INFO}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Fragment>
              <Blockset>
                <Block isWide>
                  <Row vAlignItems={Row.REFS.V_ALIGN_ITEMS.CENTER}>
                    <Cell width="auto">
                      <Header size={7}>{LABELS.OTHER.ADDRESS_INFO_REG_ADDRESS_HEADER}</Header>
                    </Cell>
                    <Cell width="auto">
                      <Field>
                        {isEditable ? (
                          <InputCheckbox
                            id={FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH}
                            checked={sectionData[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          >
                            {LABELS.FIELDS.ADDRESS_INFO_ADDRESS_MATCH}
                          </InputCheckbox>
                        ) : (
                          <InputViewMode
                            type={InputViewMode.REFS.TYPES.CHECKBOX}
                            id={FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH}
                            checked={sectionData[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                          >
                            {LABELS.FIELDS.ADDRESS_INFO_ADDRESS_MATCH}
                          </InputViewMode>
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell width="30%">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_INDEX}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={6}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="70%">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputSearch
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onSearch={handleSearch}
                            options={sectionDictionaries[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME].items}
                            isSearching={
                              sectionDictionaries[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME].isSearching
                            }
                            optionRenderer={optionRenderer}
                            isCleanable
                            popupBoxStyle={{ width: 360 }}
                            popupBoxAlign={InputSearch.REFS.POPUP_BOX_ALIGN.END}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_STATE}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={40}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_DISTRICT}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={40}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="50%">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_CITY}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={40}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="50%">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_PLACE}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={40}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_STREET}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={40}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="auto">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_BUILDING}
                      </Label>
                      <Field width="92px">
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={7}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="auto">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_BLOCK}
                      </Label>
                      <Field width="92px">
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={5}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="auto">
                      <Label htmlFor={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT}>
                        {LABELS.FIELDS.ADDRESS_INFO_REG_ADDRESS_FLAT}
                      </Label>
                      <Field width="92px">
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            disabled={formDisabled[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={9}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT}
                            value={sectionData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            isError={formErrors[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              {!sectionData[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH] && (
                <Fragment>
                  <Blockset>
                    <Block isWide>
                      <Row vAlignItems={Row.REFS.V_ALIGN_ITEMS.CENTER}>
                        <Cell width="auto">
                          <Header size={7}>{LABELS.OTHER.ADDRESS_INFO_LIV_ADDRESS_HEADER}</Header>
                        </Cell>
                      </Row>
                    </Block>
                  </Blockset>
                  <Blockset>
                    <Block>
                      <Row>
                        <Cell width="30%">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_INDEX}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={6}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                        <Cell width="70%">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME}
                            {isEditable && <Required />}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputSearch
                                locale={LABELS.LOCALE}
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                onSearch={handleSearch}
                                options={sectionDictionaries[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME].items}
                                isSearching={
                                  sectionDictionaries[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME].isSearching
                                }
                                optionRenderer={optionRenderer}
                                isCleanable
                                popupBoxStyle={{ width: 360 }}
                                popupBoxAlign={InputSearch.REFS.POPUP_BOX_ALIGN.END}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                    <Block>
                      <Row>
                        <Cell>
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_STATE}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={40}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                  </Blockset>
                  <Blockset>
                    <Block>
                      <Row>
                        <Cell>
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_DISTRICT}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={40}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                    <Block>
                      <Row>
                        <Cell width="50%">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_CITY}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={40}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                        <Cell width="50%">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_PLACE}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={40}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                  </Blockset>
                  <Blockset>
                    <Block>
                      <Row>
                        <Cell>
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_STREET}
                          </Label>
                          <Field>
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={40}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                    <Block>
                      <Row>
                        <Cell width="auto">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_BUILDING}
                          </Label>
                          <Field width="92px">
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={7}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                        <Cell width="auto">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_BLOCK}
                          </Label>
                          <Field width="92px">
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={5}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                        <Cell width="auto">
                          <Label htmlFor={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT}>
                            {LABELS.FIELDS.ADDRESS_INFO_LIV_ADDRESS_FLAT}
                          </Label>
                          <Field width="92px">
                            {isEditable ? (
                              <InputText
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                disabled={formDisabled[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                maxLength={9}
                              />
                            ) : (
                              <InputViewMode
                                id={FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT}
                                value={sectionData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                isWarning={formWarnings[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                isError={formErrors[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                rows={1}
                              />
                            )}
                          </Field>
                        </Cell>
                      </Row>
                    </Block>
                  </Blockset>
                </Fragment>
              )}
            </Fragment>
          )}
        </Section.Content>
      </Section>
    );
  }
}
