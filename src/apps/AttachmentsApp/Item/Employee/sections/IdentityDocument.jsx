import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label, Minimizer } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDateCalendar, InputSelect, InputSearch } from '@rbo/rbo-components/lib/Input';
import { Required } from '@rbo/rbo-components/lib/Styles';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class IdentityDocument extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    isMinimized: PropTypes.bool,
    onMinimizerToggle: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMinimized: false,
  };

  shouldComponentUpdate(nextProps) {
    const {
      isEditable,
      isMinimized,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
    } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        isMinimized: nextProps.isMinimized,
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !==
      JSON.stringify({
        isEditable,
        isMinimized,
        sectionData,
        sectionDictionaries,
        formWarnings,
        formErrors,
        formDisabled,
      })
    );
  }

  handleMinimizerToggle = () => {
    const { onMinimizerToggle, isMinimized } = this.props;
    onMinimizerToggle({ id: SECTION_ID.IDENTITY_DOCUMENT, value: !isMinimized });
  };

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
      isMinimized,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.IDENTITY_DOCUMENT }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Minimizer
                    label={LABELS.SECTIONS.IDENTITY_DOCUMENT}
                    isMinimized={isMinimized}
                    onToggle={this.handleMinimizerToggle}
                  />
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {!isMinimized && (
            <Fragment>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputSelect
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            options={sectionDictionaries[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION].items}
                            optionRenderer={optionRenderer}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_SERIES}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_SERIES}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_SERIES}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={14}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_SERIES}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_NUMBER}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={14}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="33.34%">
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_ISSUED_DATE}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputDateCalendar
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
              <Blockset>
                <Block>
                  <Row>
                    <Cell>
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_ISSUED_BY}
                        {isEditable && <Required />}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={150}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
                <Block>
                  <Row>
                    <Cell width="33.33%">
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputText
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            maxLength={10}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                    <Cell width="66.67%">
                      <Label htmlFor={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME}>
                        {LABELS.FIELDS.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME}
                      </Label>
                      <Field>
                        {isEditable ? (
                          <InputSearch
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            disabled={formDisabled[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onSearch={handleSearch}
                            options={sectionDictionaries[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME].items}
                            isSearching={
                              sectionDictionaries[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME].isSearching
                            }
                            optionRenderer={optionRenderer}
                            isCleanable
                            popupBoxStyle={{ width: 360 }}
                            popupBoxAlign={InputSearch.REFS.POPUP_BOX_ALIGN.END}
                          />
                        ) : (
                          <InputViewMode
                            id={FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME}
                            value={sectionData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            isWarning={formWarnings[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            isError={formErrors[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            rows={1}
                          />
                        )}
                      </Field>
                    </Cell>
                  </Row>
                </Block>
              </Blockset>
            </Fragment>
          )}
        </Section.Content>
      </Section>
    );
  }
}
