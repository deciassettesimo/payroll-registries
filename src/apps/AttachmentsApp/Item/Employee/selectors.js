import { createSelector } from 'reselect';

import localization from 'localization';
import {
  getDictionaryValue,
  getDocFormErrors,
  getFormattedAccount,
  getFormattedDate,
  mapDataToStructure,
  mapDocErrorsToSections,
  mapObject,
} from 'utils';

import { REDUCER_KEY as DOC_REDUCER_KEY, FIELD_ID as DOC_FIELD_ID } from '../constants';

import { REDUCER_KEY, FIELD_ID, OPERATION_ID } from './constants';
import { STRUCTURE, LABELS, ERRORS } from './config';
import { mapDictionaryItems } from './utils';

const docDocumentDataSelector = state => state[DOC_REDUCER_KEY].documentData;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isErrorSelector = state => state[REDUCER_KEY].isError;
const serverErrorSelector = state => state[REDUCER_KEY].serverError;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const isEditableSelector = state => state[REDUCER_KEY].isEditable;
const isDisabledSelector = state => state[REDUCER_KEY].isDisabled;
const operationsSelector = state => state[REDUCER_KEY].operations;
const activeFieldSelector = state => state[REDUCER_KEY].activeField;
const activeErrorSelector = state => state[REDUCER_KEY].activeError;
const documentStructureSelector = state => state[REDUCER_KEY].documentStructure;
const documentDataSelector = state => state[REDUCER_KEY].documentData;
const documentDictionariesSelector = state => state[REDUCER_KEY].documentDictionaries;
const documentErrorsSelector = state => state[REDUCER_KEY].documentErrors;

const labelsCreatedSelector = createSelector(
  docDocumentDataSelector,
  docDocumentData => ({
    LOCALE: localization.getLocale(),
    ...localization.translate(LABELS, {
      registryOnAttachmentsTitleNumber: docDocumentData[DOC_FIELD_ID.NUMBER],
    }),
  }),
);

const isErrorCreatedSelector = createSelector(
  isMountedSelector,
  isErrorSelector,
  (isMounted, isError) => isMounted && isError,
);

const serverErrorCreatedSelector = createSelector(
  serverErrorSelector,
  serverError => {
    if (!serverError) return null;
    return {
      status: serverError.status,
      statusText: serverError.statusText,
      url: serverError.config && serverError.config.url,
      response: serverError.data && serverError.data.error,
    };
  },
);

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isLoadingSelector,
  (isMounted, isLoading) => !isMounted || isLoading,
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  isEditableSelector,
  (operations, isEditable) =>
    operations
      .filter(operation => operation.id === OPERATION_ID.BACK || isEditable)
      .map(operation => ({
        ...operation,
        title: operation.noTitle ? null : localization.translate(operation.titleKey),
      })),
);

const documentStructureCreatedSelector = createSelector(
  activeFieldSelector,
  activeErrorSelector,
  documentStructureSelector,
  documentErrorsSelector,
  (activeField, activeError, documentStructure, documentErrors) =>
    documentStructure.map(item => ({
      id: item.id,
      title: localization.translate(item.titleKey),
      isActive: item.isActive,
      sections: item.sections
        ? mapDocErrorsToSections(
            item.sections.map(section => ({
              ...section,
              title: section.titleKey && localization.translate(section.titleKey),
              isVisible: true,
            })),
            documentErrors,
            ERRORS,
            activeField,
            activeError,
          )
        : null,
    })),
);

const documentSectionsCreatedSelector = createSelector(
  documentStructureSelector,
  documentErrorsSelector,
  (documentStructure, documentErrors) =>
    documentStructure
      .reduce((result, item) => (item.sections ? result.concat(item.sections) : result), [])
      .reduce(
        (result, section) => ({
          ...result,
          [section.id]: {
            isVisible: true,
            isMinimized: section.isMinimized && !documentErrors.length,
          },
        }),
        {},
      ),
);

const documentDataCreatedSelector = createSelector(
  isEditableSelector,
  documentDataSelector,
  documentDictionariesSelector,
  (isEditable, documentData, documentDictionaries) => {
    let dictionaryItem;
    let EMPLOYEE_RESIDENT;
    let GENDER;
    let MARRIED;
    if (isEditable) {
      EMPLOYEE_RESIDENT = documentData[FIELD_ID.EMPLOYEE_RESIDENT];
      GENDER = documentData[FIELD_ID.GENDER];
      MARRIED = documentData[FIELD_ID.MARRIED];
    } else {
      dictionaryItem = getDictionaryValue(
        documentData[FIELD_ID.EMPLOYEE_RESIDENT],
        documentDictionaries[FIELD_ID.EMPLOYEE_RESIDENT],
        'value',
      );
      EMPLOYEE_RESIDENT = dictionaryItem && localization.translate(dictionaryItem.titleKey);
      dictionaryItem = getDictionaryValue(
        documentData[FIELD_ID.GENDER],
        documentDictionaries[FIELD_ID.GENDER],
        'value',
      );
      GENDER = dictionaryItem && localization.translate(dictionaryItem.titleKey);
      dictionaryItem = getDictionaryValue(
        documentData[FIELD_ID.MARRIED],
        documentDictionaries[FIELD_ID.MARRIED],
        'value',
      );
      MARRIED = dictionaryItem && localization.translate(dictionaryItem.titleKey);
    }

    return mapDataToStructure(STRUCTURE, {
      [FIELD_ID.NPP]: documentData[FIELD_ID.NPP] && documentData[FIELD_ID.NPP].toString(),
      [FIELD_ID.EMPLOYEE_NUM]: documentData[FIELD_ID.EMPLOYEE_NUM],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]: documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS],
      [FIELD_ID.EMPLOYEE_SURNAME]: documentData[FIELD_ID.EMPLOYEE_SURNAME],
      [FIELD_ID.EMPLOYEE_NAME]: documentData[FIELD_ID.EMPLOYEE_NAME],
      [FIELD_ID.EMPLOYEE_PATRONYMIC]: documentData[FIELD_ID.EMPLOYEE_PATRONYMIC],
      [FIELD_ID.EMPLOYEE_RESIDENT]: EMPLOYEE_RESIDENT,
      [FIELD_ID.EMBOSSED_FAMILY]: documentData[FIELD_ID.EMBOSSED_FAMILY],
      [FIELD_ID.EMBOSSED_NAME]: documentData[FIELD_ID.EMBOSSED_NAME],
      [FIELD_ID.EMPLOYEE_BIRTH_DATE]: isEditable
        ? documentData[FIELD_ID.EMPLOYEE_BIRTH_DATE]
        : getFormattedDate(documentData[FIELD_ID.EMPLOYEE_BIRTH_DATE]),
      [FIELD_ID.GENDER]: GENDER,
      [FIELD_ID.CITIZENSHIP_NAME]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.CITIZENSHIP_NAME],
            documentDictionaries[FIELD_ID.CITIZENSHIP_NAME],
            'shortName',
            'id',
          )
        : documentData[FIELD_ID.CITIZENSHIP_NAME],
      [FIELD_ID.COUNTRY_NAME]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.COUNTRY_NAME],
            documentDictionaries[FIELD_ID.COUNTRY_NAME],
            'shortName',
            'id',
          )
        : documentData[FIELD_ID.COUNTRY_NAME],
      [FIELD_ID.PLACE_OF_BIRTH]: documentData[FIELD_ID.PLACE_OF_BIRTH],
      [FIELD_ID.CARD_BRANCH_CITY]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.CARD_BRANCH_CITY],
            documentDictionaries[FIELD_ID.CARD_BRANCH_CITY],
            'name',
            'id',
          )
        : documentData[FIELD_ID.CARD_BRANCH_CITY],
      [FIELD_ID.CARD_BRANCH_NAME]: isEditable
        ? documentData[FIELD_ID.CARD_BRANCH_ID]
        : documentData[FIELD_ID.CARD_BRANCH_NAME],
      [FIELD_ID.CARD_BRANCH_ADDRESS]: documentData[FIELD_ID.CARD_BRANCH_ADDRESS],
      [FIELD_ID.TARIFF_PLAN_NAME]: isEditable
        ? documentData[FIELD_ID.TARIFF_PLAN_NUMBER]
        : documentData[FIELD_ID.TARIFF_PLAN_NAME],
      [FIELD_ID.LOAD_SCHEME_NAME]: isEditable
        ? documentData[FIELD_ID.LOAD_SCHEME_NUMBER]
        : documentData[FIELD_ID.LOAD_SCHEME_NAME],
      [FIELD_ID.MARRIED]: MARRIED,
      [FIELD_ID.EMPLOYEE_POSITION]: documentData[FIELD_ID.EMPLOYEE_POSITION],
      [FIELD_ID.INN]: documentData[FIELD_ID.INN],
      [FIELD_ID.SNILS]: documentData[FIELD_ID.SNILS],
      [FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]: documentData[FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME],
            documentDictionaries[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME],
            'shortName',
            'id',
          )
        : documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK],
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME],
            documentDictionaries[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME],
            'shortName',
            'id',
          )
        : documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK],
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT],
      [FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION],
            documentDictionaries[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION],
            'desc',
            'id',
          )
        : documentData[FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION],
      [FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]: documentData[FIELD_ID.EMPLOYEE_DOCUMENT_SERIES],
      [FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]: documentData[FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER],
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]: isEditable
        ? documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]
        : getFormattedDate(documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]),
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]: documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY],
      [FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]: documentData[FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE],
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME],
            documentDictionaries[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME],
            'shortName',
            'id',
          )
        : documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME],
      [FIELD_ID.VISA_DPP_SERIES]: documentData[FIELD_ID.VISA_DPP_SERIES],
      [FIELD_ID.VISA_DPP_NUMBER]: documentData[FIELD_ID.VISA_DPP_NUMBER],
      [FIELD_ID.VISA_DPP_DATE_START]: isEditable
        ? documentData[FIELD_ID.VISA_DPP_DATE_START]
        : getFormattedDate(documentData[FIELD_ID.VISA_DPP_DATE_START]),
      [FIELD_ID.VISA_DPP_DATE_END]: isEditable
        ? documentData[FIELD_ID.VISA_DPP_DATE_END]
        : getFormattedDate(documentData[FIELD_ID.VISA_DPP_DATE_END]),
      [FIELD_ID.VISA_DPP_ISSUED_BY]: documentData[FIELD_ID.VISA_DPP_ISSUED_BY],
      [FIELD_ID.VISA_DPP_ISSUED_DATE]: isEditable
        ? documentData[FIELD_ID.VISA_DPP_ISSUED_DATE]
        : getFormattedDate(documentData[FIELD_ID.VISA_DPP_ISSUED_DATE]),
      [FIELD_ID.MIGRATION_CARD_DPP_SERIES]: documentData[FIELD_ID.MIGRATION_CARD_DPP_SERIES],
      [FIELD_ID.MIGRATION_CARD_DPP_NUMBER]: documentData[FIELD_ID.MIGRATION_CARD_DPP_NUMBER],
      [FIELD_ID.MIGRATION_CARD_DPP_DATE_START]: isEditable
        ? documentData[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]
        : getFormattedDate(documentData[FIELD_ID.MIGRATION_CARD_DPP_DATE_START]),
      [FIELD_ID.MIGRATION_CARD_DPP_DATE_END]: isEditable
        ? documentData[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]
        : getFormattedDate(documentData[FIELD_ID.MIGRATION_CARD_DPP_DATE_END]),
      [FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]: documentData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY],
      [FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]: isEditable
        ? documentData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]
        : getFormattedDate(documentData[FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]),
      [FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]: documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES],
      [FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]: documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER],
      [FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]: isEditable
        ? documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]
        : getFormattedDate(documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]),
      [FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]: isEditable
        ? documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]
        : getFormattedDate(documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]),
      [FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]: documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY],
      [FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]: isEditable
        ? documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]
        : getFormattedDate(documentData[FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]),
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]: documentData[FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE],
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]: documentData[FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE],
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]: documentData[FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE],
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]: documentData[FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS]: documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS]
        ? documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS].map(item => ({
            id: item.account,
            account: getFormattedAccount(item.account),
            currency: item.currency.isoCode,
          }))
        : [],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]: documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]:
        documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK],
    });
  },
);

const documentDictionariesCreatedSelector = createSelector(
  documentDictionariesSelector,
  documentDataSelector,
  (documentDictionaries, documentData) =>
    mapDataToStructure(
      STRUCTURE,
      mapObject((item, key) => mapDictionaryItems(item, key, documentData), documentDictionaries),
    ),
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['1'])),
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['2', '3'])),
);

const formDisabledCreatedSelector = createSelector(
  isDisabledSelector,
  documentDataSelector,
  documentDictionariesSelector,
  (isDisabled, documentData, documentDictionaries) =>
    mapDataToStructure(STRUCTURE, {
      [FIELD_ID.NPP]: isDisabled,
      [FIELD_ID.EMPLOYEE_NUM]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]: isDisabled,
      [FIELD_ID.EMPLOYEE_SURNAME]: isDisabled,
      [FIELD_ID.EMPLOYEE_NAME]: isDisabled,
      [FIELD_ID.EMPLOYEE_PATRONYMIC]: isDisabled,
      [FIELD_ID.EMPLOYEE_RESIDENT]: isDisabled,
      [FIELD_ID.EMBOSSED_FAMILY]: isDisabled,
      [FIELD_ID.EMBOSSED_NAME]: isDisabled,
      [FIELD_ID.EMPLOYEE_BIRTH_DATE]: isDisabled,
      [FIELD_ID.GENDER]: isDisabled,
      [FIELD_ID.CITIZENSHIP_NAME]: isDisabled,
      [FIELD_ID.COUNTRY_NAME]: isDisabled,
      [FIELD_ID.PLACE_OF_BIRTH]: isDisabled,
      [FIELD_ID.CARD_BRANCH_CITY]: isDisabled,
      [FIELD_ID.CARD_BRANCH_NAME]: isDisabled,
      [FIELD_ID.CARD_BRANCH_ADDRESS]: isDisabled,
      [FIELD_ID.TARIFF_PLAN_NAME]:
        isDisabled ||
        (documentData[FIELD_ID.TARIFF_PLAN_NAME] && documentDictionaries[FIELD_ID.TARIFF_PLAN_NAME].items.length === 1),
      [FIELD_ID.LOAD_SCHEME_NAME]:
        isDisabled ||
        !documentData[FIELD_ID.TARIFF_PLAN_NAME] ||
        (documentData[FIELD_ID.LOAD_SCHEME_NAME] && documentDictionaries[FIELD_ID.LOAD_SCHEME_NAME].items.length === 1),
      [FIELD_ID.MARRIED]: isDisabled,
      [FIELD_ID.EMPLOYEE_POSITION]: isDisabled,
      [FIELD_ID.INN]: isDisabled,
      [FIELD_ID.SNILS]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]: isDisabled,
      [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_SERIES]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE]: isDisabled,
      [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]: isDisabled,
      [FIELD_ID.VISA_DPP_SERIES]: isDisabled,
      [FIELD_ID.VISA_DPP_NUMBER]: isDisabled,
      [FIELD_ID.VISA_DPP_DATE_START]: isDisabled,
      [FIELD_ID.VISA_DPP_DATE_END]: isDisabled,
      [FIELD_ID.VISA_DPP_ISSUED_BY]: isDisabled,
      [FIELD_ID.VISA_DPP_ISSUED_DATE]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_SERIES]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_NUMBER]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_DATE_START]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_DATE_END]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY]: isDisabled,
      [FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY]: isDisabled,
      [FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE]: isDisabled,
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE]: isDisabled,
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE]: isDisabled,
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE]: isDisabled,
      [FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]: isDisabled,
    }),
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isError: isErrorCreatedSelector(state),
  serverError: serverErrorCreatedSelector(state),
  isLoading: isLoadingCreatedSelector(state),
  isEditable: isEditableSelector(state),
  operations: operationsCreatedSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentSections: documentSectionsCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  formDisabled: formDisabledCreatedSelector(state),
});

export default mapStateToProps;
