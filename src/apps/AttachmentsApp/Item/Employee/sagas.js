import { all, call, put, select, takeEvery } from 'redux-saga/effects';

import { ROUTE_LEAVE_CONFIRM_ENABLE, DOC_NEW_ID } from 'common/constants';
import operations from 'common/operations';
import { openAction as confirmModalOpenAction } from 'common/components/RboConfirmModal/actions';
import {
  getCountries as dalGetCountries,
  getIdentityDocumentTypes as dalGetIdentityDocumentTypes,
  getLoadSchemes as dalGetLoadSchemes,
} from 'dal/dictionaries';
import { getEmployeeErrors as dalGetEmployeeErrors } from 'dal/payroll-registries-attachments';
import localization from 'localization';
import { notificationError, notificationSuccess, notificationWarning } from 'platform';
import { dispatchErrorAction, windowRefresh } from 'utils';

import { REDUCER_KEY as DOC_REDUCER_KEY, FIELD_ID as DOC_FIELD_ID, ACTIONS as DOC_ACTIONS } from '../constants';

import { DICTIONARIES_OPTIONS, OPERATIONS_ACTIONS_MAP } from './config';
import { REDUCER_KEY, FIELD_ID, LOCALE_KEY, ACTIONS } from './constants';

const docIsEditableSelector = state => state[DOC_REDUCER_KEY].isEditable;
const docDocumentDataSelector = state => state[DOC_REDUCER_KEY].documentData;
const docOrgIdSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.CUSTOMER_ID];
const docEmployeesInfoSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.EMPLOYEES_INFO];
const docEditedEmployeeSelector = state => state[DOC_REDUCER_KEY].editedEmployee;
const docDictionariesSelector = state => state[DOC_REDUCER_KEY].documentDictionaries;
const isSavedSelector = state => state[REDUCER_KEY].isSaved;
const documentDataSelector = state => state[REDUCER_KEY].documentData;

function* getData({ id }) {
  yield put({ type: ACTIONS.GET_DATA_REQUEST });
  const docEmployeesInfo = yield select(docEmployeesInfoSelector);

  let documentData = {};

  try {
    if (id === DOC_NEW_ID) {
      documentData = {
        [FIELD_ID.NPP]: docEmployeesInfo.length + 1,
        [FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]: true,
      };
    } else {
      documentData = docEmployeesInfo.find(item => item[FIELD_ID.ID] === id);
    }
    const isEditable = yield select(docIsEditableSelector);

    yield put({ type: ACTIONS.GET_DATA_SUCCESS });
    return { documentData, isEditable };
  } catch (error) {
    yield put({ type: ACTIONS.GET_DATA_FAIL, error });
    throw error;
  }
}

function* saveData(channels) {
  yield put({ type: ACTIONS.SAVE_DATA_REQUEST });
  const documentData = yield select(documentDataSelector);
  try {
    const parentDocumentData = yield select(docDocumentDataSelector);
    const errorsResult = yield call(dalGetEmployeeErrors, {
      ...parentDocumentData,
      [DOC_FIELD_ID.EMPLOYEES_INFO]: [documentData],
    });

    if (errorsResult.errors && errorsResult.errors.filter(error => ['2', '3'].includes(error.level)).length) {
      throw new operations.RBOControlsError({ data: errorsResult });
    }
    if (errorsResult.errors && errorsResult.errors.filter(error => ['1'].includes(error.level)).length) {
      yield call(notificationWarning, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.SUCCESS_WITH_WARNINGS.TITLE),
      });
    } else {
      yield call(notificationSuccess, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.SUCCESS.TITLE),
      });
    }
    yield put({ type: ACTIONS.SAVE_DATA_SUCCESS, payload: errorsResult });
    yield put({ type: channels.success });
  } catch (error) {
    if (error instanceof operations.RBOControlsError) {
      yield put({ type: ACTIONS.SAVE_DATA_FAIL, payload: error.response.data });
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.RBO_CONTROLS.TITLE),
        message: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.RBO_CONTROLS.MESSAGE),
      });
    } else {
      yield put({ type: ACTIONS.SAVE_DATA_FAIL });
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.FAIL.TITLE),
      });
    }
    yield put({ type: channels.fail });
  }
}

function* refreshSaga() {
  const id = yield select(docEditedEmployeeSelector);
  try {
    const data = yield call(getData, { id });
    const { documentData } = data;
    yield put({ type: ACTIONS.REFRESH_SUCCESS, payload: { documentData } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.REFRESH_FAIL] });
  }
}

function* mountSaga() {
  const id = yield select(docEditedEmployeeSelector);
  let documentDictionaries = {
    [FIELD_ID.EMPLOYEE_RESIDENT]: { items: DICTIONARIES_OPTIONS[FIELD_ID.EMPLOYEE_RESIDENT] },
    [FIELD_ID.GENDER]: { items: DICTIONARIES_OPTIONS[FIELD_ID.GENDER] },
    [FIELD_ID.MARRIED]: { items: DICTIONARIES_OPTIONS[FIELD_ID.MARRIED] },
  };

  try {
    const data = yield call(getData, { id: id || DOC_NEW_ID });
    const { documentData, isEditable } = data;

    let documentErrors = [];
    if (isEditable && id !== DOC_NEW_ID) {
      const parentDocumentData = yield select(docDocumentDataSelector);
      const errorsResult = yield call(dalGetEmployeeErrors, {
        ...parentDocumentData,
        [DOC_FIELD_ID.EMPLOYEES_INFO]: [documentData],
      });
      documentErrors = errorsResult.errors;
    }

    if (isEditable) {
      const orgId = yield select(docOrgIdSelector);
      const docDictionaries = yield select(docDictionariesSelector);
      const cardBranchesCities = docDictionaries[DOC_FIELD_ID.CARD_BRANCH_CITY].items;
      const cardBranches = docDictionaries[DOC_FIELD_ID.CARD_BRANCH_NAME].items;
      const tariffPlans = docDictionaries[DOC_FIELD_ID.TARIFF_PLAN_NAME].items;

      const [
        identityDocumentTypes,
        citizenship,
        countries,
        regAddressCountries,
        livAddressCountries,
        identityDocumentIssuedCountries,
      ] = yield all([
        dalGetIdentityDocumentTypes(),
        dalGetCountries({ every: documentData[FIELD_ID.CITIZENSHIP_NAME] }),
        dalGetCountries({ every: documentData[FIELD_ID.COUNTRY_NAME] }),
        dalGetCountries({ every: documentData[FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME] }),
        dalGetCountries({ every: documentData[FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME] }),
        dalGetCountries({ every: documentData[FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME] }),
      ]);

      /* Тарифный план
       * Если у организации только один тарифный план, то поле должно заполняться его наименованием по умолчанию.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405879 */
      if (id === DOC_NEW_ID) {
        if (tariffPlans.length === 1) {
          documentData[FIELD_ID.TARIFF_PLAN_NUMBER] = tariffPlans[0].number;
          documentData[FIELD_ID.TARIFF_PLAN_NAME] = tariffPlans[0].name;
        }
      }

      const loadSchemes = yield call(dalGetLoadSchemes, { orgId, tpCode: documentData[FIELD_ID.TARIFF_PLAN_NUMBER] });

      /* Схема загрузки
       * Если в списке доступна только одна схема загрузки, то поле должно заполняться ее наименованием по умолчанию.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405883 */
      if (id === DOC_NEW_ID) {
        if (loadSchemes.length === 1) {
          documentData[FIELD_ID.LOAD_SCHEME_NUMBER] = loadSchemes[0].number;
          documentData[FIELD_ID.LOAD_SCHEME_NAME] = loadSchemes[0].name;
        }
      }

      documentDictionaries = {
        ...documentDictionaries,
        [FIELD_ID.CITIZENSHIP_NAME]: { items: citizenship },
        [FIELD_ID.COUNTRY_NAME]: { items: countries },
        [FIELD_ID.CARD_BRANCH_CITY]: { items: cardBranchesCities },
        [FIELD_ID.CARD_BRANCH_NAME]: { items: cardBranches },
        [FIELD_ID.TARIFF_PLAN_NAME]: { items: tariffPlans },
        [FIELD_ID.LOAD_SCHEME_NAME]: { items: loadSchemes },
        [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]: { items: regAddressCountries },
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]: { items: livAddressCountries },
        [FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]: { items: identityDocumentTypes },
        [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]: { items: identityDocumentIssuedCountries },
      };
    }

    yield put({
      type: ACTIONS.MOUNT_SUCCESS,
      payload: {
        isEditable,
        documentData,
        documentErrors,
        documentDictionaries,
      },
    });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  } finally {
    windowRefresh();
  }
}

function* changeDataSaga({ payload }) {
  const { fieldId } = payload;
  const documentData = yield select(documentDataSelector);
  if (fieldId === FIELD_ID.TARIFF_PLAN_NAME) {
    const orgId = yield select(docOrgIdSelector);
    const dictionaryId = FIELD_ID.LOAD_SCHEME_NAME;
    try {
      const dictionaryItems = yield call(dalGetLoadSchemes, {
        orgId,
        tpCode: documentData[FIELD_ID.TARIFF_PLAN_NUMBER],
      });
      yield put({ type: ACTIONS.DICTIONARY_SEARCH_SUCCESS, payload: { dictionaryId, dictionaryItems } });

      /* Схема загрузки
       * Если в списке доступна только одна схема загрузки, то поле должно заполняться ее наименованием по умолчанию.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405883 */
      if (dictionaryItems.length === 1) {
        yield put({
          type: ACTIONS.CHANGE_DATA,
          payload: { fieldId: FIELD_ID.LOAD_SCHEME_NAME, fieldValue: dictionaryItems[0].number },
        });
      }
    } catch (error) {
      yield dispatchErrorAction({
        error,
        types: [ACTIONS.DICTIONARY_SEARCH_FAIL, ACTIONS.DICTIONARY_SEARCH_CANCEL],
        payload: { dictionaryId },
      });
    }
  }
}

function* dictionarySearchSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  if (
    [
      FIELD_ID.CITIZENSHIP_NAME,
      FIELD_ID.COUNTRY_NAME,
      FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME,
      FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME,
      FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME,
    ].includes(dictionaryId)
  ) {
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_REQUEST, payload: { dictionaryId, dictionarySearchValue } });
  }
}

function* dictionarySearchRequestSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  let dictionaryItems;
  try {
    switch (dictionaryId) {
      case FIELD_ID.CITIZENSHIP_NAME:
      case FIELD_ID.COUNTRY_NAME:
      case FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME:
      case FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME:
      case FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME:
        dictionaryItems = yield call(dalGetCountries, { every: dictionarySearchValue }, true);
        break;
      default:
        break;
    }
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_SUCCESS, payload: { dictionaryId, dictionaryItems } });
  } catch (error) {
    yield dispatchErrorAction({
      error,
      types: [ACTIONS.DICTIONARY_SEARCH_FAIL, ACTIONS.DICTIONARY_SEARCH_CANCEL],
      payload: { dictionaryId },
    });
  }
}

function* operationsSaga({ payload }) {
  const { operationId } = payload;
  yield put({ type: OPERATIONS_ACTIONS_MAP[operationId], payload });
}

function* operationBackSaga() {
  const isSaved = yield select(isSavedSelector);
  if (ROUTE_LEAVE_CONFIRM_ENABLE && !isSaved) {
    yield put(
      confirmModalOpenAction({
        message: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_MESSAGE),
        confirm: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CONFIRM),
        cancel: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CANCEL),
        channels: {
          confirm: { type: ACTIONS.OPERATION_BACK_CONFIRM },
          cancel: { type: ACTIONS.OPERATION_BACK_CANCEL },
        },
      }),
    );
  } else {
    yield put({ type: ACTIONS.OPERATION_BACK_CONFIRM });
  }
}

function* operationBackConfirmSaga() {
  yield put({ type: DOC_ACTIONS.CLOSE_EMPLOYEE });
}

function* operationSaveSaga() {
  yield call(saveData, { success: ACTIONS.OPERATION_SAVE_SUCCESS, fail: ACTIONS.OPERATION_SAVE_FAIL });
}

function* operationSaveSuccessSaga() {
  const documentData = yield select(documentDataSelector);
  yield put({ type: DOC_ACTIONS.SAVE_EMPLOYEE, payload: documentData });
  yield put({ type: DOC_ACTIONS.CLOSE_EMPLOYEE });
}

function* operationSaveAndAddSaga() {
  yield call(saveData, { success: ACTIONS.OPERATION_SAVE_AND_ADD_SUCCESS, fail: ACTIONS.OPERATION_SAVE_AND_ADD_FAIL });
}

function* operationSaveAndAddSuccessSaga() {
  const documentData = yield select(documentDataSelector);
  yield put({ type: DOC_ACTIONS.SAVE_EMPLOYEE, payload: documentData });
  yield put({ type: DOC_ACTIONS.OPEN_EMPLOYEE, payload: { id: DOC_NEW_ID } });
  yield put({ type: ACTIONS.MOUNT_REQUEST });
}

export default function* sagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.REFRESH, refreshSaga);

  yield takeEvery(ACTIONS.CHANGE_DATA, changeDataSaga);

  yield takeEvery(ACTIONS.DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeEvery(ACTIONS.DICTIONARY_SEARCH_REQUEST, dictionarySearchRequestSaga);

  yield takeEvery(ACTIONS.OPERATIONS, operationsSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK, operationBackSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK_CONFIRM, operationBackConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE, operationSaveSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AND_ADD, operationSaveAndAddSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AND_ADD_SUCCESS, operationSaveAndAddSuccessSaga);
}
