import localization from 'localization';
import { escapeRegExp, getDictionaryValue, transliterate } from 'utils';

import { FIELD_ID } from './constants';

export function mapDictionaryItems(dictionary, key, documentData) {
  const { items } = dictionary;
  const searchValue = escapeRegExp(dictionary.searchValue);
  let searchRegExp;
  switch (key) {
    case FIELD_ID.EMPLOYEE_RESIDENT:
    case FIELD_ID.GENDER:
    case FIELD_ID.MARRIED:
      return {
        ...dictionary,
        items: items.map(item => ({
          value: item.value,
          title: localization.translate(item.titleKey),
        })),
      };
    case FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION:
      return {
        ...dictionary,
        items: items.map(item => ({
          value: item.id,
          title: item.desc,
        })),
      };
    case FIELD_ID.CITIZENSHIP_NAME:
    case FIELD_ID.COUNTRY_NAME:
    case FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME:
    case FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME:
    case FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME:
      return {
        ...dictionary,
        items: items.map(item => ({
          value: item.id,
          title: item.shortName,
          extra: `${item.mnem02}/${item.code}`,
          describe: item.name,
        })),
      };
    case FIELD_ID.CARD_BRANCH_CITY:
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.name,
          }))
          .filter(item => item.title && item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.CARD_BRANCH_NAME:
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.name,
            describe: item.address,
            city: item.city,
          }))
          /* Отделение выдачи карт
           * Список подразделений должен быть отфильтрован в зависимости от выбранного города, если он выбран
           * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405812 */
          .filter(
            item => !documentData[FIELD_ID.CARD_BRANCH_CITY] || item.city === documentData[FIELD_ID.CARD_BRANCH_CITY],
          )
          .filter(item => item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.TARIFF_PLAN_NAME:
    case FIELD_ID.LOAD_SCHEME_NAME:
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.number,
            title: item.name,
          }))
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    default:
      return dictionary;
  }
}

export const changeState = {
  /** Фамилия */
  [FIELD_ID.EMPLOYEE_SURNAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMPLOYEE_SURNAME;

    if (fieldValue !== state.documentData[fieldId]) {
      changedFields = {
        [fieldId]: fieldValue,
      };
    }

    return changeState[FIELD_ID.EMBOSSED_FAMILY](
      {
        ...state,
        documentData: {
          ...state.documentData,
          ...changedFields,
        },
      },
      { fieldValue },
    );
  },

  /** Имя */
  [FIELD_ID.EMPLOYEE_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMPLOYEE_NAME;

    if (fieldValue !== state.documentData[fieldId]) {
      changedFields = {
        [fieldId]: fieldValue,
      };
    }

    return changeState[FIELD_ID.EMBOSSED_NAME](
      {
        ...state,
        documentData: {
          ...state.documentData,
          ...changedFields,
        },
      },
      { fieldValue },
    );
  },

  /** Фамилия лат. */
  [FIELD_ID.EMBOSSED_FAMILY]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMBOSSED_FAMILY;
    const embossedFamily = fieldValue ? transliterate(fieldValue.toUpperCase()) : null;

    if (embossedFamily !== state.documentData[fieldId]) {
      changedFields = {
        [fieldId]: embossedFamily,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Имя лат. */
  [FIELD_ID.EMBOSSED_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMBOSSED_NAME;
    const embossedName = fieldValue ? transliterate(fieldValue.toUpperCase()) : null;

    if (embossedName !== state.documentData[fieldId]) {
      changedFields = {
        [fieldId]: embossedName,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Гражданство */
  [FIELD_ID.CITIZENSHIP_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.CITIZENSHIP_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const shortName = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (shortName !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: shortName,
        [FIELD_ID.CITIZENSHIP_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Страна рождения */
  [FIELD_ID.COUNTRY_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.COUNTRY_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const shortName = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (shortName !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: shortName,
        [FIELD_ID.COUNTRY_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Город выдачи карт */
  [FIELD_ID.CARD_BRANCH_CITY]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.CARD_BRANCH_CITY;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const name = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (name !== state.documentData[fieldId]) {
      /* Отделение выдачи карт
       * Значение в поле очищается при смене города
       * Если у организации только одно отделение, то поле должно заполняться его наименованием по умолчанию.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=89405812 */
      const cardBranchDictionaryItems = state.documentDictionaries[FIELD_ID.CARD_BRANCH_NAME].items.filter(
        item => item.city === name,
      );
      const cardBranchDictionaryItem = cardBranchDictionaryItems.length === 1 ? cardBranchDictionaryItems[0] : null;
      const cardBranchAddress = cardBranchDictionaryItem ? cardBranchDictionaryItem.address : null;
      const cardBranchBic = cardBranchDictionaryItem ? cardBranchDictionaryItem.bic : null;
      const cardBranchId = cardBranchDictionaryItem ? cardBranchDictionaryItem.id : null;
      const cardBranchName = cardBranchDictionaryItem ? cardBranchDictionaryItem.name : null;

      changedFields = {
        [fieldId]: name,
        [FIELD_ID.CARD_BRANCH_ADDRESS]: cardBranchAddress,
        [FIELD_ID.CARD_BRANCH_BIC]: cardBranchBic,
        [FIELD_ID.CARD_BRANCH_ID]: cardBranchId,
        [FIELD_ID.CARD_BRANCH_NAME]: cardBranchName,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },

  /** Отделение выдачи карт */
  [FIELD_ID.CARD_BRANCH_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.CARD_BRANCH_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const name = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (name !== state.documentData[fieldId]) {
      const address = (selectedDictionaryItem && selectedDictionaryItem.address) || null;
      const bic = (selectedDictionaryItem && selectedDictionaryItem.bic) || null;
      const city = fieldValue
        ? (selectedDictionaryItem && selectedDictionaryItem.city) || null
        : state.documentData[FIELD_ID.CARD_BRANCH_CITY];
      const id = (selectedDictionaryItem && selectedDictionaryItem.id) || null;
      changedFields = {
        [fieldId]: name,
        [FIELD_ID.CARD_BRANCH_ADDRESS]: address,
        [FIELD_ID.CARD_BRANCH_BIC]: bic,
        [FIELD_ID.CARD_BRANCH_CITY]: city,
        [FIELD_ID.CARD_BRANCH_ID]: id,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },

  /** Тарифный план */
  [FIELD_ID.TARIFF_PLAN_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.TARIFF_PLAN_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'number');
    const name = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (name !== state.documentData[fieldId]) {
      const number = (selectedDictionaryItem && selectedDictionaryItem.number) || null;
      const loadSchemeName = null;
      const loadSchemeNumber = null;
      changedFields = {
        [fieldId]: name,
        [FIELD_ID.TARIFF_PLAN_NUMBER]: number,
        [FIELD_ID.LOAD_SCHEME_NAME]: loadSchemeName,
        [FIELD_ID.LOAD_SCHEME_NUMBER]: loadSchemeNumber,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Схема загрузки */
  [FIELD_ID.LOAD_SCHEME_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.LOAD_SCHEME_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'number');
    const name = (selectedDictionaryItem && selectedDictionaryItem.name) || null;

    if (name !== state.documentData[fieldId]) {
      const number = (selectedDictionaryItem && selectedDictionaryItem.number) || null;
      changedFields = {
        [fieldId]: name,
        [FIELD_ID.LOAD_SCHEME_NUMBER]: number,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Адрес фактического проживания совпадает с адресом регистрации */
  [FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH;

    if (fieldValue !== state.documentData[fieldId]) {
      changedFields = {
        [fieldId]: fieldValue,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_CODE]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE]: null,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET]: null,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Адрес регистрации - Страна */
  [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const name = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (name !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: name,
        [FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Адрес проживания - Страна */
  [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const name = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (name !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: name,
        [FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Документ, удостоверяющий личность - Вид документа, удостоверяюшего личность */
  [FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const desc = (selectedDictionaryItem && selectedDictionaryItem.desc) || null;

    if (desc !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: desc,
        [FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },

  /** Документ, удостоверяющий личность - Страна, выдавшая документ */
  [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const shortName = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (shortName !== state.documentData[fieldId]) {
      const code = (selectedDictionaryItem && selectedDictionaryItem.code) || null;
      changedFields = {
        [fieldId]: shortName,
        [FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_CODE]: code,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },
};
