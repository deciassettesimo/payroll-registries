import { ACTIONS } from './constants';

export const mountAction = (params, location) => ({
  type: ACTIONS.MOUNT_REQUEST,
  payload: { params, location },
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const operationsAction = (operationId, params) => ({
  type: ACTIONS.OPERATIONS,
  payload: { operationId, params },
});

export const activateErrorAction = errorId => ({
  type: ACTIONS.ACTIVATE_ERROR,
  payload: { errorId },
});

export const fieldFocusAction = fieldId => ({
  type: ACTIONS.FIELD_FOCUS,
  payload: { fieldId },
});

export const fieldBlurAction = fieldId => ({
  type: ACTIONS.FIELD_BLUR,
  payload: { fieldId },
});

export const changeDataAction = (fieldId, fieldValue, dictionaryValue) => ({
  type: ACTIONS.CHANGE_DATA,
  payload: { fieldId, fieldValue, dictionaryValue },
});

export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTIONS.DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue },
});

export const sectionMinimizerToggleAction = (id, value) => ({
  type: ACTIONS.SECTION_MINIMIZER_TOGGLE,
  payload: { id, value },
});
