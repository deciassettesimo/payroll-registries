import { connect } from 'react-redux';
import CSVImportModal from './CSVImportModal';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default connect(
  mapStateToProps,
  actions,
)(CSVImportModal);
