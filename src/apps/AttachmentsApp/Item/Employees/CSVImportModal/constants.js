import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesAttachmentsItemEmployeesCSVImportModal';

export const LOCALE_KEY = {
  HEADER: 'loc.importFromCSV',
  ERRORS_TITLE: 'loc.errors',
  ENCODING: 'loc.fileEncoding',
  SEPARATOR: 'loc.separator',
  NOTIFICATIONS: {
    SUCCESS: {
      TITLE: 'loc.importFromCSVCompleted',
      MESSAGE: 'loc.employeesAdded',
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,
    SAVE_SETTINGS: null,

    OPEN: null,
    CLOSE: null,
    CHANGE: null,
    IMPORT_REQUEST: null,
    IMPORT_SUCCESS: null,
    IMPORT_FAIL: null,

    ACTIVATE_ERROR: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_ITEM_EMPLOYEES_CSV_IMPORT_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    FILE: null,
    ENCODING: null,
    SEPARATOR: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_ITEM_EMPLOYEES_CSV_IMPORT_MODAL_',
);
