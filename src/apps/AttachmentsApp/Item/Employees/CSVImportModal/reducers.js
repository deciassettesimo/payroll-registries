import { ENCODINGS } from './config';
import { REDUCER_KEY, ACTIONS } from './constants';

export const initialState = {
  isMounted: false,
  isVisible: false,
  isLoading: false,
  encodings: ENCODINGS,
  settings: {},
  errors: null,
  activeError: null,
};

const mountRequest = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, { settings }) => ({ ...state, isMounted: true, settings });

const mountFail = state => ({ ...state, isMounted: true });

const unmount = () => ({ ...initialState, isMounted: false });

const saveSettings = (state, { settings }) => ({ ...state, settings });

const importRequest = state => ({ ...state, isLoading: true, errors: null });

const importSuccess = (state, { errors }) => ({ ...state, isLoading: false, errors });

const importFail = state => ({ ...state, isLoading: false });

const activateError = (state, { id }) => ({ ...state, activeError: id === state.activeError ? null : id });

function reducers(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.SAVE_SETTINGS:
      return saveSettings(state, action.payload);

    case ACTIONS.OPEN:
      return { ...state, isVisible: true };

    case ACTIONS.CLOSE:
      return { ...state, isVisible: false, isLoading: false, errors: null, activeError: null };

    case ACTIONS.IMPORT_REQUEST:
      return importRequest(state);

    case ACTIONS.IMPORT_SUCCESS:
      return importSuccess(state, action.payload);

    case ACTIONS.IMPORT_FAIL:
      return importFail(state);

    case ACTIONS.ACTIVATE_ERROR:
      return activateError(state, action.payload);

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducers };
