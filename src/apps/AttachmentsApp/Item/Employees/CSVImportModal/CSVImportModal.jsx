import React, { Component } from 'react';
import PropTypes from 'prop-types';

import RboDocModal from '@rbo/rbo-components/lib/RboDocModal';
import { Block } from '@rbo/rbo-components/lib/Styles';
import Form, { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputText, InputSelect, InputFiles } from '@rbo/rbo-components/lib/Input';

import { FIELD_ID } from './constants';

export default class CSVImportModal extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isVisible: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    values: PropTypes.shape().isRequired,
    options: PropTypes.shape().isRequired,
    errors: PropTypes.arrayOf(PropTypes.shape()),
    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeAction: PropTypes.func.isRequired,
    activateErrorAction: PropTypes.func.isRequired,
  };

  static defaultProps = {
    errors: null,
  };

  componentDidMount() {
    const { mountAction } = this.props;
    mountAction();
  }

  componentWillUnmount() {
    const { unmountAction } = this.props;
    unmountAction();
  }

  handleClose = () => {
    const { closeAction } = this.props;
    closeAction();
  };

  handleChange = ({ id, value }) => {
    const { changeAction } = this.props;
    changeAction(id, value);
  };

  handleErrorClick = ({ errorId }) => {
    const { activateErrorAction } = this.props;
    activateErrorAction(errorId);
  };

  render() {
    const { LABELS, isVisible, isLoading, values, options, errors } = this.props;

    if (!isVisible) return null;

    return (
      <RboDocModal
        width={786}
        title={LABELS.HEADER}
        errorsTitle={LABELS.ERRORS_TITLE}
        errors={errors}
        onErrorClick={this.handleErrorClick}
        onClose={this.handleClose}
      >
        <Block>
          <Form dimension={Form.REFS.DIMENSIONS.XS}>
            <Row>
              <Cell>
                <Field>
                  <InputFiles
                    locale={LABELS.LOCALE}
                    id={FIELD_ID.FILE}
                    value={values[FIELD_ID.FILE]}
                    onChange={this.handleChange}
                    isLoading={isLoading}
                    isError={!!errors && !!errors.length}
                    minHeight={200}
                    maxFilesQty={1}
                    isWithComment={false}
                  />
                </Field>
              </Cell>
            </Row>
            <Row>
              <Cell width={140}>
                <Label htmlFor={FIELD_ID.ENCODING}>{LABELS.ENCODING}</Label>
                <Field>
                  <InputSelect
                    id={FIELD_ID.ENCODING}
                    value={values[FIELD_ID.ENCODING]}
                    options={options[FIELD_ID.ENCODING]}
                    onChange={this.handleChange}
                    disabled={isLoading}
                  />
                </Field>
              </Cell>
              <Cell width={100}>
                <Label htmlFor={FIELD_ID.SEPARATOR}>{LABELS.SEPARATOR}</Label>
                <Field>
                  <InputText
                    id={FIELD_ID.SEPARATOR}
                    value={values[FIELD_ID.SEPARATOR]}
                    onChange={this.handleChange}
                    disabled={isLoading}
                    maxLength={1}
                  />
                </Field>
              </Cell>
            </Row>
          </Form>
        </Block>
      </RboDocModal>
    );
  }
}
