import { compose } from 'redux';
import { connect } from 'react-redux';
import { lifecycle, withHandlers } from 'recompose';

import { lifecycleList } from 'hocs/lifecycles';
import { handlersList } from 'hocs/handlers';

import Employees from './Employees';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default compose(
  connect(
    mapStateToProps,
    actions,
  ),
  lifecycle(lifecycleList),
  withHandlers(handlersList),
)(Employees);
