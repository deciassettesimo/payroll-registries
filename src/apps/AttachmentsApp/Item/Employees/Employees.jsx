import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import Filter from '@rbo/rbo-components/lib/Filter';
import IconCircle from '@rbo/rbo-components/lib/Icon/IconCircle';
import IconCircleCheckedFilled from '@rbo/rbo-components/lib/Icon/IconCircleCheckedFilled';
import OperationsPanel from '@rbo/rbo-components/lib/OperationsPanel';
import Pagination from '@rbo/rbo-components/lib/Pagination';
import RboListFilters from '@rbo/rbo-components/lib/RboListFilters';
import RboListLayout from '@rbo/rbo-components/lib/RboListLayout';
import RboPopupBoxOption from '@rbo/rbo-components/lib/RboPopupBoxOption';
import Status from '@rbo/rbo-components/lib/Styles/Status';
import Table from '@rbo/rbo-components/lib/Table';

import RboHint from 'common/components/RboHint';

import { COLUMN_ID } from './constants';
import CSVImportModal from './CSVImportModal';

const popupBoxOptionRenderer = ({ id, option }) => {
  switch (id) {
    default:
      return <RboPopupBoxOption isInline label={option.title} />;
  }
};

const cellRenderer = ({ column, data }) => {
  switch (column) {
    case COLUMN_ID.EMPLOYEE_PROCESSING_RESULT_STATUS:
      return (
        <Status
          isSuccess={data.statusFlags.isSuccess}
          isWarning={data.statusFlags.isWarning}
          isError={data.statusFlags.isError}
        >
          {data[column]}
        </Status>
      );
    case COLUMN_ID.EMPLOYEE_RESIDENT:
      if (data[column]) {
        return (
          <IconCircleCheckedFilled
            dimension={IconCircleCheckedFilled.REFS.DIMENSIONS.XS}
            color={IconCircleCheckedFilled.REFS.COLORS.DARK_SKY_BLUE}
          />
        );
      }
      return <IconCircle dimension={IconCircle.REFS.DIMENSIONS.XS} color={IconCircle.REFS.COLORS.GRAYISH_BROWN} />;
    case COLUMN_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK:
      return <RboHint value={data[column]} />;
    default:
      return data[column];
  }
};

export default class Employees extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape({
      LOCALE: PropTypes.string.isRequired,
    }).isRequired,
    isLoading: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListGrouped: PropTypes.bool.isRequired,
    pages: PropTypes.shape().isRequired,
    conditions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    filters: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    filter: PropTypes.shape(),
    columns: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    list: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    operations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    toolbarOperations: PropTypes.arrayOf(PropTypes.shape()).isRequired,

    handleItemClick: PropTypes.func.isRequired,
    handlePageChange: PropTypes.func.isRequired,
    handlePaginationChange: PropTypes.func.isRequired,
    handleSortingChange: PropTypes.func.isRequired,
    handleResizeChange: PropTypes.func.isRequired,
    handleVisibilityChange: PropTypes.func.isRequired,
    handleGroupingChange: PropTypes.func.isRequired,
    handleFiltersRemove: PropTypes.func.isRequired,
    handleFiltersChoose: PropTypes.func.isRequired,
    handleFiltersCreate: PropTypes.func.isRequired,
    handleFiltersReset: PropTypes.func.isRequired,
    handleFiltersSave: PropTypes.func.isRequired,
    handleFiltersSaveAs: PropTypes.func.isRequired,
    handleFilterSearch: PropTypes.func.isRequired,
    handleFilterChange: PropTypes.func.isRequired,
    handleSelectChange: PropTypes.func.isRequired,
    handleTableOperationClick: PropTypes.func.isRequired,
    handleToolbarOperationClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    filter: null,
  };

  render() {
    const {
      LABELS,
      isLoading,
      isEditable,
      isListLoading,
      isListGrouped,
      pages,
      conditions,
      filters,
      filter,
      columns,
      list,
      operations,
      toolbarOperations,
      handleItemClick,
      handlePageChange,
      handlePaginationChange,
      handleSortingChange,
      handleResizeChange,
      handleVisibilityChange,
      handleGroupingChange,
      handleFiltersRemove,
      handleFiltersChoose,
      handleFiltersCreate,
      handleFiltersReset,
      handleFiltersSave,
      handleFiltersSaveAs,
      handleFilterSearch,
      handleFilterChange,
      handleSelectChange,
      handleTableOperationClick,
      handleToolbarOperationClick,
    } = this.props;

    return (
      <Fragment>
        <RboListLayout>
          <RboListLayout.Toolbar>
            <RboListLayout.Toolbar.Left>
              {!isLoading && (
                <RboListFilters
                  locale={LABELS.LOCALE}
                  items={filters}
                  filter={filter}
                  optionRenderer={this.optionRenderer}
                  onRemove={handleFiltersRemove}
                  onChoose={handleFiltersChoose}
                  onCreate={handleFiltersCreate}
                  onReset={handleFiltersReset}
                  onSave={handleFiltersSave}
                  onSaveAs={handleFiltersSaveAs}
                />
              )}
            </RboListLayout.Toolbar.Left>
            <RboListLayout.Toolbar.Right>
              {!isLoading && (
                <OperationsPanel
                  locale={LABELS.LOCALE}
                  dimension={OperationsPanel.REFS.DIMENSIONS.M}
                  align={OperationsPanel.REFS.ALIGN.RIGHT}
                  items={toolbarOperations}
                  onItemClick={handleToolbarOperationClick}
                />
              )}
            </RboListLayout.Toolbar.Right>
          </RboListLayout.Toolbar>
          {!isLoading && filter && (
            <RboListLayout.Filter>
              <Filter
                locale={LABELS.LOCALE}
                conditions={conditions}
                params={filter.params}
                optionRenderer={popupBoxOptionRenderer}
                onSearch={handleFilterSearch}
                onChange={handleFilterChange}
              />
            </RboListLayout.Filter>
          )}
          <RboListLayout.Content>
            <Table
              locale={LABELS.LOCALE}
              dimension={Table.REFS.DIMENSIONS.M}
              isLoading={isLoading || isListLoading}
              isGrouped={isListGrouped}
              columns={columns}
              items={list}
              cellRenderer={cellRenderer}
              onItemClick={handleItemClick}
              headBgColor={Table.REFS.HEAD_BG_COLORS.GRAY}
              operations={operations}
              onOperationClick={isEditable ? handleTableOperationClick : null}
              isSortable
              onSortingChange={handleSortingChange}
              isResizable
              onResizeChange={handleResizeChange}
              isSelectable={isEditable}
              onSelectChange={handleSelectChange}
              settings={{
                visibility: true,
                onVisibilityChange: handleVisibilityChange,
                grouping: true,
                onGroupingChange: handleGroupingChange,
              }}
            />
          </RboListLayout.Content>
          <RboListLayout.Footer>
            <RboListLayout.Footer.Right>
              {!isLoading && (
                <Pagination
                  locale={LABELS.LOCALE}
                  selected={pages.selected}
                  visibleQty={pages.visibleQty}
                  totalQty={pages.totalQty}
                  settings={pages.settings}
                  onItemClick={handlePageChange}
                  onSettingsChange={handlePaginationChange}
                />
              )}
            </RboListLayout.Footer.Right>
          </RboListLayout.Footer>
        </RboListLayout>
        <CSVImportModal />
      </Fragment>
    );
  }
}
