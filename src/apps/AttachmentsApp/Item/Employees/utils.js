import { CONDITIONS_TYPES } from '@rbo/rbo-components/lib/Filter/_constants';

import localization from 'localization';
import { escapeRegExp } from 'utils';

import { COLUMN_ID, CONDITION_ID, OPERATION_ID } from './constants';

export const getAllowedOperations = ({ operations }) =>
  operations.filter(operation => {
    switch (operation.id) {
      case OPERATION_ID.REMOVE:
        return true;
      default:
        return false;
    }
  });

export const mapConditionsOptions = ({ id, options }) => {
  switch (id) {
    case CONDITION_ID.EMPLOYEE_PROCESSING_RESULT_STATUS:
      return options.map(item => ({
        value: item.id,
        title: localization.translate(item.titleKey),
      }));
    default:
      return options;
  }
};

const getParamLabel = ({ value, condition, conditionOptions }) => {
  if (!value) return '...';
  let options;
  let selectedOptions;
  switch (condition.type) {
    case CONDITIONS_TYPES.MULTI_SELECT:
      if (!value) return null;
      options = mapConditionsOptions({ id: condition.id, options: conditionOptions });
      selectedOptions = options.filter(item => value.includes(item.value));
      return selectedOptions.length ? selectedOptions.map(item => item.title).join(', ') : null;
    default:
      return value;
  }
};

export const mapFilter = ({ filter, conditions, conditionsOptions }) => {
  if (!filter || !filter.params) return filter;
  const params = filter.params.map(param => ({
    ...param,
    value: param.value,
    label: getParamLabel({
      value: param.value,
      condition: conditions.find(condition => condition.id === param.id),
      conditionOptions: conditionsOptions[param.id],
    }),
  }));
  return { ...filter, params };
};

export const prepareList = ({ list, settings, filter, conditionsOptions }) => {
  const { sorting } = settings;
  const { id, direction } = sorting;

  let resultList = list.map(item => item);

  if (filter) {
    const { params } = filter;
    params.forEach(param => {
      let values;
      switch (param.id) {
        case CONDITION_ID.EMPLOYEE_PROCESSING_RESULT_STATUS:
          values = conditionsOptions[CONDITION_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]
            .filter(item => param.value.includes(item.id))
            .map(item => item.serverValue);
          resultList = resultList.filter(item => values.includes(item[param.id]));
          break;
        default:
          resultList = resultList.filter(
            item => item[param.id] && item[param.id].search(new RegExp(escapeRegExp(param.value), 'ig')) >= 0,
          );
      }
    });
  }

  resultList = resultList.sort((a, b) => {
    switch (id) {
      case COLUMN_ID.NPP:
        if (typeof a[id] !== 'number') return -direction;
        if (typeof b[id] !== 'number') return direction;
        return (a[id] - b[id]) * direction;
      default:
        if (!a[id]) return -direction;
        if (!b[id]) return direction;
        return a[id].localeCompare(b[id]) * direction;
    }
  });

  return resultList;
};
