import { createSelector } from 'reselect';

import { DOC_STATUS_ID, STATE_HISTORY_COLUMNS, SIGNATURES_COLUMNS, SIGNATURES_OPERATIONS } from 'common/constants';
import localization from 'localization';
import { REDUCER_KEY as ROOT_REDUCER_KEY } from 'store/constants';
import {
  getAllowedOperations,
  getDictionaryValue,
  getDocFormErrors,
  getFormattedDate,
  getFormattedFromBankInfoValue,
  getSignatureType,
  getStatus,
  mapDataToStructure,
  mapDocErrorsToSections,
  mapObject,
} from 'utils';

import { REDUCER_KEY, TAB_ID, SECTION_ID, FIELD_ID, OPERATION_ID, LOCALE_KEY } from './constants';
import { STRUCTURE, ERRORS, LABELS, FROM_BANK_INFO } from './config';
import { mapDictionaryItems } from './utils';

const localeSelector = state => state[ROOT_REDUCER_KEY].locale;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isErrorSelector = state => state[REDUCER_KEY].isError;
const serverErrorSelector = state => state[REDUCER_KEY].serverError;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const isDisabledSelector = state => state[REDUCER_KEY].isDisabled;
const isEditableSelector = state => state[REDUCER_KEY].isEditable;
const isTemplateSelector = state => state[REDUCER_KEY].isTemplate;
const operationsSelector = state => state[REDUCER_KEY].operations;
const activeFieldSelector = state => state[REDUCER_KEY].activeField;
const activeErrorSelector = state => state[REDUCER_KEY].activeError;
const documentHistorySelector = state => state[REDUCER_KEY].documentHistory;
const documentSignaturesSelector = state => state[REDUCER_KEY].documentSignatures;
const documentStructureSelector = state => state[REDUCER_KEY].documentStructure;
const documentDataSelector = state => state[REDUCER_KEY].documentData;
const documentDictionariesSelector = state => state[REDUCER_KEY].documentDictionaries;
const documentErrorsSelector = state => state[REDUCER_KEY].documentErrors;
const templatesSelector = state => state[REDUCER_KEY].templates;
const viewSelector = state => state[REDUCER_KEY].view;

const labelsCreatedSelector = createSelector(
  documentDataSelector,
  documentData => ({
    LOCALE: localization.getLocale(),
    ...localization.translate(LABELS, {
      registryOnAttachmentsTitleNumber: documentData[FIELD_ID.NUMBER],
      fromDateValue: getFormattedDate(documentData[FIELD_ID.DATE]),
      templateTitleValue: documentData[FIELD_ID.TEMPLATE_NAME],
    }),
  }),
);

const isErrorCreatedSelector = createSelector(
  isMountedSelector,
  isErrorSelector,
  (isMounted, isError) => isMounted && isError,
);

const serverErrorCreatedSelector = createSelector(
  serverErrorSelector,
  serverError => {
    if (!serverError) return null;
    return {
      status: serverError.status,
      statusText: serverError.statusText,
      url: serverError.config && serverError.config.url,
      response: serverError.data && serverError.data.error,
    };
  },
);

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isLoadingSelector,
  (isMounted, isLoading) => !isMounted || isLoading,
);

const isWideViewCreatedSelector = createSelector(
  documentStructureSelector,
  documentStructure => documentStructure.find(item => item.isActive).id === TAB_ID.EMPLOYEES,
);

const isShowExtraCreatedSelector = createSelector(
  documentStructureSelector,
  documentErrorsSelector,
  (documentStructure, documentErrors) =>
    documentStructure.find(item => item.isActive).id !== TAB_ID.EMPLOYEES || !!documentErrors.length,
);

const statusCreatedSelector = createSelector(
  documentDataSelector,
  documentData => ({
    title: getStatus.titleById(documentData[FIELD_ID.STATUS]),
    isSuccess: getStatus.isSuccess(documentData[FIELD_ID.STATUS]),
    isWarning: getStatus.isWarning(documentData[FIELD_ID.STATUS]),
    isError: getStatus.isError(documentData[FIELD_ID.STATUS]),
  }),
);

const fromBankInfoCreatedSelector = createSelector(
  documentDataSelector,
  documentData => {
    const fromBankInfo = FROM_BANK_INFO.map(item => ({
      id: item.id,
      label: localization.translate(item.labelKey),
      value: getFormattedFromBankInfoValue(item.type, documentData[item.id]),
    }));
    return fromBankInfo.filter(item => item.value).length ? fromBankInfo : [];
  },
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  isTemplateSelector,
  documentDataSelector,
  (operations, isTemplate, documentData) =>
    getAllowedOperations({
      operations,
      status: documentData[FIELD_ID.STATUS],
      allowed: documentData[FIELD_ID.ALLOWED_SM_ACTIONS] || {},
      isTemplate,
    })
      .filter(operation => {
        if (operation.id === OPERATION_ID.REPEAT_WITH_REFUSALS) return documentData[FIELD_ID.COPY_REJECTED_ENABLED];
        return true;
      })
      .map(operation => ({
        ...operation,
        title: operation.noTitle ? null : localization.translate(operation.titleKey),
      })),
);

const documentHistoryCreatedSelector = createSelector(
  documentHistorySelector,
  documentHistory => ({
    isVisible: documentHistory.isVisible,
    columns: STATE_HISTORY_COLUMNS.map(column => ({
      ...column,
      label: localization.translate(column.labelKey),
    })),
    items: documentHistory.items.reverse().map((item, index) => ({
      id: `${item.date}_${item.endState}`,
      date: getFormattedDate.withTime(item.date),
      user: item.user,
      status: {
        title: getStatus.titleById(item.endState),
        isSuccess: getStatus.isSuccess(item.endState),
        isWarning: getStatus.isWarning(item.endState),
        isError: getStatus.isError(item.endState),
      },
      isLastState: !index,
    })),
  }),
);

const documentSignaturesCreatedSelector = createSelector(
  documentSignaturesSelector,
  documentSignatures => ({
    isVisible: documentSignatures.isVisible,
    columns: SIGNATURES_COLUMNS.map(column => ({
      ...column,
      label: localization.translate(column.labelKey),
    })),
    operations: SIGNATURES_OPERATIONS.map(operation => ({
      ...operation,
      title: localization.translate(operation.titleKey),
    })),
    items: documentSignatures.items.map(item => ({
      id: item.signId,
      signDateTime: getFormattedDate.withTime(item.signDateTime),
      userLogin: item.userLogin,
      userName: item.userName,
      userPosition: item.userPosition,
      signType: getSignatureType.titleById(item.signType),
      signValid: {
        title: localization.translate(LOCALE_KEY.OTHER.SIGNATURE_STATUS, { isInvalid: !item.signValid }),
        isError: !item.signValid,
      },
      signHash: item.signHash,
    })),
  }),
);

const documentStructureCreatedSelector = createSelector(
  activeFieldSelector,
  activeErrorSelector,
  documentStructureSelector,
  documentErrorsSelector,
  isTemplateSelector,
  (activeField, activeError, documentStructure, documentErrors, isTemplate) =>
    documentStructure
      .filter(item => (item.id === TAB_ID.FROM_BANK_INFO ? !isTemplate : true))
      .map(item => ({
        id: item.id,
        title: localization.translate(item.titleKey),
        isActive: item.isActive,
        sections: item.sections
          ? mapDocErrorsToSections(
              item.sections.map(section => ({
                ...section,
                title: section.titleKey && localization.translate(section.titleKey),
                isVisible: section.id === SECTION_ID.TEMPLATE_NAME ? isTemplate : true,
              })),
              documentErrors,
              ERRORS,
              activeField,
              activeError,
            )
          : null,
      })),
);

const documentTabsCreatedSelector = createSelector(
  localeSelector,
  documentStructureSelector,
  isTemplateSelector,
  (locale, documentStructure, isTemplate) => ({
    items: documentStructure
      .filter(item => (item.id === TAB_ID.FROM_BANK_INFO ? !isTemplate : true))
      .map(item => ({
        id: item.id,
        title: localization.translate(item.titleKey),
        isActive: item.isActive,
      })),
    active: documentStructure.find(item => item.isActive).id,
  }),
);

const documentDataCreatedSelector = createSelector(
  isEditableSelector,
  documentDataSelector,
  documentDictionariesSelector,
  (isEditable, documentData, documentDictionaries) =>
    mapDataToStructure(STRUCTURE, {
      [FIELD_ID.TEMPLATE_NAME]: documentData[FIELD_ID.TEMPLATE_NAME],
      [FIELD_ID.NUMBER]: documentData[FIELD_ID.NUMBER],
      [FIELD_ID.DATE]: isEditable ? documentData[FIELD_ID.DATE] : getFormattedDate(documentData[FIELD_ID.DATE]),
      [FIELD_ID.CUSTOMER_NAME]: documentData[FIELD_ID.CUSTOMER_NAME],
      [FIELD_ID.CUSTOMER_INN]: documentData[FIELD_ID.CUSTOMER_INN],
      [FIELD_ID.CUSTOMER_OGRN]: documentData[FIELD_ID.CUSTOMER_OGRN],
      [FIELD_ID.CARD_BRANCH_CITY]: isEditable
        ? getDictionaryValue(
            documentData[FIELD_ID.CARD_BRANCH_CITY],
            documentDictionaries[FIELD_ID.CARD_BRANCH_CITY],
            'name',
            'id',
          )
        : documentData[FIELD_ID.CARD_BRANCH_CITY],
      [FIELD_ID.CARD_BRANCH_NAME]: isEditable
        ? documentData[FIELD_ID.CARD_BRANCH_ID]
        : documentData[FIELD_ID.CARD_BRANCH_NAME],
      [FIELD_ID.CARD_BRANCH_ADDRESS]: documentData[FIELD_ID.CARD_BRANCH_ADDRESS],

      [FIELD_ID.TARIFF_PLAN_NAME]: isEditable
        ? documentData[FIELD_ID.TARIFF_PLAN_NUMBER]
        : documentData[FIELD_ID.TARIFF_PLAN_NAME],
      [FIELD_ID.LOAD_SCHEME_NAME]: isEditable
        ? documentData[FIELD_ID.LOAD_SCHEME_NUMBER]
        : documentData[FIELD_ID.LOAD_SCHEME_NAME],

      [FIELD_ID.OFFICIAL_NAME]: documentData[FIELD_ID.OFFICIAL_NAME],

      [FIELD_ID.OFFICIAL_PHONE]: documentData[FIELD_ID.OFFICIAL_PHONE],

      [FIELD_ID.ATTACH_CONTRACT_NUMBER]: documentData[FIELD_ID.ATTACH_CONTRACT_NUMBER],

      [FIELD_ID.ATTACH_CONTRACT_DATE]: isEditable
        ? documentData[FIELD_ID.ATTACH_CONTRACT_DATE]
        : getFormattedDate(documentData[FIELD_ID.ATTACH_CONTRACT_DATE]),

      [FIELD_ID.IS_CHECK]: documentData[FIELD_ID.IS_CHECK],

      [FIELD_ID.CONFIRM_ACCEPTANCE]: documentData[FIELD_ID.CONFIRM_ACCEPTANCE],

      [FIELD_ID.NOTE]: documentData[FIELD_ID.NOTE],
    }),
);

const documentDictionariesCreatedSelector = createSelector(
  documentDictionariesSelector,
  documentDataSelector,
  (documentDictionaries, documentData) =>
    mapDataToStructure(
      STRUCTURE,
      mapObject((item, key) => mapDictionaryItems(item, key, documentData), documentDictionaries),
    ),
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['1'])),
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['2', '3'])),
);

const formDisabledCreatedSelector = createSelector(
  isDisabledSelector,
  isTemplateSelector,
  documentDataSelector,
  documentDictionariesSelector,
  (isDisabled, isTemplate, documentData, documentDictionaries) =>
    mapDataToStructure(STRUCTURE, {
      [FIELD_ID.TEMPLATE_NAME]: isDisabled,
      [FIELD_ID.NUMBER]: isDisabled || isTemplate,
      [FIELD_ID.DATE]: isDisabled || isTemplate,
      [FIELD_ID.CARD_BRANCH_CITY]: isDisabled,
      [FIELD_ID.CARD_BRANCH_NAME]: isDisabled,
      [FIELD_ID.TARIFF_PLAN_NAME]:
        isDisabled ||
        (documentData[FIELD_ID.TARIFF_PLAN_NAME] && documentDictionaries[FIELD_ID.TARIFF_PLAN_NAME].items.length === 1),
      [FIELD_ID.LOAD_SCHEME_NAME]:
        isDisabled ||
        !documentData[FIELD_ID.TARIFF_PLAN_NAME] ||
        (documentData[FIELD_ID.LOAD_SCHEME_NAME] && documentDictionaries[FIELD_ID.LOAD_SCHEME_NAME].items.length === 1),
      [FIELD_ID.OFFICIAL_NAME]: isDisabled,
      [FIELD_ID.OFFICIAL_PHONE]: isDisabled,
      [FIELD_ID.SAVE_RESPONSIBLE_PERSON_BUTTON]:
        isDisabled || !documentData[FIELD_ID.OFFICIAL_NAME] || !documentData[FIELD_ID.OFFICIAL_PHONE],
      [FIELD_ID.ATTACH_CONTRACT_NUMBER]: isDisabled,
      [FIELD_ID.ATTACH_CONTRACT_DATE]: isDisabled,
      [FIELD_ID.IS_CHECK]: isDisabled,
      [FIELD_ID.CONFIRM_ACCEPTANCE]: isDisabled || documentData[FIELD_ID.CONFIRM_ACCEPTANCE],
      [FIELD_ID.ADD_EMPLOYEES_BUTTON]: isDisabled,
      [FIELD_ID.NOTE]: isDisabled,
    }),
);

const templatesCreatedSelector = createSelector(
  templatesSelector,
  documentDataSelector,
  (templates, documentData) =>
    mapDictionaryItems(
      { ...templates, isVisible: documentData[FIELD_ID.STATUS] === DOC_STATUS_ID.NEW && templates.items.length },
      'templates',
    ),
);

const totalEmployeesCountCreatedSelector = createSelector(
  documentDataSelector,
  documentData => documentData[FIELD_ID.EMPLOYEES_INFO] && documentData[FIELD_ID.EMPLOYEES_INFO].length,
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isError: isErrorCreatedSelector(state),
  serverError: serverErrorCreatedSelector(state),
  isLoading: isLoadingCreatedSelector(state),
  isTemplate: isTemplateSelector(state),
  isEditable: isEditableSelector(state),
  isWideView: isWideViewCreatedSelector(state),
  isShowExtra: isShowExtraCreatedSelector(state),
  status: statusCreatedSelector(state),
  fromBankInfo: fromBankInfoCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  documentHistory: documentHistoryCreatedSelector(state),
  documentSignatures: documentSignaturesCreatedSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentTabs: documentTabsCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  formDisabled: formDisabledCreatedSelector(state),
  templates: templatesCreatedSelector(state),
  view: viewSelector(state),
  totalEmployeesCount: totalEmployeesCountCreatedSelector(state),
});

export default mapStateToProps;
