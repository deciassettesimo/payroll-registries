import { connect } from 'react-redux';
import ExportModal from './ExportModal';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default connect(
  mapStateToProps,
  actions,
)(ExportModal);
