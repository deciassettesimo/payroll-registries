import { LOCALE_KEY, FIELD_ID } from './constants';

export const LABELS = {
  HEADER: LOCALE_KEY.HEADER,
  FIELDS: LOCALE_KEY.FIELDS,
  OTHER: LOCALE_KEY.OTHER,
};

export const TYPES = {
  EXPORT_TO_1C: '1c',
  EXPORT_TO_CSV: 'csv',
};

export const VERSIONS_1C = [
  {
    value: '2.5',
    title: '2.5',
  },
  {
    value: '3.5',
    title: '3.5',
  },
];

export const ENCODINGS = [
  {
    value: 'windows',
    title: 'Windows',
  },
  {
    value: 'utf-8',
    title: 'UTF-8',
  },
  {
    value: 'dos',
    title: 'DOS',
  },
];

export const SETTINGS = {
  [FIELD_ID.TYPE]: TYPES.EXPORT_TO_1C,
  [FIELD_ID.MASK_1C]: 'attach_to_1c_xxxx.xml',
  [FIELD_ID.MASK_CSV]: 'attach_to_csv_xxxx.csv',
  [FIELD_ID.ENCODING]: ENCODINGS[0].value,
  [FIELD_ID.SEPARATOR]: ';',
  [FIELD_ID.VERSION_1C]: VERSIONS_1C[0].value,
  [FIELD_ID.WITHOUT_NAMESPACES]: false,
};
