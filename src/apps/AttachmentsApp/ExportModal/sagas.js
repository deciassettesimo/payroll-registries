import { call, put, select, takeEvery } from 'redux-saga/effects';

import { export1C as dalExport1C, exportCSV as dalExportCSV } from 'dal/payroll-registries-attachments';
import localization from 'localization';
import { notificationError, notificationSuccess } from 'platform';
import dispatchErrorAction from 'utils/dispatchErrorAction';

import { REDUCER_KEY as APP_REDUCER_KEY, ACTIONS as APP_ACTIONS } from '../constants';

import { SETTINGS, TYPES } from './config';
import { REDUCER_KEY, FIELD_ID, ACTIONS, LOCALE_KEY } from './constants';

const appSettingsSelector = state => state[APP_REDUCER_KEY].settings[REDUCER_KEY];
const organizationsSelector = state => state[REDUCER_KEY].organizations;
const settingsSelector = state => state[REDUCER_KEY].settings;
const valuesSelector = state => state[REDUCER_KEY].values;

function* mountSaga() {
  try {
    const settings = yield select(appSettingsSelector);
    yield put({ type: ACTIONS.MOUNT_SUCCESS, payload: { settings: settings || SETTINGS } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsSaga() {
  const settings = yield select(settingsSelector);
  yield put({ type: APP_ACTIONS.SAVE_SETTINGS_REQUEST, payload: { key: REDUCER_KEY, data: settings } });
}

function* blurSaga({ payload }) {
  const { id, value } = payload;
  const settings = yield select(settingsSelector);
  switch (id) {
    case FIELD_ID.MASK_1C:
    case FIELD_ID.MASK_CSV:
      yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings: { ...settings, [id]: value } } });
      break;
    default:
      break;
  }
}

function* changeSaga({ payload }) {
  const { id, value } = payload;
  const settings = yield select(settingsSelector);
  switch (id) {
    case FIELD_ID.TYPE:
    case FIELD_ID.ENCODING:
    case FIELD_ID.SEPARATOR:
    case FIELD_ID.WITHOUT_NAMESPACES:
      yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings: { ...settings, [id]: value } } });
      break;
    case FIELD_ID.VERSION_1C:
      yield put({
        type: ACTIONS.SAVE_SETTINGS,
        payload: { settings: { ...settings, [id]: value, [FIELD_ID.WITHOUT_NAMESPACES]: false } },
      });
      break;
    default:
      break;
  }
}

function* exportRequestSaga() {
  try {
    const organizations = yield select(organizationsSelector);
    const settings = yield select(settingsSelector);
    const values = yield select(valuesSelector);
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_1C) {
      yield call(dalExport1C, {
        orgIds: organizations,
        fromDate: values[FIELD_ID.PERIOD_FROM],
        toDate: values[FIELD_ID.PERIOD_TO],
        version: settings[FIELD_ID.VERSION_1C],
        exportFileMask: values[FIELD_ID.MASK_1C],
        encoding: settings[FIELD_ID.ENCODING],
        exportWithoutNameSpace: settings[FIELD_ID.WITHOUT_NAMESPACES],
      });
    } else if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_CSV) {
      yield call(dalExportCSV, {
        orgIds: organizations,
        fromDate: values[FIELD_ID.PERIOD_FROM],
        toDate: values[FIELD_ID.PERIOD_TO],
        exportFileMask: values[FIELD_ID.MASK_CSV],
        encoding: settings[FIELD_ID.ENCODING],
        separator: settings[FIELD_ID.SEPARATOR],
      });
    }
    yield put({ type: ACTIONS.EXPORT_SUCCESS });
    yield call(notificationSuccess, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SUCCESS.TITLE),
    });
    yield put({ type: ACTIONS.CLOSE });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.EXPORT_FAIL] });
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.FAIL.TITLE),
      message: error.message || '',
    });
  }
}

export default function* sagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS, saveSettingsSaga);
  yield takeEvery(ACTIONS.CHANGE, changeSaga);
  yield takeEvery(ACTIONS.BLUR, blurSaga);
  yield takeEvery(ACTIONS.EXPORT_REQUEST, exportRequestSaga);
}
