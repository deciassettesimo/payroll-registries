import { createSelector } from 'reselect';

import localization from 'localization';

import { LABELS, TYPES } from './config';
import { REDUCER_KEY, FIELD_ID } from './constants';
import { checkMask } from './utils';

const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const encodingsSelector = state => state[REDUCER_KEY].encodings;
const versions1CSelector = state => state[REDUCER_KEY].versions1C;
const valuesSelector = state => state[REDUCER_KEY].values;
const settingsSelector = state => state[REDUCER_KEY].settings;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isVisibleCreatedSelector = createSelector(
  isMountedSelector,
  isVisibleSelector,
  (isMounted, isVisible) => isMounted && isVisible,
);

const valuesCreatedSelector = createSelector(
  valuesSelector,
  settingsSelector,
  (values, settings) => ({
    [FIELD_ID.TYPE]: settings[FIELD_ID.TYPE],
    [FIELD_ID.PERIOD_FROM]: values[FIELD_ID.PERIOD_FROM],
    [FIELD_ID.PERIOD_TO]: values[FIELD_ID.PERIOD_TO],
    [FIELD_ID.MASK_1C]: values[FIELD_ID.MASK_1C],
    [FIELD_ID.MASK_CSV]: values[FIELD_ID.MASK_CSV],
    [FIELD_ID.ENCODING]: settings[FIELD_ID.ENCODING],
    [FIELD_ID.VERSION_1C]: settings[FIELD_ID.VERSION_1C],
    [FIELD_ID.SEPARATOR]: settings[FIELD_ID.SEPARATOR],
    [FIELD_ID.WITHOUT_NAMESPACES]: settings[FIELD_ID.WITHOUT_NAMESPACES],
  }),
);

const optionsCreatedSelector = createSelector(
  encodingsSelector,
  versions1CSelector,
  (encodings, versions1C) => ({
    [FIELD_ID.ENCODING]: encodings,
    [FIELD_ID.VERSION_1C]: versions1C,
  }),
);

const isDisabledCreatedSelector = createSelector(
  valuesSelector,
  settingsSelector,
  (values, settings) => {
    if (!values[FIELD_ID.PERIOD_FROM] || !values[FIELD_ID.PERIOD_TO]) return true;
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_1C && !checkMask(values[FIELD_ID.MASK_1C])) return true;
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_CSV && !checkMask(values[FIELD_ID.MASK_CSV])) return true;
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_CSV && !settings[FIELD_ID.SEPARATOR]) return true;
    return false;
  },
);

const isMaskErrorCreatedSelector = createSelector(
  valuesSelector,
  settingsSelector,
  (values, settings) => {
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_1C && !checkMask(values[FIELD_ID.MASK_1C])) return true;
    if (settings[FIELD_ID.TYPE] === TYPES.EXPORT_TO_CSV && !checkMask(values[FIELD_ID.MASK_CSV])) return true;
    return false;
  },
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isVisible: isVisibleCreatedSelector(state),
  values: valuesCreatedSelector(state),
  options: optionsCreatedSelector(state),
  isLoading: isLoadingSelector(state),
  isDisabled: isDisabledCreatedSelector(state),
  isMaskError: isMaskErrorCreatedSelector(state),
});

export default mapStateToProps;
