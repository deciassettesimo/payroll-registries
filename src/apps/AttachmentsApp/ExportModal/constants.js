import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesAttachmentsExportModal';

export const LOCALE_KEY = {
  HEADER: 'loc.attachmentRegistryExport',
  FIELDS: {
    TYPE: {
      EXPORT_TO_1C: 'loc.exportTo1C',
      EXPORT_TO_CSV: 'loc.exportToCSV',
    },
    PERIOD: {
      MAIN: 'loc.period',
      FROM: 'loc.rangeFrom',
      TO: 'loc.rangeTo',
    },
    MASK: 'loc.fileNameMask',
    VERSION_1C: 'loc.versionOf1C',
    ENCODING: 'loc.fileEncoding',
    SEPARATOR: 'loc.separator',
    WITHOUT_NAMESPACES: 'loc.export1C25WithoutNamespaces',
    SUBMIT_BUTTON: 'loc.export',
    CANCEL_BUTTON: 'loc.close',
  },
  NOTIFICATIONS: {
    SUCCESS: {
      TITLE: 'loc.exportCompleted',
    },
    FAIL: {
      TITLE: 'loc.unableExport',
    },
  },
  OTHER: {
    MASK_ERROR: 'loc.exportMaskError',
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,
    SAVE_SETTINGS: null,

    OPEN: null,
    CLOSE: null,
    CHANGE: null,
    BLUR: null,
    EXPORT_REQUEST: null,
    EXPORT_SUCCESS: null,
    EXPORT_FAIL: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_EXPORT_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    TYPE: null,
    PERIOD_FROM: null,
    PERIOD_TO: null,
    MASK_1C: null,
    MASK_CSV: null,
    ENCODING: null,
    VERSION_1C: null,
    SEPARATOR: null,
    WITHOUT_NAMESPACES: null,
    SUBMIT_BUTTON: null,
    CANCEL_BUTTON: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_EXPORT_MODAL_',
);
