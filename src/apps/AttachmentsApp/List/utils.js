import { CONDITIONS_TYPES, CONDITIONS_RANGE_TYPES } from '@rbo/rbo-components/lib/Filter/_constants';

import localization from 'localization';
import { getFormattedDate, getFormattedAmount, escapeRegExp } from 'utils';

import { CONDITION_ID } from './constants';

export const mapConditionsOptions = ({ id, options, searchValue }) => {
  let searchRegExp;
  switch (id) {
    case CONDITION_ID.STATUS:
      return options.map(item => ({
        value: item.id,
        title: localization.translate(item.titleKey),
      }));
    case CONDITION_ID.CARD_BRANCH_NAME:
      searchRegExp = new RegExp(escapeRegExp(searchValue), 'ig');
      return options
        .map(item => ({
          value: item.id,
          title: item.name,
          extra: item.bic,
          describe: item.address,
        }))
        .filter(item => item.title.search(searchRegExp) >= 0)
        .sort((a, b) => a.title.localeCompare(b.title));
    case CONDITION_ID.CUSTOMER_ID:
      searchRegExp = new RegExp(escapeRegExp(searchValue), 'ig');
      return options
        .map(item => ({
          value: item.id,
          title: item.shortName,
          extra: item.inn,
          describe: item.ogrn,
        }))
        .filter(item => item.title.search(searchRegExp) >= 0)
        .sort((a, b) => a.title.localeCompare(b.title));
    default:
      return options;
  }
};

const getParamValue = ({ value, condition }) => {
  if (condition.type === CONDITIONS_TYPES.RANGE && condition.rangeType === CONDITIONS_RANGE_TYPES.INTEGER) {
    if (!value) return null;
    return {
      from: !!value && typeof value.from === 'string' && value.from ? parseInt(value.from, 10) : value.from,
      to: !!value && typeof value.to === 'string' && value.to ? parseInt(value.to, 10) : value.to,
    };
  }
  if (condition.type === CONDITIONS_TYPES.RANGE && condition.rangeType === CONDITIONS_RANGE_TYPES.AMOUNT) {
    if (!value) return null;
    return {
      from: typeof value.from === 'string' && value.from ? parseFloat(value.from) : value.from,
      to: typeof value.to === 'string' && value.to ? parseFloat(value.to) : value.to,
    };
  }
  return value;
};

const getParamLabel = ({ value, condition, conditionOptions }) => {
  if (!value) return '...';
  let options;
  let selectedOptions;
  let from;
  let to;
  switch (condition.type) {
    case CONDITIONS_TYPES.MULTI_SELECT:
      if (!value) return null;
      options = mapConditionsOptions({ id: condition.id, options: conditionOptions });
      selectedOptions = options.filter(item => value.includes(item.value));
      return selectedOptions.length ? selectedOptions.map(item => item.title).join(', ') : null;
    case CONDITIONS_TYPES.RANGE:
      switch (condition.rangeType) {
        case CONDITIONS_RANGE_TYPES.DATE:
          from = getFormattedDate(value.from);
          to = getFormattedDate(value.to);
          return `${from ? `${localization.translate('loc.rangeFrom')} ${from}` : ''} ${
            to ? `${localization.translate('loc.rangeTo')} ${to}` : ''
          }`;
        case CONDITIONS_RANGE_TYPES.AMOUNT:
          from = getFormattedAmount(value.from);
          to = getFormattedAmount(value.to);
          return `${from ? `${localization.translate('loc.amountRangeFrom')} ${from}` : ''} ${
            to ? `${localization.translate('loc.amountRangeTo')} ${to}` : ''
          }`;
        case CONDITIONS_RANGE_TYPES.INTEGER:
          from = typeof value.from === 'number' ? value.from.toString() : value.from;
          to = typeof value.to === 'number' ? value.to.toString() : value.to;
          return `${from ? `${localization.translate('loc.rangeFrom')} ${from}` : ''} ${
            to ? `${localization.translate('loc.rangeTo')} ${to}` : ''
          }`;
        default:
          return `${value.from} ${value.to}`;
      }
    case CONDITIONS_TYPES.SEARCH:
      if (!value) return null;
      options = mapConditionsOptions({ id: condition.id, options: conditionOptions });
      switch (condition.id) {
        case CONDITION_ID.CARD_BRANCH_NAME:
        case CONDITION_ID.CUSTOMER_ID:
          selectedOptions = options.find(item => value === item.value);
          return selectedOptions ? selectedOptions.title : null;
        default:
          return value;
      }
    default:
      return value;
  }
};

export const mapFilter = ({ filter, conditions, conditionsOptions }) => {
  if (!filter || !filter.params) return filter;
  const params = filter.params.map(param => ({
    ...param,
    value: getParamValue({ value: param.value, condition: conditions.find(condition => condition.id === param.id) }),
    label: getParamLabel({
      value: param.value,
      condition: conditions.find(condition => condition.id === param.id),
      conditionOptions: conditionsOptions[param.id],
    }),
  }));
  return { ...filter, params };
};

export const prepareParams = ({ sections, settings, pages, conditionsOptions, filter: activeFilter, isExport }) => {
  const section = sections.find(item => item.isActive).api;
  const { sorting, visibility, pagination } = settings;

  const sort = {
    column: sorting ? sorting.id : null,
    desc: sorting ? sorting.direction === -1 : null,
  };

  const filter = {};
  if (activeFilter && activeFilter.params) {
    activeFilter.params.forEach(param => {
      let selectedOption;
      switch (param.id) {
        case CONDITION_ID.CARD_BRANCH_NAME:
          selectedOption = param.value
            ? conditionsOptions[CONDITION_ID.CARD_BRANCH_NAME].find(item => item.id === param.value)
            : null;
          filter[param.id] = selectedOption ? selectedOption.name : null;
          break;
        case CONDITION_ID.CUSTOMER_ID:
          filter[param.id] = param.value ? [param.value] : null;
          break;
        default:
          filter[param.id] = param.value;
          break;
      }
    });
  }
  if (!filter[CONDITION_ID.CUSTOMER_ID]) {
    filter[CONDITION_ID.CUSTOMER_ID] = conditionsOptions[CONDITION_ID.CUSTOMER_ID].map(item => item.id);
  }

  const params = { section, sort, filter };

  if (!isExport) {
    params.paging = {
      offset: (pages.selected - 1) * pagination,
      limit: pagination + 1,
    };
  } else {
    params.filter.visibleColumns = visibility;
  }

  return params;
};
