import { createSelector } from 'reselect';
import { v4 } from 'uuid';
import { CONDITIONS_TYPES } from '@rbo/rbo-components/lib/Filter/_constants';

import { PERMISSIONS_IDS } from 'common/constants';
import localization from 'localization';
import { getAllowedOperations, getFormattedDate, getStatus } from 'utils';
import { REDUCER_KEY as ROOT_REDUCER_KEY } from 'store/constants';

import { REDUCER_KEY as APP_REDUCER_KEY } from '../constants';

import { REDUCER_KEY, COLUMN_ID, FIELD_ID, OPERATION_ID, LOCALE_KEY } from './constants';
import { PAGINATION_OPTIONS, LABELS } from './config';
import { mapConditionsOptions, mapFilter } from './utils';

const localeSelector = state => state[ROOT_REDUCER_KEY].locale;
const appPermissionsSelector = state => state[APP_REDUCER_KEY].permissions;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isErrorSelector = state => state[REDUCER_KEY].isError;
const serverErrorSelector = state => state[REDUCER_KEY].serverError;
const isListLoadingSelector = state => state[REDUCER_KEY].isListLoading;
const settingsSelector = state => state[REDUCER_KEY].settings;
const sectionsSelector = state => state[REDUCER_KEY].sections;
const pagesSelector = state => state[REDUCER_KEY].pages;
const conditionsSelector = state => state[REDUCER_KEY].conditions;
const conditionsOptionsSelector = state => state[REDUCER_KEY].conditionsOptions;
const conditionsSearchValuesSelector = state => state[REDUCER_KEY].conditionsSearchValues;
const filtersSelector = state => state[REDUCER_KEY].filters;
const filterSelector = state => state[REDUCER_KEY].filter;
const columnsSelector = state => state[REDUCER_KEY].columns;
const listSelector = state => state[REDUCER_KEY].list;
const selectedSelector = state => state[REDUCER_KEY].selected;
const operationsSelector = state => state[REDUCER_KEY].operations;
const isExportToExcelLoadingSelector = state => state[REDUCER_KEY].isExportToExcelLoading;
const templatesSelector = state => state[REDUCER_KEY].templates;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isMounted => !isMounted,
);

const isErrorCreatedSelector = createSelector(
  isMountedSelector,
  isErrorSelector,
  (isMounted, isError) => isMounted && isError,
);

const serverErrorCreatedSelector = createSelector(
  serverErrorSelector,
  serverError => {
    if (!serverError) return null;
    return {
      status: serverError.status,
      statusText: serverError.statusText,
      url: serverError.config && serverError.config.url,
      response: serverError.data && serverError.data.error,
    };
  },
);

const isListGroupedCreatedSelector = createSelector(
  settingsSelector,
  settings => !!settings.grouped,
);

const sectionsCreateSelector = createSelector(
  sectionsSelector,
  sections => sections.map(section => ({ ...section, title: localization.translate(section.titleKey) })),
);

const pagesCreatedSelector = createSelector(
  pagesSelector,
  settingsSelector,
  (pages, settings) => ({
    ...pages,
    settings: {
      options: PAGINATION_OPTIONS,
      value: settings.pagination,
    },
  }),
);

const conditionsCreatedSelector = createSelector(
  conditionsSelector,
  conditionsOptionsSelector,
  conditionsSearchValuesSelector,
  (conditions, conditionsOptions, conditionsSearchValues) =>
    conditions
      .map(condition => ({ ...condition, title: localization.translate(condition.titleKey) }))
      .map(condition => {
        switch (condition.type) {
          case CONDITIONS_TYPES.SELECT:
          case CONDITIONS_TYPES.MULTI_SELECT:
          case CONDITIONS_TYPES.SUGGEST:
          case CONDITIONS_TYPES.SEARCH:
            return {
              ...condition,
              options: mapConditionsOptions({
                id: condition.id,
                options: conditionsOptions[condition.id],
                searchValue: conditionsSearchValues[condition.id],
              }),
            };
          default:
            return condition;
        }
      }),
);

const filtersCreatedSelector = createSelector(
  filtersSelector,
  filters =>
    filters
      .map(item => ({
        ...item,
        title: (item.isPreset ? localization.translate(item.titleKey) : item.title) || '',
      }))
      .sort((a, b) => {
        if (a.isPreset && b.isPreset) return 0;
        if (a.isPreset && !b.isPreset) return -1;
        if (!a.isPreset && b.isPreset) return 1;
        return a.title.localeCompare(b.title);
      }),
);

const filterCreatedSelector = createSelector(
  filterSelector,
  conditionsSelector,
  conditionsOptionsSelector,
  (filter, conditions, conditionsOptions) => mapFilter({ filter, conditions, conditionsOptions }),
);

const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) =>
    columns.map(item => ({
      id: item.id,
      label: localization.translate(item.labelKey),
      sorting: settings.sorting.id === item.id ? settings.sorting.direction : 0,
      width: settings.width[item.id],
      align: item.align,
      grouping: item.grouping,
      isGrouped: settings.grouped === item.id,
      isVisible: settings.visibility.includes(item.id),
    })),
);

const getListItem = ({ item, isSelected, operations }) => ({
  id: item[FIELD_ID.ID],
  [COLUMN_ID.NUMBER]: item[FIELD_ID.NUMBER],
  [COLUMN_ID.DATE]: getFormattedDate(item[FIELD_ID.DATE]),
  [COLUMN_ID.EMPL_COUNT]: item[FIELD_ID.EMPL_COUNT],
  [COLUMN_ID.STATUS]: getStatus.titleById(item[FIELD_ID.STATUS]),
  statusFlags: {
    isSuccess: getStatus.isSuccess(item[FIELD_ID.STATUS]),
    isWarning: getStatus.isWarning(item[FIELD_ID.STATUS]),
    isError: getStatus.isError(item[FIELD_ID.STATUS]),
  },
  [COLUMN_ID.BANK_MESSAGE]: item[FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE],
  [COLUMN_ID.CARD_BRANCH_NAME]: item[FIELD_ID.CARD_BRANCH_NAME],
  [COLUMN_ID.CUSTOMER_NAME]: item[FIELD_ID.CUSTOMER_NAME],
  [COLUMN_ID.CUSTOMER_INN]: item[FIELD_ID.CUSTOMER_INN],
  [COLUMN_ID.CUSTOMER_OGRN]: item[FIELD_ID.CUSTOMER_OGRN],
  [COLUMN_ID.OFFICIAL_NAME]: item[FIELD_ID.OFFICIAL_NAME],
  [COLUMN_ID.OFFICIAL_PHONE]: item[FIELD_ID.OFFICIAL_PHONE],
  [COLUMN_ID.ATTACH_CONTRACT_NUMBER]: item[FIELD_ID.ATTACH_CONTRACT_NUMBER],
  [COLUMN_ID.ATTACH_CONTRACT_DATE]: getFormattedDate(item[FIELD_ID.ATTACH_CONTRACT_DATE]),
  [COLUMN_ID.TARIFF_PLAN_NAME]: item[FIELD_ID.TARIFF_PLAN_NAME],
  [COLUMN_ID.LOAD_SCHEME_NAME]: item[FIELD_ID.LOAD_SCHEME_NAME],
  [COLUMN_ID.IS_CHECK]: item[FIELD_ID.IS_CHECK],
  [COLUMN_ID.ACCEPT_DATE]: getFormattedDate(item[FIELD_ID.FROM_BANK_INFO_ACCEPT_DATE]),
  [COLUMN_ID.OPERATION_DATE]: getFormattedDate(item[FIELD_ID.FROM_BANK_INFO_OPERATION_DATE]),
  [COLUMN_ID.NOTE]: item[FIELD_ID.NOTE],
  operations: getAllowedOperations({
    operations,
    status: item[FIELD_ID.STATUS],
    allowed: item[FIELD_ID.ALLOWED_SM_ACTIONS],
  })
    .filter(operation => operation.id !== OPERATION_ID.EXPORT)
    .filter(operation => {
      if (operation.id === OPERATION_ID.REPEAT_WITH_REFUSALS) return item[FIELD_ID.COPY_REJECTED_ENABLED];
      return true;
    })
    .reduce((result, operation) => ({ ...result, [operation.id]: true }), {}),
  isSelected,
});

const listCreatedSelector = createSelector(
  listSelector,
  selectedSelector,
  settingsSelector,
  operationsSelector,
  (list, selected, settings, operations) => {
    if (!list.length) return list;

    const mappedList = list.map(item =>
      getListItem({
        item,
        isSelected: selected.includes(item[FIELD_ID.ID]),
        operations: operations.map(operation => ({ ...operation, title: localization.translate(operation.titleKey) })),
      }),
    );
    if (!settings.grouped) return mappedList;

    const groups = mappedList.map(item => item[settings.grouped]).filter((v, i, a) => a.indexOf(v) === i);
    return groups.map(group => ({
      id: v4(group),
      title: group,
      items: mappedList.filter(item => item[settings.grouped] === group),
    }));
  },
);

const operationsCreatedSelector = createSelector(
  localeSelector,
  operationsSelector,
  (locale, operations) =>
    operations
      .filter(operation => operation.id !== OPERATION_ID.EXPORT)
      .map(operation => ({ ...operation, title: localization.translate(operation.titleKey) })),
);

const toolbarOperationsCreatedSelector = createSelector(
  operationsSelector,
  listSelector,
  selectedSelector,
  (operations, list, selected) => {
    if (!selected.length) return [];
    const selectedList = list
      .filter(item => selected.includes(item[FIELD_ID.ID]))
      .map(item => ({
        id: item[FIELD_ID.ID],
        operations: getAllowedOperations({
          operations,
          status: item[FIELD_ID.STATUS],
          allowed: item[FIELD_ID.ALLOWED_SM_ACTIONS],
        })
          .filter(operation => {
            if (operation.id === OPERATION_ID.REPEAT_WITH_REFUSALS) return item[FIELD_ID.COPY_REJECTED_ENABLED];
            return true;
          })
          .map(operation => operation.id),
      }));
    return operations
      .filter(operation => operation.id !== OPERATION_ID.EXPORT)
      .filter(operation => selectedList.length === 1 || operation.grouped)
      .filter(
        operation => selectedList.length === selectedList.filter(item => item.operations.includes(operation.id)).length,
      )
      .map(operation => ({ ...operation, title: localization.translate(operation.titleKey) }));
  },
);

const selectedInfoCreateSelector = createSelector(
  selectedSelector,
  selected => (selected.length ? `${localization.translate(LOCALE_KEY.OTHER.SELECTED_INFO)} ${selected.length}` : null),
);

const templatesCreateSelector = createSelector(
  templatesSelector,
  templates =>
    templates.map(item => ({
      id: item[FIELD_ID.ID],
      title: item[FIELD_ID.TEMPLATE_NAME],
      describe: item[FIELD_ID.CUSTOMER_NAME],
    })),
);

const isImport1CAllowedCreatedSelector = createSelector(
  appPermissionsSelector,
  appPermissions => appPermissions[PERMISSIONS_IDS.IMPORT],
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isError: isErrorCreatedSelector(state),
  serverError: serverErrorCreatedSelector(state),
  isLoading: isLoadingCreatedSelector(state),
  isListLoading: isListLoadingSelector(state),
  isListGrouped: isListGroupedCreatedSelector(state),
  isExportToExcelLoading: isExportToExcelLoadingSelector(state),
  sections: sectionsCreateSelector(state),
  pages: pagesCreatedSelector(state),
  conditions: conditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  filter: filterCreatedSelector(state),
  columns: columnsCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  toolbarOperations: toolbarOperationsCreatedSelector(state),
  selectedInfo: selectedInfoCreateSelector(state),
  templates: templatesCreateSelector(state),
  isImport1CAllowed: isImport1CAllowedCreatedSelector(state),
});

export default mapStateToProps;
