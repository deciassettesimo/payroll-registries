import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesAttachmentsListImport1CModal';

export const TAB_ID = keyMirrorWithPrefix(
  {
    UPLOAD: null,
    SETTINGS: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_IMPORT_1C_MODAL_',
);

export const LOCALE_KEY = {
  HEADER: 'loc.importFrom1C',
  TABS: {
    UPLOAD: 'loc.upload',
    SETTINGS: 'loc.settings',
  },
  FIELDS: {
    ENCODING: 'loc.fileEncoding',
    VERSION_1C: 'loc.versionOf1C',
    NUMBER_FROM_FILE: 'loc.registryNumberFromImport',
    DATE_FROM_FILE: 'loc.registryDateFromImport',
    NPP_FROM_FILE: 'loc.orderNumberFromImport',
    DUPLICATE_CONTROLS: 'loc.controlDuplicates',
    DUPLICATE_CONTROL_NUMBER: 'loc.documentNumber',
    DUPLICATE_CONTROL_DATE: 'loc.documentDate',
    BUTTON_ROUTE_TO_DOC: 'loc.goToImportedDocument',
    BUTTON_START_NEW_IMPORT: 'loc.importNew',
    BUTTON_CLOSE: 'loc.close',
  },
  RESULTS: {
    TASK_ID: 'loc.taskId.withColon',
    START: 'loc.start.withColon',
    FINISH: 'loc.finish.verb.withColon',
    RESULT_HEADER: 'loc.fileImportResult',
    TOTAL_NUMBER: 'loc.employeesTotalNumber.withColon',
    STATUS: 'loc.result.withColon',
  },
  NOTIFICATIONS: {
    FAIL: {
      TITLE: 'loc.unableImport',
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,
    SAVE_SETTINGS: null,

    OPEN: null,
    CLOSE: null,
    CHANGE: null,
    CHANGE_TAB: null,
    IMPORT_REQUEST: null,
    IMPORT_UPLOADED: null,
    IMPORT_FAIL: null,
    IMPORT_SUCCESS: null,
    MONITORING_REQUEST: null,
    MONITORING_SUCCESS: null,
    MONITORING_FAIL: null,
    ROUTE_TO_DOC: null,
    START_NEW_IMPORT: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_IMPORT_1C_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    FILE: null,
    ENCODING: null,
    VERSION_1C: null,
    NUMBER_FROM_FILE: null,
    DATE_FROM_FILE: null,
    NPP_FROM_FILE: null,
    DUPLICATE_CONTROLS: null,
    DUPLICATE_CONTROL_NUMBER: null,
    DUPLICATE_CONTROL_DATE: null,
    BUTTON_ROUTE_TO_DOC: null,
    BUTTON_START_NEW_IMPORT: null,
    BUTTON_CLOSE: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_IMPORT_1C_MODAL_',
);

export const RESULTS = keyMirrorWithPrefix(
  {
    TASK_ID: null,
    START: null,
    FINISH: null,
    TOTAL_NUMBER: null,
    STATUS: null,
    DOC_ID: null,
    MESSAGE: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_IMPORT_1C_MODAL_',
);

export const MONITORING_INTERVAL = 500;
