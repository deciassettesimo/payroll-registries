import { LOCALE_KEY, TAB_ID, FIELD_ID } from './constants';

export const LABELS = {
  HEADER: LOCALE_KEY.HEADER,
  FIELDS: LOCALE_KEY.FIELDS,
  RESULTS: LOCALE_KEY.RESULTS,
};

export const TABS = [
  {
    id: TAB_ID.UPLOAD,
    titleKey: LOCALE_KEY.TABS.UPLOAD,
  },
  {
    id: TAB_ID.SETTINGS,
    titleKey: LOCALE_KEY.TABS.SETTINGS,
  },
];

export const ENCODINGS = [
  {
    value: 'windows',
    title: 'Windows',
  },
  {
    value: 'utf-8',
    title: 'UTF-8',
  },
  {
    value: 'dos',
    title: 'DOS',
  },
];

export const VERSIONS_1C = [
  {
    value: '2.5',
    title: '2.5',
  },
  {
    value: '3.5',
    title: '3.5',
  },
];

export const SETTINGS = {
  [FIELD_ID.ENCODING]: ENCODINGS[0].value,
  [FIELD_ID.VERSION_1C]: VERSIONS_1C[0].value,
  [FIELD_ID.NUMBER_FROM_FILE]: false,
  [FIELD_ID.DATE_FROM_FILE]: false,
  [FIELD_ID.NPP_FROM_FILE]: false,
  [FIELD_ID.DUPLICATE_CONTROLS]: false,
  [FIELD_ID.DUPLICATE_CONTROL_NUMBER]: false,
  [FIELD_ID.DUPLICATE_CONTROL_DATE]: false,
};
