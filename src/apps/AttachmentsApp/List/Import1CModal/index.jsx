import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Import1CModal from './Import1CModal';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    actions,
  ),
)(Import1CModal);
