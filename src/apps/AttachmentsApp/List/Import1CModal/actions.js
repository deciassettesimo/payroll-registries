import { ACTIONS } from './constants';

export const mountAction = () => ({
  type: ACTIONS.MOUNT_REQUEST,
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const openAction = () => ({
  type: ACTIONS.OPEN,
});

export const closeAction = () => ({
  type: ACTIONS.CLOSE,
});

export const changeTabAction = id => ({
  type: ACTIONS.CHANGE_TAB,
  payload: { id },
});

export const changeAction = (id, value) => ({
  type: ACTIONS.CHANGE,
  payload: { id, value },
});

export const importRequestAction = () => ({
  type: ACTIONS.IMPORT_REQUEST,
});

export const importSuccessAction = () => ({
  type: ACTIONS.IMPORT_SUCCESS,
});

export const importFailAction = errors => ({
  type: ACTIONS.IMPORT_FAIL,
  payload: { errors },
});

export const routeToDocAction = params => ({
  type: ACTIONS.ROUTE_TO_DOC,
  payload: { params },
});

export const startNewImportAction = () => ({
  type: ACTIONS.START_NEW_IMPORT,
});
