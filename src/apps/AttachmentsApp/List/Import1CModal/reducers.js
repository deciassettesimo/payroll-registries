import { TABS, ENCODINGS, VERSIONS_1C } from './config';
import { REDUCER_KEY, TAB_ID, ACTIONS } from './constants';

export const initialState = {
  isMounted: false,
  isVisible: false,
  isLoading: false,
  isUploaded: false,
  isFinished: false,
  tabs: TABS,
  activeTab: TAB_ID.UPLOAD,
  encodings: ENCODINGS,
  versions1C: VERSIONS_1C,
  settings: {},
  taskId: null,
  results: {},
};

const mountRequest = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, { settings }) => ({ ...state, isMounted: true, settings });

const mountFail = state => ({ ...state, isMounted: true });

const unmount = () => ({ ...initialState, isMounted: false });

const saveSettings = (state, { settings }) => ({ ...state, settings });

const changeTab = (state, { id }) => ({ ...state, activeTab: id });

const importRequest = state => ({ ...state, isLoading: true });

const importUploaded = (state, { taskId }) => ({ ...state, isLoading: false, isUploaded: true, taskId });

const importFail = state => ({
  ...state,
  isLoading: false,
  isUploaded: false,
  isFinished: false,
  taskId: null,
  results: {},
});

const importSuccess = state => ({ ...state, isLoading: false, isUploaded: false, isFinished: true });

const monitoringFail = state => ({ ...state, isLoading: false, isUploaded: false, isFinished: true });

const monitoringSuccess = (state, { results }) => ({ ...state, results });

const startNewImport = state => ({
  ...state,
  isLoading: false,
  isUploaded: false,
  isFinished: false,
  taskId: null,
  results: {},
});

function reducers(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.SAVE_SETTINGS:
      return saveSettings(state, action.payload);

    case ACTIONS.OPEN:
      return { ...state, isVisible: true };

    case ACTIONS.CLOSE:
      return { ...state, isVisible: false, isLoading: false, isUploaded: false, isFinished: false };

    case ACTIONS.CHANGE_TAB:
      return changeTab(state, action.payload);

    case ACTIONS.IMPORT_REQUEST:
      return importRequest(state);

    case ACTIONS.IMPORT_UPLOADED:
      return importUploaded(state, action.payload);

    case ACTIONS.IMPORT_FAIL:
      return importFail(state);

    case ACTIONS.IMPORT_SUCCESS:
      return importSuccess(state);

    case ACTIONS.MONITORING_SUCCESS:
      return monitoringSuccess(state, action.payload);

    case ACTIONS.MONITORING_FAIL:
      return monitoringFail(state);

    case ACTIONS.START_NEW_IMPORT:
      return startNewImport(state);

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducers };
