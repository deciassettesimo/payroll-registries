import { OPERATION_ID as COMMON_OPERATION_ID, OPERATIONS_ACTIONS_TYPES } from 'common/constants';
import {
  GET_LIST_SECTIONS as DAL_GET_LIST_SECTIONS,
  FIELD_ID as DAL_FIELD_ID,
} from 'dal/payroll-registries-attachments/constants';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesAttachmentsList';

export const SECTION_ID = {
  WORKING: 'working',
  ARCHIVE: 'archive',
  TRASH: 'trash',
};

export const SECTION_API = { ...DAL_GET_LIST_SECTIONS };

export const FIELD_ID = { ...DAL_FIELD_ID };

export const BUTTON_ID = keyMirrorWithPrefix(
  {
    REFRESH: null,
    CREATE_NEW_DOC: null,
    CREATE_NEW_DOC_FROM_TEMPLATES: null,
    IMPORT_1C: null,
    EXPORT: null,
    EXPORT_TO_EXCEL: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_',
);

export const COLUMN_ID = {
  NUMBER: FIELD_ID.NUMBER,
  DATE: FIELD_ID.DATE,
  EMPL_COUNT: FIELD_ID.EMPL_COUNT,
  STATUS: FIELD_ID.STATUS,
  BANK_MESSAGE: FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE,
  CARD_BRANCH_NAME: FIELD_ID.CARD_BRANCH_NAME,
  CUSTOMER_NAME: FIELD_ID.CUSTOMER_NAME,
  CUSTOMER_INN: FIELD_ID.CUSTOMER_INN,
  CUSTOMER_OGRN: FIELD_ID.CUSTOMER_OGRN,
  OFFICIAL_NAME: FIELD_ID.OFFICIAL_NAME,
  OFFICIAL_PHONE: FIELD_ID.OFFICIAL_PHONE,
  ATTACH_CONTRACT_NUMBER: FIELD_ID.ATTACH_CONTRACT_NUMBER,
  ATTACH_CONTRACT_DATE: FIELD_ID.ATTACH_CONTRACT_DATE,
  TARIFF_PLAN_NAME: FIELD_ID.TARIFF_PLAN_NAME,
  LOAD_SCHEME_NAME: FIELD_ID.LOAD_SCHEME_NAME,
  IS_CHECK: FIELD_ID.IS_CHECK,
  ACCEPT_DATE: FIELD_ID.FROM_BANK_INFO_ACCEPT_DATE,
  OPERATION_DATE: FIELD_ID.FROM_BANK_INFO_OPERATION_DATE,
  NOTE: FIELD_ID.NOTE,
};

export const CONDITION_ID = {
  NUMBER: FIELD_ID.NUMBER,
  DATE: FIELD_ID.DATE,
  STATUS: FIELD_ID.STATUS,
  BANK_MESSAGE: FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE,
  CARD_BRANCH_NAME: FIELD_ID.CARD_BRANCH_NAME,
  CUSTOMER_ID: FIELD_ID.CUSTOMER_ID,
  NOTE: FIELD_ID.NOTE,
};

export const SYSTEM_FILTER_ID = {
  ERRORS: `${REDUCER_KEY}SystemFilterErrors`,
  TODAY: `${REDUCER_KEY}SystemFilterToday`,
  TO_SIGN: `${REDUCER_KEY}SystemFilterToSign`,
  REJECTED: `${REDUCER_KEY}SystemFilterRejected`,
};

export const OPERATION_ID = COMMON_OPERATION_ID;

export const LOCALE_KEY = {
  HEADER: {
    TITLE: 'loc.registriesOnAttachmentsTitle',
    BUTTONS: {
      CREATE_NEW_DOC: 'loc.create',
      CREATE_NEW_DOC_FROM_TEMPLATES: 'loc.documentAccordingToTemplate.withColon',
      IMPORT_1C: 'loc.importFrom1C',
      EXPORT: 'loc.export',
    },
  },
  SECTIONS: {
    WORKING: 'loc.workingDocuments',
    ARCHIVE: 'loc.archive',
    TRASH: 'loc.deleted.adjective',
  },
  COLUMNS: {
    NUMBER: 'loc.number',
    DATE: 'loc.date',
    EMPL_COUNT: 'loc.quantityOfEmployees',
    STATUS: 'loc.status',
    BANK_MESSAGE: 'loc.bankMessage',
    CARD_BRANCH_NAME: 'loc.cardsIssuingOffice',
    CUSTOMER_NAME: 'loc.organization',
    CUSTOMER_INN: 'loc.tinOrFcc',
    CUSTOMER_OGRN: 'loc.psrn',
    OFFICIAL_NAME: 'loc.responsibleExecutorName.abbr',
    OFFICIAL_PHONE: 'loc.responsibleExecutorPhone.abbr',
    ATTACH_CONTRACT_NUMBER: 'loc.agreementNumber',
    ATTACH_CONTRACT_DATE: 'loc.agreementDate',
    TARIFF_PLAN_NAME: 'loc.tariffPlan',
    LOAD_SCHEME_NAME: 'loc.loadingScheme',
    IS_CHECK: 'loc.check',
    ACCEPT_DATE: 'loc.receivedByBank',
    OPERATION_DATE: 'loc.processingDate',
    NOTE: 'loc.notes',
  },
  CONDITIONS: {
    NUMBER: 'loc.number',
    DATE: 'loc.date',
    STATUS: 'loc.status',
    BANK_MESSAGE: 'loc.bankMessage',
    CARD_BRANCH_NAME: 'loc.cardsIssuingOffice',
    CUSTOMER_ID: 'loc.organization',
    NOTE: 'loc.notes',
  },
  SYSTEM_FILTERS: {
    ERRORS: 'loc.errors',
    TODAY: 'loc.forToday',
    TO_SIGN: 'loc.forSignature',
    REJECTED: 'loc.rejected',
  },
  OTHER: {
    EXPORT_TO_EXCEL: 'loc.unloadListToExcel',
    EXPORT_TO_EXCEL_LOADING: 'loc.loading.withDots',
    SELECTED_INFO: 'loc.selected.withColon',
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,

    SAVE_SETTINGS: null,
    GET_LIST_REQUEST: null,
    GET_LIST_SUCCESS: null,
    GET_LIST_FAIL: null,
    EXPORT_TO_EXCEL_REQUEST: null,
    EXPORT_TO_EXCEL_SUCCESS: null,
    EXPORT_TO_EXCEL_FAIL: null,
    GET_TEMPLATES_REQUEST: null,

    SECTION_CHANGE: null,
    PAGE_CHANGE: null,
    PAGINATION_CHANGE: null,
    SORTING_CHANGE: null,
    RESIZE_CHANGE: null,
    VISIBILITY_CHANGE: null,
    GROUPING_CHANGE: null,
    FILTERS_REMOVE: null,
    FILTERS_CHOOSE: null,
    FILTERS_CREATE: null,
    FILTERS_RESET: null,
    FILTERS_SAVE: null,
    FILTERS_SAVE_AS: null,
    FILTER_SEARCH: null,
    FILTER_CHANGE: null,
    SELECT_CHANGE: null,

    CREATE_NEW_DOC: null,
    ROUTE_TO_DOC: null,
    ROUTE_TO_TEMPLATE: null,

    OPERATIONS: null,
    ...OPERATIONS_ACTIONS_TYPES.ARCHIVE,
    ...OPERATIONS_ACTIONS_TYPES.BACK,
    ...OPERATIONS_ACTIONS_TYPES.CHECK_SIGN,
    ...OPERATIONS_ACTIONS_TYPES.CREATE_FROM_TEMPLATE,
    ...OPERATIONS_ACTIONS_TYPES.EXPORT,
    ...OPERATIONS_ACTIONS_TYPES.STATE_HISTORY,
    ...OPERATIONS_ACTIONS_TYPES.REMOVE,
    ...OPERATIONS_ACTIONS_TYPES.REMOVE_SIGN,
    ...OPERATIONS_ACTIONS_TYPES.REPEAT,
    ...OPERATIONS_ACTIONS_TYPES.REPEAT_WITH_REFUSALS,
    ...OPERATIONS_ACTIONS_TYPES.SAVE,
    ...OPERATIONS_ACTIONS_TYPES.SAVE_AS_TEMPLATE,
    ...OPERATIONS_ACTIONS_TYPES.SEND,
    ...OPERATIONS_ACTIONS_TYPES.SIGN,
    ...OPERATIONS_ACTIONS_TYPES.UNARCHIVE,

    IMPORT_1C_START: null,
  },
  'PAYROLL_REGISTRIES_ATTACHMENTS_LIST_',
);
