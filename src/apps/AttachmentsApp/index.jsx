import { compose } from 'redux';
import { connect } from 'react-redux';
import { lifecycle } from 'recompose';

import createApp from 'hocs/createApp';
import { lifecycleApp } from 'hocs/lifecycles';

import App from './App';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default compose(
  createApp,
  connect(
    mapStateToProps,
    actions,
  ),
  lifecycle(lifecycleApp),
)(App);
