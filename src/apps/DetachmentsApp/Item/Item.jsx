import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import Header from '@rbo/rbo-components/lib/Styles/Header';
import Label from '@rbo/rbo-components/lib/Styles/Label';
import List from '@rbo/rbo-components/lib/Styles/List';
import Loader from '@rbo/rbo-components/lib/Loader';
import OperationsPanel from '@rbo/rbo-components/lib/OperationsPanel';
import RboDocHeader, { Row, Block, Unit } from '@rbo/rbo-components/lib/RboDocHeader';
import RboDocInfo from '@rbo/rbo-components/lib/RboDocInfo';
import RboDocLayout from '@rbo/rbo-components/lib/RboDocLayout';
import RboDocStructure from '@rbo/rbo-components/lib/RboDocStructure';
import RboDocTop from '@rbo/rbo-components/lib/RboDocTop';
import RboPopupBoxOption from '@rbo/rbo-components/lib/RboPopupBoxOption';
import Status from '@rbo/rbo-components/lib/Styles/Status';
import Table from '@rbo/rbo-components/lib/Table';
import Tabs from '@rbo/rbo-components/lib/Tabs';

import RboDocHistoryCell from 'common/components/RboDocHistoryCell';
import RboDocNotes from 'common/components/RboDocNotes';
import RboDocSignaturesCell from 'common/components/RboDocSignaturesCell';
import RboDocTemplates from 'common/components/RboDocTemplates';
import RboServerError from 'common/components/RboServerError';

import { VIEW_ID, TAB_ID, SECTION_ID, FIELD_ID } from './constants';
import MainFields from './MainFields';
import Employees from './Employees';
import Employee from './Employee';

const docHistoryCellRenderer = props => <RboDocHistoryCell {...props} />;

const docSignaturesCellRenderer = props => <RboDocSignaturesCell {...props} />;

const popupBoxOptionRenderer = ({ id, option }) => {
  switch (id) {
    case FIELD_ID.OFFICIAL_NAME:
    case FIELD_ID.BRANCH_INFO_NAME:
      return (
        <RboPopupBoxOption
          title={option.title}
          describe={option.describe}
          highlight={{ value: option.searchValue, inTitle: true }}
        />
      );
    default:
      return <RboPopupBoxOption isInline label={option.title} />;
  }
};

export default class Item extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape({
      LOCALE: PropTypes.string.isRequired,
    }).isRequired,
    isError: PropTypes.bool.isRequired,
    serverError: PropTypes.shape(),
    isLoading: PropTypes.bool.isRequired,
    isTemplate: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    status: PropTypes.shape({
      title: PropTypes.string.isRequired,
      isSuccess: PropTypes.bool.isRequired,
      isError: PropTypes.bool.isRequired,
    }).isRequired,
    fromBankInfo: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    operations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    isWideView: PropTypes.bool.isRequired,
    isShowExtra: PropTypes.bool.isRequired,
    documentHistory: PropTypes.shape({
      items: PropTypes.arrayOf(PropTypes.shape()).isRequired,
      isVisible: PropTypes.bool.isRequired,
    }).isRequired,
    documentSignatures: PropTypes.shape({
      items: PropTypes.arrayOf(PropTypes.shape()).isRequired,
      isVisible: PropTypes.bool.isRequired,
    }).isRequired,
    documentStructure: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    documentTabs: PropTypes.shape({
      items: PropTypes.arrayOf(PropTypes.shape()).isRequired,
      active: PropTypes.string.isRequired,
    }).isRequired,
    documentData: PropTypes.shape().isRequired,
    documentDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    templates: PropTypes.shape().isRequired,
    view: PropTypes.oneOf(Object.values(VIEW_ID)).isRequired,
    totalEmployeesCount: PropTypes.number,

    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    handleTabClick: PropTypes.func.isRequired,
    handleStructureTabClick: PropTypes.func.isRequired,
    handleStructureSectionClick: PropTypes.func.isRequired,
    handleStructureErrorClick: PropTypes.func.isRequired,
    handleOperationClick: PropTypes.func.isRequired,
    handleTemplatesSearch: PropTypes.func.isRequired,
    handleTemplatesChoose: PropTypes.func.isRequired,
    handleTemplatesItemEditClick: PropTypes.func.isRequired,
    handleTopClose: PropTypes.func.isRequired,
    handleTopSignatureOperationClick: PropTypes.func.isRequired,

    saveResponsiblePersonAction: PropTypes.func.isRequired,
  };

  static defaultProps = {
    serverError: null,
    totalEmployeesCount: 0,
  };

  handleAddEmployeesButtonClick = () => {
    const { handleTabClick } = this.props;
    handleTabClick(TAB_ID.EMPLOYEES);
  };

  render() {
    const {
      LABELS,
      isError,
      serverError,
      isLoading,
      isTemplate,
      isEditable,
      status,
      fromBankInfo,
      operations,
      isWideView,
      isShowExtra,
      documentHistory,
      documentSignatures,
      documentStructure,
      documentTabs,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      templates,
      view,
      totalEmployeesCount,

      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
      handleTabClick,
      handleStructureTabClick,
      handleStructureSectionClick,
      handleStructureErrorClick,
      handleOperationClick,
      handleTemplatesSearch,
      handleTemplatesChoose,
      handleTemplatesItemEditClick,
      handleTopClose,
      handleTopSignatureOperationClick,

      saveResponsiblePersonAction,
    } = this.props;

    if (isError) return <RboServerError serverData={serverError} />;
    if (isLoading) return <Loader dimension={Loader.REFS.DIMENSIONS.L} isCentered />;

    const sectionsProps = {
      LABELS,
      isEditable,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer: popupBoxOptionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
    };

    return (
      <Fragment>
        <RboDocLayout>
          {view === VIEW_ID.MAIN && (
            <Fragment>
              <RboDocLayout.Panel>
                <OperationsPanel
                  locale={LABELS.LOCALE}
                  dimension={OperationsPanel.REFS.DIMENSIONS.XS}
                  items={operations}
                  onItemClick={handleOperationClick}
                />
              </RboDocLayout.Panel>
              <RboDocLayout.Content maxWidth={!isWideView ? 1280 : null}>
                <RboDocLayout.Content.Main isWide={isWideView && !isShowExtra}>
                  {(documentHistory.isVisible || documentSignatures.isVisible) && (
                    <RboDocTop onClose={handleTopClose}>
                      {documentHistory.isVisible && (
                        <Table
                          items={documentHistory.items}
                          columns={documentHistory.columns}
                          cellRenderer={docHistoryCellRenderer}
                          dimension={Table.REFS.DIMENSIONS.S}
                          isMultiLine
                        />
                      )}
                      {documentSignatures.isVisible && (
                        <Table
                          items={documentSignatures.items}
                          columns={documentSignatures.columns}
                          cellRenderer={docSignaturesCellRenderer}
                          dimension={Table.REFS.DIMENSIONS.S}
                          isMultiLine
                          operations={documentSignatures.operations}
                          onOperationClick={handleTopSignatureOperationClick}
                        />
                      )}
                    </RboDocTop>
                  )}
                  <RboDocHeader
                    bgColor={
                      isTemplate ? RboDocHeader.REFS.BG_COLORS.CORPORATE_YELLOW_12 : RboDocHeader.REFS.BG_COLORS.WHITE
                    }
                  >
                    <Row>
                      <Header size={3}>{isTemplate ? LABELS.HEADER.TEMPLATE_TITLE : LABELS.HEADER.TITLE}</Header>
                    </Row>
                    <Row>
                      <Block isWithBottomPadding>
                        <Unit>{isTemplate ? LABELS.HEADER.TEMPLATE_DESCRIBE : LABELS.HEADER.DATE}</Unit>
                        {!isTemplate && (
                          <Unit>
                            <Label>{LABELS.HEADER.STATUS} </Label>
                            <Status isSuccess={status.isSuccess} isWarning={status.isWarning} isError={status.isError}>
                              {status.title}
                            </Status>
                          </Unit>
                        )}
                      </Block>
                      <Block isGrow>
                        <Tabs
                          items={documentTabs.items}
                          onItemClick={handleTabClick}
                          align={Tabs.REFS.ALIGN.RIGHT}
                          dimension={Tabs.REFS.DIMENSIONS.XS}
                          isEmphasis
                        />
                      </Block>
                    </Row>
                  </RboDocHeader>
                  {documentTabs.active === TAB_ID.MAIN_FIELDS && (
                    <MainFields
                      isTemplate={isTemplate}
                      sectionsProps={sectionsProps}
                      onAddEmployeesButtonClick={this.handleAddEmployeesButtonClick}
                      onSaveResponsiblePersonButtonClick={saveResponsiblePersonAction}
                    />
                  )}
                  {documentTabs.active === TAB_ID.EMPLOYEES && (
                    <RboDocLayout.Table>
                      <Employees />
                    </RboDocLayout.Table>
                  )}
                  {documentTabs.active === TAB_ID.NOTES && (
                    <RboDocNotes
                      isEditable={isEditable}
                      fieldId={FIELD_ID.NOTE}
                      value={documentData[TAB_ID.NOTES][SECTION_ID.NOTES][FIELD_ID.NOTE]}
                      disabled={formDisabled[FIELD_ID.NOTE]}
                      onChange={handleChange}
                    />
                  )}
                  {documentTabs.active === TAB_ID.FROM_BANK_INFO && (
                    <RboDocInfo items={fromBankInfo} emptyLabel={LABELS.FROM_BANK_INFO.EMPTY} />
                  )}
                </RboDocLayout.Content.Main>
                {isShowExtra && (
                  <RboDocLayout.Content.Extra>
                    {!!templates.isVisible && (
                      <RboDocLayout.Content.Extra.Section title={LABELS.EXTRA.TEMPLATES}>
                        <RboDocTemplates
                          params={templates}
                          onSearch={handleTemplatesSearch}
                          onChoose={handleTemplatesChoose}
                          onItemEditClick={handleTemplatesItemEditClick}
                        />
                      </RboDocLayout.Content.Extra.Section>
                    )}
                    <RboDocLayout.Content.Extra.Section title={LABELS.EXTRA.STRUCTURE}>
                      <RboDocStructure
                        structure={documentStructure}
                        onTabClick={handleStructureTabClick}
                        onSectionClick={handleStructureSectionClick}
                        onErrorClick={handleStructureErrorClick}
                      />
                    </RboDocLayout.Content.Extra.Section>
                  </RboDocLayout.Content.Extra>
                )}
              </RboDocLayout.Content>
              <RboDocLayout.Panel isBottom>
                <List fontSize={List.REFS.FONT_SIZES.S} isInline vPadding={8}>
                  <List.Item>
                    {LABELS.OTHER.TOTAL_EMPLOYEES_COUNT} <strong>{totalEmployeesCount}</strong>
                  </List.Item>
                </List>
              </RboDocLayout.Panel>
            </Fragment>
          )}
          {view === VIEW_ID.EMPLOYEE && <Employee />}
        </RboDocLayout>
      </Fragment>
    );
  }
}
