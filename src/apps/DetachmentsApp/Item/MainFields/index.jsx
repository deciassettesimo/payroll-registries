import React from 'react';
import PropTypes from 'prop-types';

import Form from '@rbo/rbo-components/lib/Form';
import RboDocForm from '@rbo/rbo-components/lib/RboDocForm';

import { TAB_ID, SECTION_ID, FIELD_ID } from '../constants';

import * as Sections from './sections';

const MainFields = props => {
  const { isTemplate, sectionsProps, onAddEmployeesButtonClick, onSaveResponsiblePersonButtonClick } = props;

  const autoFocus = isTemplate ? FIELD_ID.TEMPLATE_NAME : null;

  const {
    documentData,
    documentDictionaries,
    formWarnings,
    formErrors,
    formDisabled,
    ...otherSectionsProps
  } = sectionsProps;

  return (
    <RboDocForm>
      <Form dimension={Form.REFS.DIMENSIONS.XS} autoFocus={autoFocus}>
        {isTemplate && (
          <Sections.TemplateName
            {...otherSectionsProps}
            sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.TEMPLATE_NAME]}
            sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.TEMPLATE_NAME]}
            formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.TEMPLATE_NAME]}
            formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.TEMPLATE_NAME]}
            formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.TEMPLATE_NAME]}
          />
        )}
        <Sections.CommonInfo
          {...sectionsProps}
          sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
          sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
          formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
          formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
          formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
        />
        <Sections.ResponsiblePerson
          {...sectionsProps}
          sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.RESPONSIBLE_PERSON]}
          sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.RESPONSIBLE_PERSON]}
          formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.RESPONSIBLE_PERSON]}
          formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.RESPONSIBLE_PERSON]}
          formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.RESPONSIBLE_PERSON]}
          onSaveResponsiblePersonButtonClick={onSaveResponsiblePersonButtonClick}
        />
        <Sections.OtherData
          {...sectionsProps}
          sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.OTHER_DATA]}
          sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.OTHER_DATA]}
          formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.OTHER_DATA]}
          formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.OTHER_DATA]}
          formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.OTHER_DATA]}
          onAddEmployeesButtonClick={onAddEmployeesButtonClick}
        />
      </Form>
    </RboDocForm>
  );
};

MainFields.propTypes = {
  isTemplate: PropTypes.bool.isRequired,
  sectionsProps: PropTypes.shape().isRequired,
  onAddEmployeesButtonClick: PropTypes.func.isRequired,
  onSaveResponsiblePersonButtonClick: PropTypes.func.isRequired,
};

export default MainFields;
