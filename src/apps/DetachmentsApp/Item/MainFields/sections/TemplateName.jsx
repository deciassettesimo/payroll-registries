import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field } from '@rbo/rbo-components/lib/Form';
import InputText from '@rbo/rbo-components/lib/Input/InputText';

import { SECTION_ID, FIELD_ID } from '../../constants';

export default class TemplateName extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { sectionData, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ sectionData, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      sectionData,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.TEMPLATE_NAME }}>
        <Section.Header>
          <Section.Title>{LABELS.SECTIONS.TEMPLATE_NAME}</Section.Title>
        </Section.Header>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Field>
                    <InputText
                      id={FIELD_ID.TEMPLATE_NAME}
                      value={sectionData[FIELD_ID.TEMPLATE_NAME]}
                      isWarning={formWarnings[FIELD_ID.TEMPLATE_NAME]}
                      isError={formErrors[FIELD_ID.TEMPLATE_NAME]}
                      disabled={formDisabled[FIELD_ID.TEMPLATE_NAME]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      maxLength={255}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
