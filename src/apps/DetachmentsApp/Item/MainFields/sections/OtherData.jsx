import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDateCalendar, InputCheckbox } from '@rbo/rbo-components/lib/Input';
import Button from '@rbo/rbo-components/lib/Button';
import IconUser from '@rbo/rbo-components/lib/Icon/IconUser';

import { SECTION_ID, FIELD_ID } from '../../constants';

export default class OtherData extends Component {
  static propTypes = {
    isEditable: PropTypes.bool.isRequired,
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    onAddEmployeesButtonClick: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, sectionData, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, sectionData, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
      onAddEmployeesButtonClick,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.OTHER_DATA }}>
        <Section.Header>
          <Section.Title>{LABELS.SECTIONS.OTHER_DATA}</Section.Title>
        </Section.Header>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.DETACH_REQUEST_NUMBER}>{LABELS.FIELDS.DETACH_REQUEST_NUMBER}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.DETACH_REQUEST_NUMBER}
                        value={sectionData[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        isWarning={formWarnings[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        isError={formErrors[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        disabled={formDisabled[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={100}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.DETACH_REQUEST_NUMBER}
                        value={sectionData[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        isWarning={formWarnings[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        isError={formErrors[FIELD_ID.DETACH_REQUEST_NUMBER]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="auto">
                  <Label htmlFor={FIELD_ID.DETACH_REQUEST_DATE}>{LABELS.FIELDS.DETACH_REQUEST_DATE}</Label>
                  <Field width="120px">
                    {isEditable ? (
                      <InputDateCalendar
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.DETACH_REQUEST_DATE}
                        value={sectionData[FIELD_ID.DETACH_REQUEST_DATE]}
                        isWarning={formWarnings[FIELD_ID.DETACH_REQUEST_DATE]}
                        isError={formErrors[FIELD_ID.DETACH_REQUEST_DATE]}
                        disabled={formDisabled[FIELD_ID.DETACH_REQUEST_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.DETACH_REQUEST_DATE}
                        value={sectionData[FIELD_ID.DETACH_REQUEST_DATE]}
                        isWarning={formWarnings[FIELD_ID.DETACH_REQUEST_DATE]}
                        isError={formErrors[FIELD_ID.DETACH_REQUEST_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Field>
                    {isEditable ? (
                      <InputCheckbox
                        id={FIELD_ID.CONFIRM_ACCEPTANCE}
                        checked={sectionData[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        isWarning={formWarnings[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        isError={formErrors[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        disabled={formDisabled[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      >
                        {LABELS.FIELDS.CONFIRM_ACCEPTANCE}
                      </InputCheckbox>
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.CONFIRM_ACCEPTANCE}
                        type={InputViewMode.REFS.TYPES.CHECKBOX}
                        checked={sectionData[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        isWarning={formWarnings[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        isError={formErrors[FIELD_ID.CONFIRM_ACCEPTANCE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                      >
                        {LABELS.FIELDS.CONFIRM_ACCEPTANCE}
                      </InputViewMode>
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          {isEditable && (
            <Blockset>
              <Block isWide>
                <Row>
                  <Cell>
                    <Field>
                      <Button
                        id={FIELD_ID.ADD_EMPLOYEES_BUTTON}
                        type={Button.REFS.TYPES.LINK}
                        isWithIcon
                        disabled={formDisabled[FIELD_ID.ADD_EMPLOYEES_BUTTON]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onClick={onAddEmployeesButtonClick}
                      >
                        <IconUser dimension={IconUser.REFS.DIMENSIONS.XS} />
                        <span>{LABELS.FIELDS.ADD_EMPLOYEES_BUTTON}</span>
                      </Button>
                    </Field>
                  </Cell>
                </Row>
              </Block>
            </Blockset>
          )}
        </Section.Content>
      </Section>
    );
  }
}
