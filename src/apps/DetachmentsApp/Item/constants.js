import { OPERATION_ID as COMMON_OPERATION_ID, OPERATIONS_ACTIONS_TYPES } from 'common/constants';
import {
  FIELD_ID as DAL_FIELD_ID,
  EMPLOYEES_INFO_ITEM_FIELD_ID as DAL_EMPLOYEES_INFO_ITEM_FIELD_ID,
  ALLOWED_SM_ACTION_ID as DAL_ALLOWED_SM_ACTION_ID,
} from 'dal/payroll-registries-detachments/constants';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesDetachmentsItem';

export const VIEW_ID = keyMirrorWithPrefix(
  {
    MAIN: null,
    EMPLOYEE: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_',
);

export const TAB_ID = keyMirrorWithPrefix(
  {
    MAIN_FIELDS: null,
    EMPLOYEES: null,
    NOTES: null,
    FROM_BANK_INFO: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_',
);

export const SECTION_ID = keyMirrorWithPrefix(
  {
    TEMPLATE_NAME: null,
    COMMON_INFO: null,
    OTHER_DATA: null,
    RESPONSIBLE_PERSON: null,
    EMPLOYEES: null,
    NOTES: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_',
);

export const FIELD_ID = {
  ...DAL_FIELD_ID,
  ...keyMirrorWithPrefix({ ADD_EMPLOYEES_BUTTON: null, SAVE_RESPONSIBLE_PERSON_BUTTON: null }),
};

export const EMPLOYEES_INFO_ITEM_FIELD_ID = DAL_EMPLOYEES_INFO_ITEM_FIELD_ID;

export const ALLOWED_SM_ACTION_ID = DAL_ALLOWED_SM_ACTION_ID;

export const OPERATION_ID = COMMON_OPERATION_ID;

export const LOCALE_KEY = {
  HEADER: {
    TITLE: 'loc.registryOnDetachmentsTitle',
    DATE: 'loc.fromDate',
    STATUS: 'loc.status.withColon',
    TEMPLATE_TITLE: 'loc.templateTitle',
    TEMPLATE_DESCRIBE: 'loc.registryOnDetachmentsTemplateDescribe',
  },
  EXTRA: {
    TEMPLATES: 'loc.templates',
    STRUCTURE: 'loc.documentStructure',
  },
  TABS: {
    MAIN_FIELDS: 'loc.mainFields',
    EMPLOYEES: 'loc.employees',
    NOTES: 'loc.notes',
    FROM_BANK_INFO: 'loc.fromBankInfo',
  },
  SECTIONS: {
    TEMPLATE_NAME: 'loc.templateName',
    COMMON_INFO: 'loc.commonInfo',
    OTHER_DATA: 'loc.otherData',
    RESPONSIBLE_PERSON: 'loc.responsiblePerson',
  },
  FIELDS: {
    NUMBER: 'loc.number',
    DATE: 'loc.date',
    BRANCH_INFO_NAME: 'loc.subdivision',
    CUSTOMER_NAME: 'loc.from',
    CUSTOMER_INN: 'loc.tinOrFcc',
    CUSTOMER_OGRN: 'loc.psrn',
    OFFICIAL_NAME: 'loc.executor',
    OFFICIAL_PHONE: 'loc.phone',
    DETACH_REQUEST_NUMBER: 'loc.agreement',
    DETACH_REQUEST_DATE: 'loc.agreementDate',
    CONFIRM_ACCEPTANCE: 'loc.confirmAcceptance',
    ADD_EMPLOYEES_BUTTON: 'loc.employees',
    SAVE_RESPONSIBLE_PERSON_BUTTON: 'loc.addToDirectory',
  },
  FROM_BANK_INFO: {
    EMPTY: 'loc.additionalInfoForDocumentIsEmpty',
    ACCEPT_DATE: 'loc.receivedByBank',
    OPERATION_DATE: 'loc.processingDate',
    BANK_MESSAGE: 'loc.bankMessage',
    BANK_MESSAGE_AUTHOR: 'loc.assignedPerson',
  },
  OTHER: {
    SIGNATURE_STATUS: 'loc.signatureStatus',
    TOTAL_EMPLOYEES_COUNT: 'loc.totalEmployeesInList.withColon',
    PAGE_LEAVE_MESSAGE: 'loc.changesNotSavedAndWillBeLoose.closeDocument.question',
    PAGE_LEAVE_CONFIRM: 'loc.goTo',
    PAGE_LEAVE_CANCEL: 'loc.stay',
  },
  NOTIFICATIONS: {
    RESPONSIBLE_PERSON_SAVE: {
      SUCCESS: {
        TITLE: 'loc.responsiblePersonAddedToDictionary',
      },
      FAIL: {
        TITLE: 'loc.unableAddResponsiblePersonToDictionary',
      },
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    REFRESH: null,
    REFRESH_CHECKING: null,
    REFRESH_SUCCESS: null,
    REFRESH_FAIL: null,
    UNMOUNT: null,

    GET_DATA_REQUEST: null,
    GET_DATA_SUCCESS: null,
    GET_DATA_FAIL: null,
    SAVE_DATA_REQUEST: null,
    SAVE_DATA_SUCCESS: null,
    SAVE_DATA_FAIL: null,

    CLOSE_TOP: null,
    SIGNATURES_OPERATIONS: null,
    CHANGE_TAB: null,
    ACTIVATE_ERROR: null,
    FIELD_FOCUS: null,
    FIELD_BLUR: null,
    CHANGE_DATA: null,

    DICTIONARY_SEARCH: null,
    DICTIONARY_SEARCH_REQUEST: null,
    DICTIONARY_SEARCH_SUCCESS: null,
    DICTIONARY_SEARCH_CANCEL: null,
    DICTIONARY_SEARCH_FAIL: null,

    TEMPLATES_SEARCH: null,
    TEMPLATES_CHOOSE: null,
    TEMPLATES_ROUTE_TO: null,

    OPERATIONS: null,
    ...OPERATIONS_ACTIONS_TYPES.ARCHIVE,
    ...OPERATIONS_ACTIONS_TYPES.BACK,
    ...OPERATIONS_ACTIONS_TYPES.CHECK_SIGN,
    ...OPERATIONS_ACTIONS_TYPES.CREATE_FROM_TEMPLATE,
    ...OPERATIONS_ACTIONS_TYPES.STATE_HISTORY,
    ...OPERATIONS_ACTIONS_TYPES.REMOVE,
    ...OPERATIONS_ACTIONS_TYPES.REMOVE_SIGN,
    ...OPERATIONS_ACTIONS_TYPES.REPEAT,
    ...OPERATIONS_ACTIONS_TYPES.REPEAT_WITH_REFUSALS,
    ...OPERATIONS_ACTIONS_TYPES.SAVE,
    ...OPERATIONS_ACTIONS_TYPES.SAVE_AS_TEMPLATE,
    ...OPERATIONS_ACTIONS_TYPES.SEND,
    ...OPERATIONS_ACTIONS_TYPES.SIGN,
    ...OPERATIONS_ACTIONS_TYPES.UNARCHIVE,

    SAVE_RESPONSIBLE_PERSON_REQUEST: null,
    SAVE_RESPONSIBLE_PERSON_FAIL: null,
    SAVE_RESPONSIBLE_PERSON_SUCCESS: null,

    REMOVE_EMPLOYEES: null,
    ADD_EMPLOYEES: null,
    OPEN_EMPLOYEE: null,
    SAVE_EMPLOYEE: null,
    CLOSE_EMPLOYEE: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_',
);
