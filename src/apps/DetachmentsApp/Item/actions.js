import { ACTIONS } from './constants';

export const mountAction = (params, location) => ({
  type: ACTIONS.MOUNT_REQUEST,
  payload: { params, location },
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const operationsAction = (operationId, params) => ({
  type: ACTIONS.OPERATIONS,
  payload: { operationId, params },
});

export const closeTopAction = () => ({
  type: ACTIONS.CLOSE_TOP,
});

export const signaturesOperationsAction = (operationId, itemId) => ({
  type: ACTIONS.SIGNATURES_OPERATIONS,
  payload: { operationId, itemId },
});

export const changeTabAction = tabId => ({
  type: ACTIONS.CHANGE_TAB,
  payload: { tabId },
});

export const activateErrorAction = errorId => ({
  type: ACTIONS.ACTIVATE_ERROR,
  payload: { errorId },
});

export const fieldFocusAction = fieldId => ({
  type: ACTIONS.FIELD_FOCUS,
  payload: { fieldId },
});

export const fieldBlurAction = fieldId => ({
  type: ACTIONS.FIELD_BLUR,
  payload: { fieldId },
});

export const changeDataAction = (fieldId, fieldValue, dictionaryValue) => ({
  type: ACTIONS.CHANGE_DATA,
  payload: { fieldId, fieldValue, dictionaryValue },
});

export const dictionarySearchAction = (dictionaryId, dictionarySearchValue) => ({
  type: ACTIONS.DICTIONARY_SEARCH,
  payload: { dictionaryId, dictionarySearchValue },
});

export const templatesSearchAction = searchValue => ({
  type: ACTIONS.TEMPLATES_SEARCH,
  payload: { searchValue },
});

export const templatesChooseAction = (templateId, params) => ({
  type: ACTIONS.TEMPLATES_CHOOSE,
  payload: { templateId, params },
});

export const templatesRouteToAction = (templateId, params) => ({
  type: ACTIONS.TEMPLATES_ROUTE_TO,
  payload: { templateId, params },
});

export const saveResponsiblePersonAction = () => ({
  type: ACTIONS.SAVE_RESPONSIBLE_PERSON_REQUEST,
});
