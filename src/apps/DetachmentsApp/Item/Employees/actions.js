import { ACTIONS } from './constants';

export const mountAction = () => ({
  type: ACTIONS.MOUNT_REQUEST,
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const listRefreshAction = () => ({
  type: ACTIONS.GET_LIST_REQUEST,
});

export const pageChangeAction = page => ({
  type: ACTIONS.PAGE_CHANGE,
  payload: { page },
});

export const paginationChangeAction = value => ({
  type: ACTIONS.PAGINATION_CHANGE,
  payload: { value },
});

export const sortingChangeAction = sorting => ({
  type: ACTIONS.SORTING_CHANGE,
  payload: { sorting },
});

export const resizeChangeAction = width => ({
  type: ACTIONS.RESIZE_CHANGE,
  payload: { width },
});

export const visibilityChangeAction = visibility => ({
  type: ACTIONS.VISIBILITY_CHANGE,
  payload: { visibility },
});

export const groupingChangeAction = grouped => ({
  type: ACTIONS.GROUPING_CHANGE,
  payload: { grouped },
});

export const filtersRemoveAction = id => ({
  type: ACTIONS.FILTERS_REMOVE,
  payload: { id },
});

export const filtersChooseAction = (id, params) => ({
  type: ACTIONS.FILTERS_CHOOSE,
  payload: { id, params },
});

export const filtersCreateAction = () => ({
  type: ACTIONS.FILTERS_CREATE,
});

export const filtersResetAction = params => ({
  type: ACTIONS.FILTERS_RESET,
  payload: { params },
});

export const filtersSaveAction = () => ({
  type: ACTIONS.FILTERS_SAVE,
});

export const filtersSaveAsAction = title => ({
  type: ACTIONS.FILTERS_SAVE_AS,
  payload: { title },
});

export const filterChangeAction = (filter, isChanged) => ({
  type: ACTIONS.FILTER_CHANGE,
  payload: { filter, isChanged },
});

export const filterSearchAction = (conditionId, conditionSearchValue) => ({
  type: ACTIONS.FILTER_SEARCH,
  payload: { conditionId, conditionSearchValue },
});

export const selectChangeAction = selected => ({
  type: ACTIONS.SELECT_CHANGE,
  payload: { selected },
});

export const openItemAction = (id, params) => ({
  type: ACTIONS.OPEN_ITEM,
  payload: { id, params },
});

export const operationsAction = (operationId, itemId) => ({
  type: ACTIONS.OPERATIONS,
  payload: { operationId, itemId },
});
