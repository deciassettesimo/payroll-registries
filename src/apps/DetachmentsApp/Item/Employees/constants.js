import { OPERATION_ID as COMMON_OPERATION_ID, OPERATIONS_ACTIONS_TYPES } from 'common/constants';
import { EMPLOYEES_INFO_ITEM_FIELD_ID as DAL_FIELD_ID } from 'dal/payroll-registries-detachments/constants';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesDetachmentsItemEmployees';

export const FIELD_ID = { ...DAL_FIELD_ID };

export const COLUMN_ID = {
  NPP: FIELD_ID.NPP,
  EMPLOYEE_NUM: FIELD_ID.EMPLOYEE_NUM,
  EMPLOYEE_SURNAME: FIELD_ID.EMPLOYEE_SURNAME,
  EMPLOYEE_NAME: FIELD_ID.EMPLOYEE_NAME,
  EMPLOYEE_PATRONYMIC: FIELD_ID.EMPLOYEE_PATRONYMIC,
  EMPLOYEE_BIRTH_DATE: FIELD_ID.EMPLOYEE_BIRTH_DATE,
  ACCOUNT: FIELD_ID.ACCOUNT,
  EMPLOYEE_PROCESSING_RESULT_STATUS: FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
  EMPLOYEE_PROCESSING_RESULT_CODE_1C: FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C,
  EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK: FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK,
};

export const CONDITION_ID = {
  EMPLOYEE_NUM: FIELD_ID.EMPLOYEE_NUM,
  EMPLOYEE_SURNAME: FIELD_ID.EMPLOYEE_SURNAME,
  EMPLOYEE_NAME: FIELD_ID.EMPLOYEE_NAME,
  EMPLOYEE_PATRONYMIC: FIELD_ID.EMPLOYEE_PATRONYMIC,
  ACCOUNT: FIELD_ID.ACCOUNT,
  EMPLOYEE_PROCESSING_RESULT_STATUS: FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
};

export const SYSTEM_FILTER_ID = {
  DECLINED_BY_BANK: `${REDUCER_KEY}SystemFilterDeclinedByBank`,
  CREATED_WITH_WARNINGS: `${REDUCER_KEY}SystemFilterCreatedWithWarnings`,
  INVALID_CONTROLS: `${REDUCER_KEY}SystemFilterInvalidControls`,
};

export const OPERATION_ID = {
  ADD_EMPLOYEE: 'ADD_EMPLOYEE',
  CSV_IMPORT: 'CSV_IMPORT',
  ...COMMON_OPERATION_ID,
};

export const LOCALE_KEY = {
  COLUMNS: {
    NPP: 'loc.orderNumber.abbr',
    EMPLOYEE_NUM: 'loc.personnelNumber',
    EMPLOYEE_SURNAME: 'loc.surname',
    EMPLOYEE_NAME: 'loc.name',
    EMPLOYEE_PATRONYMIC: 'loc.patronymic',
    EMPLOYEE_BIRTH_DATE: 'loc.dateOfBirth',
    ACCOUNT: 'loc.account',
    EMPLOYEE_PROCESSING_RESULT_STATUS: 'loc.status',
    EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK: 'loc.bankMessage',
    EMPLOYEE_PROCESSING_RESULT_CODE_1C: 'loc.result',
  },
  CONDITIONS: {
    EMPLOYEE_NUM: 'loc.personnelNumber',
    EMPLOYEE_SURNAME: 'loc.surname',
    EMPLOYEE_NAME: 'loc.name',
    EMPLOYEE_PATRONYMIC: 'loc.patronymic',
    ACCOUNT: 'loc.account',
    EMPLOYEE_PROCESSING_RESULT_STATUS: 'loc.status',
  },
  SYSTEM_FILTERS: {
    DECLINED_BY_BANK: 'loc.declinedByBank',
    CREATED_WITH_WARNINGS: 'loc.haveWarnings',
    INVALID_CONTROLS: 'loc.haveErrors',
  },
  NOTIFICATIONS: {
    REMOVE: {
      CONFIRM: {
        TITLE: 'loc.removingEmployee',
        MESSAGE: 'loc.doYourWantRemoveEmployee',
        LABELS: {
          OK: 'loc.confirm',
          CANCEL: 'loc.cancel',
        },
      },
      SUCCESS: {
        TITLE: 'loc.employeeRemoved',
      },
      FAIL: {
        TITLE: 'loc.unableRemoveEmployee',
      },
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,

    SAVE_SETTINGS: null,
    GET_LIST_REQUEST: null,
    GET_LIST_SUCCESS: null,
    GET_LIST_FAIL: null,
    GET_TEMPLATES_REQUEST: null,

    SECTION_CHANGE: null,
    PAGE_CHANGE: null,
    PAGINATION_CHANGE: null,
    SORTING_CHANGE: null,
    RESIZE_CHANGE: null,
    VISIBILITY_CHANGE: null,
    GROUPING_CHANGE: null,
    FILTERS_REMOVE: null,
    FILTERS_CHOOSE: null,
    FILTERS_CREATE: null,
    FILTERS_RESET: null,
    FILTERS_SAVE: null,
    FILTERS_SAVE_AS: null,
    FILTER_SEARCH: null,
    FILTER_SEARCH_REQUEST: null,
    FILTER_SEARCH_SUCCESS: null,
    FILTER_SEARCH_CANCEL: null,
    FILTER_SEARCH_FAIL: null,
    FILTER_CHANGE: null,
    SELECT_CHANGE: null,

    OPEN_ITEM: null,

    OPERATIONS: null,
    OPERATION_ADD_EMPLOYEE: null,
    OPERATION_CSV_IMPORT: null,
    ...OPERATIONS_ACTIONS_TYPES.REMOVE,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_EMPLOYEES_',
);
