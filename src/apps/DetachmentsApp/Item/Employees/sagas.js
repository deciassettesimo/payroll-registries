import { all, call, put, select, takeEvery } from 'redux-saga/effects';

import { DOC_NEW_ID } from 'common/constants';
import localization from 'localization';
import { notificationConfirm } from 'platform';
import store from 'store';
import { dispatchErrorAction, normalizeListSettings } from 'utils';

import { REDUCER_KEY as APP_REDUCER_KEY, ACTIONS as APP_ACTIONS } from '../../constants';

import { REDUCER_KEY as DOC_REDUCER_KEY, FIELD_ID as DOC_FIELD_ID, ACTIONS as DOC_ACTIONS } from '../constants';

import { OPERATIONS_ACTIONS_MAP, SETTINGS, COLUMNS, CONDITIONS } from './config';
import { REDUCER_KEY, ACTIONS, LOCALE_KEY } from './constants';
import { prepareList } from './utils';
import CSVImportModalSagas from './CSVImportModal/sagas';
import { openAction as CSVImportModalOpenAction } from './CSVImportModal/actions';

const appSettingsSelector = state => state[APP_REDUCER_KEY].settings[REDUCER_KEY];
const docEmployeesInfoSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.EMPLOYEES_INFO];
const settingsSelector = state => state[REDUCER_KEY].settings;
const pagesSelector = state => state[REDUCER_KEY].pages;
const filtersSelector = state => state[REDUCER_KEY].filters;
const filterSelector = state => state[REDUCER_KEY].filter;
const conditionsOptionsSelector = state => state[REDUCER_KEY].conditionsOptions;
const selectedSelector = state => state[REDUCER_KEY].selected;

function* mountSaga() {
  try {
    const appSettings = yield select(appSettingsSelector);
    const { filters, ...settings } = normalizeListSettings(appSettings, SETTINGS, COLUMNS, CONDITIONS);

    yield put({
      type: ACTIONS.MOUNT_SUCCESS,
      payload: { filters, settings, conditionsOptions: {} },
    });
    yield put({ type: ACTIONS.GET_LIST_REQUEST });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsSaga() {
  const settings = yield select(settingsSelector);
  const filters = yield select(filtersSelector);
  const userFilters = filters
    .filter(filter => !filter.isPreset && !filter.isNew)
    .map(filter => ({ id: filter.id, title: filter.title, params: filter.params }));
  const newSettings = { ...settings, filters: userFilters };
  yield put({ type: APP_ACTIONS.SAVE_SETTINGS_REQUEST, payload: { key: REDUCER_KEY, data: newSettings } });
}

function* pageChangeSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* paginationChangeSaga({ payload }) {
  const { value } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, pagination: value };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* sortingChangeSaga({ payload }) {
  const { sorting } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = {
    ...currentSettings,
    sorting,
    grouped: currentSettings.grouped !== sorting.id ? null : currentSettings.grouped,
  };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* resizeChangeSaga({ payload }) {
  const { width } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, width };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
}

function* visibilityChangeSaga({ payload }) {
  const { visibility } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, visibility };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
}

function* groupingChangeSaga({ payload }) {
  const { grouped } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = {
    ...currentSettings,
    grouped,
    sorting: grouped ? { id: grouped, direction: 1 } : currentSettings.sorting,
  };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersRemoveSaga() {
  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filtersChooseSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersSaveSaga() {
  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filtersResetSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersSaveAsSaga() {
  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filterChangeSaga({ payload }) {
  const { isChanged } = payload;
  if (isChanged) yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* getListSaga() {
  try {
    const list = yield select(docEmployeesInfoSelector);
    const settings = yield select(settingsSelector);
    const savedPages = yield select(pagesSelector);
    const filter = yield select(filterSelector);
    const conditionsOptions = yield select(conditionsOptionsSelector);
    const result = prepareList({ list, settings, filter, conditionsOptions });

    const pages = { ...savedPages, totalQty: Math.ceil(result.length / settings.pagination) || 1 };
    const startIndex = settings.pagination * (pages.selected - 1);
    const endIndex = startIndex + settings.pagination;

    yield put({ type: ACTIONS.GET_LIST_SUCCESS, payload: { list: result.slice(startIndex, endIndex), pages } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.GET_LIST_FAIL] });
  }
}

function* openItemSaga({ payload }) {
  const { id } = payload;
  yield put({ type: DOC_ACTIONS.OPEN_EMPLOYEE, payload: { id } });
}

function* operationsSaga({ payload }) {
  const { operationId } = payload;
  yield put({ type: OPERATIONS_ACTIONS_MAP[operationId], payload });
}

function* operationRemoveSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const ids = itemId ? [itemId] : selected;

  const plural = ids.length > 1;
  yield call(notificationConfirm, {
    level: 'error',
    title: localization.translate(LOCALE_KEY.NOTIFICATIONS.REMOVE.CONFIRM.TITLE, { plural }),
    message: localization.translate(LOCALE_KEY.NOTIFICATIONS.REMOVE.CONFIRM.MESSAGE, { plural }),
    ok: {
      label: localization.translate(LOCALE_KEY.NOTIFICATIONS.REMOVE.CONFIRM.LABELS.OK),
      callback: () => store.dispatch({ type: ACTIONS.OPERATION_REMOVE_CONFIRM, payload: { ids } }),
    },
    cancel: {
      label: localization.translate(LOCALE_KEY.NOTIFICATIONS.REMOVE.CONFIRM.LABELS.CANCEL),
      callback: () => store.dispatch({ type: ACTIONS.OPERATION_REMOVE_CANCEL }),
    },
  });
}

function* operationRemoveConfirmSaga({ payload }) {
  const { ids } = payload;
  yield put({ type: DOC_ACTIONS.REMOVE_EMPLOYEES, payload: { ids } });
  yield put({ type: ACTIONS.OPERATION_REMOVE_SUCCESS });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationAddEmployeeSaga() {
  yield put({ type: DOC_ACTIONS.OPEN_EMPLOYEE, payload: { id: DOC_NEW_ID } });
}

function* operationCSVImportSaga() {
  yield put(CSVImportModalOpenAction());
}

function* containerSagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS, saveSettingsSaga);

  yield takeEvery(ACTIONS.PAGE_CHANGE, pageChangeSaga);
  yield takeEvery(ACTIONS.PAGINATION_CHANGE, paginationChangeSaga);
  yield takeEvery(ACTIONS.SORTING_CHANGE, sortingChangeSaga);
  yield takeEvery(ACTIONS.RESIZE_CHANGE, resizeChangeSaga);
  yield takeEvery(ACTIONS.VISIBILITY_CHANGE, visibilityChangeSaga);
  yield takeEvery(ACTIONS.GROUPING_CHANGE, groupingChangeSaga);
  yield takeEvery(ACTIONS.FILTERS_REMOVE, filtersRemoveSaga);
  yield takeEvery(ACTIONS.FILTERS_CHOOSE, filtersChooseSaga);
  yield takeEvery(ACTIONS.FILTERS_RESET, filtersResetSaga);
  yield takeEvery(ACTIONS.FILTERS_SAVE, filtersSaveSaga);
  yield takeEvery(ACTIONS.FILTERS_SAVE_AS, filtersSaveAsSaga);
  yield takeEvery(ACTIONS.FILTER_CHANGE, filterChangeSaga);
  yield takeEvery(ACTIONS.GET_LIST_REQUEST, getListSaga);

  yield takeEvery(ACTIONS.OPEN_ITEM, openItemSaga);

  yield takeEvery(ACTIONS.OPERATIONS, operationsSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE, operationRemoveSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_ADD_EMPLOYEE, operationAddEmployeeSaga);
  yield takeEvery(ACTIONS.OPERATION_CSV_IMPORT, operationCSVImportSaga);
}

export default function* sagas() {
  yield all([containerSagas(), CSVImportModalSagas()]);
}
