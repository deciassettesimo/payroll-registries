import { call, put, select, takeEvery } from 'redux-saga/effects';

import { CSVImport as dalCSVImport } from 'dal/payroll-registries-detachments';
import localization from 'localization';
import { notificationSuccess } from 'platform';
import dispatchErrorAction from 'utils/dispatchErrorAction';

import { REDUCER_KEY as APP_REDUCER_KEY, ACTIONS as APP_ACTIONS } from '../../../constants';

import { REDUCER_KEY as DOC_REDUCER_KEY, ACTIONS as DOC_ACTIONS, FIELD_ID as DOC_FIELD_ID } from '../../constants';

import { ACTIONS as EMPLOYEES_ACTIONS } from '../constants';

import { SETTINGS } from './config';
import { REDUCER_KEY, FIELD_ID, ACTIONS, LOCALE_KEY } from './constants';

const appSettingsSelector = state => state[APP_REDUCER_KEY].settings[REDUCER_KEY];
const settingsSelector = state => state[REDUCER_KEY].settings;
const docEmployeesInfoSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.EMPLOYEES_INFO];

function* mountSaga() {
  try {
    const settings = yield select(appSettingsSelector);
    yield put({ type: ACTIONS.MOUNT_SUCCESS, payload: { settings: settings || SETTINGS } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsSaga() {
  const settings = yield select(settingsSelector);
  yield put({ type: APP_ACTIONS.SAVE_SETTINGS_REQUEST, payload: { key: REDUCER_KEY, data: settings } });
}

function* changeSaga({ payload }) {
  const { id, value } = payload;
  const settings = yield select(settingsSelector);
  switch (id) {
    case FIELD_ID.FILE:
      yield put({ type: ACTIONS.IMPORT_REQUEST, payload: { file: value[0].file } });
      break;
    case FIELD_ID.ENCODING:
    case FIELD_ID.SEPARATOR:
    default:
      yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings: { ...settings, [id]: value } } });
  }
}

function* importRequestSaga({ payload }) {
  try {
    const { file } = payload;
    const settings = yield select(settingsSelector);
    const docEmployeesInfo = yield select(docEmployeesInfoSelector);
    const result = yield call(dalCSVImport, file, {
      encoding: settings[FIELD_ID.ENCODING],
      separator: settings[FIELD_ID.SEPARATOR],
      lastNpp: docEmployeesInfo.length,
    });
    yield put({ type: ACTIONS.IMPORT_SUCCESS, payload: result });
    if (result.employeesInfo && result.employeesInfo.length) {
      yield put({ type: DOC_ACTIONS.ADD_EMPLOYEES, payload: { employeesInfo: result.employeesInfo } });
      yield call(notificationSuccess, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SUCCESS.TITLE),
        message: localization.translate(LOCALE_KEY.NOTIFICATIONS.SUCCESS.MESSAGE, {
          value: result.employeesInfo.length,
        }),
      });
      yield put({ type: EMPLOYEES_ACTIONS.GET_LIST_REQUEST });
    }
    if (!result.errors.length) {
      yield put({ type: ACTIONS.CLOSE });
    }
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.IMPORT_FAIL] });
  }
}

export default function* sagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS, saveSettingsSaga);
  yield takeEvery(ACTIONS.CHANGE, changeSaga);
  yield takeEvery(ACTIONS.IMPORT_REQUEST, importRequestSaga);
}
