import { ACTIONS } from './constants';

export const mountAction = () => ({
  type: ACTIONS.MOUNT_REQUEST,
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const openAction = () => ({
  type: ACTIONS.OPEN,
});

export const closeAction = () => ({
  type: ACTIONS.CLOSE,
});

export const changeAction = (id, value) => ({
  type: ACTIONS.CHANGE,
  payload: { id, value },
});

export const importRequestAction = () => ({
  type: ACTIONS.IMPORT_REQUEST,
});

export const importSuccessAction = () => ({
  type: ACTIONS.IMPORT_SUCCESS,
});

export const importFailAction = errors => ({
  type: ACTIONS.IMPORT_FAIL,
  payload: { errors },
});

export const activateErrorAction = id => ({
  type: ACTIONS.ACTIVATE_ERROR,
  payload: { id },
});
