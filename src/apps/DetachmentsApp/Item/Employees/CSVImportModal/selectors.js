import { createSelector } from 'reselect';

import localization from 'localization';

import { LABELS } from './config';
import { REDUCER_KEY, FIELD_ID } from './constants';

const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const encodingsSelector = state => state[REDUCER_KEY].encodings;
const settingsSelector = state => state[REDUCER_KEY].settings;
const errorsSelector = state => state[REDUCER_KEY].errors;
const activeErrorsSelector = state => state[REDUCER_KEY].activeError;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isVisibleCreatedSelector = createSelector(
  isMountedSelector,
  isVisibleSelector,
  (isMounted, isVisible) => isMounted && isVisible,
);

const valuesCreatedSelector = createSelector(
  settingsSelector,
  settings => ({
    [FIELD_ID.FILE]: [],
    [FIELD_ID.ENCODING]: settings[FIELD_ID.ENCODING],
    [FIELD_ID.SEPARATOR]: settings[FIELD_ID.SEPARATOR],
  }),
);

const optionsCreatedSelector = createSelector(
  encodingsSelector,
  encodings => ({
    [FIELD_ID.ENCODING]: encodings,
  }),
);

const errorsCreatedSelector = createSelector(
  errorsSelector,
  activeErrorsSelector,
  (errors, activeErrors) =>
    errors && errors.map(error => ({ ...error, fieldsIds: [FIELD_ID.FILE], isActive: error.id === activeErrors })),
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isVisible: isVisibleCreatedSelector(state),
  values: valuesCreatedSelector(state),
  options: optionsCreatedSelector(state),
  isLoading: isLoadingSelector(state),
  errors: errorsCreatedSelector(state),
});

export default mapStateToProps;
