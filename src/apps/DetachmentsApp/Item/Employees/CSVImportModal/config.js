import { LOCALE_KEY, FIELD_ID } from './constants';

export const LABELS = {
  HEADER: LOCALE_KEY.HEADER,
  ERRORS_TITLE: LOCALE_KEY.ERRORS_TITLE,
  ENCODING: LOCALE_KEY.ENCODING,
  SEPARATOR: LOCALE_KEY.SEPARATOR,
};

export const ENCODINGS = [
  {
    value: 'windows',
    title: 'Windows',
  },
  {
    value: 'utf-8',
    title: 'UTF-8',
  },
  {
    value: 'dos',
    title: 'DOS',
  },
];

export const SETTINGS = {
  [FIELD_ID.ENCODING]: ENCODINGS[0].value,
  [FIELD_ID.SEPARATOR]: ';',
};
