import { createSelector } from 'reselect';
import { v4 } from 'uuid';

import { CONDITIONS_TYPES } from '@rbo/rbo-components/lib/Filter/_constants';

import localization from 'localization';
import { getFormattedDate, getFormattedAccount, getStatus } from 'utils';
import { REDUCER_KEY as ROOT_REDUCER_KEY } from 'store/constants';

import { REDUCER_KEY as DOC_REDUCER_KEY } from '../constants';

import { REDUCER_KEY, COLUMN_ID, FIELD_ID } from './constants';
import { PAGINATION_OPTIONS, LABELS } from './config';
import { getAllowedOperations, mapConditionsOptions, mapFilter } from './utils';

const localeSelector = state => state[ROOT_REDUCER_KEY].locale;
const isEditableSelector = state => state[DOC_REDUCER_KEY].isEditable;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isListLoadingSelector = state => state[REDUCER_KEY].isListLoading;
const settingsSelector = state => state[REDUCER_KEY].settings;
const pagesSelector = state => state[REDUCER_KEY].pages;
const conditionsSelector = state => state[REDUCER_KEY].conditions;
const conditionsOptionsSelector = state => state[REDUCER_KEY].conditionsOptions;
const conditionsSearchValuesSelector = state => state[REDUCER_KEY].conditionsSearchValues;
const filtersSelector = state => state[REDUCER_KEY].filters;
const filterSelector = state => state[REDUCER_KEY].filter;
const columnsSelector = state => state[REDUCER_KEY].columns;
const listSelector = state => state[REDUCER_KEY].list;
const selectedSelector = state => state[REDUCER_KEY].selected;
const operationsSelector = state => state[REDUCER_KEY].operations;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isMounted => !isMounted,
);

const isListGroupedCreatedSelector = createSelector(
  settingsSelector,
  settings => !!settings.grouped,
);

const pagesCreatedSelector = createSelector(
  pagesSelector,
  settingsSelector,
  (pages, settings) => ({
    ...pages,
    settings: {
      options: PAGINATION_OPTIONS,
      value: settings.pagination,
    },
  }),
);

const conditionsCreatedSelector = createSelector(
  conditionsSelector,
  conditionsOptionsSelector,
  conditionsSearchValuesSelector,
  (conditions, conditionsOptions, conditionsSearchValues) =>
    conditions
      .map(condition => ({ ...condition, title: localization.translate(condition.titleKey) }))
      .map(condition => {
        switch (condition.type) {
          case CONDITIONS_TYPES.SELECT:
          case CONDITIONS_TYPES.MULTI_SELECT:
          case CONDITIONS_TYPES.SUGGEST:
          case CONDITIONS_TYPES.SEARCH:
            return {
              ...condition,
              options: mapConditionsOptions({
                id: condition.id,
                options: conditionsOptions[condition.id],
                searchValue: conditionsSearchValues[condition.id],
              }),
            };
          default:
            return condition;
        }
      }),
);

const filtersCreatedSelector = createSelector(
  filtersSelector,
  filters =>
    filters
      .map(item => ({
        ...item,
        title: (item.isPreset ? localization.translate(item.titleKey) : item.title) || '',
      }))
      .sort((a, b) => {
        if (a.isPreset && b.isPreset) return 0;
        if (a.isPreset && !b.isPreset) return -1;
        if (!a.isPreset && b.isPreset) return 1;
        return a.title.localeCompare(b.title);
      }),
);

const filterCreatedSelector = createSelector(
  filterSelector,
  conditionsSelector,
  conditionsOptionsSelector,
  (filter, conditions, conditionsOptions) => mapFilter({ filter, conditions, conditionsOptions }),
);

const columnsCreatedSelector = createSelector(
  columnsSelector,
  settingsSelector,
  (columns, settings) =>
    columns.map(item => ({
      id: item.id,
      label: localization.translate(item.labelKey),
      sorting: settings.sorting.id === item.id ? settings.sorting.direction : 0,
      width: settings.width[item.id],
      align: item.align,
      grouping: item.grouping,
      isGrouped: settings.grouped === item.id,
      isVisible: settings.visibility.includes(item.id),
    })),
);

const getListItem = ({ item, isSelected, operations }) => {
  const status = getStatus.employee.byServerValue(item[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]);
  return {
    id: item[FIELD_ID.ID],
    [COLUMN_ID.NPP]: typeof item[FIELD_ID.NPP] === 'number' ? item[FIELD_ID.NPP].toString() : null,
    [COLUMN_ID.EMPLOYEE_NUM]: item[FIELD_ID.EMPLOYEE_NUM],
    [COLUMN_ID.EMPLOYEE_SURNAME]: item[FIELD_ID.EMPLOYEE_SURNAME],
    [COLUMN_ID.EMPLOYEE_NAME]: item[FIELD_ID.EMPLOYEE_NAME],
    [COLUMN_ID.EMPLOYEE_PATRONYMIC]: item[FIELD_ID.EMPLOYEE_PATRONYMIC],
    [COLUMN_ID.EMPLOYEE_BIRTH_DATE]: getFormattedDate(item[FIELD_ID.EMPLOYEE_BIRTH_DATE]),
    [COLUMN_ID.ACCOUNT]: getFormattedAccount(item[FIELD_ID.ACCOUNT]),
    [COLUMN_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]: status ? localization.translate(status.titleKey) : null,
    statusFlags: {
      isSuccess: status ? getStatus.employee.isSuccess(status.id) : false,
      isWarning: status ? getStatus.employee.isWarning(status.id) : false,
      isError: status ? getStatus.employee.isError(status.id) : false,
    },
    [COLUMN_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]:
      item[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK],
    [COLUMN_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]: item[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C],
    operations: getAllowedOperations({ operations }).reduce(
      (result, operation) => ({ ...result, [operation.id]: true }),
      {},
    ),
    isSelected,
  };
};

const listCreatedSelector = createSelector(
  listSelector,
  selectedSelector,
  settingsSelector,
  operationsSelector,
  (list, selected, settings, operations) => {
    if (!list.length) return list;

    const mappedList = list.map(item =>
      getListItem({
        item,
        isSelected: selected.includes(item[FIELD_ID.ID]),
        operations: operations.map(operation => ({ ...operation, title: localization.translate(operation.titleKey) })),
      }),
    );
    if (!settings.grouped) return mappedList;

    const groups = mappedList.map(item => item[settings.grouped]).filter((v, i, a) => a.indexOf(v) === i);
    return groups.map(group => ({
      id: v4(group),
      title: group,
      items: mappedList.filter(item => item[settings.grouped] === group),
    }));
  },
);

const operationsCreatedSelector = createSelector(
  localeSelector,
  operationsSelector,
  isEditableSelector,
  (locale, operations, isEditable) =>
    isEditable
      ? operations.map(operation => ({ ...operation, title: localization.translate(operation.titleKey) }))
      : [],
);

const toolbarOperationsCreatedSelector = createSelector(
  operationsSelector,
  listSelector,
  selectedSelector,
  isEditableSelector,
  (operations, list, selected, isEditable) => {
    if (!isEditable) return [];

    const selectedList = list
      .filter(item => selected.includes(item[FIELD_ID.ID]))
      .map(item => ({
        id: item[FIELD_ID.ID],
        operations: getAllowedOperations({ operations }).map(operation => operation.id),
      }));
    if (!selectedList.length) {
      return operations
        .filter(operation => !operation.selected)
        .map(operation => ({ ...operation, title: localization.translate(operation.titleKey) }));
    }
    return operations
      .filter(operation => operation.selected && (selectedList.length === 1 || operation.grouped))
      .filter(
        operation => selectedList.length === selectedList.filter(item => item.operations.includes(operation.id)).length,
      )
      .map(operation => ({ ...operation, title: localization.translate(operation.titleKey) }));
  },
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isLoading: isLoadingCreatedSelector(state),
  isEditable: isEditableSelector(state),
  isListLoading: isListLoadingSelector(state),
  isListGrouped: isListGroupedCreatedSelector(state),
  pages: pagesCreatedSelector(state),
  conditions: conditionsCreatedSelector(state),
  filters: filtersCreatedSelector(state),
  filter: filterCreatedSelector(state),
  columns: columnsCreatedSelector(state),
  list: listCreatedSelector(state),
  operations: operationsCreatedSelector(state),
  toolbarOperations: toolbarOperationsCreatedSelector(state),
});

export default mapStateToProps;
