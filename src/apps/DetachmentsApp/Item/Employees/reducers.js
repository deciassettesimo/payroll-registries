import { v4 } from 'uuid';

import { REDUCER_KEY, OPERATION_ID, ACTIONS } from './constants';
import { SETTINGS, SYSTEM_FILTERS, PAGES, COLUMNS, CONDITIONS, CONDITIONS_OPTIONS, OPERATIONS } from './config';
import CSVImportModalReducers from './CSVImportModal/reducers';

const initialState = {
  isMounted: false,
  isListLoading: false,
  settings: SETTINGS,
  pages: PAGES,
  conditions: CONDITIONS,
  conditionsOptions: CONDITIONS_OPTIONS,
  conditionsSearchValues: {},
  filters: [],
  filter: null,
  columns: COLUMNS,
  list: [],
  selected: [],
  operations: OPERATIONS.map(item => ({ ...item, disabled: false, progress: false })),
};

const mountRequest = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, payload) => {
  const { filters, settings, conditionsOptions } = payload;

  return {
    ...state,
    isMounted: true,
    settings,
    conditionsOptions: { ...state.conditionsOptions, ...conditionsOptions },
    filters: SYSTEM_FILTERS.map(item => ({ ...item, isPreset: true })).concat(filters),
  };
};

const mountFail = (state, serverError) => ({ ...state, isMounted: true, serverError });

const unmount = () => ({ ...initialState, isMounted: false });

const saveSettings = (state, payload) =>
  payload && payload.settings ? { ...state, settings: payload.settings } : state;

const pageChange = (state, { page }) => ({ ...state, pages: { ...state.pages, selected: page } });

const paginationChange = state => ({ ...state, pages: { ...PAGES } });

const filtersRemove = (state, { id }) => {
  const filters = state.filters.filter(item => item.id !== id);
  return { ...state, filters };
};

const filtersChoose = (state, { id }) => {
  const filters = state.filters.map(item => ({ ...item, isSelected: item.id === id }));
  const filter = filters.find(item => item.isSelected);
  return { ...state, filters, filter, pages: { ...PAGES } };
};

const filtersReset = (state, isChanged) => {
  const filters = state.filters
    .filter(item => !item.isNew)
    .map(item => ({ ...item, isSelected: false, isChanged: false }));

  return { ...state, filters, filter: null, pages: isChanged ? { ...PAGES } : state.pages };
};

const filtersCreate = state => {
  const filter = { params: [], isPreset: false, isNew: true, isSelected: true, isChanged: false };
  const filters = state.filters.map(item => item);
  filters.push(filter);

  return { ...state, filters, filter };
};

const filtersSave = state => {
  const filter = { ...state.filter, isChanged: false };
  const filters = state.filters.map(item => (item.isSelected ? { ...item, ...filter } : item));

  return { ...state, filters, filter };
};

const filtersSaveAs = (state, { title }) => {
  const filter = {
    ...state.filter,
    id: v4(),
    title,
    isNew: false,
    isPreset: false,
    isSelected: true,
    isChanged: false,
  };
  const filters = state.filters.filter(item => !item.isNew).map(item => ({ ...item, isSelected: false }));
  filters.push(filter);

  return { ...state, filters, filter };
};

const filterChange = (state, { filter: params, isChanged }) => {
  if (!params || !params.length) return filtersReset(state, isChanged);
  const filters = isChanged
    ? state.filters.map(item => (item.isSelected ? { ...item, isChanged } : item))
    : state.filters;
  const filter = {
    ...state.filter,
    isChanged: filters.find(item => item.isSelected).isChanged,
    params: params.map(param => ({ ...param })),
  };
  const pages = isChanged ? { ...PAGES } : state.pages;
  return { ...state, filters, filter, pages };
};

const filterSearch = (state, { conditionId, conditionSearchValue }) => ({
  ...state,
  conditionsSearchValues: { ...state.conditionsSearchValues, [conditionId]: conditionSearchValue },
});

const filterSearchSuccess = (state, { conditionId, conditionOptions }) => ({
  ...state,
  conditionsOptions: { ...state.conditionsOptions, [conditionId]: conditionOptions },
});

const selectChange = (state, { selected }) => ({ ...state, selected });

const getListRequest = state => ({ ...state, isListLoading: true });

const getListSuccess = (state, { list, pages }) => ({ ...state, isListLoading: false, list, pages });

const getListFail = state => ({ ...state, isListLoading: false });

const operationStart = (state, operationId, disableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? true : operation.progress,
    disabled: disableOtherOperations ? true : operation.disabled,
  })),
});

const operationFinish = (state, operationId, enableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? false : operation.progress,
    disabled: enableOtherOperations ? false : operation.disabled,
  })),
});

function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.SAVE_SETTINGS:
      return saveSettings(state, action.payload);

    case ACTIONS.PAGE_CHANGE:
      return pageChange(state, action.payload);

    case ACTIONS.PAGINATION_CHANGE:
      return paginationChange(state, action.payload);

    case ACTIONS.FILTERS_REMOVE:
      return filtersRemove(state, action.payload);

    case ACTIONS.FILTERS_CHOOSE:
      return filtersChoose(state, action.payload);

    case ACTIONS.FILTERS_RESET:
      return filtersReset(state, true);

    case ACTIONS.FILTERS_CREATE:
      return filtersCreate(state);

    case ACTIONS.FILTERS_SAVE:
      return filtersSave(state);

    case ACTIONS.FILTERS_SAVE_AS:
      return filtersSaveAs(state, action.payload);

    case ACTIONS.FILTER_CHANGE:
      return filterChange(state, action.payload);

    case ACTIONS.FILTER_SEARCH:
      return filterSearch(state, action.payload);

    case ACTIONS.FILTER_SEARCH_SUCCESS:
      return filterSearchSuccess(state, action.payload);

    case ACTIONS.SELECT_CHANGE:
      return selectChange(state, action.payload);

    case ACTIONS.GET_LIST_REQUEST:
      return getListRequest(state);

    case ACTIONS.GET_LIST_SUCCESS:
      return getListSuccess(state, action.payload);

    case ACTIONS.GET_LIST_FAIL:
      return getListFail(state);

    case ACTIONS.OPERATION_REMOVE:
      return operationStart(state, OPERATION_ID.REMOVE, true);
    case ACTIONS.OPERATION_REMOVE_SUCCESS:
    case ACTIONS.OPERATION_REMOVE_CANCEL:
    case ACTIONS.OPERATION_REMOVE_FAIL:
      return operationFinish(state, OPERATION_ID.REMOVE, true);

    default:
      return state;
  }
}

export default {
  [REDUCER_KEY]: reducer,
  ...CSVImportModalReducers,
};
