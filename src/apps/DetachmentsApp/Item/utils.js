import { escapeRegExp, getDictionaryValue } from 'utils';

import { FIELD_ID } from './constants';

export function mapDictionaryItems(dictionary, key) {
  const { items } = dictionary;
  const searchValue = escapeRegExp(dictionary.searchValue);
  let searchRegExp;
  switch (key) {
    case FIELD_ID.BRANCH_INFO_NAME:
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.shortName,
            describe: item.bic,
          }))
          .filter(item => item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case FIELD_ID.OFFICIAL_NAME:
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item.id,
            title: item.fio,
            describe: item.phone,
          }))
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    case 'templates':
      searchRegExp = new RegExp(searchValue, 'ig');
      return {
        ...dictionary,
        items: items
          .map(item => ({
            value: item[FIELD_ID.ID],
            title: item[FIELD_ID.TEMPLATE_NAME],
            describe: item[FIELD_ID.CUSTOMER_NAME],
          }))
          .filter(item => item.title.search(searchRegExp) >= 0)
          .sort((a, b) => a.title.localeCompare(b.title)),
      };
    default:
      return dictionary;
  }
}

export const changeState = {
  /** Подразделение */
  [FIELD_ID.BRANCH_INFO_NAME]: (state, { fieldValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.BRANCH_INFO_NAME;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');
    const branchInfoName = (selectedDictionaryItem && selectedDictionaryItem.shortName) || null;

    if (branchInfoName !== state.documentData[fieldId]) {
      const branchInfoId = selectedDictionaryItem ? selectedDictionaryItem.id : null;
      changedFields = {
        [fieldId]: branchInfoName,
        [FIELD_ID.BRANCH_INFO_ID]: branchInfoId,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },

  /** Исполнитель */
  [FIELD_ID.OFFICIAL_NAME]: (state, { fieldValue, dictionaryValue }) => {
    let changedFields = {};
    const fieldId = FIELD_ID.OFFICIAL_NAME;

    if (dictionaryValue) {
      const selectedDictionaryItem = getDictionaryValue(dictionaryValue, state.documentDictionaries[fieldId], 'id');
      const officialName = (selectedDictionaryItem && selectedDictionaryItem.fio) || null;
      const officialPhone = (selectedDictionaryItem && selectedDictionaryItem.phone) || null;
      changedFields = {
        [fieldId]: officialName,
        [FIELD_ID.OFFICIAL_PHONE]: officialPhone,
      };
    } else {
      changedFields = {
        [fieldId]: fieldValue,
      };
    }

    return {
      ...state,
      documentData: {
        ...state.documentData,
        ...changedFields,
      },
    };
  },
};
