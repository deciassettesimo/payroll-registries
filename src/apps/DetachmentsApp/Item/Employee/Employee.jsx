import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Form from '@rbo/rbo-components/lib/Form';
import Header from '@rbo/rbo-components/lib/Styles/Header';
import Link from '@rbo/rbo-components/lib/Styles/Link';
import Loader from '@rbo/rbo-components/lib/Loader/index';
import OperationsPanel from '@rbo/rbo-components/lib/OperationsPanel';
import RboDocForm from '@rbo/rbo-components/lib/RboDocForm';
import RboDocHeader, { Row } from '@rbo/rbo-components/lib/RboDocHeader';
import RboDocLayout, { Panel, Content } from '@rbo/rbo-components/lib/RboDocLayout';
import RboDocStructure from '@rbo/rbo-components/lib/RboDocStructure';
import RboPopupBoxOption from '@rbo/rbo-components/lib/RboPopupBoxOption';

import RboServerError from 'common/components/RboServerError';

import { TAB_ID, SECTION_ID, FIELD_ID, OPERATION_ID } from './constants';
import * as Sections from './sections';

const popupBoxOptionRenderer = ({ id, option }) => {
  switch (id) {
    case FIELD_ID.SEARCH_EMPLOYEE:
      return (
        <RboPopupBoxOption
          title={option.title}
          extra={option.extra}
          describe={option.describe}
          highlight={{ value: option.searchValue, inTitle: true, inExtra: true }}
        />
      );
    default:
      return <RboPopupBoxOption isInline label={option.title} />;
  }
};

export default class Employee extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape({
      LOCALE: PropTypes.string.isRequired,
    }).isRequired,
    isError: PropTypes.bool.isRequired,
    serverError: PropTypes.shape(),
    isLoading: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    operations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    documentStructure: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    documentSections: PropTypes.shape().isRequired,
    documentData: PropTypes.shape().isRequired,
    documentDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
    handleStructureTabClick: PropTypes.func.isRequired,
    handleStructureSectionClick: PropTypes.func.isRequired,
    handleStructureErrorClick: PropTypes.func.isRequired,
    handleOperationClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    serverError: null,
  };

  handleDocLinkClick = () => {
    const { handleOperationClick } = this.props;
    handleOperationClick(OPERATION_ID.BACK);
  };

  render() {
    const {
      LABELS,
      isError,
      serverError,
      isLoading,
      isEditable,
      operations,
      documentStructure,
      documentSections,
      documentData,
      documentDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
      handleStructureTabClick,
      handleStructureSectionClick,
      handleStructureErrorClick,
      handleOperationClick,
    } = this.props;

    if (isError) return <RboServerError serverData={serverError} />;
    if (isLoading) return <Loader dimension={Loader.REFS.DIMENSIONS.L} isCentered />;

    return (
      <RboDocLayout>
        <Panel>
          <OperationsPanel
            locale={LABELS.LOCALE}
            dimension={OperationsPanel.REFS.DIMENSIONS.XS}
            items={operations}
            onItemClick={handleOperationClick}
          />
        </Panel>
        <Content maxWidth={1280}>
          <Content.Main>
            <RboDocHeader>
              <Row>
                <Header size={3}>
                  <Link isPseudo onClick={this.handleDocLinkClick}>
                    {LABELS.HEADER.DOC_TITLE}
                  </Link>
                  {' / '}
                  {LABELS.HEADER.TITLE}
                </Header>
              </Row>
            </RboDocHeader>
            <RboDocForm>
              <Form dimension={Form.REFS.DIMENSIONS.XS}>
                {documentSections[SECTION_ID.SEARCH_EMPLOYEE].isVisible && (
                  <Sections.SearchEmployee
                    LABELS={LABELS}
                    sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.SEARCH_EMPLOYEE]}
                    sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.SEARCH_EMPLOYEE]}
                    formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.SEARCH_EMPLOYEE]}
                    formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.SEARCH_EMPLOYEE]}
                    formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.SEARCH_EMPLOYEE]}
                    handleFocus={handleFocus}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    handleSearch={handleSearch}
                    optionRenderer={popupBoxOptionRenderer}
                  />
                )}
                {documentSections[SECTION_ID.COMMON_INFO].isVisible && (
                  <Sections.CommonInfo
                    LABELS={LABELS}
                    isEditable={isEditable}
                    sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
                    sectionDictionaries={documentDictionaries[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
                    formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
                    formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
                    formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.COMMON_INFO]}
                    handleFocus={handleFocus}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    handleSearch={handleSearch}
                    optionRenderer={popupBoxOptionRenderer}
                  />
                )}
                {documentSections[SECTION_ID.FROM_BANK_INFO].isVisible && (
                  <Sections.FromBankInfo
                    LABELS={LABELS}
                    sectionData={documentData[TAB_ID.MAIN_FIELDS][SECTION_ID.FROM_BANK_INFO]}
                    formWarnings={formWarnings[TAB_ID.MAIN_FIELDS][SECTION_ID.FROM_BANK_INFO]}
                    formErrors={formErrors[TAB_ID.MAIN_FIELDS][SECTION_ID.FROM_BANK_INFO]}
                    formDisabled={formDisabled[TAB_ID.MAIN_FIELDS][SECTION_ID.FROM_BANK_INFO]}
                    handleFocus={handleFocus}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                  />
                )}
              </Form>
            </RboDocForm>
          </Content.Main>
          <Content.Extra>
            <Content.Extra.Section title={LABELS.EXTRA.STRUCTURE}>
              <RboDocStructure
                structure={documentStructure}
                onTabClick={handleStructureTabClick}
                onSectionClick={handleStructureSectionClick}
                onErrorClick={handleStructureErrorClick}
              />
            </Content.Extra.Section>
          </Content.Extra>
        </Content>
      </RboDocLayout>
    );
  }
}
