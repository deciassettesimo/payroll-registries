import { getDictionaryValue, getFormattedDate } from 'utils';

import { FIELD_ID } from './constants';

export function mapDictionaryItems(dictionary, key) {
  const { items } = dictionary;
  switch (key) {
    case FIELD_ID.SEARCH_EMPLOYEE:
      return {
        ...dictionary,
        items: items.map(item => ({
          value: item.id,
          title: `${item.employee.surname || ''} ${item.employee.name || ''} ${item.employee.patronymic || ''}`,
          extra: item.employee.employeeNum,
          describe: getFormattedDate(item.employee.birthDate),
        })),
      };
    default:
      return dictionary;
  }
}

export const changeState = {
  /** Поиск сотрудника */
  [FIELD_ID.SEARCH_EMPLOYEE]: (state, { fieldValue }) => {
    const fieldId = FIELD_ID.SEARCH_EMPLOYEE;
    const selectedDictionaryItem = getDictionaryValue(fieldValue, state.documentDictionaries[fieldId], 'id');

    if (selectedDictionaryItem) {
      const changedFields = {
        [FIELD_ID.EMPLOYEE_NUM]: selectedDictionaryItem.employee.employeeNum,
        [FIELD_ID.EMPLOYEE_SURNAME]: selectedDictionaryItem.employee.surname,
        [FIELD_ID.EMPLOYEE_NAME]: selectedDictionaryItem.employee.name,
        [FIELD_ID.EMPLOYEE_PATRONYMIC]: selectedDictionaryItem.employee.patronymic,
        [FIELD_ID.EMPLOYEE_BIRTH_DATE]: selectedDictionaryItem.employee.birthDate,
        [FIELD_ID.ACCOUNT]: selectedDictionaryItem.employee.recipientAcc.account,
      };
      return {
        ...state,
        documentData: {
          ...state.documentData,
          ...changedFields,
        },
        documentDictionaries: {
          ...state.documentDictionaries,
          [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
        },
      };
    }

    return {
      ...state,
      documentDictionaries: {
        ...state.documentDictionaries,
        [fieldId]: { ...state.documentDictionaries[fieldId], searchValue: '' },
      },
    };
  },
};
