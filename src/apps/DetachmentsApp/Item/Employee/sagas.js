import { call, put, select, takeEvery } from 'redux-saga/effects';

import { ROUTE_LEAVE_CONFIRM_ENABLE, DOC_NEW_ID } from 'common/constants';
import operations from 'common/operations';
import { openAction as confirmModalOpenAction } from 'common/components/RboConfirmModal/actions';
import { getList as dalGetEmployees } from 'dal/employees';
import { getEmployeeErrors as dalGetEmployeeErrors } from 'dal/payroll-registries-detachments';
import localization from 'localization';
import { notificationError, notificationSuccess, notificationWarning } from 'platform';
import { dispatchErrorAction, windowRefresh } from 'utils';

import { REDUCER_KEY as DOC_REDUCER_KEY, FIELD_ID as DOC_FIELD_ID, ACTIONS as DOC_ACTIONS } from '../constants';

import { OPERATIONS_ACTIONS_MAP } from './config';
import { REDUCER_KEY, FIELD_ID, LOCALE_KEY, ACTIONS } from './constants';

const docIsEditableSelector = state => state[DOC_REDUCER_KEY].isEditable;
const docDocumentDataSelector = state => state[DOC_REDUCER_KEY].documentData;
const docOrgIdSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.CUSTOMER_ID];
const docEmployeesInfoSelector = state => state[DOC_REDUCER_KEY].documentData[DOC_FIELD_ID.EMPLOYEES_INFO];
const docEditedEmployeeSelector = state => state[DOC_REDUCER_KEY].editedEmployee;
const isSavedSelector = state => state[REDUCER_KEY].isSaved;
const documentDataSelector = state => state[REDUCER_KEY].documentData;

function* getData({ id }) {
  yield put({ type: ACTIONS.GET_DATA_REQUEST });
  const docEmployeesInfo = yield select(docEmployeesInfoSelector);

  let documentData = {};

  try {
    if (id === DOC_NEW_ID) {
      documentData = {
        [FIELD_ID.NPP]: docEmployeesInfo.length + 1,
      };
    } else {
      documentData = docEmployeesInfo.find(item => item[FIELD_ID.ID] === id);
    }
    const isEditable = yield select(docIsEditableSelector);

    yield put({ type: ACTIONS.GET_DATA_SUCCESS });
    return { documentData, isEditable };
  } catch (error) {
    yield put({ type: ACTIONS.GET_DATA_FAIL, error });
    throw error;
  }
}

function* saveData(channels) {
  yield put({ type: ACTIONS.SAVE_DATA_REQUEST });
  const documentData = yield select(documentDataSelector);
  try {
    const parentDocumentData = yield select(docDocumentDataSelector);
    const errorsResult = yield call(dalGetEmployeeErrors, {
      ...parentDocumentData,
      [DOC_FIELD_ID.EMPLOYEES_INFO]: [documentData],
    });

    if (errorsResult.errors && errorsResult.errors.filter(error => ['2', '3'].includes(error.level)).length) {
      throw new operations.RBOControlsError({ data: errorsResult });
    }
    if (errorsResult.errors && errorsResult.errors.filter(error => ['1'].includes(error.level)).length) {
      yield call(notificationWarning, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.SUCCESS_WITH_WARNINGS.TITLE),
      });
    } else {
      yield call(notificationSuccess, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.SUCCESS.TITLE),
      });
    }
    yield put({ type: ACTIONS.SAVE_DATA_SUCCESS, payload: errorsResult });
    yield put({ type: channels.success });
  } catch (error) {
    if (error instanceof operations.RBOControlsError) {
      yield put({ type: ACTIONS.SAVE_DATA_FAIL, payload: error.response.data });
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.RBO_CONTROLS.TITLE),
        message: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.RBO_CONTROLS.MESSAGE),
      });
    } else {
      yield put({ type: ACTIONS.SAVE_DATA_FAIL });
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SAVE.FAIL.TITLE),
      });
    }
    yield put({ type: channels.fail });
  }
}

function* refreshSaga() {
  const id = yield select(docEditedEmployeeSelector);
  try {
    const data = yield call(getData, { id });
    const { documentData } = data;
    yield put({ type: ACTIONS.REFRESH_SUCCESS, payload: { documentData } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.REFRESH_FAIL] });
  }
}

function* mountSaga() {
  const id = yield select(docEditedEmployeeSelector);

  try {
    const data = yield call(getData, { id: id || DOC_NEW_ID });
    const { documentData, isEditable } = data;

    let documentErrors = [];
    if (isEditable && id !== DOC_NEW_ID) {
      const parentDocumentData = yield select(docDocumentDataSelector);
      const errorsResult = yield call(dalGetEmployeeErrors, {
        ...parentDocumentData,
        [DOC_FIELD_ID.EMPLOYEES_INFO]: [documentData],
      });
      documentErrors = errorsResult.errors;
    }

    yield put({
      type: ACTIONS.MOUNT_SUCCESS,
      payload: {
        isEditable,
        documentData,
        documentErrors,
        documentDictionaries: {},
      },
    });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  } finally {
    windowRefresh();
  }
}

function* dictionarySearchSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  if ([FIELD_ID.SEARCH_EMPLOYEE].includes(dictionaryId)) {
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_REQUEST, payload: { dictionaryId, dictionarySearchValue } });
  }
}

function* dictionarySearchRequestSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  const orgId = yield select(docOrgIdSelector);
  let dictionaryItems;
  try {
    switch (dictionaryId) {
      case FIELD_ID.SEARCH_EMPLOYEE:
        dictionaryItems = yield call(
          dalGetEmployees,
          {
            paging: { offset: 0, limit: 30 },
            sorting: { column: 'employee.surname', desc: false },
            filter: {
              customer: { id: [orgId] },
              surnameTab: dictionarySearchValue && dictionarySearchValue.split(' ')[0],
            },
          },
          true,
        );
        break;
      default:
        break;
    }
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_SUCCESS, payload: { dictionaryId, dictionaryItems } });
  } catch (error) {
    yield dispatchErrorAction({
      error,
      types: [ACTIONS.DICTIONARY_SEARCH_FAIL, ACTIONS.DICTIONARY_SEARCH_CANCEL],
      payload: { dictionaryId },
    });
  }
}

function* operationsSaga({ payload }) {
  const { operationId } = payload;
  yield put({ type: OPERATIONS_ACTIONS_MAP[operationId], payload });
}

function* operationBackSaga() {
  const isSaved = yield select(isSavedSelector);
  if (ROUTE_LEAVE_CONFIRM_ENABLE && !isSaved) {
    yield put(
      confirmModalOpenAction({
        message: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_MESSAGE),
        confirm: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CONFIRM),
        cancel: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CANCEL),
        channels: {
          confirm: { type: ACTIONS.OPERATION_BACK_CONFIRM },
          cancel: { type: ACTIONS.OPERATION_BACK_CANCEL },
        },
      }),
    );
  } else {
    yield put({ type: ACTIONS.OPERATION_BACK_CONFIRM });
  }
}

function* operationBackConfirmSaga() {
  yield put({ type: DOC_ACTIONS.CLOSE_EMPLOYEE });
}

function* operationSaveSaga() {
  yield call(saveData, { success: ACTIONS.OPERATION_SAVE_SUCCESS, fail: ACTIONS.OPERATION_SAVE_FAIL });
}

function* operationSaveSuccessSaga() {
  const documentData = yield select(documentDataSelector);
  yield put({ type: DOC_ACTIONS.SAVE_EMPLOYEE, payload: documentData });
  yield put({ type: DOC_ACTIONS.CLOSE_EMPLOYEE });
}

function* operationSaveAndAddSaga() {
  yield call(saveData, { success: ACTIONS.OPERATION_SAVE_AND_ADD_SUCCESS, fail: ACTIONS.OPERATION_SAVE_AND_ADD_FAIL });
}

function* operationSaveAndAddSuccessSaga() {
  const documentData = yield select(documentDataSelector);
  yield put({ type: DOC_ACTIONS.SAVE_EMPLOYEE, payload: documentData });
  yield put({ type: DOC_ACTIONS.OPEN_EMPLOYEE, payload: { id: DOC_NEW_ID } });
  yield put({ type: ACTIONS.MOUNT_REQUEST });
}

export default function* sagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.REFRESH, refreshSaga);

  yield takeEvery(ACTIONS.DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeEvery(ACTIONS.DICTIONARY_SEARCH_REQUEST, dictionarySearchRequestSaga);

  yield takeEvery(ACTIONS.OPERATIONS, operationsSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK, operationBackSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK_CONFIRM, operationBackConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE, operationSaveSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AND_ADD, operationSaveAndAddSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AND_ADD_SUCCESS, operationSaveAndAddSuccessSaga);
}
