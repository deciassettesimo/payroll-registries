import { createSelector } from 'reselect';

import localization from 'localization';
import {
  getDocFormErrors,
  getFormattedAccount,
  getFormattedDate,
  mapDataToStructure,
  mapDocErrorsToSections,
  mapObject,
} from 'utils';

import { REDUCER_KEY as DOC_REDUCER_KEY, FIELD_ID as DOC_FIELD_ID } from '../constants';

import { REDUCER_KEY, SECTION_ID, FIELD_ID, OPERATION_ID } from './constants';
import { STRUCTURE, LABELS, ERRORS } from './config';
import { mapDictionaryItems } from './utils';

const docDocumentDataSelector = state => state[DOC_REDUCER_KEY].documentData;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isErrorSelector = state => state[REDUCER_KEY].isError;
const serverErrorSelector = state => state[REDUCER_KEY].serverError;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const isEditableSelector = state => state[REDUCER_KEY].isEditable;
const isDisabledSelector = state => state[REDUCER_KEY].isDisabled;
const operationsSelector = state => state[REDUCER_KEY].operations;
const activeFieldSelector = state => state[REDUCER_KEY].activeField;
const activeErrorSelector = state => state[REDUCER_KEY].activeError;
const documentStructureSelector = state => state[REDUCER_KEY].documentStructure;
const documentDataSelector = state => state[REDUCER_KEY].documentData;
const documentDictionariesSelector = state => state[REDUCER_KEY].documentDictionaries;
const documentErrorsSelector = state => state[REDUCER_KEY].documentErrors;

const labelsCreatedSelector = createSelector(
  docDocumentDataSelector,
  docDocumentData => ({
    LOCALE: localization.getLocale(),
    ...localization.translate(LABELS, {
      registryOnDetachmentsTitleNumber: docDocumentData[DOC_FIELD_ID.NUMBER],
    }),
  }),
);

const isErrorCreatedSelector = createSelector(
  isMountedSelector,
  isErrorSelector,
  (isMounted, isError) => isMounted && isError,
);

const serverErrorCreatedSelector = createSelector(
  serverErrorSelector,
  serverError => {
    if (!serverError) return null;
    return {
      status: serverError.status,
      statusText: serverError.statusText,
      url: serverError.config && serverError.config.url,
      response: serverError.data && serverError.data.error,
    };
  },
);

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isLoadingSelector,
  (isMounted, isLoading) => !isMounted || isLoading,
);

const operationsCreatedSelector = createSelector(
  operationsSelector,
  isEditableSelector,
  (operations, isEditable) =>
    operations
      .filter(operation => operation.id === OPERATION_ID.BACK || isEditable)
      .map(operation => ({
        ...operation,
        title: operation.noTitle ? null : localization.translate(operation.titleKey),
      })),
);

const documentStructureCreatedSelector = createSelector(
  activeFieldSelector,
  activeErrorSelector,
  documentStructureSelector,
  documentErrorsSelector,
  isEditableSelector,
  (activeField, activeError, documentStructure, documentErrors, isEditable) =>
    documentStructure.map(item => ({
      id: item.id,
      title: localization.translate(item.titleKey),
      isActive: item.isActive,
      sections: item.sections
        ? mapDocErrorsToSections(
            item.sections.map(section => ({
              ...section,
              title: section.titleKey && localization.translate(section.titleKey),
              isVisible: section.id === SECTION_ID.SEARCH_EMPLOYEE ? isEditable : true,
            })),
            documentErrors,
            ERRORS,
            activeField,
            activeError,
          )
        : null,
    })),
);

const documentSectionsCreatedSelector = createSelector(
  documentStructureSelector,
  isEditableSelector,
  (documentStructure, isEditable) =>
    documentStructure
      .reduce((result, item) => (item.sections ? result.concat(item.sections) : result), [])
      .reduce(
        (result, section) => ({
          ...result,
          [section.id]: {
            isVisible: section.id === SECTION_ID.SEARCH_EMPLOYEE ? isEditable : true,
          },
        }),
        {},
      ),
);

const documentDataCreatedSelector = createSelector(
  isEditableSelector,
  documentDataSelector,
  (isEditable, documentData) =>
    mapDataToStructure(STRUCTURE, {
      [FIELD_ID.SEARCH_EMPLOYEE]: '',
      [FIELD_ID.NPP]: documentData[FIELD_ID.NPP] && documentData[FIELD_ID.NPP].toString(),
      [FIELD_ID.EMPLOYEE_NUM]: documentData[FIELD_ID.EMPLOYEE_NUM],
      [FIELD_ID.EMPLOYEE_SURNAME]: documentData[FIELD_ID.EMPLOYEE_SURNAME],
      [FIELD_ID.EMPLOYEE_NAME]: documentData[FIELD_ID.EMPLOYEE_NAME],
      [FIELD_ID.EMPLOYEE_PATRONYMIC]: documentData[FIELD_ID.EMPLOYEE_PATRONYMIC],
      [FIELD_ID.EMPLOYEE_BIRTH_DATE]: isEditable
        ? documentData[FIELD_ID.EMPLOYEE_BIRTH_DATE]
        : getFormattedDate(documentData[FIELD_ID.EMPLOYEE_BIRTH_DATE]),
      [FIELD_ID.ACCOUNT]: isEditable
        ? documentData[FIELD_ID.ACCOUNT]
        : getFormattedAccount(documentData[FIELD_ID.ACCOUNT]),
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]: documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]: documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C],
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]:
        documentData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK],
    }),
);

const documentDictionariesCreatedSelector = createSelector(
  documentDictionariesSelector,
  documentDataSelector,
  (documentDictionaries, documentData) =>
    mapDataToStructure(
      STRUCTURE,
      mapObject((item, key) => mapDictionaryItems(item, key, documentData), documentDictionaries),
    ),
);

const formWarningsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['1'])),
);

const formErrorsCreatedSelector = createSelector(
  documentErrorsSelector,
  documentErrors => mapDataToStructure(STRUCTURE, getDocFormErrors(documentErrors, ERRORS, ['2', '3'])),
);

const formDisabledCreatedSelector = createSelector(
  isDisabledSelector,
  isDisabled =>
    mapDataToStructure(STRUCTURE, {
      [FIELD_ID.SEARCH_EMPLOYEE]: isDisabled,
      [FIELD_ID.NPP]: isDisabled,
      [FIELD_ID.EMPLOYEE_NUM]: isDisabled,
      [FIELD_ID.EMPLOYEE_SURNAME]: isDisabled,
      [FIELD_ID.EMPLOYEE_NAME]: isDisabled,
      [FIELD_ID.EMPLOYEE_PATRONYMIC]: isDisabled,
      [FIELD_ID.EMPLOYEE_BIRTH_DATE]: isDisabled,
      [FIELD_ID.ACCOUNT]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]: isDisabled,
      [FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]: isDisabled,
    }),
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isError: isErrorCreatedSelector(state),
  serverError: serverErrorCreatedSelector(state),
  isLoading: isLoadingCreatedSelector(state),
  isEditable: isEditableSelector(state),
  operations: operationsCreatedSelector(state),
  documentStructure: documentStructureCreatedSelector(state),
  documentSections: documentSectionsCreatedSelector(state),
  documentData: documentDataCreatedSelector(state),
  documentDictionaries: documentDictionariesCreatedSelector(state),
  formWarnings: formWarningsCreatedSelector(state),
  formErrors: formErrorsCreatedSelector(state),
  formDisabled: formDisabledCreatedSelector(state),
});

export default mapStateToProps;
