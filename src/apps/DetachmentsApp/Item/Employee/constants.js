import { OPERATION_ID as COMMON_OPERATION_ID, OPERATIONS_ACTIONS_TYPES } from 'common/constants';
import { EMPLOYEES_INFO_ITEM_FIELD_ID as DAL_FIELD_ID } from 'dal/payroll-registries-detachments/constants';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesDetachmentsItemEmployee';

export const TAB_ID = keyMirrorWithPrefix(
  {
    MAIN_FIELDS: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_EMPLOYEE_',
);

export const SECTION_ID = keyMirrorWithPrefix(
  {
    SEARCH_EMPLOYEE: null,
    COMMON_INFO: null,
    FROM_BANK_INFO: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_EMPLOYEE_',
);

export const FIELD_ID = {
  ...DAL_FIELD_ID,
  ...keyMirrorWithPrefix({ SEARCH_EMPLOYEE: null }),
};

export const OPERATION_ID = {
  ...COMMON_OPERATION_ID,
  SAVE_AND_ADD: 'SAVE_AND_ADD',
};

export const LOCALE_KEY = {
  HEADER: {
    DOC_TITLE: 'loc.registryOnDetachmentsTitle',
    TITLE: 'loc.employeeInformation',
  },
  EXTRA: {
    STRUCTURE: 'loc.documentStructure',
  },
  TABS: {
    MAIN_FIELDS: 'loc.mainFields',
  },
  SECTIONS: {
    COMMON_INFO: 'loc.commonInfo',
    FROM_BANK_INFO: 'loc.fromBankInfo',
  },
  FIELDS: {
    NPP: 'loc.orderNumber.abbr',
    EMPLOYEE_NUM: 'loc.personnelNumber.abbr',
    EMPLOYEE_SURNAME: 'loc.surname',
    EMPLOYEE_NAME: 'loc.name',
    EMPLOYEE_PATRONYMIC: 'loc.patronymic',
    EMPLOYEE_BIRTH_DATE: 'loc.dateOfBirth',
    ACCOUNT: 'loc.account',

    EMPLOYEE_PROCESSING_RESULT_STATUS: 'loc.status',
    EMPLOYEE_PROCESSING_RESULT_CODE_1C: 'loc.code1C',
    EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK: 'loc.bankMessage',
  },
  OPERATIONS: {
    SAVE_AND_ADD: 'loc.saveAndAddNew',
  },
  OTHER: {
    SEARCH_EMPLOYEE_PLACEHOLDER: 'loc.searchEmployeeBySurnameOrPersonnelNumber',
    PAGE_LEAVE_MESSAGE: 'loc.changesNotSavedAndWillBeLoose.closeDocument.question',
    PAGE_LEAVE_CONFIRM: 'loc.goTo',
    PAGE_LEAVE_CANCEL: 'loc.stay',
  },
  NOTIFICATIONS: {
    SAVE: {
      SUCCESS: {
        TITLE: 'loc.employeeAdded',
      },
      FAIL: {
        TITLE: 'loc.unableSaveEmployee',
      },
      RBO_CONTROLS: {
        TITLE: 'loc.employeeCannotBeSaved',
        MESSAGE: 'loc.toSaveEmployeeFixErrors',
      },
      SUCCESS_WITH_WARNINGS: {
        TITLE: 'loc.employeeSavedWithWarnings',
      },
    },
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,

    REFRESH: null,
    REFRESH_SUCCESS: null,
    REFRESH_FAIL: null,

    GET_DATA_REQUEST: null,
    GET_DATA_SUCCESS: null,
    GET_DATA_FAIL: null,
    SAVE_DATA_REQUEST: null,
    SAVE_DATA_SUCCESS: null,
    SAVE_DATA_FAIL: null,

    ACTIVATE_ERROR: null,
    FIELD_FOCUS: null,
    FIELD_BLUR: null,
    CHANGE_DATA: null,

    DICTIONARY_SEARCH: null,
    DICTIONARY_SEARCH_REQUEST: null,
    DICTIONARY_SEARCH_SUCCESS: null,
    DICTIONARY_SEARCH_CANCEL: null,
    DICTIONARY_SEARCH_FAIL: null,

    OPERATIONS: null,
    ...OPERATIONS_ACTIONS_TYPES.BACK,
    ...OPERATIONS_ACTIONS_TYPES.SAVE,
    OPERATION_SAVE_AND_ADD: null,
    OPERATION_SAVE_AND_ADD_SUCCESS: null,
    OPERATION_SAVE_AND_ADD_FAIL: null,

    SECTION_MINIMIZER_TOGGLE: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_ITEM_EMPLOYEE_',
);
