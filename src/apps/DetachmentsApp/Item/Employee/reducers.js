import { REDUCER_KEY, TAB_ID, FIELD_ID, OPERATION_ID, ACTIONS } from './constants';
import { STRUCTURE, DICTIONARIES, OPERATIONS } from './config';
import { changeState } from './utils';

const initialState = {
  isMounted: false,
  isError: false,
  serverError: null,
  isSaved: true,
  isLoading: true,
  isDisabled: false,
  isEditable: false,
  isTemplate: false,
  isArchived: false,
  isChecking: false,
  organization: null,
  operations: OPERATIONS.map(item => ({ ...item, disabled: false, progress: false })),
  documentHistory: { isVisible: false, items: [] },
  documentSignatures: { isVisible: false, items: [] },
  activeField: null,
  activeError: null,
  documentStructure: STRUCTURE.map(tab => ({ ...tab, isActive: tab.id === TAB_ID.MAIN_FIELDS })),
  documentData: {},
  documentErrors: [],
  documentDictionaries: Object.keys(DICTIONARIES).reduce(
    (result, key) => ({ ...result, [key]: { items: [], searchValue: '', isSearching: false } }),
    {},
  ),
};

const mountRequest = state => ({ ...state, isMounted: true, isLoading: true });

const unmount = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, payload) => {
  const { isEditable, documentData, documentErrors, documentDictionaries } = payload;

  return {
    ...state,
    isLoading: false,
    isEditable,
    documentData,
    documentErrors,
    documentDictionaries: {
      ...state.documentDictionaries,
      ...Object.keys(documentDictionaries).reduce(
        (result, key) => ({ ...result, [key]: { ...state.documentDictionaries[key], ...documentDictionaries[key] } }),
        {},
      ),
    },
  };
};

const mountFail = (state, serverError) => ({ ...state, isMounted: true, isLoading: false, isError: true, serverError });

const getDataRequest = state => ({ ...state, isDisabled: true });

const getDataFinish = state => ({ ...state, isDisabled: false });

const saveDataRequest = state => ({ ...state, isDisabled: true });

const saveDataSuccess = (state, payload) => ({
  ...state,
  isDisabled: false,
  documentErrors: payload.errors,
});

const saveDataFail = (state, payload) => ({
  ...state,
  isDisabled: false,
  documentErrors: payload ? payload.errors : state.documentErrors,
});

const refreshSuccess = (state, { documentData }) => ({ ...state, documentData });

const refreshFail = (state, serverError) => ({ ...state, isError: true, serverError });

const activateError = (state, { errorId }) => ({ ...state, activeError: errorId });

const fieldFocus = (state, { fieldId }) => ({ ...state, activeField: fieldId, activeError: null });

const fieldBlur = state => ({ ...state, activeField: null });

const changeData = (state, { fieldId, fieldValue }) => {
  switch (fieldId) {
    case FIELD_ID.SEARCH_EMPLOYEE:
      return changeState[fieldId](state, { fieldValue });
    default:
      return { ...state, documentData: { ...state.documentData, [fieldId]: fieldValue } };
  }
};

const dictionarySearch = (state, { dictionaryId, dictionarySearchValue }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      searchValue: dictionarySearchValue,
    },
  },
});

const dictionarySearchRequest = (state, { dictionaryId }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      isSearching: true,
    },
  },
});

const dictionarySearchSuccess = (state, { dictionaryId, dictionaryItems }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      items: dictionaryItems,
      isSearching: false,
    },
  },
});

const dictionarySearchCancel = (state, { dictionaryId }) =>
  dictionaryId === state.activeField
    ? state
    : {
        ...state,
        documentDictionaries: {
          ...state.documentDictionaries,
          [dictionaryId]: {
            ...state.documentDictionaries[dictionaryId],
            isSearching: false,
          },
        },
      };

const dictionarySearchFail = (state, { dictionaryId }) => ({
  ...state,
  documentDictionaries: {
    ...state.documentDictionaries,
    [dictionaryId]: {
      ...state.documentDictionaries[dictionaryId],
      isSearching: false,
    },
  },
});

const operationStart = (state, operationId, disableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? true : operation.progress,
    disabled: disableOtherOperations ? true : operation.disabled,
  })),
});

const operationFinish = (state, operationId, enableOtherOperations) => ({
  ...state,
  operations: state.operations.map(operation => ({
    ...operation,
    progress: operation.id === operationId ? false : operation.progress,
    disabled: enableOtherOperations ? false : operation.disabled,
  })),
});

function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest(state);

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.GET_DATA_REQUEST:
      return getDataRequest(state);

    case ACTIONS.GET_DATA_SUCCESS:
    case ACTIONS.GET_DATA_FAIL:
      return getDataFinish(state);

    case ACTIONS.SAVE_DATA_REQUEST:
      return saveDataRequest(state);

    case ACTIONS.SAVE_DATA_SUCCESS:
      return {
        ...saveDataSuccess(state, action.payload),
        isSaved: true,
      };

    case ACTIONS.SAVE_DATA_FAIL:
      return saveDataFail(state, action.payload);

    case ACTIONS.REFRESH_SUCCESS:
      return refreshSuccess(state, action.payload);

    case ACTIONS.REFRESH_FAIL:
      return refreshFail(state, action.error);

    case ACTIONS.ACTIVATE_ERROR:
      return activateError(state, action.payload);

    case ACTIONS.FIELD_FOCUS:
      return fieldFocus(state, action.payload);

    case ACTIONS.FIELD_BLUR:
      return fieldBlur(state);

    case ACTIONS.CHANGE_DATA:
      return {
        ...changeData(state, action.payload),
        isSaved: false,
      };

    case ACTIONS.DICTIONARY_SEARCH:
      return dictionarySearch(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_REQUEST:
      return dictionarySearchRequest(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_SUCCESS:
      return dictionarySearchSuccess(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_CANCEL:
      return dictionarySearchCancel(state, action.payload);
    case ACTIONS.DICTIONARY_SEARCH_FAIL:
      return dictionarySearchFail(state, action.payload);

    case ACTIONS.OPERATION_SAVE:
      return operationStart(state, OPERATION_ID.SAVE, true);
    case ACTIONS.OPERATION_SAVE_SUCCESS:
    case ACTIONS.OPERATION_SAVE_FAIL:
      return operationFinish(state, OPERATION_ID.SAVE, true);

    case ACTIONS.OPERATION_SAVE_AND_ADD:
      return operationStart(state, OPERATION_ID.SAVE_AND_ADD, true);
    case ACTIONS.OPERATION_SAVE_AND_ADD_SUCCESS:
    case ACTIONS.OPERATION_SAVE_AND_ADD_FAIL:
      return operationFinish(state, OPERATION_ID.SAVE_AND_ADD, true);

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducer };
