import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputViewMode } from '@rbo/rbo-components/lib/Input';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class FromBankInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { sectionData, formWarnings, formErrors } = this.props;
    return (
      JSON.stringify({
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
      }) !== JSON.stringify({ sectionData, formWarnings, formErrors })
    );
  }

  render() {
    const { LABELS, sectionData, formWarnings, formErrors, handleFocus, handleBlur } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.FROM_BANK_INFO }}>
        <Section.Header>
          <Section.Title>{LABELS.SECTIONS.FROM_BANK_INFO}</Section.Title>
        </Section.Header>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS}>
                    {LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_STATUS}
                  </Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS}
                      value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_CODE_1C}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C}
                      value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                      isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                      isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Label>{LABELS.FIELDS.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK}
                      value={sectionData[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                      isWarning={formWarnings[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                      isError={formErrors[FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
