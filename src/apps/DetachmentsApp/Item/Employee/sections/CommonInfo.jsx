import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputText, InputViewMode, InputDateCalendar, InputAccount } from '@rbo/rbo-components/lib/Input';
import { Required } from '@rbo/rbo-components/lib/Styles';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class CommonInfo extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isEditable: PropTypes.bool.isRequired,
    sectionData: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { isEditable, sectionData, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        isEditable: nextProps.isEditable,
        sectionData: nextProps.sectionData,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ isEditable, sectionData, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      isEditable,
      sectionData,
      formWarnings,
      formErrors,
      formDisabled,
      handleFocus,
      handleBlur,
      handleChange,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.COMMON_INFO }}>
        <Section.Content>
          <Blockset>
            <Block>
              <Row>
                <Cell width="25%">
                  <Label htmlFor={FIELD_ID.NPP}>{LABELS.FIELDS.NPP}</Label>
                  <Field>
                    <InputViewMode
                      id={FIELD_ID.NPP}
                      value={sectionData[FIELD_ID.NPP]}
                      isWarning={formWarnings[FIELD_ID.NPP]}
                      isError={formErrors[FIELD_ID.NPP]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      rows={1}
                    />
                  </Field>
                </Cell>
                <Cell width="25%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_NUM}>{LABELS.FIELDS.EMPLOYEE_NUM}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_NUM}
                        value={sectionData[FIELD_ID.EMPLOYEE_NUM]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NUM]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NUM]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_NUM]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={60}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_NUM}
                        value={sectionData[FIELD_ID.EMPLOYEE_NUM]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NUM]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NUM]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_SURNAME}>
                    {LABELS.FIELDS.EMPLOYEE_SURNAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_SURNAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_SURNAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_SURNAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_SURNAME]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_SURNAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_SURNAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_SURNAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_SURNAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_SURNAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_NAME}>
                    {LABELS.FIELDS.EMPLOYEE_NAME}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_NAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NAME]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_NAME}
                        value={sectionData[FIELD_ID.EMPLOYEE_NAME]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_NAME]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_NAME]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_PATRONYMIC}>{LABELS.FIELDS.EMPLOYEE_PATRONYMIC}</Label>
                  <Field>
                    {isEditable ? (
                      <InputText
                        id={FIELD_ID.EMPLOYEE_PATRONYMIC}
                        value={sectionData[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        maxLength={50}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_PATRONYMIC}
                        value={sectionData[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_PATRONYMIC]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.EMPLOYEE_BIRTH_DATE}>{LABELS.FIELDS.EMPLOYEE_BIRTH_DATE}</Label>
                  <Field>
                    {isEditable ? (
                      <InputDateCalendar
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.EMPLOYEE_BIRTH_DATE}
                        value={sectionData[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        disabled={formDisabled[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.EMPLOYEE_BIRTH_DATE}
                        value={sectionData[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isWarning={formWarnings[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        isError={formErrors[FIELD_ID.EMPLOYEE_BIRTH_DATE]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
          <Blockset>
            <Block>
              <Row>
                <Cell width="50%">
                  <Label htmlFor={FIELD_ID.ACCOUNT}>
                    {LABELS.FIELDS.ACCOUNT}
                    {isEditable && <Required />}
                  </Label>
                  <Field>
                    {isEditable ? (
                      <InputAccount
                        locale={LABELS.LOCALE}
                        id={FIELD_ID.ACCOUNT}
                        value={sectionData[FIELD_ID.ACCOUNT]}
                        isWarning={formWarnings[FIELD_ID.ACCOUNT]}
                        isError={formErrors[FIELD_ID.ACCOUNT]}
                        disabled={formDisabled[FIELD_ID.ACCOUNT]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    ) : (
                      <InputViewMode
                        id={FIELD_ID.ACCOUNT}
                        value={sectionData[FIELD_ID.ACCOUNT]}
                        isWarning={formWarnings[FIELD_ID.ACCOUNT]}
                        isError={formErrors[FIELD_ID.ACCOUNT]}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        rows={1}
                      />
                    )}
                  </Field>
                </Cell>
              </Row>
            </Block>
            <Block />
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
