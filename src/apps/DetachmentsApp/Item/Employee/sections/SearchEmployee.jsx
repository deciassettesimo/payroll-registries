import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Section, Blockset, Block } from '@rbo/rbo-components/lib/RboDocForm';
import Form, { Row, Cell, Field } from '@rbo/rbo-components/lib/Form';
import { InputSearch } from '@rbo/rbo-components/lib/Input';

import { SECTION_ID, FIELD_ID } from '../constants';

export default class SearchEmployee extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    sectionData: PropTypes.shape().isRequired,
    sectionDictionaries: PropTypes.shape().isRequired,
    formWarnings: PropTypes.shape().isRequired,
    formErrors: PropTypes.shape().isRequired,
    formDisabled: PropTypes.shape().isRequired,
    optionRenderer: PropTypes.func.isRequired,
    handleFocus: PropTypes.func.isRequired,
    handleBlur: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSearch: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled } = this.props;
    return (
      JSON.stringify({
        sectionData: nextProps.sectionData,
        sectionDictionaries: nextProps.sectionDictionaries,
        formWarnings: nextProps.formWarnings,
        formErrors: nextProps.formErrors,
        formDisabled: nextProps.formDisabled,
      }) !== JSON.stringify({ sectionData, sectionDictionaries, formWarnings, formErrors, formDisabled })
    );
  }

  render() {
    const {
      LABELS,
      sectionData,
      sectionDictionaries,
      formWarnings,
      formErrors,
      formDisabled,
      optionRenderer,
      handleFocus,
      handleBlur,
      handleChange,
      handleSearch,
    } = this.props;

    return (
      <Section dataAttributes={{ id: SECTION_ID.SEARCH_EMPLOYEE }}>
        <Section.Content>
          <Blockset>
            <Block isWide>
              <Row>
                <Cell>
                  <Field dimension={Form.REFS.DIMENSIONS.M}>
                    <InputSearch
                      locale={LABELS.LOCALE}
                      id={FIELD_ID.SEARCH_EMPLOYEE}
                      value={sectionData[FIELD_ID.SEARCH_EMPLOYEE]}
                      isWarning={formWarnings[FIELD_ID.SEARCH_EMPLOYEE]}
                      isError={formErrors[FIELD_ID.SEARCH_EMPLOYEE]}
                      disabled={formDisabled[FIELD_ID.SEARCH_EMPLOYEE]}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      onSearch={handleSearch}
                      options={sectionDictionaries[FIELD_ID.SEARCH_EMPLOYEE].items}
                      isSearching={sectionDictionaries[FIELD_ID.SEARCH_EMPLOYEE].isSearching}
                      optionRenderer={optionRenderer}
                      placeholder={LABELS.OTHER.SEARCH_EMPLOYEE_PLACEHOLDER}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Blockset>
        </Section.Content>
      </Section>
    );
  }
}
