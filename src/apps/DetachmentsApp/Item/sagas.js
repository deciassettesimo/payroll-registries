import { all, call, put, select, takeEvery } from 'redux-saga/effects';
import qs from 'qs';

import { ROUTE_LEAVE_CONFIRM_ENABLE, DOC_NEW_ID, DOC_STATUS_ID, PERMISSIONS_IDS } from 'common/constants';
import operations from 'common/operations';
import {
  openAction as saveAsTemplateModalOpenAction,
  closeAction as saveAsTemplateModalCloseAction,
  saveRequestAction as saveAsTemplateModalRequestAction,
  saveSuccessAction as saveAsTemplateModalSuccessAction,
  saveFailAction as saveAsTemplateModalFailAction,
} from 'common/components/RboDocSaveAsTemplateModal/actions';
import { openAction as confirmModalOpenAction } from 'common/components/RboConfirmModal/actions';
import { getOfficials as dalGetOfficials, saveOfficial as dalSaveOfficial } from 'dal/dictionaries';
import {
  getDefaultData as dalGetDefaultData,
  getRepeatData as dalGetRepeatData,
  getRepeatWithRefusalsData as dalGetRepeatWithRefusalsData,
  getFromTemplateData as dalGetFromTemplateData,
  getDocData as dalGetDocData,
  getDocErrors as dalGetDocErrors,
  getDocHeaderErrors as dalGetDocHeaderErrors,
  getDocStatus as dalGetDocStatus,
  saveDocData as dalSaveDocData,
  getTemplates as dalGetTemplates,
  getTemplateData as dalGetTemplateData,
  getTemplateErrors as dalGetTemplateErrors,
  saveTemplateData as dalSaveTemplateData,
  saveAsTemplate as dalSaveAsTemplate,
  removeTemplate as dalRemoveTemplate,
  getBranches as dalGetBranches,
} from 'dal/payroll-registries-detachments';
import localization from 'localization';
import { notificationSuccess, notificationError, saved } from 'platform';
import { getIsDocEditable, getIsDocNeedGetErrors, dispatchErrorAction, getRoutePath, windowRefresh } from 'utils';

import { REDUCER_KEY as APP_REDUCER_KEY, ROUTES_IDS as APP_ROUTES_IDS } from '../constants';

import { OPERATIONS_ACTIONS_MAP } from './config';
import { REDUCER_KEY, FIELD_ID, ACTIONS, ALLOWED_SM_ACTION_ID, LOCALE_KEY } from './constants';
import employeesSagas from './Employees/sagas';
import employeeSagas from './Employee/sagas';

import { REDUCER_KEY as EMPLOYEES_REDUCER_KEY, ACTIONS as EMPLOYEES_ACTIONS } from './Employees/constants';

const appSelectedOrganizationsSelector = state => state[APP_REDUCER_KEY].settings.selectedOrganizations;
const appPermissionsSelector = state => state[APP_REDUCER_KEY].permissions;
const appOrganizationsSelector = state => state[APP_REDUCER_KEY].organizations;
const appRoutesSelector = state => state[APP_REDUCER_KEY].routes;
const isSavedSelector = state => state[REDUCER_KEY].isSaved;
const isTemplateSelector = state => state[REDUCER_KEY].isTemplate;
const documentDataSelector = state => state[REDUCER_KEY].documentData;
const orgIdSelector = state => state[REDUCER_KEY].documentData[FIELD_ID.CUSTOMER_ID];
const documentIdSelector = state => state[REDUCER_KEY].documentData[FIELD_ID.ID];
const documentSignaturesSelector = state => state[REDUCER_KEY].documentSignatures;
const defaultTemplateIdSelector = state => state[REDUCER_KEY].templates.defaultValue;
const employeesIsMountedSelector = state => state[EMPLOYEES_REDUCER_KEY].isMounted;

const delay = ms => new Promise(res => setTimeout(res, ms));

function* getData({ id, query, isTemplate }) {
  yield put({ type: ACTIONS.GET_DATA_REQUEST });

  let documentData = {};

  try {
    if (isTemplate) {
      documentData = yield call(dalGetTemplateData, id);
      documentData[FIELD_ID.NUMBER] = null;
      documentData[FIELD_ID.DATE] = null;
      documentData[FIELD_ID.CONFIRM_ACCEPTANCE] = true;
    } else if (id === DOC_NEW_ID) {
      if (query.type === 'repeat') {
        documentData = yield call(dalGetRepeatData, query.id);
      } else if (query.type === 'repeatWithRefusals') {
        documentData = yield call(dalGetRepeatWithRefusalsData, query.id);
      } else if (query.type === 'template') {
        documentData = yield call(dalGetFromTemplateData, query.id);
      } else {
        const selectedOrganizations = yield select(appSelectedOrganizationsSelector);
        const organizationId = selectedOrganizations && selectedOrganizations[0]; /* todo добавить выбор организации */
        documentData = yield call(dalGetDefaultData, organizationId);
        documentData[FIELD_ID.CUSTOMER_ID] = organizationId;
      }
      documentData[FIELD_ID.STATUS] = DOC_STATUS_ID.NEW;
      documentData[FIELD_ID.CONFIRM_ACCEPTANCE] = true;
    } else {
      documentData = yield call(dalGetDocData, id);
    }
    const isArchived = !!(
      documentData[FIELD_ID.ALLOWED_SM_ACTIONS] &&
      documentData[FIELD_ID.ALLOWED_SM_ACTIONS][ALLOWED_SM_ACTION_ID.UNARCHIVE]
    );
    const isEditable = getIsDocEditable(documentData[FIELD_ID.STATUS], isArchived, isTemplate);

    yield put({ type: ACTIONS.GET_DATA_SUCCESS });
    return {
      documentData,
      isEditable,
      isArchived,
    };
  } catch (error) {
    yield put({ type: ACTIONS.GET_DATA_FAIL, error });
    throw error;
  }
}

function* saveData(channels, { history }) {
  yield put({ type: ACTIONS.SAVE_DATA_REQUEST });
  const isTemplate = yield select(isTemplateSelector);
  const documentData = yield select(documentDataSelector);
  const status = documentData[FIELD_ID.STATUS];
  let result;
  if (status === DOC_STATUS_ID.NEW || documentData[FIELD_ID.ALLOWED_SM_ACTIONS][ALLOWED_SM_ACTION_ID.SAVE]) {
    try {
      // Вызывается REST-метод проверки шапки реестра
      const errorsResult = isTemplate
        ? yield call(dalGetTemplateErrors, documentData)
        : yield call(dalGetDocErrors, documentData);
      if (errorsResult.errors && errorsResult.errors.filter(item => ['2', '3'].includes(item.level)).length) {
        throw new operations.RBOControlsError({ data: errorsResult });
      }

      if (isTemplate) {
        result = yield call(dalSaveTemplateData, documentData);
      } else {
        result = yield call(dalSaveDocData, documentData);
      }
      yield put({ type: ACTIONS.SAVE_DATA_SUCCESS, payload: result });
      yield call(operations.save.success, { response: result, channels });
      yield call(saved, true);

      if (status === DOC_STATUS_ID.NEW) {
        const routes = yield select(appRoutesSelector);
        yield call(history.replace, getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', result[FIELD_ID.ID]));
      }
    } catch (error) {
      if (error instanceof operations.RBOControlsError) {
        yield put({ type: ACTIONS.SAVE_DATA_FAIL, payload: error.response.data });
      } else {
        yield put({ type: ACTIONS.SAVE_DATA_FAIL });
      }
      yield call(operations.save.fail, { error, isTemplate, channels });
    }
  } else {
    yield call(operations.save.notAllowed, { channels });
  }
}

function* mountSaga({ payload }) {
  yield call(saved, true);

  const { params, location } = payload;
  const { id, templateId } = params;
  const query = location.search.replace(/^\?/, '');
  const { type: queryType, repeatedId: queryRepeatedId, templateId: queryTemplateId } = qs.parse(query);

  const isTemplate = !id && !!templateId;
  let documentDictionaries = {};
  let templates = [];

  try {
    const data = yield call(getData, {
      id: isTemplate ? templateId : id,
      query: { type: queryType, id: queryRepeatedId || queryTemplateId },
      isTemplate,
    });
    const { documentData, isEditable, isArchived } = data;

    let documentErrors = [];
    if (isTemplate) {
      // todo template https://jira.raiffeisen.ru/browse/EL-32439 нет метода метода templates/bank-message/{templateId}
      const errorsResult = yield call(dalGetTemplateErrors, documentData);
      documentErrors = errorsResult.errors;
    } else if (getIsDocNeedGetErrors(documentData[FIELD_ID.STATUS])) {
      const errorsResult = yield call(dalGetDocHeaderErrors, id);
      documentErrors = errorsResult.errors || [];
    }

    const permissions = yield select(appPermissionsSelector);
    if (permissions[PERMISSIONS_IDS.CREATE_FROM_TEMPLATE] && documentData[FIELD_ID.STATUS] === DOC_STATUS_ID.NEW) {
      templates = yield call(dalGetTemplates, { orgId: [documentData[FIELD_ID.CUSTOMER_ID]] });
    }

    if (isEditable) {
      const officials = yield call(dalGetOfficials, {
        orgId: [documentData[FIELD_ID.CUSTOMER_ID]],
        fio: documentData[FIELD_ID.OFFICIAL_NAME],
      });

      const branches = yield call(dalGetBranches, [documentData[FIELD_ID.CUSTOMER_ID]]);

      /* Подразделение
       * Если для выбора доступно только одно подразделение, то поле должно заполняться по умолчанию этим значением.
       * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=90807507 */
      if (documentData[FIELD_ID.STATUS] === DOC_STATUS_ID.NEW) {
        if (branches.length === 1) {
          documentData[FIELD_ID.BRANCH_INFO_ID] = branches[0].id;
          documentData[FIELD_ID.BRANCH_INFO_NAME] = branches[0].shortName;
        }
      }

      documentDictionaries = {
        [FIELD_ID.OFFICIAL_NAME]: { items: officials },
        [FIELD_ID.BRANCH_INFO_NAME]: { items: branches },
      };
    }

    const organizations = yield select(appOrganizationsSelector);
    const organization = organizations.find(item => item.id === documentData[FIELD_ID.CUSTOMER_ID]);

    yield put({
      type: ACTIONS.MOUNT_SUCCESS,
      payload: {
        isTemplate,
        isArchived,
        isEditable,
        organization,
        documentData,
        documentErrors,
        documentDictionaries,
        templates: {
          items: templates,
          defaultValue: queryTemplateId,
          value: queryTemplateId,
        },
      },
    });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  } finally {
    windowRefresh();
  }
}

function* refreshSaga() {
  const id = yield select(documentIdSelector);
  const isTemplate = yield select(isTemplateSelector);
  const documentData = yield select(documentDataSelector);
  const isChecking =
    !isTemplate && [DOC_STATUS_ID.CHECKING, DOC_STATUS_ID.SIGNING].includes(documentData[FIELD_ID.STATUS]);
  try {
    if (isChecking) {
      yield delay(500);
      const data = yield call(dalGetDocStatus, id);
      yield put({ type: ACTIONS.REFRESH_CHECKING, payload: { documentData: data } });
      yield put({ type: ACTIONS.REFRESH });
    } else {
      const data = yield call(getData, { id, isTemplate });
      let documentErrors = [];
      if (isTemplate) {
        // todo template https://jira.raiffeisen.ru/browse/EL-32439 нет метода метода templates/bank-message/{templateId}
        const errorsResult = yield call(dalGetTemplateErrors, documentData);
        documentErrors = errorsResult.errors;
      } else if (getIsDocNeedGetErrors(data.documentData[FIELD_ID.STATUS])) {
        const errorsResult = yield call(dalGetDocHeaderErrors, id);
        documentErrors = errorsResult.errors || [];
      }

      yield put({ type: ACTIONS.REFRESH_SUCCESS, payload: { ...data, documentErrors } });

      /* Обновляем список сотрудников, если открыта вкладка "Сотрудники" */
      const employeesIsMounted = yield select(employeesIsMountedSelector);
      if (employeesIsMounted) yield put({ type: EMPLOYEES_ACTIONS.GET_LIST_REQUEST });

      if ([DOC_STATUS_ID.CHECKING, DOC_STATUS_ID.SIGNING].includes(data.documentData[FIELD_ID.STATUS])) {
        yield put({ type: ACTIONS.REFRESH });
      }
    }
  } catch (error) {
    yield put({ type: ACTIONS.REFRESH_FAIL, error });
  }
}

function* changeDataSaga() {
  yield call(saved, false);
}

function* dictionarySearchSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  if ([FIELD_ID.OFFICIAL_NAME].includes(dictionaryId)) {
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_REQUEST, payload: { dictionaryId, dictionarySearchValue } });
  }
}

function* dictionarySearchRequestSaga({ payload }) {
  const { dictionaryId, dictionarySearchValue } = payload;
  const orgId = yield select(orgIdSelector);
  let dictionaryItems;
  try {
    switch (dictionaryId) {
      case FIELD_ID.OFFICIAL_NAME:
        dictionaryItems = yield call(dalGetOfficials, { orgId: [orgId], fio: dictionarySearchValue }, true);
        break;
      default:
        break;
    }
    yield put({ type: ACTIONS.DICTIONARY_SEARCH_SUCCESS, payload: { dictionaryId, dictionaryItems } });
  } catch (error) {
    yield dispatchErrorAction({
      error,
      types: [ACTIONS.DICTIONARY_SEARCH_FAIL, ACTIONS.DICTIONARY_SEARCH_CANCEL],
      payload: { dictionaryId },
    });
  }
}

function* templatesChooseSaga({ payload }) {
  const { templateId, params } = payload;
  const defaultTemplateId = yield select(defaultTemplateIdSelector);
  if (templateId && templateId !== defaultTemplateId) {
    const routes = yield select(appRoutesSelector);
    yield call(params.history.push, {
      pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
      search: `?${qs.stringify({ type: 'template', templateId })}`,
    });
  }
}

function* templatesRouteToSaga({ payload }) {
  const { templateId, params } = payload;
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, getRoutePath(routes, APP_ROUTES_IDS.TEMPLATE).replace(':templateId', templateId));
}

function* operationsSaga({ payload }) {
  const { operationId } = payload;
  yield put({ type: OPERATIONS_ACTIONS_MAP[operationId], payload });
}

function* operationArchiveSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(saved, true);
  yield call(operations.archive, {
    ids: [documentId],
    channels: { success: ACTIONS.OPERATION_ARCHIVE_SUCCESS, fail: ACTIONS.OPERATION_ARCHIVE_FAIL },
  });
}

function* operationArchiveSuccessSaga() {
  yield put({ type: ACTIONS.REFRESH });
}

function* operationBackSaga({ payload }) {
  const isSaved = yield select(isSavedSelector);
  if (ROUTE_LEAVE_CONFIRM_ENABLE && !isSaved) {
    yield put(
      confirmModalOpenAction({
        message: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_MESSAGE),
        confirm: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CONFIRM),
        cancel: localization.translate(LOCALE_KEY.OTHER.PAGE_LEAVE_CANCEL),
        channels: {
          confirm: { type: ACTIONS.OPERATION_BACK_CONFIRM, payload },
          cancel: { type: ACTIONS.OPERATION_BACK_CANCEL },
        },
      }),
    );
  } else {
    yield put({ type: ACTIONS.OPERATION_BACK_CONFIRM, payload });
  }
}

function* operationBackConfirmSaga({ payload }) {
  const { params } = payload;

  yield call(saved, true);

  const routes = yield select(appRoutesSelector);
  yield call(operations.back, { indexRouter: getRoutePath(routes, APP_ROUTES_IDS.INDEX), ...params });
}

function* operationCheckSignSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.checkSign, {
    id: documentId,
    channels: { success: ACTIONS.OPERATION_CHECK_SIGN_SUCCESS, fail: ACTIONS.OPERATION_CHECK_SIGN_FAIL },
  });
}

function* operationCreateFromTemplateSaga({ payload }) {
  const { params } = payload;
  const templateId = yield select(documentIdSelector);
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, {
    pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
    search: `?${qs.stringify({ type: 'template', templateId })}`,
  });
}

function* operationStateHistorySaga() {
  yield put({ type: ACTIONS.REFRESH });
  const documentId = yield select(documentIdSelector);
  yield call(operations.stateHistory, {
    id: documentId,
    channels: { success: ACTIONS.OPERATION_STATE_HISTORY_SUCCESS, fail: ACTIONS.OPERATION_STATE_HISTORY_FAIL },
  });
}

function* operationRemoveSaga({ payload }) {
  const { params } = payload;
  const documentId = yield select(documentIdSelector);
  const isTemplate = yield select(isTemplateSelector);
  yield call(operations.remove.start, {
    ids: [documentId],
    isTemplate,
    channels: { confirm: ACTIONS.OPERATION_REMOVE_CONFIRM, cancel: ACTIONS.OPERATION_REMOVE_CANCEL },
    ...params,
  });
}

function* operationRemoveConfirmSaga({ payload }) {
  const routes = yield select(appRoutesSelector);
  yield call(saved, true);
  yield call(operations.remove.finish, {
    ...payload,
    dalRemoveTemplate,
    indexRouter: getRoutePath(routes, APP_ROUTES_IDS.INDEX),
    channels: { success: ACTIONS.OPERATION_REMOVE_SUCCESS, fail: ACTIONS.OPERATION_REMOVE_FAIL },
  });
}

function* operationRemoveSignSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.removeSign, {
    ids: [documentId],
    channels: {
      cancel: ACTIONS.OPERATION_REMOVE_SIGN_CANCEL,
      success: ACTIONS.OPERATION_REMOVE_SIGN_SUCCESS,
      fail: ACTIONS.OPERATION_REMOVE_SIGN_FAIL,
    },
  });
}

function* operationRemoveSignSuccessSaga() {
  yield put({ type: ACTIONS.REFRESH });
}

function* operationRepeatSaga({ payload }) {
  const { params } = payload;
  const repeatedId = yield select(documentIdSelector);
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, {
    pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
    search: `?${qs.stringify({ type: 'repeat', repeatedId })}`,
  });
}

function* operationRepeatWithRefusalsSaga({ payload }) {
  const { params } = payload;
  const repeatedId = yield select(documentIdSelector);
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, {
    pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
    search: `?${qs.stringify({ type: 'repeatWithRefusals', repeatedId })}`,
  });
}

function* operationSaveSaga({ payload }) {
  const { params } = payload;
  yield call(saveData, { success: ACTIONS.OPERATION_SAVE_SUCCESS, fail: ACTIONS.OPERATION_SAVE_FAIL }, params);
}

function* operationSaveSuccessSaga({ payload }) {
  const { status } = payload;
  yield put({ type: ACTIONS.REFRESH });
  const isTemplate = yield select(isTemplateSelector);
  yield call(operations.save.showSuccessNotification, { status, isTemplate });
}

function* operationSaveAsTemplateSaga() {
  yield put(
    saveAsTemplateModalOpenAction({ channels: { submit: { type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_REQUEST } } }),
  );
}

function* operationSaveAsTemplateRequestSaga({ payload }) {
  const id = yield select(documentIdSelector);
  const { name } = payload;
  try {
    yield put(saveAsTemplateModalRequestAction());
    yield call(dalSaveAsTemplate, id, name);
    yield put(saveAsTemplateModalSuccessAction());
    yield put(saveAsTemplateModalCloseAction());
    yield call(operations.saveAsTemplate.showSuccessNotification, { name });
    yield put({ type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_SUCCESS });
  } catch (error) {
    if (error instanceof operations.RBOControlsError) {
      yield put(saveAsTemplateModalFailAction(error.response.data.errors));
    } else {
      yield put(saveAsTemplateModalCloseAction());
      yield call(operations.saveAsTemplate.showErrorNotification);
    }
    yield put({ type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_FAIL });
  }
}

function* operationSendSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.send, {
    ids: [documentId],
    channels: { success: ACTIONS.OPERATION_SEND_SUCCESS, fail: ACTIONS.OPERATION_SEND_FAIL },
  });
}

function* operationSendSuccessSaga() {
  yield put({ type: ACTIONS.REFRESH });
}

function* operationSignSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.sign.start, {
    ids: [documentId],
    channels: {
      cancel: ACTIONS.OPERATION_SIGN_CANCEL,
      success: ACTIONS.OPERATION_SIGN_SUCCESS,
      fail: ACTIONS.OPERATION_SIGN_FAIL,
    },
  });
}

function* operationSignSuccessSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.sign.showSuccessNotification, { ids: [documentId] });
  yield put({ type: ACTIONS.REFRESH });
}

function* operationUnarchiveSaga() {
  const documentId = yield select(documentIdSelector);
  yield call(operations.unarchive, {
    ids: [documentId],
    channels: { success: ACTIONS.OPERATION_UNARCHIVE_SUCCESS, fail: ACTIONS.OPERATION_UNARCHIVE_FAIL },
  });
}

function* operationUnarchiveSuccessSaga() {
  yield put({ type: ACTIONS.REFRESH });
}

function* signatureOperationsSaga({ payload }) {
  const { operationId, itemId: signId } = payload;
  const docId = yield select(documentIdSelector);
  const documentSignatures = yield select(documentSignaturesSelector);
  const signature = documentSignatures.items.find(item => item.signId === signId);
  const signDate = signature.signDateTime;
  yield call(operations.signature, { operationId, docId, signId, signDate });
}

function* saveResponsiblePersonSaga() {
  const documentData = yield select(documentDataSelector);
  const data = {
    orgId: documentData[FIELD_ID.CUSTOMER_ID],
    fio: documentData[FIELD_ID.OFFICIAL_NAME],
    phone: documentData[FIELD_ID.OFFICIAL_PHONE],
  };
  try {
    yield call(dalSaveOfficial, data);
    yield put({ type: ACTIONS.SAVE_RESPONSIBLE_PERSON_SUCCESS });
    yield call(notificationSuccess, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.RESPONSIBLE_PERSON_SAVE.SUCCESS.TITLE),
    });
  } catch (error) {
    yield put({ type: ACTIONS.SAVE_RESPONSIBLE_PERSON_FAIL });
    if (error instanceof operations.RBOControlsError) {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.RESPONSIBLE_PERSON_SAVE.FAIL.TITLE),
        message: error.response.data.errors.map(item => item.text).join(', '),
      });
    } else {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.RESPONSIBLE_PERSON_SAVE.FAIL.TITLE),
      });
    }
  }
}

function* removeEmployeesSaga() {
  yield call(saved, false);
}

function* saveEmployeeSaga() {
  yield call(saved, false);
}

function* unmountSaga() {
  yield call(saved, true);
}

function* containerSagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.REFRESH, refreshSaga);

  yield takeEvery(ACTIONS.CHANGE_DATA, changeDataSaga);

  yield takeEvery(ACTIONS.DICTIONARY_SEARCH, dictionarySearchSaga);
  yield takeEvery(ACTIONS.DICTIONARY_SEARCH_REQUEST, dictionarySearchRequestSaga);
  yield takeEvery(ACTIONS.TEMPLATES_CHOOSE, templatesChooseSaga);
  yield takeEvery(ACTIONS.TEMPLATES_ROUTE_TO, templatesRouteToSaga);

  yield takeEvery(ACTIONS.OPERATIONS, operationsSaga);
  yield takeEvery(ACTIONS.OPERATION_ARCHIVE, operationArchiveSaga);
  yield takeEvery(ACTIONS.OPERATION_ARCHIVE_SUCCESS, operationArchiveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK, operationBackSaga);
  yield takeEvery(ACTIONS.OPERATION_BACK_CONFIRM, operationBackConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_CHECK_SIGN, operationCheckSignSaga);
  yield takeEvery(ACTIONS.OPERATION_CREATE_FROM_TEMPLATE, operationCreateFromTemplateSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE, operationRemoveSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_SIGN, operationRemoveSignSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_SIGN_SUCCESS, operationRemoveSignSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_REPEAT, operationRepeatSaga);
  yield takeEvery(ACTIONS.OPERATION_REPEAT_WITH_REFUSALS, operationRepeatWithRefusalsSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE, operationSaveSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_SUCCESS, operationSaveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AS_TEMPLATE, operationSaveAsTemplateSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AS_TEMPLATE_REQUEST, operationSaveAsTemplateRequestSaga);
  yield takeEvery(ACTIONS.OPERATION_SEND, operationSendSaga);
  yield takeEvery(ACTIONS.OPERATION_SEND_SUCCESS, operationSendSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_SIGN, operationSignSaga);
  yield takeEvery(ACTIONS.OPERATION_SIGN_SUCCESS, operationSignSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_STATE_HISTORY, operationStateHistorySaga);
  yield takeEvery(ACTIONS.OPERATION_UNARCHIVE, operationUnarchiveSaga);
  yield takeEvery(ACTIONS.OPERATION_UNARCHIVE_SUCCESS, operationUnarchiveSuccessSaga);

  yield takeEvery(ACTIONS.SIGNATURES_OPERATIONS, signatureOperationsSaga);

  yield takeEvery(ACTIONS.SAVE_RESPONSIBLE_PERSON_REQUEST, saveResponsiblePersonSaga);

  yield takeEvery(ACTIONS.REMOVE_EMPLOYEES, removeEmployeesSaga);
  yield takeEvery(ACTIONS.SAVE_EMPLOYEE, saveEmployeeSaga);

  yield takeEvery(ACTIONS.UNMOUNT, unmountSaga);
}

export default function* sagas() {
  yield all([containerSagas(), employeesSagas(), employeeSagas()]);
}
