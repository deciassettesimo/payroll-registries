import { ACTIONS } from './constants';

export const mountAction = (routerBasename, permissions) => ({
  type: ACTIONS.MOUNT_REQUEST,
  payload: { routerBasename, permissions },
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});
