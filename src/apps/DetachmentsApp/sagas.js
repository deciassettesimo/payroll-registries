import { all, call, put, select, takeEvery } from 'redux-saga/effects';

import dispatchErrorAction from 'utils/dispatchErrorAction';
import {
  getOrganizationsAndBranches as dalGetOrganizationsAndBranches,
  getSettings as dalGetSettings,
  saveSettings as dalSaveSettings,
} from 'dal/user';
import { getStatuses as dalGetStatuses } from 'dal/payroll-registries-detachments';

import { ACTIONS, REDUCER_KEY } from './constants';
import exportModalSagas from './ExportModal/sagas';
import itemContainerSagas from './Item/sagas';
import listContainerSagas from './List/sagas';

const isInitializedSelector = state => state[REDUCER_KEY].isInitialized;
const settingsSelector = state => state[REDUCER_KEY].settings;

function* mountRequestSaga() {
  const isInitialized = yield select(isInitializedSelector);
  try {
    const settings = yield call(dalGetSettings);
    if (isInitialized) {
      yield put({ type: ACTIONS.MOUNT_SUCCESS, payload: { settings } });
    } else {
      const organizationsAndBranches = yield call(dalGetOrganizationsAndBranches);
      const statuses = yield call(dalGetStatuses);
      yield put({
        type: ACTIONS.MOUNT_SUCCESS,
        payload: {
          organizations: organizationsAndBranches.organizations,
          branches: organizationsAndBranches.branches,
          settings,
          statuses,
        },
      });
    }
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsRequestSaga({ payload }) {
  try {
    const { key, data } = payload;
    const settings = yield select(settingsSelector);
    const newSettings = { ...settings, [key]: data };
    yield call(dalSaveSettings, newSettings);
    yield put({ type: ACTIONS.SAVE_SETTINGS_SUCCESS, payload: { settings: newSettings } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.SAVE_SETTINGS_FAIL] });
  }
}

function* appSagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountRequestSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS_REQUEST, saveSettingsRequestSaga);
}

export default function* sagas() {
  yield all([appSagas(), exportModalSagas(), itemContainerSagas(), listContainerSagas()]);
}
