import { ENCODINGS } from './config';
import { REDUCER_KEY, ACTIONS, FIELD_ID } from './constants';

export const initialState = {
  isMounted: false,
  isVisible: false,
  isLoading: false,
  encodings: ENCODINGS,
  organizations: [],
  values: {},
  settings: {},
  activeError: null,
};

const mountRequest = () => ({ ...initialState, isMounted: false });

const mountSuccess = (state, { settings }) => ({
  ...state,
  isMounted: true,
  settings,
  values: {
    ...state.values,
    [FIELD_ID.MASK_CSV]: settings[FIELD_ID.MASK_CSV],
  },
});

const mountFail = state => ({ ...state, isMounted: true });

const unmount = () => ({ ...initialState, isMounted: false });

const saveSettings = (state, { settings }) => ({ ...state, settings });

const open = (state, { organizations, periodFrom, periodTo }) => ({
  ...state,
  isVisible: true,
  organizations,
  values: {
    ...state.values,
    [FIELD_ID.PERIOD_FROM]: periodFrom,
    [FIELD_ID.PERIOD_TO]: periodTo,
  },
});

const change = (state, { id, value }) => {
  switch (id) {
    case FIELD_ID.PERIOD_FROM:
    case FIELD_ID.PERIOD_TO:
    case FIELD_ID.MASK_CSV:
      return { ...state, values: { ...state.values, [id]: value } };
    default:
      return state;
  }
};

const exportRequest = state => ({ ...state, isLoading: true });

const exportSuccess = state => ({ ...state, isLoading: false });

const exportFail = state => ({ ...state, isLoading: false });

function reducers(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT_REQUEST:
      return mountRequest();

    case ACTIONS.MOUNT_SUCCESS:
      return mountSuccess(state, action.payload);

    case ACTIONS.MOUNT_FAIL:
      return mountFail(state, action.error);

    case ACTIONS.UNMOUNT:
      return unmount();

    case ACTIONS.SAVE_SETTINGS:
      return saveSettings(state, action.payload);

    case ACTIONS.OPEN:
      return open(state, action.payload);

    case ACTIONS.CLOSE:
      return { ...state, isVisible: false, organizations: [] };

    case ACTIONS.CHANGE:
      return change(state, action.payload);

    case ACTIONS.EXPORT_REQUEST:
      return exportRequest(state);

    case ACTIONS.EXPORT_SUCCESS:
      return exportSuccess(state);

    case ACTIONS.EXPORT_FAIL:
      return exportFail(state);

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducers };
