import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesDetachmentsExportModal';

export const LOCALE_KEY = {
  HEADER: 'loc.exportToCSV',
  FIELDS: {
    PERIOD: {
      MAIN: 'loc.period',
      FROM: 'loc.rangeFrom',
      TO: 'loc.rangeTo',
    },
    MASK: 'loc.fileNameMask',
    ENCODING: 'loc.fileEncoding',
    SEPARATOR: 'loc.separator',
    SUBMIT_BUTTON: 'loc.export',
    CANCEL_BUTTON: 'loc.close',
  },
  NOTIFICATIONS: {
    SUCCESS: {
      TITLE: 'loc.exportCompleted',
    },
    FAIL: {
      TITLE: 'loc.unableExport',
    },
  },
  OTHER: {
    MASK_ERROR: 'loc.exportMaskError',
  },
};

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,
    SAVE_SETTINGS: null,

    OPEN: null,
    CLOSE: null,
    CHANGE: null,
    BLUR: null,
    EXPORT_REQUEST: null,
    EXPORT_SUCCESS: null,
    EXPORT_FAIL: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_EXPORT_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    PERIOD_FROM: null,
    PERIOD_TO: null,
    MASK_CSV: null,
    ENCODING: null,
    SEPARATOR: null,
    SUBMIT_BUTTON: null,
    CANCEL_BUTTON: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_EXPORT_MODAL_',
);
