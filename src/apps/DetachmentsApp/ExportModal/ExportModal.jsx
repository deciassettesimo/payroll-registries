import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ModalWindow from '@rbo/rbo-components/lib/ModalWindow';
import { Block, Header, Status } from '@rbo/rbo-components/lib/Styles';
import Form, { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import Button from '@rbo/rbo-components/lib/Button';
import { InputText, InputDateCalendar, InputSelect } from '@rbo/rbo-components/lib/Input';

import { FIELD_ID } from './constants';

export default class ExportModal extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isVisible: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    isMaskError: PropTypes.bool.isRequired,
    values: PropTypes.shape().isRequired,
    options: PropTypes.shape().isRequired,
    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeAction: PropTypes.func.isRequired,
    blurAction: PropTypes.func.isRequired,
    exportRequestAction: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { mountAction } = this.props;
    mountAction();
  }

  componentWillUnmount() {
    const { unmountAction } = this.props;
    unmountAction();
  }

  handleClose = () => {
    const { closeAction } = this.props;
    closeAction();
  };

  handleChange = ({ id, value }) => {
    const { changeAction } = this.props;
    changeAction(id, value);
  };

  handleBlur = ({ id, value }) => {
    const { blurAction } = this.props;
    blurAction(id, value);
  };

  handleSubmit = () => {
    const { exportRequestAction } = this.props;
    exportRequestAction();
  };

  render() {
    const { LABELS, isVisible, isLoading, isDisabled, isMaskError, values, options } = this.props;

    if (!isVisible) return null;

    return (
      <ModalWindow isClosingOnOutClick={false} isClosingOnEscPress={false} onClose={this.handleClose}>
        <div style={{ width: 480 }}>
          <Block borderBottomColor={Block.REFS.BORDER_COLORS.MARIGOLD}>
            <Header size={3}>{LABELS.HEADER}</Header>
          </Block>
          <Form dimension={Form.REFS.DIMENSIONS.XS}>
            <Block>
              <Row>
                <Cell width={140}>
                  <Label htmlFor={FIELD_ID.PERIOD_FROM}>
                    {LABELS.FIELDS.PERIOD.MAIN} {LABELS.FIELDS.PERIOD.FROM}
                  </Label>
                  <Field>
                    <InputDateCalendar
                      id={FIELD_ID.PERIOD_FROM}
                      value={values[FIELD_ID.PERIOD_FROM]}
                      onChange={this.handleChange}
                      disabled={isLoading}
                    />
                  </Field>
                </Cell>
                <Cell width={140}>
                  <Label htmlFor={FIELD_ID.PERIOD_TO}>{LABELS.FIELDS.PERIOD.TO}</Label>
                  <Field>
                    <InputDateCalendar
                      id={FIELD_ID.PERIOD_TO}
                      value={values[FIELD_ID.PERIOD_TO]}
                      onChange={this.handleChange}
                      disabled={isLoading}
                    />
                  </Field>
                </Cell>
              </Row>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.MASK_CSV}>{LABELS.FIELDS.MASK}</Label>
                  <Field>
                    <InputText
                      id={FIELD_ID.MASK_CSV}
                      value={values[FIELD_ID.MASK_CSV]}
                      onChange={this.handleChange}
                      onBlur={this.handleBlur}
                      disabled={isLoading}
                      isError={isMaskError}
                    />
                  </Field>
                  {isMaskError && <Status isError>{LABELS.OTHER.MASK_ERROR}</Status>}
                </Cell>
              </Row>
              <Row>
                <Cell width={140}>
                  <Label htmlFor={FIELD_ID.ENCODING}>{LABELS.FIELDS.ENCODING}</Label>
                  <Field>
                    <InputSelect
                      id={FIELD_ID.ENCODING}
                      value={values[FIELD_ID.ENCODING]}
                      options={options[FIELD_ID.ENCODING]}
                      onChange={this.handleChange}
                      disabled={isLoading}
                    />
                  </Field>
                </Cell>
                <Cell width={100}>
                  <Label htmlFor={FIELD_ID.SEPARATOR}>{LABELS.FIELDS.SEPARATOR}</Label>
                  <Field>
                    <InputText
                      id={FIELD_ID.SEPARATOR}
                      value={values[FIELD_ID.SEPARATOR]}
                      onChange={this.handleChange}
                      disabled={isLoading}
                      maxLength={1}
                    />
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Form>
          <Block borderTopColor={Block.REFS.BORDER_COLORS.BLACK_24}>
            <Row>
              <Cell width="auto">
                <Field>
                  <Button
                    id={FIELD_ID.SUBMIT_BUTTON}
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    isLoading={isLoading}
                    type={Button.REFS.TYPES.BASE_PRIMARY}
                  >
                    {LABELS.FIELDS.SUBMIT_BUTTON}
                  </Button>
                </Field>
              </Cell>
              <Cell width="auto">
                <Field>
                  <Button
                    id={FIELD_ID.CANCEL_BUTTON}
                    onClick={this.handleClose}
                    disabled={isLoading}
                    type={Button.REFS.TYPES.BASE_SECONDARY}
                  >
                    {LABELS.FIELDS.CANCEL_BUTTON}
                  </Button>
                </Field>
              </Cell>
            </Row>
          </Block>
        </div>
      </ModalWindow>
    );
  }
}
