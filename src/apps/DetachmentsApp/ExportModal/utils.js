export const checkMask = value => value && typeof value === 'string' && value.search(new RegExp('xxxx', 'ig')) >= 0;
