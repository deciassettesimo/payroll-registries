import { LOCALE_KEY, FIELD_ID } from './constants';

export const LABELS = {
  HEADER: LOCALE_KEY.HEADER,
  FIELDS: LOCALE_KEY.FIELDS,
  OTHER: LOCALE_KEY.OTHER,
};

export const ENCODINGS = [
  {
    value: 'windows',
    title: 'Windows',
  },
  {
    value: 'utf-8',
    title: 'UTF-8',
  },
  {
    value: 'dos',
    title: 'DOS',
  },
];

export const SETTINGS = {
  [FIELD_ID.MASK_CSV]: 'detach_to_csv_xxxx.csv',
  [FIELD_ID.ENCODING]: ENCODINGS[0].value,
  [FIELD_ID.SEPARATOR]: ';',
};
