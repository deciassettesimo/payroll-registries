import { ACTIONS } from './constants';

export const mountAction = () => ({
  type: ACTIONS.MOUNT_REQUEST,
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const openAction = (organizations, periodFrom, periodTo) => ({
  type: ACTIONS.OPEN,
  payload: { organizations, periodFrom, periodTo },
});

export const closeAction = () => ({
  type: ACTIONS.CLOSE,
});

export const changeAction = (id, value) => ({
  type: ACTIONS.CHANGE,
  payload: { id, value },
});

export const blurAction = (id, value) => ({
  type: ACTIONS.BLUR,
  payload: { id, value },
});

export const exportRequestAction = () => ({
  type: ACTIONS.EXPORT_REQUEST,
});

export const exportSuccessAction = () => ({
  type: ACTIONS.EXPORT_SUCCESS,
});

export const exportFailAction = errors => ({
  type: ACTIONS.EXPORT_FAIL,
  payload: { errors },
});
