import { createSelector } from 'reselect';

import localization from 'localization';

import { LABELS } from './config';
import { REDUCER_KEY, FIELD_ID } from './constants';
import { checkMask } from './utils';

const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const encodingsSelector = state => state[REDUCER_KEY].encodings;
const valuesSelector = state => state[REDUCER_KEY].values;
const settingsSelector = state => state[REDUCER_KEY].settings;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isVisibleCreatedSelector = createSelector(
  isMountedSelector,
  isVisibleSelector,
  (isMounted, isVisible) => isMounted && isVisible,
);

const valuesCreatedSelector = createSelector(
  valuesSelector,
  settingsSelector,
  (values, settings) => ({
    [FIELD_ID.TYPE]: settings[FIELD_ID.TYPE],
    [FIELD_ID.PERIOD_FROM]: values[FIELD_ID.PERIOD_FROM],
    [FIELD_ID.PERIOD_TO]: values[FIELD_ID.PERIOD_TO],
    [FIELD_ID.MASK_CSV]: values[FIELD_ID.MASK_CSV],
    [FIELD_ID.ENCODING]: settings[FIELD_ID.ENCODING],
    [FIELD_ID.SEPARATOR]: settings[FIELD_ID.SEPARATOR],
  }),
);

const optionsCreatedSelector = createSelector(
  encodingsSelector,
  encodings => ({
    [FIELD_ID.ENCODING]: encodings,
  }),
);

const isDisabledCreatedSelector = createSelector(
  valuesSelector,
  settingsSelector,
  (values, settings) => {
    if (!values[FIELD_ID.PERIOD_FROM] || !values[FIELD_ID.PERIOD_TO]) return true;
    if (!checkMask(values[FIELD_ID.MASK_CSV])) return true;
    if (!settings[FIELD_ID.SEPARATOR]) return true;
    return false;
  },
);

const isMaskErrorCreatedSelector = createSelector(
  valuesSelector,
  values => !checkMask(values[FIELD_ID.MASK_CSV]),
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isVisible: isVisibleCreatedSelector(state),
  values: valuesCreatedSelector(state),
  options: optionsCreatedSelector(state),
  isLoading: isLoadingSelector(state),
  isDisabled: isDisabledCreatedSelector(state),
  isMaskError: isMaskErrorCreatedSelector(state),
});

export default mapStateToProps;
