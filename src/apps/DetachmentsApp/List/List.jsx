import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import Button from '@rbo/rbo-components/lib/Button';
import ComboButton from '@rbo/rbo-components/lib/ComboButton';
import Filter from '@rbo/rbo-components/lib/Filter';
import Group from '@rbo/rbo-components/lib/Styles/Group';
import Header from '@rbo/rbo-components/lib/Styles/Header';
import IconRefresh from '@rbo/rbo-components/lib/Icon/IconRefresh';
import OperationsPanel from '@rbo/rbo-components/lib/OperationsPanel';
import Pagination from '@rbo/rbo-components/lib/Pagination';
import RboListFilters from '@rbo/rbo-components/lib/RboListFilters';
import RboListInfoBar from '@rbo/rbo-components/lib/RboListInfoBar';
import RboListLayout from '@rbo/rbo-components/lib/RboListLayout';
import RboPopupBoxOption from '@rbo/rbo-components/lib/RboPopupBoxOption';
import Status from '@rbo/rbo-components/lib/Styles/Status';
import Table from '@rbo/rbo-components/lib/Table';
import Tabs from '@rbo/rbo-components/lib/Tabs';

import RboHint from 'common/components/RboHint';
import RboServerError from 'common/components/RboServerError';

import { BUTTON_ID, COLUMN_ID, CONDITION_ID, OPERATION_ID } from './constants';
import Import1CModal from './Import1CModal';

const popupBoxOptionRenderer = ({ id, option }) => {
  switch (id) {
    case CONDITION_ID.BRANCH_INFO_NAME:
      return (
        <RboPopupBoxOption
          title={option.title}
          extra={option.extra}
          highlight={{ value: option.searchValue, inTitle: true }}
        />
      );
    case CONDITION_ID.CUSTOMER_ID:
      return (
        <RboPopupBoxOption
          title={option.title}
          extra={option.extra}
          describe={option.describe}
          highlight={{ value: option.searchValue, inTitle: true }}
        />
      );
    default:
      return <RboPopupBoxOption isInline label={option.title} />;
  }
};

const cellRenderer = ({ column, data }) => {
  switch (column) {
    case COLUMN_ID.STATUS:
      return (
        <Status
          isSuccess={data.statusFlags.isSuccess}
          isWarning={data.statusFlags.isWarning}
          isError={data.statusFlags.isError}
        >
          {data[column]}
        </Status>
      );
    case COLUMN_ID.BANK_MESSAGE:
      return <RboHint value={data[column]} />;
    default:
      return data[column];
  }
};

export default class List extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape({
      LOCALE: PropTypes.string.isRequired,
    }).isRequired,
    isError: PropTypes.bool.isRequired,
    serverError: PropTypes.shape(),
    isLoading: PropTypes.bool.isRequired,
    isListLoading: PropTypes.bool.isRequired,
    isListGrouped: PropTypes.bool.isRequired,
    isExportToExcelLoading: PropTypes.bool.isRequired,
    sections: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    pages: PropTypes.shape().isRequired,
    conditions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    filters: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    filter: PropTypes.shape(),
    columns: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    list: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    operations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    toolbarOperations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    selectedInfo: PropTypes.string,
    templates: PropTypes.arrayOf(PropTypes.shape()).isRequired,

    handleRefreshButtonClick: PropTypes.func.isRequired,
    handleCreateNewDocButtonClick: PropTypes.func.isRequired,
    handleCreateNewDocComboButtonClick: PropTypes.func.isRequired,
    handleItemClick: PropTypes.func.isRequired,
    handleSectionClick: PropTypes.func.isRequired,
    handlePageChange: PropTypes.func.isRequired,
    handlePaginationChange: PropTypes.func.isRequired,
    handleSortingChange: PropTypes.func.isRequired,
    handleResizeChange: PropTypes.func.isRequired,
    handleVisibilityChange: PropTypes.func.isRequired,
    handleGroupingChange: PropTypes.func.isRequired,
    handleFiltersRemove: PropTypes.func.isRequired,
    handleFiltersChoose: PropTypes.func.isRequired,
    handleFiltersCreate: PropTypes.func.isRequired,
    handleFiltersReset: PropTypes.func.isRequired,
    handleFiltersSave: PropTypes.func.isRequired,
    handleFiltersSaveAs: PropTypes.func.isRequired,
    handleFilterSearch: PropTypes.func.isRequired,
    handleFilterChange: PropTypes.func.isRequired,
    handleSelectChange: PropTypes.func.isRequired,
    handleTableOperationClick: PropTypes.func.isRequired,
    handleToolbarOperationClick: PropTypes.func.isRequired,
    handleExportToExcelClick: PropTypes.func.isRequired,
    handleTemplateEditClick: PropTypes.func.isRequired,

    isImport1CAllowed: PropTypes.bool.isRequired,
    import1CStartAction: PropTypes.func.isRequired,
  };

  static defaultProps = {
    serverError: null,
    filter: null,
    selectedInfo: null,
  };

  templatesItemRenderer = ({ data }) => {
    const { handleTemplateEditClick } = this.props;

    return (
      <RboPopupBoxOption
        value={data.id}
        title={data.title}
        describe={data.describe}
        onEditClick={handleTemplateEditClick}
      />
    );
  };

  handleExportButtonClick = () => {
    const { handleToolbarOperationClick } = this.props;
    handleToolbarOperationClick(OPERATION_ID.EXPORT);
  };

  render() {
    const {
      isError,
      serverError,
      isLoading,
      isListLoading,
      isListGrouped,
      isExportToExcelLoading,
      LABELS,
      sections,
      pages,
      conditions,
      filters,
      filter,
      columns,
      list,
      operations,
      toolbarOperations,
      selectedInfo,
      templates,

      handleRefreshButtonClick,
      handleCreateNewDocButtonClick,
      handleCreateNewDocComboButtonClick,
      handleItemClick,
      handleSectionClick,
      handlePageChange,
      handlePaginationChange,
      handleSortingChange,
      handleResizeChange,
      handleVisibilityChange,
      handleGroupingChange,
      handleFiltersRemove,
      handleFiltersChoose,
      handleFiltersCreate,
      handleFiltersReset,
      handleFiltersSave,
      handleFiltersSaveAs,
      handleFilterSearch,
      handleFilterChange,
      handleSelectChange,
      handleTableOperationClick,
      handleToolbarOperationClick,
      handleExportToExcelClick,

      isImport1CAllowed,
      import1CStartAction,
    } = this.props;

    if (isError) return <RboServerError serverData={serverError} />;

    return (
      <Fragment>
        <RboListLayout>
          <RboListLayout.Header>
            <RboListLayout.Header.Left>
              <RboListLayout.Header.Title>
                <Header size={3}>{LABELS.HEADER.TITLE}</Header>
              </RboListLayout.Header.Title>
              <RboListLayout.Header.Buttons>
                <Group>
                  <Button
                    id={BUTTON_ID.REFRESH}
                    type={Button.REFS.TYPES.LINK_SECONDARY}
                    dimension={Button.REFS.DIMENSIONS.S}
                    isWithIcon
                    onClick={handleRefreshButtonClick}
                  >
                    <IconRefresh dimension={IconRefresh.REFS.DIMENSIONS.XS} />
                  </Button>
                  {!!templates.length && (
                    <ComboButton
                      id={BUTTON_ID.CREATE_NEW_DOC}
                      type={Button.REFS.TYPES.ACCENT_PRIMARY}
                      dimension={Button.REFS.DIMENSIONS.S}
                      onClick={handleCreateNewDocComboButtonClick}
                      popupWidth={320}
                      items={[
                        {
                          id: BUTTON_ID.CREATE_NEW_DOC_FROM_TEMPLATES,
                          title: LABELS.HEADER.BUTTONS.CREATE_NEW_DOC_FROM_TEMPLATES,
                          items: templates,
                          itemRenderer: this.templatesItemRenderer,
                        },
                      ]}
                    >
                      {LABELS.HEADER.BUTTONS.CREATE_NEW_DOC}
                    </ComboButton>
                  )}
                  {!templates.length && (
                    <Button
                      id={BUTTON_ID.CREATE_NEW_DOC}
                      type={Button.REFS.TYPES.ACCENT_PRIMARY}
                      dimension={Button.REFS.DIMENSIONS.S}
                      onClick={handleCreateNewDocButtonClick}
                    >
                      {LABELS.HEADER.BUTTONS.CREATE_NEW_DOC}
                    </Button>
                  )}
                  {isImport1CAllowed && (
                    <Button
                      id={BUTTON_ID.IMPORT_1C}
                      type={Button.REFS.TYPES.BASE_PRIMARY}
                      dimension={Button.REFS.DIMENSIONS.S}
                      onClick={import1CStartAction}
                    >
                      {LABELS.HEADER.BUTTONS.IMPORT_1C}
                    </Button>
                  )}
                  <Button
                    id={BUTTON_ID.EXPORT}
                    type={Button.REFS.TYPES.BASE_PRIMARY}
                    dimension={Button.REFS.DIMENSIONS.S}
                    onClick={this.handleExportButtonClick}
                  >
                    {LABELS.HEADER.BUTTONS.EXPORT}
                  </Button>
                </Group>
              </RboListLayout.Header.Buttons>
            </RboListLayout.Header.Left>
            <RboListLayout.Header.Right>
              <Tabs
                dimension={Tabs.REFS.DIMENSIONS.L}
                align={Tabs.REFS.ALIGN.RIGHT}
                isEmphasis
                items={sections}
                onItemClick={handleSectionClick}
              />
            </RboListLayout.Header.Right>
          </RboListLayout.Header>
          <RboListLayout.Toolbar>
            <RboListLayout.Toolbar.Wide>
              {!isLoading && (
                <Fragment>
                  {!toolbarOperations.length ? (
                    <RboListFilters
                      locale={LABELS.LOCALE}
                      items={filters}
                      filter={filter}
                      optionRenderer={this.optionRenderer}
                      onRemove={handleFiltersRemove}
                      onChoose={handleFiltersChoose}
                      onCreate={handleFiltersCreate}
                      onReset={handleFiltersReset}
                      onSave={handleFiltersSave}
                      onSaveAs={handleFiltersSaveAs}
                    />
                  ) : (
                    <OperationsPanel
                      locale={LABELS.LOCALE}
                      dimension={OperationsPanel.REFS.DIMENSIONS.M}
                      items={toolbarOperations}
                      onItemClick={handleToolbarOperationClick}
                    />
                  )}
                </Fragment>
              )}
            </RboListLayout.Toolbar.Wide>
          </RboListLayout.Toolbar>
          {!isLoading && filter && (
            <RboListLayout.Filter>
              <Filter
                locale={LABELS.LOCALE}
                conditions={conditions}
                params={filter.params}
                optionRenderer={popupBoxOptionRenderer}
                onSearch={handleFilterSearch}
                onChange={handleFilterChange}
              />
            </RboListLayout.Filter>
          )}
          <RboListLayout.Content>
            <Table
              locale={LABELS.LOCALE}
              dimension={Table.REFS.DIMENSIONS.M}
              isLoading={isLoading || isListLoading}
              isGrouped={isListGrouped}
              columns={columns}
              items={list}
              cellRenderer={cellRenderer}
              onItemClick={handleItemClick}
              headBgColor={Table.REFS.HEAD_BG_COLORS.GRAY}
              operations={operations}
              isOperationsCompact
              onOperationClick={handleTableOperationClick}
              isSortable
              onSortingChange={handleSortingChange}
              isResizable
              onResizeChange={handleResizeChange}
              isSelectable
              onSelectChange={handleSelectChange}
              settings={{
                visibility: true,
                onVisibilityChange: handleVisibilityChange,
                grouping: true,
                onGroupingChange: handleGroupingChange,
              }}
            />
          </RboListLayout.Content>
          <RboListLayout.Footer>
            <RboListLayout.Footer.Left>
              {!selectedInfo && !!list.length && (
                <Button
                  id={BUTTON_ID.EXPORT_TO_EXCEL}
                  dimension={Button.REFS.DIMENSIONS.S}
                  type={Button.REFS.TYPES.LINK}
                  onClick={handleExportToExcelClick}
                  disabled={isExportToExcelLoading}
                >
                  {isExportToExcelLoading ? LABELS.OTHER.EXPORT_TO_EXCEL_LOADING : LABELS.OTHER.EXPORT_TO_EXCEL}
                </Button>
              )}
              {selectedInfo && <RboListInfoBar value={selectedInfo} />}
            </RboListLayout.Footer.Left>
            <RboListLayout.Footer.Right>
              {!isLoading && (
                <Pagination
                  locale={LABELS.LOCALE}
                  selected={pages.selected}
                  visibleQty={pages.visibleQty}
                  totalQty={pages.totalQty}
                  settings={pages.settings}
                  onItemClick={handlePageChange}
                  onSettingsChange={handlePaginationChange}
                />
              )}
            </RboListLayout.Footer.Right>
          </RboListLayout.Footer>
        </RboListLayout>
        <Import1CModal />
      </Fragment>
    );
  }
}
