import { ACTIONS } from './constants';

export const mountAction = location => ({
  type: ACTIONS.MOUNT_REQUEST,
  payload: { location },
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const changeSectionAction = (sectionId, params) => ({
  type: ACTIONS.SECTION_CHANGE,
  payload: { sectionId, params },
});

export const listRefreshAction = () => ({
  type: ACTIONS.GET_LIST_REQUEST,
});

export const createNewDocAction = (types, params) => ({
  type: ACTIONS.CREATE_NEW_DOC,
  payload: { types, params },
});

export const openItemAction = (id, params) => ({
  type: ACTIONS.ROUTE_TO_DOC,
  payload: { id, params },
});

export const pageChangeAction = (page, params) => ({
  type: ACTIONS.PAGE_CHANGE,
  payload: { page, params },
});

export const paginationChangeAction = (value, params) => ({
  type: ACTIONS.PAGINATION_CHANGE,
  payload: { value, params },
});

export const sortingChangeAction = sorting => ({
  type: ACTIONS.SORTING_CHANGE,
  payload: { sorting },
});

export const resizeChangeAction = width => ({
  type: ACTIONS.RESIZE_CHANGE,
  payload: { width },
});

export const visibilityChangeAction = visibility => ({
  type: ACTIONS.VISIBILITY_CHANGE,
  payload: { visibility },
});

export const groupingChangeAction = grouped => ({
  type: ACTIONS.GROUPING_CHANGE,
  payload: { grouped },
});

export const filtersRemoveAction = id => ({
  type: ACTIONS.FILTERS_REMOVE,
  payload: { id },
});

export const filtersChooseAction = (id, params) => ({
  type: ACTIONS.FILTERS_CHOOSE,
  payload: { id, params },
});

export const filtersCreateAction = () => ({
  type: ACTIONS.FILTERS_CREATE,
});

export const filtersResetAction = params => ({
  type: ACTIONS.FILTERS_RESET,
  payload: { params },
});

export const filtersSaveAction = () => ({
  type: ACTIONS.FILTERS_SAVE,
});

export const filtersSaveAsAction = (title, params) => ({
  type: ACTIONS.FILTERS_SAVE_AS,
  payload: { title, params },
});

export const filterChangeAction = (filter, isChanged, params) => ({
  type: ACTIONS.FILTER_CHANGE,
  payload: { filter, isChanged, params },
});

export const filterSearchAction = (conditionId, conditionSearchValue) => ({
  type: ACTIONS.FILTER_SEARCH,
  payload: { conditionId, conditionSearchValue },
});

export const selectChangeAction = selected => ({
  type: ACTIONS.SELECT_CHANGE,
  payload: { selected },
});

export const operationsAction = (operationId, itemId, params) => ({
  type: ACTIONS.OPERATIONS,
  payload: { operationId, itemId, params },
});

export const exportToExcelAction = () => ({
  type: ACTIONS.EXPORT_TO_EXCEL_REQUEST,
});

export const openTemplateAction = (id, params) => ({
  type: ACTIONS.ROUTE_TO_TEMPLATE,
  payload: { id, params },
});

export const import1CStartAction = () => ({
  type: ACTIONS.IMPORT_1C_START,
});
