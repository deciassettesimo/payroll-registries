import { compose } from 'redux';
import { connect } from 'react-redux';
import { lifecycle, withHandlers } from 'recompose';
import { withLastLocation } from 'react-router-last-location';

import { lifecycleList } from 'hocs/lifecycles';
import { handlersList, handlersListTemplates } from 'hocs/handlers';

import List from './List';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default compose(
  withLastLocation,
  connect(
    mapStateToProps,
    actions,
  ),
  lifecycle(lifecycleList),
  withHandlers({
    ...handlersList,
    ...handlersListTemplates,
  }),
)(List);
