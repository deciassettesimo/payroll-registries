import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import ModalWindow from '@rbo/rbo-components/lib/ModalWindow';
import Tabs from '@rbo/rbo-components/lib/Tabs';
import Loader from '@rbo/rbo-components/lib/Loader';
import { List, Block, Header, Label as StylesLabel, Status } from '@rbo/rbo-components/lib/Styles';
import Form, { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import { InputSelect, InputFiles, InputCheckbox } from '@rbo/rbo-components/lib/Input';
import Button from '@rbo/rbo-components/lib/Button/index';

import { TAB_ID, FIELD_ID, RESULTS } from './constants';

export default class Import1CModal extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isVisible: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isUploaded: PropTypes.bool.isRequired,
    isFinished: PropTypes.bool.isRequired,
    tabs: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    activeTab: PropTypes.oneOf(Object.values(TAB_ID)).isRequired,
    values: PropTypes.shape().isRequired,
    options: PropTypes.shape().isRequired,
    results: PropTypes.shape().isRequired,
    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeTabAction: PropTypes.func.isRequired,
    changeAction: PropTypes.func.isRequired,
    routeToDocAction: PropTypes.func.isRequired,
    startNewImportAction: PropTypes.func.isRequired,

    history: PropTypes.shape().isRequired,
  };

  componentDidMount() {
    const { mountAction } = this.props;
    mountAction();
  }

  componentWillUnmount() {
    const { unmountAction } = this.props;
    unmountAction();
  }

  handleClose = () => {
    const { closeAction } = this.props;
    closeAction();
  };

  handleChange = ({ id, value }) => {
    const { changeAction } = this.props;
    changeAction(id, value);
  };

  handleRouteToDocButtonClick = () => {
    const { routeToDocAction, history } = this.props;
    routeToDocAction({ history });
  };

  render() {
    const {
      LABELS,
      isVisible,
      isLoading,
      isUploaded,
      isFinished,
      tabs,
      activeTab,
      values,
      options,
      results,
      changeTabAction,
      startNewImportAction,
    } = this.props;

    if (!isVisible) return null;

    return (
      <ModalWindow isClosingOnOutClick={false} isClosingOnEscPress={false} onClose={this.handleClose}>
        <div style={{ width: 786, minHeight: 360 }}>
          <Block borderBottomColor={Block.REFS.BORDER_COLORS.MARIGOLD}>
            <List
              isInline
              vAlignItems={List.REFS.V_ALIGN_ITEMS.BOTTOM}
              vPadding={0}
              hPadding={0}
              innerVPadding={0}
              innerHPadding={32}
            >
              <List.Item>
                <Header size={3}>{LABELS.HEADER}</Header>
              </List.Item>
              <List.Item>
                <div style={{ marginBottom: -16 }}>
                  <Tabs dimension={Tabs.REFS.DIMENSIONS.S} items={tabs} onItemClick={changeTabAction} />
                </div>
              </List.Item>
            </List>
          </Block>
          {activeTab === TAB_ID.UPLOAD && (
            <Fragment>
              {!isUploaded && !isFinished && (
                <Block>
                  <Form dimension={Form.REFS.DIMENSIONS.XS}>
                    <Row>
                      <Cell>
                        <Field>
                          <InputFiles
                            locale={LABELS.LOCALE}
                            id={FIELD_ID.FILE}
                            value={values[FIELD_ID.FILE]}
                            onChange={this.handleChange}
                            isLoading={isLoading}
                            minHeight={200}
                            maxFilesQty={1}
                            isWithComment={false}
                          />
                        </Field>
                      </Cell>
                    </Row>
                    <Row>
                      <Cell width={140}>
                        <Label htmlFor={FIELD_ID.ENCODING}>{LABELS.FIELDS.ENCODING}</Label>
                        <Field>
                          <InputSelect
                            id={FIELD_ID.ENCODING}
                            value={values[FIELD_ID.ENCODING]}
                            options={options[FIELD_ID.ENCODING]}
                            onChange={this.handleChange}
                            disabled={isLoading}
                          />
                        </Field>
                      </Cell>
                    </Row>
                  </Form>
                </Block>
              )}
              {(isUploaded || isFinished) && (
                <Block>
                  <List>
                    <List.Item>
                      <StylesLabel>{LABELS.RESULTS.TASK_ID}</StylesLabel> {results[RESULTS.TASK_ID]}
                    </List.Item>
                    <List.Item>
                      <StylesLabel>{LABELS.RESULTS.START}</StylesLabel> {results[RESULTS.START]}
                    </List.Item>
                    {results[RESULTS.FINISH] && (
                      <List.Item>
                        <StylesLabel>{LABELS.RESULTS.FINISH}</StylesLabel> {results[RESULTS.FINISH]}
                      </List.Item>
                    )}
                  </List>
                </Block>
              )}
              {isUploaded && (
                <Block>
                  <Loader isCentered />
                </Block>
              )}
              {isFinished && (
                <Block>
                  <List>
                    <List.Item>
                      <Header size={5}>{LABELS.RESULTS.RESULT_HEADER}</Header>
                    </List.Item>
                    <List.Item>
                      <StylesLabel>{LABELS.RESULTS.STATUS}</StylesLabel>{' '}
                      <Status isError={results[RESULTS.STATUS].isError} isSuccess={results[RESULTS.STATUS].isSuccess}>
                        {results[RESULTS.STATUS].title}
                      </Status>
                    </List.Item>
                    {results[RESULTS.MESSAGE] && <List.Item>{results[RESULTS.MESSAGE]}</List.Item>}
                    {results[RESULTS.TOTAL_NUMBER] && (
                      <List.Item>
                        <StylesLabel>{LABELS.RESULTS.TOTAL_NUMBER}</StylesLabel> {results[RESULTS.TOTAL_NUMBER]}
                      </List.Item>
                    )}
                  </List>
                </Block>
              )}
              {isFinished && (
                <Block borderTopColor={Block.REFS.BORDER_COLORS.BLACK_24}>
                  <Form>
                    <Row>
                      <Cell width="auto">
                        {results[RESULTS.STATUS].isSuccess && (
                          <Field>
                            <Button
                              type={Button.REFS.TYPES.BASE_PRIMARY}
                              id={FIELD_ID.BUTTON_ROUTE_TO_DOC}
                              onClick={this.handleRouteToDocButtonClick}
                            >
                              {LABELS.FIELDS.BUTTON_ROUTE_TO_DOC}
                            </Button>
                          </Field>
                        )}
                        {results[RESULTS.STATUS].isError && (
                          <Field>
                            <Button
                              type={Button.REFS.TYPES.BASE_PRIMARY}
                              id={FIELD_ID.BUTTON_START_NEW_IMPORT}
                              onClick={startNewImportAction}
                            >
                              {LABELS.FIELDS.BUTTON_START_NEW_IMPORT}
                            </Button>
                          </Field>
                        )}
                      </Cell>
                      <Cell width="auto">
                        <Field>
                          <Button id={FIELD_ID.BUTTON_CLOSE} onClick={this.handleClose}>
                            {LABELS.FIELDS.BUTTON_CLOSE}
                          </Button>
                        </Field>
                      </Cell>
                    </Row>
                  </Form>
                </Block>
              )}
            </Fragment>
          )}
          {activeTab === TAB_ID.SETTINGS && (
            <Block>
              <Form dimension={Form.REFS.DIMENSIONS.XS}>
                <Row>
                  <Cell>
                    <Field>
                      <InputCheckbox
                        id={FIELD_ID.NUMBER_FROM_FILE}
                        checked={values[FIELD_ID.NUMBER_FROM_FILE]}
                        onChange={this.handleChange}
                      >
                        {LABELS.FIELDS.NUMBER_FROM_FILE}
                      </InputCheckbox>
                    </Field>
                  </Cell>
                </Row>
                <Row>
                  <Cell>
                    <Field>
                      <InputCheckbox
                        id={FIELD_ID.DATE_FROM_FILE}
                        checked={values[FIELD_ID.DATE_FROM_FILE]}
                        onChange={this.handleChange}
                      >
                        {LABELS.FIELDS.DATE_FROM_FILE}
                      </InputCheckbox>
                    </Field>
                  </Cell>
                </Row>
                <Row>
                  <Cell>
                    <Field>
                      <InputCheckbox
                        id={FIELD_ID.NPP_FROM_FILE}
                        checked={values[FIELD_ID.NPP_FROM_FILE]}
                        onChange={this.handleChange}
                      >
                        {LABELS.FIELDS.NPP_FROM_FILE}
                      </InputCheckbox>
                    </Field>
                  </Cell>
                </Row>
                <Row>
                  <Cell>
                    <Field>
                      <InputCheckbox
                        id={FIELD_ID.DUPLICATE_CONTROLS}
                        checked={values[FIELD_ID.DUPLICATE_CONTROLS]}
                        onChange={this.handleChange}
                      >
                        {LABELS.FIELDS.DUPLICATE_CONTROLS}
                      </InputCheckbox>
                    </Field>
                  </Cell>
                </Row>
                {values[FIELD_ID.DUPLICATE_CONTROLS] && (
                  <Row>
                    <Cell width={24} />
                    <Cell width="auto">
                      <Field>
                        <InputCheckbox
                          id={FIELD_ID.DUPLICATE_CONTROL_NUMBER}
                          checked={values[FIELD_ID.DUPLICATE_CONTROL_NUMBER]}
                          onChange={this.handleChange}
                        >
                          {LABELS.FIELDS.DUPLICATE_CONTROL_NUMBER}
                        </InputCheckbox>
                      </Field>
                    </Cell>
                    <Cell width="auto">
                      <Field>
                        <InputCheckbox
                          id={FIELD_ID.DUPLICATE_CONTROL_DATE}
                          checked={values[FIELD_ID.DUPLICATE_CONTROL_DATE]}
                          onChange={this.handleChange}
                        >
                          {LABELS.FIELDS.DUPLICATE_CONTROL_DATE}
                        </InputCheckbox>
                      </Field>
                    </Cell>
                  </Row>
                )}
              </Form>
            </Block>
          )}
        </div>
      </ModalWindow>
    );
  }
}
