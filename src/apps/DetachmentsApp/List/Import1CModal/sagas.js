import { call, put, select, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import { IMPORT_TASK_STATUS_ID } from 'common/constants';
import {
  import1C as dalImport1C,
  import1CMonitoring as dalImport1CMonitoring,
} from 'dal/payroll-registries-detachments';
import localization from 'localization';
import { notificationError } from 'platform';
import { dispatchErrorAction, getRoutePath } from 'utils';

import { REDUCER_KEY as APP_REDUCER_KEY, ROUTES_IDS as APP_ROUTES_IDS, ACTIONS as APP_ACTIONS } from '../../constants';

import { ACTIONS as LIST_ACTIONS } from '../constants';

import { SETTINGS } from './config';
import { REDUCER_KEY, FIELD_ID, ACTIONS, MONITORING_INTERVAL, LOCALE_KEY } from './constants';

const appSettingsSelector = state => state[APP_REDUCER_KEY].settings[REDUCER_KEY];
const appRoutesSelector = state => state[APP_REDUCER_KEY].routes;
const settingsSelector = state => state[REDUCER_KEY].settings;
const taskIdSelector = state => state[REDUCER_KEY].taskId;
const resultsSelector = state => state[REDUCER_KEY].results;

function* mountSaga() {
  try {
    const settings = yield select(appSettingsSelector);
    yield put({ type: ACTIONS.MOUNT_SUCCESS, payload: { settings: settings || SETTINGS } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsSaga() {
  const settings = yield select(settingsSelector);
  yield put({ type: APP_ACTIONS.SAVE_SETTINGS_REQUEST, payload: { key: REDUCER_KEY, data: settings } });
}

function* changeSaga({ payload }) {
  const { id, value } = payload;
  const settings = yield select(settingsSelector);
  switch (id) {
    case FIELD_ID.FILE:
      yield put({ type: ACTIONS.IMPORT_REQUEST, payload: { file: value[0].file } });
      break;
    case FIELD_ID.ENCODING:
    case FIELD_ID.NUMBER_FROM_FILE:
    case FIELD_ID.DATE_FROM_FILE:
    case FIELD_ID.NPP_FROM_FILE:
    case FIELD_ID.DUPLICATE_CONTROLS:
    case FIELD_ID.DUPLICATE_CONTROL_NUMBER:
    case FIELD_ID.DUPLICATE_CONTROL_DATE:
    default:
      yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings: { ...settings, [id]: value } } });
  }
}

function* importRequestSaga({ payload }) {
  try {
    const { file } = payload;
    const settings = yield select(settingsSelector);
    const result = yield call(dalImport1C, file, {
      encoding: settings[FIELD_ID.ENCODING],
      numberFromFile: settings[FIELD_ID.NUMBER_FROM_FILE],
      dateFromFile: settings[FIELD_ID.DATE_FROM_FILE],
      nppFromFile: settings[FIELD_ID.NPP_FROM_FILE],
      controlDuplicate: settings[FIELD_ID.DUPLICATE_CONTROLS],
      numberControlDuplicate: settings[FIELD_ID.DUPLICATE_CONTROL_NUMBER],
      dateControlDuplicate: settings[FIELD_ID.DUPLICATE_CONTROL_DATE],
    });
    if (result.error) {
      yield dispatchErrorAction({ error: {}, types: [ACTIONS.IMPORT_FAIL] });
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.FAIL.TITLE),
        message: result.error,
      });
    }
    if (result.taskId) {
      yield put({ type: ACTIONS.IMPORT_UPLOADED, payload: result });
      yield put({ type: ACTIONS.MONITORING_REQUEST });
    }
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.IMPORT_FAIL] });
  }
}

function* importSuccessSaga() {
  const results = yield select(resultsSelector);
  if (results.status.id === IMPORT_TASK_STATUS_ID.SUCCESS) {
    yield put({ type: LIST_ACTIONS.GET_LIST_REQUEST });
  }
}

function* monitoringSaga() {
  try {
    const taskId = yield select(taskIdSelector);
    const result = yield call(dalImport1CMonitoring, taskId);
    yield put({ type: ACTIONS.MONITORING_SUCCESS, payload: { results: result } });
    if (result.status.id === IMPORT_TASK_STATUS_ID.NOT_COMPLETED) {
      yield delay(MONITORING_INTERVAL);
      yield put({ type: ACTIONS.MONITORING_REQUEST, payload: { results: result } });
    } else {
      yield put({ type: ACTIONS.IMPORT_SUCCESS, payload: { results: result } });
    }
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MONITORING_FAIL] });
  }
}

function* routeToDocSaga({ payload }) {
  const { params } = payload;
  const results = yield select(resultsSelector);
  const id = results.docId;
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', id));
}

export default function* sagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS, saveSettingsSaga);
  yield takeEvery(ACTIONS.CHANGE, changeSaga);
  yield takeEvery(ACTIONS.IMPORT_REQUEST, importRequestSaga);
  yield takeEvery(ACTIONS.IMPORT_SUCCESS, importSuccessSaga);
  yield takeEvery(ACTIONS.MONITORING_REQUEST, monitoringSaga);
  yield takeEvery(ACTIONS.ROUTE_TO_DOC, routeToDocSaga);
}
