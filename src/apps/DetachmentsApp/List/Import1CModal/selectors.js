import { createSelector } from 'reselect';

import { IMPORT_TASK_STATUS_ID } from 'common/constants';
import localization from 'localization';
import { REDUCER_KEY as ROOT_REDUCER_KEY } from 'store/constants';
import getFormattedDate from 'utils/getFormattedDate';

import { LABELS } from './config';
import { REDUCER_KEY, TAB_ID, FIELD_ID, RESULTS } from './constants';

const localeSelector = state => state[ROOT_REDUCER_KEY].locale;
const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const isLoadingSelector = state => state[REDUCER_KEY].isLoading;
const isUploadedSelector = state => state[REDUCER_KEY].isUploaded;
const isFinishedSelector = state => state[REDUCER_KEY].isFinished;
const tabsSelector = state => state[REDUCER_KEY].tabs;
const activeTabSelector = state => state[REDUCER_KEY].activeTab;
const encodingsSelector = state => state[REDUCER_KEY].encodings;
const settingsSelector = state => state[REDUCER_KEY].settings;
const taskIdSelector = state => state[REDUCER_KEY].taskId;
const resultsSelector = state => state[REDUCER_KEY].results;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isVisibleCreatedSelector = createSelector(
  isMountedSelector,
  isVisibleSelector,
  (isMounted, isVisible) => isMounted && isVisible,
);

const tabsCreatedSelector = createSelector(
  localeSelector,
  tabsSelector,
  activeTabSelector,
  isLoadingSelector,
  isUploadedSelector,
  isFinishedSelector,
  (locale, tabs, activeTab, isLoading, isUploaded, isFinished) =>
    tabs.map(item => ({
      id: item.id,
      title: localization.translate(item.titleKey),
      isActive: item.id === activeTab,
      disabled: item.id === TAB_ID.SETTINGS && (isLoading || isUploaded || isFinished),
    })),
);

const valuesCreatedSelector = createSelector(
  settingsSelector,
  settings => ({
    [FIELD_ID.FILE]: [],
    [FIELD_ID.ENCODING]: settings[FIELD_ID.ENCODING],
    [FIELD_ID.NUMBER_FROM_FILE]: settings[FIELD_ID.NUMBER_FROM_FILE],
    [FIELD_ID.DATE_FROM_FILE]: settings[FIELD_ID.DATE_FROM_FILE],
    [FIELD_ID.NPP_FROM_FILE]: settings[FIELD_ID.NPP_FROM_FILE],
    [FIELD_ID.DUPLICATE_CONTROLS]: settings[FIELD_ID.DUPLICATE_CONTROLS],
    [FIELD_ID.DUPLICATE_CONTROL_NUMBER]: settings[FIELD_ID.DUPLICATE_CONTROL_NUMBER],
    [FIELD_ID.DUPLICATE_CONTROL_DATE]: settings[FIELD_ID.DUPLICATE_CONTROL_DATE],
  }),
);

const optionsCreatedSelector = createSelector(
  encodingsSelector,
  encodings => ({
    [FIELD_ID.ENCODING]: encodings,
  }),
);

const resultsCreatedSelector = createSelector(
  taskIdSelector,
  resultsSelector,
  (taskId, results) => ({
    [RESULTS.TASK_ID]: taskId,
    [RESULTS.START]: getFormattedDate.withTime(results.startTime),
    [RESULTS.FINISH]: getFormattedDate.withTime(results.finishTime),
    [RESULTS.TOTAL_NUMBER]: results.emplAmount,
    [RESULTS.STATUS]: results.status
      ? {
          title: localization.translate(results.status.titleKey),
          isSuccess: results.status.id === IMPORT_TASK_STATUS_ID.SUCCESS,
          isError: results.status.id === IMPORT_TASK_STATUS_ID.FAILED,
        }
      : {},
    [RESULTS.DOC_ID]: results.docId,
    [RESULTS.MESSAGE]: results.message,
  }),
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isVisible: isVisibleCreatedSelector(state),
  isLoading: isLoadingSelector(state),
  isUploaded: isUploadedSelector(state),
  isFinished: isFinishedSelector(state),
  tabs: tabsCreatedSelector(state),
  activeTab: activeTabSelector(state),
  values: valuesCreatedSelector(state),
  options: optionsCreatedSelector(state),
  results: resultsCreatedSelector(state),
});

export default mapStateToProps;
