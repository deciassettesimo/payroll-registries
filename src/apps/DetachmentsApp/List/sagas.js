import { all, call, put, select, takeEvery } from 'redux-saga/effects';
import qs from 'qs';

import { DOC_NEW_ID, PERMISSIONS_IDS } from 'common/constants';
import operations from 'common/operations';
import {
  openAction as saveAsTemplateModalOpenAction,
  closeAction as saveAsTemplateModalCloseAction,
  saveRequestAction as saveAsTemplateModalRequestAction,
  saveSuccessAction as saveAsTemplateModalSuccessAction,
  saveFailAction as saveAsTemplateModalFailAction,
} from 'common/components/RboDocSaveAsTemplateModal/actions';
import {
  getList as dalGetList,
  exportToExcel as dalExportToExcel,
  getTemplates as dalGetTemplates,
  saveAsTemplate as dalSaveAsTemplate,
  getBranches as dalGetBranches,
} from 'dal/payroll-registries-detachments';
import {
  dispatchErrorAction,
  normalizeListSettings,
  getFilterForQuery,
  getFilterFromQuery,
  getFormattedDate,
  getRoutePath,
} from 'utils';

import { REDUCER_KEY as APP_REDUCER_KEY, ACTIONS as APP_ACTIONS, ROUTES_IDS as APP_ROUTES_IDS } from '../constants';
import { openAction as exportModalOpenAction } from '../ExportModal/actions';

import { OPERATIONS_ACTIONS_MAP, SETTINGS, COLUMNS, CONDITIONS } from './config';
import { REDUCER_KEY, SECTION_ID, CONDITION_ID, ACTIONS, FIELD_ID, BUTTON_ID } from './constants';
import { prepareParams } from './utils';
import import1CModalSagas from './Import1CModal/sagas';
import { openAction as import1CModalOpenAction } from './Import1CModal/actions';

const appSettingsSelector = state => state[APP_REDUCER_KEY].settings[REDUCER_KEY];
const appPermissionsSelector = state => state[APP_REDUCER_KEY].permissions;
const appOrganizationsSelector = state => state[APP_REDUCER_KEY].organizations;
const appStatusesSelector = state => state[APP_REDUCER_KEY].statuses;
const appSelectedOrganizationsSelector = state => state[APP_REDUCER_KEY].settings.selectedOrganizations;
const appRoutesSelector = state => state[APP_REDUCER_KEY].routes;
const settingsSelector = state => state[REDUCER_KEY].settings;
const sectionsSelector = state => state[REDUCER_KEY].sections;
const pagesSelector = state => state[REDUCER_KEY].pages;
const conditionsOptionsSelector = state => state[REDUCER_KEY].conditionsOptions;
const filtersSelector = state => state[REDUCER_KEY].filters;
const filterSelector = state => state[REDUCER_KEY].filter;
const selectedSelector = state => state[REDUCER_KEY].selected;
const listSelector = state => state[REDUCER_KEY].list;

function* mountSaga({ payload }) {
  try {
    const { location } = payload;
    const { pathname } = location;
    const sections = yield select(sectionsSelector);
    const activeSection = sections.filter(item => pathname.match(new RegExp(`${item.route}`, 'gi')));
    const sectionId = activeSection.length ? activeSection[activeSection.length - 1].id : SECTION_ID.WORKING;
    const query = location.search.replace(/^\?/, '');
    const { page, filter } = qs.parse(query);
    const appSettings = yield select(appSettingsSelector);
    const sectionSettings = appSettings && appSettings[sectionId];
    const { filters, ...settings } = normalizeListSettings(sectionSettings, SETTINGS, COLUMNS, CONDITIONS);

    const statuses = yield select(appStatusesSelector);
    const organizations = yield select(appOrganizationsSelector);
    const selectedOrganizations = yield select(appSelectedOrganizationsSelector);
    const branches = yield call(dalGetBranches, selectedOrganizations);
    const permissions = yield select(appPermissionsSelector);
    const templates = permissions[PERMISSIONS_IDS.CREATE_FROM_TEMPLATE]
      ? yield call(dalGetTemplates, { orgId: selectedOrganizations })
      : [];

    yield put({
      type: ACTIONS.MOUNT_SUCCESS,
      payload: {
        sectionId,
        page: page ? parseInt(page, 10) : 1,
        filter: getFilterFromQuery(filter),
        filters,
        settings,
        templates,
        conditionsOptions: {
          [CONDITION_ID.STATUS]: statuses,
          [CONDITION_ID.CUSTOMER_ID]: selectedOrganizations.map(item =>
            organizations.find(organization => organization.id === item),
          ),
          [CONDITION_ID.BRANCH_INFO_NAME]: branches,
        },
      },
    });
    yield put({ type: ACTIONS.GET_LIST_REQUEST });
    yield put({ type: ACTIONS.GET_TEMPLATES_REQUEST });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.MOUNT_FAIL] });
  }
}

function* saveSettingsSaga() {
  const sections = yield select(sectionsSelector);
  const settings = yield select(settingsSelector);
  const filters = yield select(filtersSelector);
  const userFilters = filters
    .filter(filter => !filter.isPreset && !filter.isNew)
    .map(filter => ({ id: filter.id, title: filter.title, params: filter.params }));
  const sectionId = sections.find(section => section.isActive).id;
  const appSettings = yield select(appSettingsSelector);
  const newSettings = { ...appSettings, [sectionId]: { ...settings, filters: userFilters } };
  yield put({ type: APP_ACTIONS.SAVE_SETTINGS_REQUEST, payload: { key: REDUCER_KEY, data: newSettings } });
}

function* sectionChangeSaga({ payload }) {
  const { sectionId, params } = payload;
  const sections = yield select(sectionsSelector);
  const activeSection = sections.find(item => item.id === sectionId);
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, getRoutePath(routes, activeSection.route));
}

function* pageChangeSaga({ payload }) {
  const { params } = payload;
  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);

  yield call(params.history.push, {
    pathname: params.location.pathname,
    search: `?${qs.stringify({ ...query, page: pages.selected })}`,
  });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* paginationChangeSaga({ payload }) {
  const { value, params } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, pagination: value };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });

  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);
  yield call(params.history.replace, {
    pathname: params.location.pathname,
    search: `?${qs.stringify({ ...query, page: pages.selected })}`,
  });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* sortingChangeSaga({ payload }) {
  const { sorting } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = {
    ...currentSettings,
    sorting,
    grouped: currentSettings.grouped !== sorting.id ? null : currentSettings.grouped,
  };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* resizeChangeSaga({ payload }) {
  const { width } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, width };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
}

function* visibilityChangeSaga({ payload }) {
  const { visibility } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = { ...currentSettings, visibility };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
}

function* groupingChangeSaga({ payload }) {
  const { grouped } = payload;
  const currentSettings = yield select(settingsSelector);
  const settings = {
    ...currentSettings,
    grouped,
    sorting: grouped ? { id: grouped, direction: 1 } : currentSettings.sorting,
  };
  yield put({ type: ACTIONS.SAVE_SETTINGS, payload: { settings } });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersRemoveSaga() {
  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filtersChooseSaga({ payload }) {
  const { params } = payload;
  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);
  const selectedFilter = yield select(filterSelector);
  const filter = getFilterForQuery(selectedFilter);

  yield call(params.history.replace, {
    pathname: params.location.pathname,
    search: `?${qs.stringify({ ...query, page: pages.selected, filter })}`,
  });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersSaveSaga() {
  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filtersResetSaga({ payload }) {
  const { params } = payload;
  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);
  const { filter, ...restQuery } = query;

  yield call(params.history.replace, {
    pathname: params.location.pathname,
    search: `?${qs.stringify({ ...restQuery, page: pages.selected })}`,
  });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* filtersSaveAsSaga({ payload }) {
  const { params } = payload;
  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);
  const selectedFilter = yield select(filterSelector);
  const filter = getFilterForQuery(selectedFilter);

  yield call(params.history.replace, {
    pathname: params.location.pathname,
    search: `?${qs.stringify({ ...query, filter, page: pages.selected })}`,
  });

  yield put({ type: ACTIONS.SAVE_SETTINGS });
}

function* filterChangeSaga({ payload }) {
  const { isChanged, params } = payload;
  if (!isChanged) return;
  const query = qs.parse(params.location.search.replace(/^\?/, ''));
  const pages = yield select(pagesSelector);
  const { filter: queryFilter, ...restQuery } = query;
  const selectedFilter = yield select(filterSelector);
  const filter = selectedFilter ? getFilterForQuery(selectedFilter) : null;

  yield call(params.history.replace, {
    pathname: params.location.pathname,
    search: `?${qs.stringify(
      filter ? { ...restQuery, filter, page: pages.selected } : { ...restQuery, page: pages.selected },
    )}`,
  });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* getListSaga() {
  try {
    const sections = yield select(sectionsSelector);
    const settings = yield select(settingsSelector);
    const conditionsOptions = yield select(conditionsOptionsSelector);
    const filter = yield select(filterSelector);
    const pages = yield select(pagesSelector);
    const { section, ...data } = prepareParams({
      sections,
      settings,
      conditionsOptions,
      filter,
      pages,
    });
    const result = yield call(dalGetList, section, data);
    yield put({ type: ACTIONS.GET_LIST_SUCCESS, payload: { list: result } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.GET_LIST_FAIL] });
  }
}

function* exportToExcelSaga() {
  try {
    const sections = yield select(sectionsSelector);
    const settings = yield select(settingsSelector);
    const conditionsOptions = yield select(conditionsOptionsSelector);
    const filter = yield select(filterSelector);
    const pages = yield select(pagesSelector);
    const { section, ...data } = prepareParams({
      isExport: true,
      sections,
      settings,
      conditionsOptions,
      filter,
      pages,
    });
    const result = yield call(dalExportToExcel, section, data);
    yield put({ type: ACTIONS.EXPORT_TO_EXCEL_SUCCESS, payload: { list: result } });
  } catch (error) {
    yield dispatchErrorAction({ error: (error && error.response) || {}, types: [ACTIONS.EXPORT_TO_EXCEL_FAIL] });
  }
}

function* createNewDocSaga({ payload }) {
  const { types, params } = payload;
  const routes = yield select(appRoutesSelector);
  if (types && types.length) {
    if (types[0] === BUTTON_ID.CREATE_NEW_DOC_FROM_TEMPLATES && types[1]) {
      yield call(params.history.push, {
        pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
        search: `?${qs.stringify({ type: 'template', templateId: types[1] })}`,
      });
    }
  } else {
    yield call(params.history.push, getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID));
  }
}

function* routeToDocSaga({ payload }) {
  const { id, params } = payload;
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', id));
}

function* routeToTemplateSaga({ payload }) {
  const { id, params } = payload;
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, getRoutePath(routes, APP_ROUTES_IDS.TEMPLATE).replace(':templateId', id));
}

function* operationsSaga({ payload }) {
  const { operationId } = payload;
  yield put({ type: OPERATIONS_ACTIONS_MAP[operationId], payload });
}

function* operationArchiveSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.archive, {
    ids: documentIds,
    channels: { success: ACTIONS.OPERATION_ARCHIVE_SUCCESS, fail: ACTIONS.OPERATION_ARCHIVE_FAIL },
  });
}

function* operationArchiveSuccessSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationExportSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  let periodFrom = getFormattedDate.today('start');
  let periodTo = getFormattedDate.today('end');

  if (documentIds.length) {
    const list = yield select(listSelector);
    const documentsDates = list
      .filter(item => documentIds.includes(item[FIELD_ID.ID]))
      .map(item => item[FIELD_ID.DATE])
      .sort((a, b) => a.localeCompare(b));
    periodFrom = documentsDates[0];
    periodTo = documentsDates[documentsDates.length - 1];
  }

  const selectedOrganizations = yield select(appSelectedOrganizationsSelector);
  const filter = yield select(filterSelector);
  const organizationFilter =
    filter && filter.params && filter.params.find(item => item.id === CONDITION_ID.CUSTOMER_ID);
  const organizations = organizationFilter ? [organizationFilter.value] : selectedOrganizations;

  yield put(exportModalOpenAction(organizations, periodFrom, periodTo));
}

function* operationRemoveSaga({ payload }) {
  const { itemId, params } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.remove.start, {
    ids: documentIds,
    channels: { confirm: ACTIONS.OPERATION_REMOVE_CONFIRM, cancel: ACTIONS.OPERATION_REMOVE_CANCEL },
    ...params,
  });
}

function* operationRemoveConfirmSaga({ payload }) {
  yield call(operations.remove.finish, {
    ...payload,
    channels: { success: ACTIONS.OPERATION_REMOVE_SUCCESS, fail: ACTIONS.OPERATION_REMOVE_FAIL },
  });
}

function* operationRemoveSuccessSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationRemoveSignSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.removeSign, {
    ids: documentIds,
    channels: {
      cancel: ACTIONS.OPERATION_REMOVE_SIGN_CANCEL,
      success: ACTIONS.OPERATION_REMOVE_SIGN_SUCCESS,
      fail: ACTIONS.OPERATION_REMOVE_SIGN_FAIL,
    },
  });
}

function* operationRemoveSignSuccessSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationRepeatSaga({ payload }) {
  const { itemId, params } = payload;
  const selected = yield select(selectedSelector);
  const documentId = itemId || selected[0];
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, {
    pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
    search: `?${qs.stringify({ type: 'repeat', repeatedId: documentId })}`,
  });
}

function* operationRepeatWithRefusalsSaga({ payload }) {
  const { itemId, params } = payload;
  const selected = yield select(selectedSelector);
  const documentId = itemId || selected[0];
  const routes = yield select(appRoutesSelector);
  yield call(params.history.push, {
    pathname: getRoutePath(routes, APP_ROUTES_IDS.ITEM).replace(':id', DOC_NEW_ID),
    search: `?${qs.stringify({ type: 'repeatWithRefusals', repeatedId: documentId })}`,
  });
}

function* operationSaveAsTemplateSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentId = itemId || selected[0];
  yield put(
    saveAsTemplateModalOpenAction({
      documentId,
      channels: { submit: { type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_REQUEST } },
    }),
  );
}

function* operationSaveAsTemplateRequestSaga({ payload }) {
  const { documentId, name } = payload;
  try {
    yield put(saveAsTemplateModalRequestAction());
    yield call(dalSaveAsTemplate, documentId, name);
    yield put(saveAsTemplateModalSuccessAction());
    yield put(saveAsTemplateModalCloseAction());
    yield call(operations.saveAsTemplate.showSuccessNotification, { name });
    const selectedOrganizations = yield select(appSelectedOrganizationsSelector);
    const permissions = yield select(appPermissionsSelector);
    const templates = permissions[PERMISSIONS_IDS.CREATE_FROM_TEMPLATE]
      ? yield call(dalGetTemplates, { orgId: selectedOrganizations })
      : [];
    yield put({ type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_SUCCESS, payload: { templates } });
  } catch (error) {
    if (error instanceof operations.RBOControlsError) {
      yield put(saveAsTemplateModalFailAction(error.response.data.errors));
    } else {
      yield put(saveAsTemplateModalCloseAction());
      yield call(operations.saveAsTemplate.showErrorNotification);
    }
    yield put({ type: ACTIONS.OPERATION_SAVE_AS_TEMPLATE_FAIL });
  }
}

function* operationSendSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.send, {
    ids: documentIds,
    channels: { success: ACTIONS.OPERATION_SEND_SUCCESS, fail: ACTIONS.OPERATION_SEND_FAIL },
  });
}

function* operationSendSuccessSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationSignSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.sign.start, {
    ids: documentIds,
    channels: {
      cancel: ACTIONS.OPERATION_SIGN_CANCEL,
      success: ACTIONS.OPERATION_SIGN_SUCCESS,
      fail: ACTIONS.OPERATION_SIGN_FAIL,
    },
  });
}

function* operationSignSuccessSaga({ payload }) {
  const { ids } = payload;
  yield call(operations.sign.showSuccessNotification, { ids });
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* operationUnarchiveSaga({ payload }) {
  const { itemId } = payload;
  const selected = yield select(selectedSelector);
  const documentIds = itemId ? [itemId] : selected;
  yield call(operations.unarchive, {
    ids: documentIds,
    channels: { success: ACTIONS.OPERATION_UNARCHIVE_SUCCESS, fail: ACTIONS.OPERATION_UNARCHIVE_FAIL },
  });
}

function* operationUnarchiveSuccessSaga() {
  yield put({ type: ACTIONS.GET_LIST_REQUEST });
}

function* import1CStartSaga() {
  yield put(import1CModalOpenAction());
}

function* containerSagas() {
  yield takeEvery(ACTIONS.MOUNT_REQUEST, mountSaga);
  yield takeEvery(ACTIONS.SAVE_SETTINGS, saveSettingsSaga);

  yield takeEvery(ACTIONS.SECTION_CHANGE, sectionChangeSaga);
  yield takeEvery(ACTIONS.PAGE_CHANGE, pageChangeSaga);
  yield takeEvery(ACTIONS.PAGINATION_CHANGE, paginationChangeSaga);
  yield takeEvery(ACTIONS.SORTING_CHANGE, sortingChangeSaga);
  yield takeEvery(ACTIONS.RESIZE_CHANGE, resizeChangeSaga);
  yield takeEvery(ACTIONS.VISIBILITY_CHANGE, visibilityChangeSaga);
  yield takeEvery(ACTIONS.GROUPING_CHANGE, groupingChangeSaga);
  yield takeEvery(ACTIONS.FILTERS_REMOVE, filtersRemoveSaga);
  yield takeEvery(ACTIONS.FILTERS_CHOOSE, filtersChooseSaga);
  yield takeEvery(ACTIONS.FILTERS_RESET, filtersResetSaga);
  yield takeEvery(ACTIONS.FILTERS_SAVE, filtersSaveSaga);
  yield takeEvery(ACTIONS.FILTERS_SAVE_AS, filtersSaveAsSaga);
  yield takeEvery(ACTIONS.FILTER_CHANGE, filterChangeSaga);
  yield takeEvery(ACTIONS.GET_LIST_REQUEST, getListSaga);
  yield takeEvery(ACTIONS.EXPORT_TO_EXCEL_REQUEST, exportToExcelSaga);

  yield takeEvery(ACTIONS.CREATE_NEW_DOC, createNewDocSaga);
  yield takeEvery(ACTIONS.ROUTE_TO_DOC, routeToDocSaga);
  yield takeEvery(ACTIONS.ROUTE_TO_TEMPLATE, routeToTemplateSaga);

  yield takeEvery(ACTIONS.OPERATIONS, operationsSaga);
  yield takeEvery(ACTIONS.OPERATION_ARCHIVE, operationArchiveSaga);
  yield takeEvery(ACTIONS.OPERATION_ARCHIVE_SUCCESS, operationArchiveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_EXPORT, operationExportSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE, operationRemoveSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_CONFIRM, operationRemoveConfirmSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_SUCCESS, operationRemoveSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_SIGN, operationRemoveSignSaga);
  yield takeEvery(ACTIONS.OPERATION_REMOVE_SIGN_SUCCESS, operationRemoveSignSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_REPEAT, operationRepeatSaga);
  yield takeEvery(ACTIONS.OPERATION_REPEAT_WITH_REFUSALS, operationRepeatWithRefusalsSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AS_TEMPLATE, operationSaveAsTemplateSaga);
  yield takeEvery(ACTIONS.OPERATION_SAVE_AS_TEMPLATE_REQUEST, operationSaveAsTemplateRequestSaga);
  yield takeEvery(ACTIONS.OPERATION_SEND, operationSendSaga);
  yield takeEvery(ACTIONS.OPERATION_SEND_SUCCESS, operationSendSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_SIGN, operationSignSaga);
  yield takeEvery(ACTIONS.OPERATION_SIGN_SUCCESS, operationSignSuccessSaga);
  yield takeEvery(ACTIONS.OPERATION_UNARCHIVE, operationUnarchiveSaga);
  yield takeEvery(ACTIONS.OPERATION_UNARCHIVE_SUCCESS, operationUnarchiveSuccessSaga);

  yield takeEvery(ACTIONS.IMPORT_1C_START, import1CStartSaga);
}

export default function* sagas() {
  yield all([containerSagas(), import1CModalSagas()]);
}
