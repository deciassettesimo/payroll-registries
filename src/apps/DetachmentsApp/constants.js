import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'payrollRegistriesDetachments';

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT_REQUEST: null,
    MOUNT_SUCCESS: null,
    MOUNT_FAIL: null,
    UNMOUNT: null,
    SAVE_SETTINGS_REQUEST: null,
    SAVE_SETTINGS_SUCCESS: null,
    SAVE_SETTINGS_FAIL: null,
  },
  'PAYROLL_REGISTRIES_DETACHMENTS_APP_',
);

export const ROUTES_IDS = {
  INDEX: 'list',
  ARCHIVE: 'archive',
  TRASH: 'trash',
  TEMPLATE: 'template',
  ITEM: 'item',
  NOT_FOUND: 'notFound',
};

export const ROUTES_PATHS = {
  INDEX: '/',
  ARCHIVE: '/archive',
  TRASH: '/trash',
  TEMPLATE: '/templates/:templateId',
  ITEM: '/:id',
  NOT_FOUND: '/*',
};
