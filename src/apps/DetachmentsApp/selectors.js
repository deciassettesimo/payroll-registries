import { createSelector } from 'reselect';

import { REDUCER_KEY } from './constants';

const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isErrorSelector = state => state[REDUCER_KEY].isError;
const serverErrorSelector = state => state[REDUCER_KEY].serverError;
const routesSelector = state => state[REDUCER_KEY].routes;

const isLoadingCreatedSelector = createSelector(
  isMountedSelector,
  isMounted => !isMounted,
);

const isErrorCreatedSelector = createSelector(
  isMountedSelector,
  isErrorSelector,
  (isMounted, isError) => isMounted && isError,
);

const serverErrorCreatedSelector = createSelector(
  serverErrorSelector,
  serverError => {
    if (!serverError) return null;
    return {
      status: serverError.status,
      statusText: serverError.statusText,
      url: serverError.config && serverError.config.url,
      response: serverError.data && serverError.data.error,
    };
  },
);

const mapStateToProps = state => ({
  isLoading: isLoadingCreatedSelector(state),
  isError: isErrorCreatedSelector(state),
  serverError: serverErrorCreatedSelector(state),
  routes: routesSelector(state),
});

export default mapStateToProps;
