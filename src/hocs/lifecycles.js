export const lifecycleApp = {
  componentDidMount() {
    this.props.mountAction(this.props.routerBasename, this.props.permissions);
  },

  componentWillUnmount() {
    this.props.unmountAction();
  },
};

export const lifecycleList = {
  componentDidMount() {
    this.props.mountAction(this.props.location);
  },

  componentWillUnmount() {
    this.props.unmountAction();
  },
};

export const lifecycleDoc = {
  componentDidMount() {
    this.props.mountAction(this.props.match.params, this.props.location);
  },

  componentWillReceiveProps(nextProps) {
    if (
      (nextProps.location.pathname !== this.props.location.pathname ||
        nextProps.location.search !== this.props.location.search) &&
      nextProps.history.action !== 'REPLACE'
    ) {
      this.props.mountAction(nextProps.match.params, nextProps.location);
    }
  },

  componentWillUnmount() {
    this.props.unmountAction();
  },
};
