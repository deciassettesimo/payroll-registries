import { getScrollTopByDataId, smoothScrollTo, getFieldElementSelectorByIds, windowRefresh } from '../utils';

export const handlersDocFields = {
  handleChange: props => ({ id, value, optionValue }) => {
    const { changeDataAction } = props;
    changeDataAction(id, value, optionValue);
  },

  handleSearch: props => ({ id, value }) => {
    const { dictionarySearchAction } = props;
    dictionarySearchAction(id, value);
  },

  handleFocus: props => ({ id }) => {
    const { fieldFocusAction } = props;
    fieldFocusAction(id);
  },

  handleBlur: props => ({ id }) => {
    const { fieldBlurAction } = props;
    fieldBlurAction(id);
  },

  handleSectionCollapseToggle: props => ({ sectionId, isCollapsed }) => {
    const { sectionCollapseToggleAction } = props;
    sectionCollapseToggleAction(sectionId, isCollapsed);
  },
};

export const handlersDocStructure = {
  handleTabClick: props => tabId => {
    const { changeTabAction } = props;
    changeTabAction(tabId);
  },

  handleStructureTabClick: props => ({ tabId }) => {
    const { changeTabAction } = props;
    changeTabAction(tabId);
  },

  handleStructureSectionClick: props => ({ tabId, sectionId }) => {
    const { changeTabAction, documentStructure } = props;
    const parentTab = documentStructure.find(tab => tab.id === tabId);
    if (!parentTab.isActive) changeTabAction(tabId);
    windowRefresh(); /* eslint-disable-line */ // при переходе с вкладки Сотрудники в Основные поля и обратно необходимо обновить RboLayoutExtra
    setTimeout(() => {
      const fieldsIds = parentTab.sections.find(section => section.id === sectionId).fields;
      smoothScrollTo(
        getScrollTopByDataId(sectionId) - 72, // высота панелей пользователя и операций
        () => {
          const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
          if (fieldElementSelector) {
            setTimeout(() => {
              fieldElementSelector.focus();
            }, 0);
          }
        },
      );
    }, 0);
  },

  handleStructureErrorClick: props => ({ tabId, sectionId, errorId, fieldsIds }) => {
    const { changeTabAction, activateErrorAction, documentStructure } = props;
    const parentTab = documentStructure.find(tab => tab.id === tabId);
    if (!parentTab.isActive) changeTabAction(tabId);
    windowRefresh(); /* eslint-disable-line */ // при переходе с вкладки Сотрудники в Основные поля и обратно необъодимо обновить RboLayoutExtra
    const fieldElementSelector = getFieldElementSelectorByIds(fieldsIds);
    smoothScrollTo(
      getScrollTopByDataId(sectionId) - 72, // высота панелей пользователя и операций
      () => {
        if (fieldElementSelector) {
          setTimeout(() => {
            fieldElementSelector.focus();
          }, 0);
        }
      },
    );
    activateErrorAction(errorId);
  },
};

export const handlersDocOperations = {
  handleOperationClick: props => operationId => {
    const { operationsAction, history, location, lastLocation } = props;
    operationsAction(operationId, { history, location, lastLocation });
  },
};

export const handlersDocTemplates = {
  handleTemplatesSearch: props => ({ value }) => {
    const { templatesSearchAction } = props;
    templatesSearchAction(value);
  },

  handleTemplatesChoose: props => ({ value }) => {
    const { templatesChooseAction, history } = props;
    templatesChooseAction(value, { history });
  },

  handleTemplatesItemEditClick: props => ({ value }) => {
    const { templatesRouteToAction, history } = props;
    templatesRouteToAction(value, { history });
  },
};

export const handlersDocTop = {
  handleTopClose: props => () => {
    const { closeTopAction } = props;
    closeTopAction();
  },

  handleTopSignatureOperationClick: props => ({ operationId, itemId }) => {
    const { signaturesOperationsAction } = props;
    signaturesOperationsAction(operationId, itemId);
  },
};

export const handlersDocSections = {
  handleSectionMinimizerToggle: props => ({ id, value }) => {
    const { sectionMinimizerToggleAction } = props;
    sectionMinimizerToggleAction(id, value);
  },
};

export const handlersList = {
  handleRefreshButtonClick: props => () => {
    const { listRefreshAction } = props;
    listRefreshAction();
  },

  handleCreateNewDocButtonClick: props => () => {
    const { createNewDocAction, history } = props;
    createNewDocAction(null, { history });
  },

  handleCreateNewDocComboButtonClick: props => ({ items }) => {
    const { createNewDocAction, history } = props;
    createNewDocAction(items, { history });
  },

  handleItemClick: props => ({ id }) => {
    const { openItemAction, history } = props;
    openItemAction(id, { history });
  },

  handleSectionClick: props => sectionId => {
    const { changeSectionAction, history } = props;
    changeSectionAction(sectionId, { history });
  },

  handlePageChange: props => page => {
    const { pageChangeAction, history, location } = props;
    pageChangeAction(page, { history, location });
  },

  handlePaginationChange: props => value => {
    const { paginationChangeAction, history, location } = props;
    paginationChangeAction(value, { history, location });
  },

  handleSortingChange: props => ({ sorting }) => {
    const { sortingChangeAction } = props;
    sortingChangeAction(sorting);
  },

  handleResizeChange: props => ({ widths }) => {
    const { resizeChangeAction } = props;
    resizeChangeAction(widths);
  },

  handleVisibilityChange: props => ({ visibility }) => {
    const { visibilityChangeAction } = props;
    visibilityChangeAction(visibility);
  },

  handleGroupingChange: props => ({ grouped }) => {
    const { groupingChangeAction } = props;
    groupingChangeAction(grouped);
  },

  handleFiltersRemove: props => id => {
    const { filtersRemoveAction } = props;
    filtersRemoveAction(id);
  },

  handleFiltersChoose: props => id => {
    const { filtersChooseAction, history, location } = props;
    filtersChooseAction(id, { history, location });
  },

  handleFiltersCreate: props => () => {
    const { filtersCreateAction } = props;
    filtersCreateAction();
  },

  handleFiltersReset: props => () => {
    const { filtersResetAction, history, location } = props;
    filtersResetAction({ history, location });
  },

  handleFiltersSave: props => () => {
    const { filtersSaveAction } = props;
    filtersSaveAction();
  },

  handleFiltersSaveAs: props => title => {
    const { filtersSaveAsAction, history, location } = props;
    filtersSaveAsAction(title, { history, location });
  },

  handleFilterChange: props => ({ params, isChanged }) => {
    const { filterChangeAction, history, location } = props;
    filterChangeAction(params, isChanged, { history, location });
  },

  handleFilterSearch: props => ({ id, value }) => {
    const { filterSearchAction } = props;
    filterSearchAction(id, value);
  },

  handleSelectChange: props => ({ selected }) => {
    const { selectChangeAction } = props;
    selectChangeAction(selected);
  },

  handleTableOperationClick: props => ({ operationId, itemId }) => {
    const { operationsAction, history, location, lastLocation } = props;
    operationsAction(operationId, itemId, { history, location, lastLocation });
  },

  handleToolbarOperationClick: props => operationId => {
    const { operationsAction, history, location, lastLocation } = props;
    operationsAction(operationId, null, { history, location, lastLocation });
  },

  handleExportToExcelClick: props => () => {
    const { exportToExcelAction } = props;
    exportToExcelAction();
  },
};

export const handlersListTemplates = {
  handleTemplateEditClick: props => id => {
    const { openTemplateAction, history } = props;
    openTemplateAction(id, { history });
  },

  handleSaveAsTemplate: props => ({ documentId, value }) => {
    const { saveAsTemplateAction } = props;
    saveAsTemplateAction(documentId, value);
  },
};
