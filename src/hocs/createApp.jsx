import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { LastLocationProvider } from 'react-router-last-location';

import { configureAxiosInstance } from 'dal/axios';
import { configureAxiosMockAdapter } from 'dal/mock';
import { configureLocalization } from 'localization';
import { configureActions } from 'platform/actions';
import store from 'store';

export default function createApp(App) {
  const Component = withRouter(App);

  return class CreatedApp extends PureComponent {
    static propTypes = {
      locale: PropTypes.string.isRequired,
      apiAccessToken: PropTypes.string.isRequired,
      apiIdToken: PropTypes.string.isRequired,
      apiServiceBaseUrl: PropTypes.string.isRequired,
      apiMock: PropTypes.bool,
      platformActions: PropTypes.shape({
        logout: PropTypes.func.isRequired,
      }).isRequired,
      history: PropTypes.shape().isRequired,
      routerBasename: PropTypes.string.isRequired,
      permissions: PropTypes.shape().isRequired,
    };

    static defaultProps = {
      apiMock: false,
    };

    constructor(props) {
      super(props);

      const { locale, apiAccessToken, apiIdToken, apiServiceBaseUrl, apiMock, platformActions } = this.props;
      configureActions({ platformActions });
      configureLocalization({ locale });
      configureAxiosInstance({ apiAccessToken, apiIdToken, apiServiceBaseUrl });
      if (apiMock) configureAxiosMockAdapter();
    }

    render() {
      const { routerBasename, history, permissions } = this.props;
      return (
        <Provider store={store}>
          <Router history={history}>
            <LastLocationProvider>
              <Component routerBasename={routerBasename} permissions={permissions} />
            </LastLocationProvider>
          </Router>
        </Provider>
      );
    }
  };
}
