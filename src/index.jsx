import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';

import AttachmentsApp from 'apps/AttachmentsApp';
import DetachmentsApp from 'apps/DetachmentsApp';

import { logout, notification, sign, removeSign, saved } from './_dev/actions';
import { checkAuth } from './_dev/auth';
import { settings } from './_dev/settings';

const appProps = {
  locale: settings.LOCALE,
  permissions: { create: true, import: true, export: true, createByTemplate: true },
  apiAccessToken: '',
  apiIdToken: '',
  apiServiceBaseUrl: settings.SERVICE_BASE_URL,
  apiMock: false,
  routerBasename: '',
  history: createBrowserHistory(),
  platformActions: { logout, notification, sign, removeSign, saved },
};

const AppTypeError = () => (
  <div>
    <h1>Wrong REACT_APP_TYPE environment variable</h1>
    <p>You must start application with REACT_APP_TYPE environment variable (one of: attachments, detachments)</p>
    <h3>Start attachments application:</h3>
    <pre>yarn start:attachments</pre>
    <h3>Start detachments application:</h3>
    <pre>yarn start:detachments</pre>
  </div>
);

function renderPayrollRegistries() {
  switch (process.env.REACT_APP_TYPE) {
    case 'attachments':
      ReactDOM.render(<AttachmentsApp {...appProps} />, document.getElementById('root'));
      break;
    case 'detachments':
      ReactDOM.render(<DetachmentsApp {...appProps} />, document.getElementById('root'));
      break;
    default:
      ReactDOM.render(<AppTypeError />, document.getElementById('root'));
  }
}

function init() {
  if (process.env.REACT_APP_MOCK) {
    appProps.apiMock = true;
    renderPayrollRegistries();
  } else {
    checkAuth().then(
      ({ accessToken, idToken }) => {
        appProps.apiAccessToken = accessToken;
        appProps.apiIdToken = idToken;
        renderPayrollRegistries();
      },
      () => {},
    );
  }
}

document.addEventListener('DOMContentLoaded', init);
