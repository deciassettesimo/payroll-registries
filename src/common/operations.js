import { call, put } from 'redux-saga/effects';

import {
  notificationError,
  notificationWarning,
  notificationSuccess,
  notificationConfirm,
  sign,
  removeSign as platformRemoveSign,
} from 'platform';
import { RBOControlsError } from 'dal/errors';
import {
  archive as dalArchive,
  getSignsCheck as dalGetSignsCheck,
  getStateHistory as dalGetStateHistory,
  remove as dalRemove,
  send as dalSend,
  unarchive as dalUnarchive,
} from 'dal/documents';
import { download as dalSignatureDownload, print as dalSignaturePrint } from 'dal/signature';
import localization from 'localization';
import store from 'store';
import smoothScrollTo from 'utils/smoothScrollTo';

import { LOCALE_KEY, SAVE_STATUSES, SIGNATURES_OPERATION_ID } from './constants';

function* archive({ ids, channels }) {
  try {
    const result = yield call(dalArchive, ids);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    yield call(notificationSuccess, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.ARCHIVE.SUCCESS.TITLE, { plural: ids.length > 1 }),
    });
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.ARCHIVE.FAIL.TITLE, { plural: ids.length > 1 }),
    });
  }
}

function* back({ indexRouter, history, location, lastLocation }) {
  if (lastLocation && lastLocation.pathname !== location.pathname) {
    yield call(history.goBack);
  } else {
    yield call(history.replace, indexRouter);
  }
}

function* saveSuccess({ response, channels }) {
  const { errors } = response;
  if (channels.success) {
    if (errors && errors.length) {
      if (errors.filter(error => error.level === '2').length) {
        yield put({ type: channels.success, payload: { status: SAVE_STATUSES.SUCCESS_WITH_ERRORS } });
      } else if (errors.filter(error => error.level === '1').length) {
        yield put({ type: channels.success, payload: { status: SAVE_STATUSES.SUCCESS_WITH_WARNINGS } });
      }
    } else {
      yield put({ type: channels.success, payload: { status: SAVE_STATUSES.SUCCESS } });
    }
  }
}

function* saveFail({ error, isTemplate, channels }) {
  if (channels.fail) yield put({ type: channels.fail, payload: { status: SAVE_STATUSES.FAIL } });
  if (error instanceof RBOControlsError) {
    if (isTemplate) {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.RBO_CONTROLS.TITLE),
        message: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.RBO_CONTROLS.MESSAGE),
      });
    } else {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.RBO_CONTROLS.TITLE),
        message: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.RBO_CONTROLS.MESSAGE),
      });
    }
  } else if (isTemplate) {
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.FAIL.TITLE),
    });
  } else {
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.FAIL.TITLE),
    });
  }
}

function* saveNotAllowed({ channels }) {
  yield put({ type: channels.success, payload: { status: SAVE_STATUSES.NOT_ALLOWED } });
}

function* saveShowSuccessNotification({ status, isTemplate }) {
  if (isTemplate) {
    switch (status) {
      case SAVE_STATUSES.SUCCESS_WITH_ERRORS:
        yield call(notificationError, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.SUCCESS_WITH_ERRORS.TITLE),
        });
        break;
      case SAVE_STATUSES.SUCCESS_WITH_WARNINGS:
        yield call(notificationWarning, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.SUCCESS_WITH_WARNINGS.TITLE),
        });
        break;
      case SAVE_STATUSES.SUCCESS:
      default:
        yield call(notificationSuccess, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.SAVE.SUCCESS.TITLE),
        });
    }
  } else {
    switch (status) {
      case SAVE_STATUSES.NOT_ALLOWED:
        yield call(notificationError, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.NOT_ALLOWED.TITLE),
        });
        break;
      case SAVE_STATUSES.SUCCESS_WITH_ERRORS:
        yield call(notificationError, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.SUCCESS_WITH_ERRORS.TITLE),
          message: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.SUCCESS_WITH_ERRORS.MESSAGE),
        });
        break;
      case SAVE_STATUSES.SUCCESS_WITH_WARNINGS:
        yield call(notificationWarning, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.SUCCESS_WITH_WARNINGS.TITLE),
          message: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.SUCCESS_WITH_WARNINGS.MESSAGE),
        });
        break;
      case SAVE_STATUSES.SUCCESS:
      default:
        yield call(notificationSuccess, {
          title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE.SUCCESS.TITLE),
        });
    }
  }
}

function* saveAsTemplateShowSuccessNotification({ name }) {
  yield call(notificationSuccess, {
    title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE_AS_TEMPLATE.SUCCESS.TITLE),
    message: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE_AS_TEMPLATE.SUCCESS.MESSAGE, { name }),
  });
}

function* saveAsTemplateShowErrorNotification() {
  yield call(notificationError, {
    title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SAVE_AS_TEMPLATE.FAIL.TITLE),
  });
}

function* checkSign({ id, channels }) {
  try {
    const result = yield call(dalGetSignsCheck, id);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    smoothScrollTo(0);
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.CHECK_SIGN.FAIL.TITLE),
    });
  }
}

function* removeStart({ ids, isTemplate, channels, history }) {
  if (isTemplate) {
    yield call(notificationConfirm, {
      level: 'error',
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.CONFIRM.TITLE),
      message: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.CONFIRM.MESSAGE),
      ok: {
        label: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.CONFIRM.LABELS.OK),
        callback: () => store.dispatch({ type: channels.confirm, payload: { ids, history, isTemplate } }),
      },
      cancel: {
        label: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.CONFIRM.LABELS.CANCEL),
        callback: () => store.dispatch({ type: channels.cancel }),
      },
    });
  } else {
    const plural = ids.length > 1;
    yield call(notificationConfirm, {
      level: 'error',
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.CONFIRM.TITLE, { plural }),
      message: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.CONFIRM.MESSAGE, { plural }),
      ok: {
        label: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.CONFIRM.LABELS.OK),
        callback: () => store.dispatch({ type: channels.confirm, payload: { ids, history } }),
      },
      cancel: {
        label: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.CONFIRM.LABELS.CANCEL),
        callback: () => store.dispatch({ type: channels.cancel }),
      },
    });
  }
}

function* removeFinish({ ids, isTemplate, indexRouter, channels, history, dalRemoveTemplate }) {
  try {
    const result = isTemplate ? yield call(dalRemoveTemplate, ids[0]) : yield call(dalRemove, ids);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    if (isTemplate) {
      yield call(notificationSuccess, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.SUCCESS.TITLE),
      });
    } else {
      yield call(notificationSuccess, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.SUCCESS.TITLE, { plural: ids.length > 1 }),
      });
    }
    if (indexRouter) yield call(history.replace, indexRouter);
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    if (isTemplate) {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.TEMPLATE.REMOVE.FAIL.TITLE),
      });
    } else {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.REMOVE.FAIL.TITLE, { plural: ids.length > 1 }),
      });
    }
  }
}

function* removeSign({ ids, channels }) {
  yield call(platformRemoveSign, {
    ids,
    callbacks: {
      cancel: () => store.dispatch({ type: channels.cancel, payload: { ids } }),
      success: () => store.dispatch({ type: channels.success, payload: { ids } }),
      fail: () => store.dispatch({ type: channels.fail, payload: { ids } }),
    },
  });
}

function* send({ ids, channels }) {
  try {
    const result = yield call(dalSend, ids);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    yield call(notificationSuccess, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SEND.SUCCESS.TITLE, { plural: ids.length > 1 }),
    });
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SEND.FAIL.TITLE, { plural: ids.length > 1 }),
    });
  }
}

function* signStart({ ids, channels }) {
  yield call(sign, {
    ids,
    callbacks: {
      cancel: () => store.dispatch({ type: channels.cancel, payload: { ids } }),
      success: () => store.dispatch({ type: channels.success, payload: { ids } }),
      fail: () => store.dispatch({ type: channels.fail, payload: { ids } }),
    },
  });
}

function* signShowSuccessNotification({ ids }) {
  yield call(notificationSuccess, {
    title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.SIGN.SUCCESS.TITLE, { plural: ids.length > 1 }),
  });
}

function* signature({ operationId, docId, signId, signDate }) {
  try {
    if (operationId === SIGNATURES_OPERATION_ID.DOWNLOAD) {
      yield call(dalSignatureDownload, { docId, signId, signDate });
    } else if (operationId === SIGNATURES_OPERATION_ID.PRINT) {
      yield call(dalSignaturePrint, { docId, signId, signDate });
    }
  } catch (error) {
    if (operationId === SIGNATURES_OPERATION_ID.DOWNLOAD) {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SIGNATURE.DOWNLOAD.FAIL.TITLE),
      });
    } else if (operationId === SIGNATURES_OPERATION_ID.PRINT) {
      yield call(notificationError, {
        title: localization.translate(LOCALE_KEY.NOTIFICATIONS.SIGNATURE.PRINT.FAIL.TITLE),
      });
    }
  }
}

function* stateHistory({ id, channels }) {
  try {
    const result = yield call(dalGetStateHistory, id);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    smoothScrollTo(0);
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.STATE_HISTORY.FAIL.TITLE),
    });
  }
}

function* unarchive({ ids, channels }) {
  try {
    const result = yield call(dalUnarchive, ids);
    if (channels.success) {
      yield put({ type: channels.success, payload: result });
    }
    yield call(notificationSuccess, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.UNARCHIVE.SUCCESS.TITLE, { plural: ids.length > 1 }),
    });
  } catch (error) {
    if (channels.fail) {
      yield put({ type: channels.fail, error });
    }
    yield call(notificationError, {
      title: localization.translate(LOCALE_KEY.NOTIFICATIONS.DOC.UNARCHIVE.FAIL.TITLE, { plural: ids.length > 1 }),
    });
  }
}

export default {
  archive,
  back,
  checkSign,
  remove: {
    start: removeStart,
    finish: removeFinish,
  },
  removeSign,
  save: {
    statuses: SAVE_STATUSES,
    success: saveSuccess,
    fail: saveFail,
    notAllowed: saveNotAllowed,
    showSuccessNotification: saveShowSuccessNotification,
  },
  saveAsTemplate: {
    showSuccessNotification: saveAsTemplateShowSuccessNotification,
    showErrorNotification: saveAsTemplateShowErrorNotification,
  },
  send,
  sign: {
    start: signStart,
    showSuccessNotification: signShowSuccessNotification,
  },
  signature,
  stateHistory,
  unarchive,
  RBOControlsError,
};
