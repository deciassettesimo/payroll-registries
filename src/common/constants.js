import { TYPES as ICON_TYPES } from '@rbo/rbo-components/lib/Icon/_constants';

import composeObjects from 'utils/composeObjects';
import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

// export const ROUTE_LEAVE_CONFIRM_ENABLE = process.env.NODE_ENV !== 'development';
export const ROUTE_LEAVE_CONFIRM_ENABLE = true;

export const LOCALES = {
  EN: 'en',
  RU: 'ru',
};

export const DOC_NEW_ID = 'new';

export const LOCALE_KEY = {
  DOC_STATUSES: {
    ACCEPTED: 'loc.accepted',
    ACCEPTED_BY_ABS: 'loc.acceptedByAbs',
    ACCEPTED_BY_CC: 'loc.acceptedByCurrencyControl',
    ACCEPTED_FOR_PROCESSING: 'loc.acceptedForProcessing',
    ACCOUNTED: 'loc.accounted',
    APPROVED_OR_PROCESSED: 'loc.aApprovedOrProcessed',
    APPROVED_BY_CC: 'loc.approvedByCC',
    CERTIFIED: 'loc.certified',
    CLOSED: 'loc.closed',
    CREATED: 'loc.created',
    DECLINED: 'loc.declined',
    DECLINED_BY_BANK: 'loc.declinedByBank',
    DECLINED_BY_ABS: 'loc.declinedByAbs',
    DELAYED: 'loc.delayed',
    DELETED: 'loc.deleted',
    DELIVERED: 'loc.delivered',
    EXECUTED: 'loc.executed',
    IMPORTED: 'loc.imported',
    INVALID_CONTROLS: 'loc.invalidControls',
    INVALID_PROPS: 'loc.invalidProps',
    INVALID_SIGN: 'loc.invalidSign',
    NEW: 'loc.new',
    PARTLY_EXECUTED: 'loc.partlyExecuted',
    PARTLY_SIGNED: 'loc.partlySigned',
    PROCESSED: 'loc.processed',
    PROCESSED_BY_CC: 'loc.processedByCurrencyControl',
    QUEUED: 'loc.queued',
    RECALLED: 'loc.recalled',
    REFUSED_BY_CC: 'loc.refusedByCurrencyControl',
    SIGNED: 'loc.signed',
    UNLOADED: 'loc.unloaded',
    CHECKING: 'loc.checking',
    CREATED_WITH_WARNINGS: 'loc.createdWithWarnings',
    SIGNING: 'loc.signing',
    CHECKED: 'loc.checked',
    CHECKED_REQUIRE_CLARIFICATIONS: 'loc.checkedRequireClarifications',
  },
  EMPLOYEE_STATUSES: {
    CREATED: 'loc.createdCorrectly.feminine',
    CREATED_WITH_WARNINGS: 'loc.haveWarnings',
    INVALID_CONTROLS: 'loc.haveErrors',
    PROCESSING: 'loc.processing',
    EXECUTED: 'loc.executed.feminine',
    CHECKED: 'loc.checked.feminine',
    ERROR_REQUIRE_CLARIFICATIONS: 'errorRequireClarifications',
    DECLINED_BY_BANK: 'loc.declinedByBank.feminine',
  },
  IMPORT_TASK_STATUSES: {
    NOT_COMPLETED: 'loc.notCompleted.participle',
    FAILED: 'loc.failed.participle',
    SUCCESS: 'loc.successfully.participle',
  },
  OPERATIONS: {
    ARCHIVE: 'loc.moveToArchive',
    BACK: 'loc.back',
    CHECK_SIGN: 'loc.verifySignature',
    CREATE_FROM_TEMPLATE: 'loc.documentAccordingToTemplate',
    DOWNLOAD: 'loc.download',
    EXPORT: 'loc.export',
    STATE_HISTORY: 'loc.statusesHistory',
    PRINT: 'loc.print',
    RECALL: 'loc.recall',
    REMOVE: 'loc.delete',
    REMOVE_SIGN: 'loc.removeSignature',
    REPEAT: 'loc.repeat',
    REPEAT_WITH_REFUSALS: 'loc.repeatWithRefusals',
    SAVE: 'loc.save',
    SAVE_AS_TEMPLATE: 'loc.saveAsTemplate',
    SEND: 'loc.send',
    SIGN: 'loc.sign',
    SIGN_AND_SEND: 'loc.signAndSend',
    UNARCHIVE: 'loc.returnFromArchive',
    VISA: 'loc.visa',
  },
  SIGNATURES_TYPES: {
    BANK: 'loc.bankSignature',
    CONTROLLER: 'loc.auditor',
    FIRST: 'loc.firstSignature',
    SECOND: 'loc.secondSignature',
    SINGLE: 'loc.singleSignature',
    VISA: 'loc.visaSignature',
    VK: 'loc.signatureForCC',
  },
  NOTIFICATIONS: {
    DOC: {
      ARCHIVE: {
        SUCCESS: {
          TITLE: 'loc.documentArchived',
        },
        FAIL: {
          TITLE: 'loc.unableArchiveDocument',
        },
      },
      CHECK_SIGN: {
        FAIL: {
          TITLE: 'loc.unableCheckSign',
        },
      },
      SAVE: {
        FAIL: {
          TITLE: 'loc.unableSaveDocument',
        },
        RBO_CONTROLS: {
          TITLE: 'loc.documentCannotBeSaved',
          MESSAGE: 'loc.toSaveDocumentFixErrors',
        },
        NOT_ALLOWED: {
          TITLE: 'loc.documentSaveNotAllowed',
        },
        SUCCESS_WITH_ERRORS: {
          TITLE: 'loc.documentSavedWithErrors',
          MESSAGE: 'loc.toConfirmDocumentFixErrors',
        },
        SUCCESS_WITH_WARNINGS: {
          TITLE: 'loc.documentSavedWithWarnings',
          MESSAGE: 'loc.beforeConfirmDocumentCheckWarnings',
        },
        SUCCESS: {
          TITLE: 'loc.documentSaved',
        },
      },
      SAVE_AS_TEMPLATE: {
        FAIL: {
          TITLE: 'loc.unableSaveDocumentAsTemplate',
        },
        SUCCESS: {
          TITLE: 'loc.templateSavedSuccessfully',
          MESSAGE: 'loc.templateSavedWithName',
        },
      },
      SEND: {
        SUCCESS: {
          TITLE: 'loc.documentSent',
        },
        FAIL: {
          TITLE: 'loc.unableSendDocument',
        },
      },
      SIGN: {
        SUCCESS: {
          TITLE: 'loc.documentSigned',
        },
      },
      STATE_HISTORY: {
        FAIL: {
          TITLE: 'loc.unableLoadStateHistory',
        },
      },
      REMOVE: {
        CONFIRM: {
          TITLE: 'loc.removingDocument',
          MESSAGE: 'loc.doYourWantRemoveDocument',
          LABELS: {
            OK: 'loc.confirm',
            CANCEL: 'loc.cancel',
          },
        },
        SUCCESS: {
          TITLE: 'loc.documentRemoved',
        },
        FAIL: {
          TITLE: 'loc.unableRemoveDocument',
        },
      },
      UNARCHIVE: {
        SUCCESS: {
          TITLE: 'loc.documentUnarchived',
        },
        FAIL: {
          TITLE: 'loc.unableUnarchiveDocument',
        },
      },
    },
    TEMPLATE: {
      REMOVE: {
        CONFIRM: {
          TITLE: 'loc.removingTemplate',
          MESSAGE: 'loc.doYourWantRemoveTemplate',
          LABELS: {
            OK: 'loc.confirm',
            CANCEL: 'loc.cancel',
          },
        },
        SUCCESS: {
          TITLE: 'loc.templateRemoved',
        },
        FAIL: {
          TITLE: 'loc.unableRemoveTemplate',
        },
      },
      SAVE: {
        FAIL: {
          TITLE: 'loc.unableSaveTemplate',
        },
        RBO_CONTROLS: {
          TITLE: 'loc.templateCannotBeSaved',
          MESSAGE: 'loc.toSaveTemplateFixErrors',
        },
        SUCCESS_WITH_ERRORS: {
          TITLE: 'loc.templateSavedWithErrors',
        },
        SUCCESS_WITH_WARNINGS: {
          TITLE: 'loc.templateSavedWithWarnings',
        },
        SUCCESS: {
          TITLE: 'loc.templateSaved',
        },
      },
    },
    SIGNATURE: {
      DOWNLOAD: {
        FAIL: {
          TITLE: 'loc.unableDownloadSignature',
        },
      },
      PRINT: {
        FAIL: {
          TITLE: 'loc.unablePrintSignature',
        },
      },
    },
  },
};

export const DOC_STATUS_ID = {
  ACCEPTED: 'accepted',
  ACCEPTED_BY_ABS: 'acceptedByAbs',
  ACCEPTED_BY_CC: 'acceptedByCC',
  ACCEPTED_FOR_PROCESSING: 'acceptedForProcessing',
  ACCOUNTED: 'accounted',
  APPROVED_OR_PROCESSED: 'approvedOrProcessed',
  APPROVED_BY_CC: 'approvedByCC',
  CERTIFIED: 'certified',
  CLOSED: 'closed',
  CREATED: 'created',
  DECLINED: 'declined',
  DECLINED_BY_BANK: 'declinedByBank',
  DECLINED_BY_ABS: 'declinedByAbs',
  DELAYED: 'delayed',
  DELETED: 'deleted',
  DELIVERED: 'delivered',
  EXECUTED: 'executed',
  IMPORTED: 'imported',
  INVALID_CONTROLS: 'invalidControls',
  INVALID_PROPS: 'invalidProps',
  INVALID_SIGN: 'invalidSign',
  NEW: 'new',
  PARTLY_EXECUTED: 'partlyExecuted',
  PARTLY_SIGNED: 'partlySigned',
  PROCESSED: 'processed',
  PROCESSED_BY_CC: 'processedByCC',
  QUEUED: 'queued',
  RECALLED: 'recalled',
  REFUSED_BY_CC: 'refusedByCC',
  SIGNED: 'signed',
  UNLOADED: 'unloaded',
  CHECKING: 'checking',
  CREATED_WITH_WARNINGS: 'createdWithWarnings',
  SIGNING: 'signing',
  CHECKED: 'checked',
  CHECKED_REQUIRE_CLARIFICATIONS: 'checkedRequireClarifications',
};

export const DOC_STATUS_SERVER_VALUE = {
  ACCEPTED: 'Принят',
  ACCEPTED_BY_ABS: 'Принят в АБС',
  ACCEPTED_BY_CC: 'Принят ВК',
  ACCEPTED_FOR_PROCESSING: 'Принят в обработку',
  ACCOUNTED: 'Учтено Банком',
  APPROVED_OR_PROCESSED: 'Подтвержден/Обработан',
  APPROVED_BY_CC: 'Подтвержден ВК',
  CERTIFIED: 'Заверен',
  CLOSED: 'Закрыт',
  CREATED: 'Создан',
  DECLINED: 'Отвергнут',
  DECLINED_BY_BANK: 'Отвергнут Банком',
  DECLINED_BY_ABS: 'Отказан АБС',
  DELAYED: 'Отложен',
  DELETED: 'Удалён',
  DELIVERED: 'Доставлен',
  EXECUTED: 'Исполнен',
  IMPORTED: 'Импортирован',
  INVALID_CONTROLS: 'Ошибка контроля',
  INVALID_PROPS: 'Ошибка реквизитов',
  INVALID_SIGN: 'ЭП/АСП неверна',
  NEW: 'Новый',
  PARTLY_EXECUTED: 'Частично исполнен',
  PARTLY_SIGNED: 'Частично подписан',
  PROCESSED: 'Обработан',
  PROCESSED_BY_CC: 'Обработан ВК',
  QUEUED: 'Помещен в очередь',
  RECALLED: 'Отозван',
  REFUSED_BY_CC: 'Отказан ВК',
  SIGNED: 'Подписан',
  UNLOADED: 'Выгружен',
  CHECKING: 'Проверяется',
  CREATED_WITH_WARNINGS: 'Создан с предупреждениями',
  SIGNING: 'Подписывается',
  CHECKED: 'Проверен',
  CHECKED_REQUIRE_CLARIFICATIONS: 'Проверен - требуются уточнения',
};

export const DOC_STATUSES = composeObjects(Object.keys(DOC_STATUS_ID), {
  id: DOC_STATUS_ID,
  serverValue: DOC_STATUS_SERVER_VALUE,
  titleKey: LOCALE_KEY.DOC_STATUSES,
});

export const OPERATION_ID = {
  ARCHIVE: 'archive',
  BACK: 'back',
  CHECK_SIGN: 'checkSign',
  CREATE_FROM_TEMPLATE: 'createFromTemplate',
  EXPORT: 'export',
  DOWNLOAD: 'download',
  STATE_HISTORY: 'stateHistory',
  PRINT: 'print',
  RECALL: 'recall',
  REMOVE: 'remove',
  REMOVE_SIGN: 'removeSign',
  REPEAT: 'repeat',
  REPEAT_WITH_REFUSALS: 'repeatWithRefusals',
  SAVE: 'save',
  SAVE_AS_TEMPLATE: 'saveAsTemplate',
  SEND: 'send',
  SIGN: 'sign',
  SIGN_AND_SEND: 'signAndSend',
  UNARCHIVE: 'unarchive',
  VISA: 'visa',
};

const operations = composeObjects(Object.keys(OPERATION_ID), {
  id: OPERATION_ID,
  titleKey: LOCALE_KEY.OPERATIONS,
});

export const OPERATIONS = {
  ARCHIVE: {
    ...operations.ARCHIVE,
    icon: ICON_TYPES.ARCHIVE,
    grouped: true,
  },
  BACK: {
    ...operations.BACK,
    icon: ICON_TYPES.ARROW_LEFT_TAILED,
  },
  CHECK_SIGN: {
    ...operations.CHECK_SIGN,
    icon: ICON_TYPES.SIGN_CHECKED,
  },
  CREATE_FROM_TEMPLATE: {
    ...operations.CREATE_FROM_TEMPLATE,
    icon: ICON_TYPES.DOCUMENT_CREATE_FROM_TEMPLATE,
  },
  DOWNLOAD: {
    ...operations.DOWNLOAD,
    icon: ICON_TYPES.DOWNLOAD,
  },
  EXPORT: {
    ...operations.EXPORT,
    icon: ICON_TYPES.EXPORT,
  },
  STATE_HISTORY: {
    ...operations.STATE_HISTORY,
    icon: ICON_TYPES.DOCUMENT_HISTORY,
  },
  PRINT: {
    ...operations.PRINT,
    icon: ICON_TYPES.PRINT,
    grouped: true,
  },
  RECALL: {
    ...operations.RECALL,
    icon: ICON_TYPES.RECALL,
  },
  REMOVE: {
    ...operations.REMOVE,
    icon: ICON_TYPES.REMOVE,
    grouped: true,
  },
  REMOVE_SIGN: {
    ...operations.REMOVE_SIGN,
    icon: ICON_TYPES.SIGN_REMOVE,
    grouped: true,
  },
  REPEAT: {
    ...operations.REPEAT,
    icon: ICON_TYPES.REPEAT,
  },
  REPEAT_WITH_REFUSALS: {
    ...operations.REPEAT_WITH_REFUSALS,
    icon: ICON_TYPES.REPEAT_WITH_ERRORS,
  },
  SAVE: {
    ...operations.SAVE,
    icon: ICON_TYPES.SAVE,
  },
  SAVE_AS_TEMPLATE: {
    ...operations.SAVE_AS_TEMPLATE,
    icon: ICON_TYPES.DOCUMENT_SAVE_AS_TEMPLATE,
  },
  SEND: {
    ...operations.SEND,
    icon: ICON_TYPES.DOCUMENT_SEND,
    grouped: true,
  },
  SIGN: {
    ...operations.SIGN,
    icon: ICON_TYPES.DOCUMENT_SIGN,
    grouped: true,
  },
  SIGN_AND_SEND: {
    ...operations.SIGN_AND_SEND,
    icon: ICON_TYPES.DOCUMENT_SIGN_AND_SEND,
    grouped: true,
  },
  UNARCHIVE: {
    ...operations.UNARCHIVE,
    icon: ICON_TYPES.UNARCHIVE,
    grouped: true,
  },
  VISA: {
    ...operations.VISA,
    icon: ICON_TYPES.DOCUMENT_SIGN_VISA,
    grouped: true,
  },
};

export const OPERATIONS_ACTIONS_TYPES = {
  ARCHIVE: {
    OPERATION_ARCHIVE: null,
    OPERATION_ARCHIVE_SUCCESS: null,
    OPERATION_ARCHIVE_FAIL: null,
  },
  BACK: {
    OPERATION_BACK: null,
    OPERATION_BACK_CONFIRM: null,
    OPERATION_BACK_CANCEL: null,
  },
  CHECK_SIGN: {
    OPERATION_CHECK_SIGN: null,
    OPERATION_CHECK_SIGN_SUCCESS: null,
    OPERATION_CHECK_SIGN_FAIL: null,
  },
  CREATE_FROM_TEMPLATE: { OPERATION_CREATE_FROM_TEMPLATE: null },
  DOWNLOAD: {
    DOWNLOAD: null,
    DOWNLOAD_SUCCESS: null,
    DOWNLOAD_FAIL: null,
  },
  EXPORT: { OPERATION_EXPORT: null },
  STATE_HISTORY: {
    OPERATION_STATE_HISTORY: null,
    OPERATION_STATE_HISTORY_SUCCESS: null,
    OPERATION_STATE_HISTORY_FAIL: null,
  },
  PRINT: {
    OPERATION_PRINT: null,
    OPERATION_PRINT_SUCCESS: null,
    OPERATION_PRINT_FAIL: null,
  },
  RECALL: { OPERATION_RECALL: null },
  REMOVE: {
    OPERATION_REMOVE: null,
    OPERATION_REMOVE_CONFIRM: null,
    OPERATION_REMOVE_CANCEL: null,
    OPERATION_REMOVE_SUCCESS: null,
    OPERATION_REMOVE_FAIL: null,
  },
  REMOVE_SIGN: {
    OPERATION_REMOVE_SIGN: null,
    OPERATION_REMOVE_SIGN_CANCEL: null,
    OPERATION_REMOVE_SIGN_SUCCESS: null,
    OPERATION_REMOVE_SIGN_FAIL: null,
  },
  REPEAT: { OPERATION_REPEAT: null },
  REPEAT_WITH_REFUSALS: { OPERATION_REPEAT_WITH_REFUSALS: null },
  SAVE: {
    OPERATION_SAVE: null,
    OPERATION_SAVE_SUCCESS: null,
    OPERATION_SAVE_FAIL: null,
  },
  SAVE_AS_TEMPLATE: {
    OPERATION_SAVE_AS_TEMPLATE: null,
    OPERATION_SAVE_AS_TEMPLATE_REQUEST: null,
    OPERATION_SAVE_AS_TEMPLATE_SUCCESS: null,
    OPERATION_SAVE_AS_TEMPLATE_FAIL: null,
  },
  SEND: {
    OPERATION_SEND: null,
    OPERATION_SEND_SUCCESS: null,
    OPERATION_SEND_FAIL: null,
  },
  SIGN: {
    OPERATION_SIGN: null,
    OPERATION_SIGN_CANCEL: null,
    OPERATION_SIGN_SUCCESS: null,
    OPERATION_SIGN_FAIL: null,
  },
  SIGN_AND_SEND: { OPERATION_SIGN_AND_SEND: null },
  UNARCHIVE: {
    OPERATION_UNARCHIVE: null,
    OPERATION_UNARCHIVE_SUCCESS: null,
    OPERATION_UNARCHIVE_FAIL: null,
  },
  VISA: { OPERATION_VISA: null },
};

export const FROM_BANK_INFO_TYPES = {
  STRING: 'string',
  DATE: 'date',
  REF: 'ref',
};

export const SAVE_STATUSES = {
  NOT_ALLOWED: 'notAllowed',
  SUCCESS: 'success',
  SUCCESS_WITH_ERRORS: 'successWithErrors',
  SUCCESS_WITH_WARNINGS: 'successWithWarnings',
  FAIL: 'fail',
};

export const STATE_HISTORY_COLUMNS = [
  {
    id: 'date',
    labelKey: 'loc.date',
    align: 'left',
    width: 180,
    isVisible: true,
  },
  {
    id: 'user',
    labelKey: 'loc.login',
    align: 'left',
    width: 140,
    isVisible: true,
  },
  {
    id: 'status',
    labelKey: 'loc.status',
    align: 'left',
    width: 320,
    isVisible: true,
  },
];

export const SIGNATURES_COLUMNS = [
  {
    id: 'signDateTime',
    labelKey: 'loc.date',
    align: 'left',
    width: 80,
    isVisible: true,
  },
  {
    id: 'userLogin',
    labelKey: 'loc.login',
    align: 'left',
    width: 120,
    isVisible: true,
  },
  {
    id: 'userName',
    labelKey: 'loc.name',
    align: 'left',
    width: 120,
    isVisible: true,
  },
  {
    id: 'userPosition',
    labelKey: 'loc.post',
    align: 'left',
    width: 120,
    isVisible: true,
  },
  {
    id: 'signType',
    labelKey: 'loc.signType',
    align: 'left',
    width: 120,
    isVisible: true,
  },
  {
    id: 'signValid',
    labelKey: 'loc.resultOfCheck',
    align: 'left',
    width: 144,
    isVisible: true,
  },
  {
    id: 'signHash',
    labelKey: 'loc.documentElectronicSign',
    align: 'left',
    width: 190,
    isVisible: true,
  },
];

export const SIGNATURES_OPERATION_ID = {
  DOWNLOAD: OPERATION_ID.DOWNLOAD,
  PRINT: OPERATION_ID.PRINT,
};

export const SIGNATURES_OPERATIONS = [OPERATIONS.DOWNLOAD, OPERATIONS.PRINT];

export const SIGNATURE_TYPE_ID = {
  BANK: '-1',
  CONTROLLER: '6',
  FIRST: '1',
  SECOND: '2',
  SINGLE: '4',
  VISA: '3',
  VK: '5',
};

export const SIGNATURES_TYPES = composeObjects(Object.keys(SIGNATURE_TYPE_ID), {
  id: SIGNATURE_TYPE_ID,
  titleKey: LOCALE_KEY.SIGNATURES_TYPES,
});

export const EMPLOYEE_STATUS_ID = keyMirrorWithPrefix({
  CREATED: null,
  CREATED_WITH_WARNINGS: null,
  INVALID_CONTROLS: null,
  PROCESSING: null,
  EXECUTED: null,
  CHECKED: null,
  ERROR_REQUIRE_CLARIFICATIONS: null,
  DECLINED_BY_BANK: null,
});

export const EMPLOYEE_STATUS_SERVER_VALUE = {
  CREATED: 'Создана корректно',
  CREATED_WITH_WARNINGS: 'Есть предупреждения',
  INVALID_CONTROLS: 'Есть ошибки',
  PROCESSING: 'В обработке',
  EXECUTED: 'Исполнена',
  CHECKED: 'Проверена',
  ERROR_REQUIRE_CLARIFICATIONS: 'Ошибка/Требуется уточнение',
  DECLINED_BY_BANK: 'Отвергнута Банком',
};

export const EMPLOYEE_STATUSES = composeObjects(Object.keys(EMPLOYEE_STATUS_ID), {
  id: EMPLOYEE_STATUS_ID,
  titleKey: LOCALE_KEY.EMPLOYEE_STATUSES,
  serverValue: EMPLOYEE_STATUS_SERVER_VALUE,
});

export const EMPLOYEES_CONTROLS = {
  WARNINGS: {
    id: 'employeesInfoErrors',
    level: '1',
    text: 'В списке сотрудников есть предупреждения',
  },
  ERRORS: {
    id: 'employeesInfoErrors',
    level: '2',
    text: 'В списке сотрудников есть ошибки',
  },
};

export const IMPORT_TASK_STATUS_ID = keyMirrorWithPrefix({
  NOT_COMPLETED: null,
  FAILED: null,
  SUCCESS: null,
});

export const IMPORT_TASK_STATUS_SERVER_VALUE = {
  NOT_COMPLETED: 'Не обработано',
  FAILED: 'Ошибочно',
  SUCCESS: 'Успешно',
};

export const IMPORT_TASK_STATUSES = composeObjects(Object.keys(IMPORT_TASK_STATUS_ID), {
  id: IMPORT_TASK_STATUS_ID,
  titleKey: LOCALE_KEY.IMPORT_TASK_STATUSES,
  serverValue: IMPORT_TASK_STATUS_SERVER_VALUE,
});

export const PERMISSIONS_IDS = {
  CREATE: 'create',
  IMPORT: 'import',
  EXPORT: 'export',
  CREATE_FROM_TEMPLATE: 'createByTemplate',
};
