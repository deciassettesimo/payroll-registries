import React from 'react';
import PropTypes from 'prop-types';

import Status from '@rbo/rbo-components/lib/Styles/Status';

const RboDocSignaturesCell = ({ column, data }) => {
  if (column === 'signValid') return <Status isError={data.signValid.isError}>{data.signValid.title}</Status>;
  return data[column];
};

RboDocSignaturesCell.propTypes = {
  column: PropTypes.string,
  data: PropTypes.shape(),
};

export default RboDocSignaturesCell;
