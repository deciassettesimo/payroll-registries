import { connect } from 'react-redux';

import localization from 'localization';

import { LABELS } from './config';
import RboServerError from './RboServerError';

export default connect(() => ({
  LABELS: localization.translate(LABELS),
}))(RboServerError);
