export const LABELS = {
  TITLE: 'loc.loadingDataError',
  ADDITIONAL_INFO: {
    OPENER: 'loc.additionalInfo',
    ERROR: 'loc.error.withColon',
    METHOD: 'loc.method.withColon',
    RESPONSE: 'loc.serverResponse.withColon',
  },
};
