import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import GeneralError from '@rbo/rbo-components/lib/GeneralError';
import IconArrowDown from '@rbo/rbo-components/lib/Icon/IconArrowDown';
import IconArrowUp from '@rbo/rbo-components/lib/Icon/IconArrowUp';
import { Label, Link as RboLink, List, Paragraph } from '@rbo/rbo-components/lib/Styles';

export default class RboServerError extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    serverData: PropTypes.shape({
      status: PropTypes.number,
      statusText: PropTypes.string,
      url: PropTypes.string,
      response: PropTypes.any,
    }).isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
    };
  }

  onAdditionalInfoClick = () => {
    const { isOpened } = this.state;
    this.setState({ isOpened: !isOpened });
  };

  render() {
    const { LABELS, serverData } = this.props;
    const { isOpened } = this.state;

    return (
      <GeneralError>
        <Paragraph>{LABELS.TITLE}</Paragraph>
        <Paragraph>
          <RboLink isDashed isPseudo isWithIcon onClick={this.onAdditionalInfoClick}>
            <span>{LABELS.ADDITIONAL_INFO.OPENER}</span>
            {isOpened ? (
              <IconArrowUp dimension={IconArrowUp.REFS.DIMENSIONS.XXS} />
            ) : (
              <IconArrowDown dimension={IconArrowDown.REFS.DIMENSIONS.XXS} />
            )}
          </RboLink>
        </Paragraph>
        {isOpened && (
          <List>
            <List.Item>
              <Label>{LABELS.ADDITIONAL_INFO.ERROR} </Label>
              {serverData.status} {serverData.statusText}
            </List.Item>
            <List.Item>
              <Label>{LABELS.ADDITIONAL_INFO.METHOD} </Label>
              {serverData.url}
            </List.Item>
            {serverData.response && (
              <List.Item>
                <Label>{LABELS.ADDITIONAL_INFO.RESPONSE} </Label>
                {serverData.response}
              </List.Item>
            )}
          </List>
        )}
      </GeneralError>
    );
  }
}
