import { connect } from 'react-redux';

import localization from 'localization';

import { LABELS } from './config';
import RboDocTemplates from './RboDocTemplates';

export default connect(() => ({
  LABELS: {
    LOCALE: localization.getLocale(),
    ...localization.translate(LABELS),
  },
}))(RboDocTemplates);
