import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import RboPopupBoxOption from '@rbo/rbo-components/lib/RboPopupBoxOption';
import { InputSearch, InputText } from '@rbo/rbo-components/lib/Input';

export default class RboDocTemplates extends PureComponent {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    params: PropTypes.shape({
      value: PropTypes.string,
      items: PropTypes.array,
      isSearching: PropTypes.bool,
    }).isRequired,
    onSearch: PropTypes.func.isRequired,
    onChoose: PropTypes.func.isRequired,
    onItemEditClick: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = { isEdit: false };
  }

  handleSearch = params => {
    const { onSearch } = this.props;
    onSearch(params);
  };

  handleChoose = ({ value }) => {
    const { isEdit } = this.state;
    const { onChoose } = this.props;
    if (!isEdit) onChoose({ value });
  };

  handleEditClick = value => {
    this.setState({ isEdit: true });
    const { onItemEditClick } = this.props;
    onItemEditClick({ value });
  };

  optionRenderer = ({ option, handlePopupClose }) => (
    <RboPopupBoxOption
      value={option.value}
      title={option.title}
      describe={option.describe}
      onEditClick={this.handleEditClick}
      highlight={{
        value: option.searchValue,
        inTitle: true,
        inDescribe: true,
      }}
      handlePopupClose={handlePopupClose}
    />
  );

  render() {
    const { params, LABELS } = this.props;

    return (
      <InputSearch
        id="templatesSelect"
        locale={LABELS.LOCALE}
        inputElement={InputText}
        value={params.value}
        options={params.items}
        searchValue={params.searchValue}
        optionRenderer={this.optionRenderer}
        isSearching={params.isSearching}
        onSearch={this.handleSearch}
        onChange={this.handleChoose}
        placeholder={LABELS.PLACEHOLDER}
        dimension={InputSearch.REFS.DIMENSIONS.XS}
        isCleanable
      />
    );
  }
}
