import { connect } from 'react-redux';

import localization from 'localization';

import { LABELS } from './config';
import RboDocNotes from './RboDocNotes';

export default connect(() => ({
  LABELS: localization.translate(LABELS),
}))(RboDocNotes);
