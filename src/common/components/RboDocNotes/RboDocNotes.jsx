import React from 'react';
import PropTypes from 'prop-types';

import Form, { Row, Cell, Field } from '@rbo/rbo-components/lib/Form';
import RboDocForm, { Section, Blockset, Block, Footnote } from '@rbo/rbo-components/lib/RboDocForm';
import RboDocInfo from '@rbo/rbo-components/lib/RboDocInfo';
import { InputTextArea, InputViewMode } from '@rbo/rbo-components/lib/Input';

const RboDocNotes = props => {
  const { LABELS, isEditable, fieldId, placeholder, value, disabled, onChange } = props;

  if (!isEditable && !value) return <RboDocInfo emptyLabel={LABELS.EMPTY} />;

  return (
    <RboDocForm>
      <Section dataAttributes={{ id: 'notes' }}>
        <Section.Content>
          <Form dimension={Form.REFS.DIMENSIONS.XS}>
            <Blockset>
              <Block isWide>
                <Row>
                  <Cell>
                    <Field>
                      {isEditable ? (
                        <InputTextArea
                          id={fieldId}
                          value={value}
                          disabled={disabled}
                          maxLength={4000}
                          onChange={onChange}
                          placeholder={placeholder}
                        />
                      ) : (
                        <InputViewMode id={fieldId} value={value} rows={3} />
                      )}
                    </Field>
                  </Cell>
                </Row>
                {isEditable && (
                  <Row>
                    <Cell>
                      <Footnote>{LABELS.DESCRIPTION}</Footnote>
                    </Cell>
                  </Row>
                )}
              </Block>
            </Blockset>
          </Form>
        </Section.Content>
      </Section>
    </RboDocForm>
  );
};

RboDocNotes.propTypes = {
  LABELS: PropTypes.shape().isRequired,
  isEditable: PropTypes.bool.isRequired,
  fieldId: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

RboDocNotes.defaultProps = {
  placeholder: null,
  value: null,
  disabled: false,
};

export default RboDocNotes;
