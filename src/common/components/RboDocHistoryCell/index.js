import { connect } from 'react-redux';

import localization from 'localization';

import { LABELS } from './config';
import RboDocHistoryCell from './RboDocHistoryCell';

export default connect(() => ({
  LABELS: localization.translate(LABELS),
}))(RboDocHistoryCell);
