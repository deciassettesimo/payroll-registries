import React from 'react';
import PropTypes from 'prop-types';

import Status from '@rbo/rbo-components/lib/Styles/Status';

const RboDocHistoryCell = ({ LABELS, column, data }) => {
  if (column === 'status') {
    return (
      <span>
        <Status isSuccess={data.status.isSuccess} isWarning={data.status.isWarning} isError={data.status.isError}>
          {data.status.title}
        </Status>
        {data.isLastState && ` (${LABELS.LAST_STATE})`}
      </span>
    );
  }
  return data[column];
};

RboDocHistoryCell.propTypes = {
  LABELS: PropTypes.shape().isRequired,
  column: PropTypes.string,
  data: PropTypes.shape(),
};

export default RboDocHistoryCell;
