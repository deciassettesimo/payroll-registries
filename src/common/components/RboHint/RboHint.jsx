import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Popup, { Opener, Box } from '@rbo/rbo-components/lib/Popup';
import Card from '@rbo/rbo-components/lib/Card';

export default class RboHint extends PureComponent {
  static propTypes = {
    value: PropTypes.string,
  };

  static defaultProps = {
    value: null,
  };

  constructor(props) {
    super(props);

    this.state = { isOpened: false };
  }

  handleOpen = () => {
    this.setState({ isOpened: true });
  };

  handleClose = () => {
    this.setState({ isOpened: false });
  };

  render() {
    const { value } = this.props;
    const { isOpened } = this.state;

    if (!value) return null;

    return (
      <Popup isOpened={isOpened} onClose={this.handleClose}>
        <Opener isNotAutoOpen>
          <span onMouseEnter={this.handleOpen}>{value}</span>
        </Opener>
        <Box isClosingOnMouseLeave>
          <Card isShadowed>
            <div style={{ width: 320, padding: 8, whiteSpace: 'pre-line' }}>{value}</div>
          </Card>
        </Box>
      </Popup>
    );
  }
}
