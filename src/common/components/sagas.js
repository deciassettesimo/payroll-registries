import { all } from 'redux-saga/effects';

import rboConfirmModalSagas from './RboConfirmModal/sagas';
import RboDocSaveAsTemplateModalSagas from './RboDocSaveAsTemplateModal/sagas';

export default function* sagas() {
  yield all([rboConfirmModalSagas(), RboDocSaveAsTemplateModalSagas()]);
}
