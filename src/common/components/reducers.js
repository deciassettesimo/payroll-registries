import rboConfirmModalReducer from './RboConfirmModal/reducers';
import rboDocSaveAsTemplateModalReducer from './RboDocSaveAsTemplateModal/reducers';

export default {
  ...rboConfirmModalReducer,
  ...rboDocSaveAsTemplateModalReducer,
};
