export const LABELS = {
  HEADER: 'loc.savingDocumentTemplate',
  INPUT_LABEL: 'loc.templateName',
  BUTTON_CANCEL: 'loc.cancel',
  BUTTON_SAVE: 'loc.save',
};
