import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'rboDocSaveAsTemplateModal';

export const ACTIONS = keyMirrorWithPrefix(
  {
    MOUNT: null,
    UNMOUNT: null,
    OPEN: null,
    CLOSE: null,
    CHANGE: null,
    SUBMIT: null,
    SAVE_REQUEST: null,
    SAVE_SUCCESS: null,
    SAVE_FAIL: null,
  },
  'RBO_DOC_SAVE_AS_TEMPLATE_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    INPUT: null,
    BUTTON_CANCEL: null,
    BUTTON_SAVE: null,
  },
  'RBO_DOC_SAVE_AS_TEMPLATE_MODAL_',
);
