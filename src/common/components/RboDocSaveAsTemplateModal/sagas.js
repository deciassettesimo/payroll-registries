import { put, select, takeEvery } from 'redux-saga/effects';

import { REDUCER_KEY, ACTIONS } from './constants';

const channelsSelector = state => state[REDUCER_KEY].channels;

function* submitSaga({ payload }) {
  const channels = yield select(channelsSelector);
  yield put({ ...channels.submit, payload });
}

export default function* sagas() {
  yield takeEvery(ACTIONS.SUBMIT, submitSaga);
}
