import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalWindow from '@rbo/rbo-components/lib/ModalWindow';
import { Block, Header, Status, List } from '@rbo/rbo-components/lib/Styles';
import Form, { Row, Cell, Field, Label } from '@rbo/rbo-components/lib/Form';
import InputText from '@rbo/rbo-components/lib/Input/InputText';
import Button from '@rbo/rbo-components/lib/Button';

import { FIELD_ID } from './constants';

export default class RboDocSaveAsTemplateModal extends Component {
  static propTypes = {
    LABELS: PropTypes.shape().isRequired,
    isVisible: PropTypes.bool.isRequired,
    isSaving: PropTypes.bool.isRequired,
    documentId: PropTypes.string,
    name: PropTypes.string,
    errors: PropTypes.arrayOf(PropTypes.shape()),
    mountAction: PropTypes.func.isRequired,
    unmountAction: PropTypes.func.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeAction: PropTypes.func.isRequired,
    submitAction: PropTypes.func.isRequired,
  };

  static defaultProps = {
    documentId: null,
    name: null,
    errors: null,
  };

  componentDidMount() {
    const { mountAction } = this.props;
    mountAction();
  }

  componentWillUnmount() {
    const { unmountAction } = this.props;
    unmountAction();
  }

  handleClose = () => {
    const { closeAction } = this.props;
    closeAction();
  };

  handleSubmit = () => {
    const { documentId, changeAction, submitAction, name } = this.props;
    const formattedValue = name && name.trim();

    changeAction(formattedValue);
    submitAction(documentId, formattedValue);
  };

  handleChange = ({ value }) => {
    const { changeAction } = this.props;
    changeAction(value);
  };

  render() {
    const { LABELS, isVisible, isSaving, name, errors } = this.props;

    if (!isVisible) return null;

    return (
      <ModalWindow onClose={this.handleClose}>
        <div style={{ width: 460 }}>
          <Block borderBottomColor={Block.REFS.BORDER_COLORS.MARIGOLD}>
            <Header size={3}>{LABELS.HEADER}</Header>
          </Block>
          <Form autoFocus={FIELD_ID.INPUT} onSubmit={this.handleSubmit}>
            <Block>
              <Row>
                <Cell>
                  <Label htmlFor={FIELD_ID.INPUT}>{LABELS.INPUT_LABEL}</Label>
                  <Field>
                    <InputText
                      id={FIELD_ID.INPUT}
                      value={name}
                      onChange={this.handleChange}
                      isError={!!errors && !!errors.length}
                      maxLength={255}
                      disabled={isSaving}
                    />
                  </Field>
                  {errors && errors.length && (
                    <List innerVPadding={0} vPadding={0}>
                      {errors.map(error => (
                        <List.Item key={error.id}>
                          <Status isError>{error.text}</Status>
                        </List.Item>
                      ))}
                    </List>
                  )}
                </Cell>
              </Row>
            </Block>
            <Block borderTopColor={Block.REFS.BORDER_COLORS.BLACK_24}>
              <Row>
                <Cell width="50%">
                  <Field>
                    <Button id={FIELD_ID.BUTTON_CANCEL} onClick={this.handleClose} disabled={isSaving}>
                      {LABELS.BUTTON_CANCEL}
                    </Button>
                  </Field>
                </Cell>
                <Cell width="50%">
                  <Field align={Field.REFS.ALIGN.RIGHT}>
                    <Button
                      type={Button.REFS.TYPES.BASE_PRIMARY}
                      id={FIELD_ID.BUTTON_SAVE}
                      onClick={this.handleSubmit}
                      isLoading={isSaving}
                    >
                      {LABELS.BUTTON_SAVE}
                    </Button>
                  </Field>
                </Cell>
              </Row>
            </Block>
          </Form>
        </div>
      </ModalWindow>
    );
  }
}
