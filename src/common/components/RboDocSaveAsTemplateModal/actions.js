import { ACTIONS } from './constants';

export const mountAction = () => ({
  type: ACTIONS.MOUNT,
});

export const unmountAction = () => ({
  type: ACTIONS.UNMOUNT,
});

export const openAction = params => ({
  type: ACTIONS.OPEN,
  payload: params,
});

export const closeAction = () => ({
  type: ACTIONS.CLOSE,
});

export const changeAction = name => ({
  type: ACTIONS.CHANGE,
  payload: { name },
});

export const submitAction = (documentId, name) => ({
  type: ACTIONS.SUBMIT,
  payload: { documentId, name },
});

export const saveRequestAction = () => ({
  type: ACTIONS.SAVE_REQUEST,
});

export const saveSuccessAction = () => ({
  type: ACTIONS.SAVE_SUCCESS,
});

export const saveFailAction = errors => ({
  type: ACTIONS.SAVE_FAIL,
  payload: { errors },
});
