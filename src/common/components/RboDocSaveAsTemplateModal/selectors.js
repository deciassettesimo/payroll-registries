import { createSelector } from 'reselect';

import localization from 'localization';

import { LABELS } from './config';
import { REDUCER_KEY } from './constants';

const isMountedSelector = state => state[REDUCER_KEY].isMounted;
const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const isSavingSelector = state => state[REDUCER_KEY].isSaving;
const documentIdSelector = state => state[REDUCER_KEY].documentId;
const nameSelector = state => state[REDUCER_KEY].name;
const errorsSelector = state => state[REDUCER_KEY].errors;

const labelsCreatedSelector = () => ({
  LOCALE: localization.getLocale(),
  ...localization.translate(LABELS),
});

const isVisibleCreatedSelector = createSelector(
  isMountedSelector,
  isVisibleSelector,
  (isMounted, isVisible) => isMounted && isVisible,
);

const mapStateToProps = state => ({
  LABELS: labelsCreatedSelector(state),
  isVisible: isVisibleCreatedSelector(state),
  isSaving: isSavingSelector(state),
  documentId: documentIdSelector(state),
  name: nameSelector(state),
  errors: errorsSelector(state),
});

export default mapStateToProps;
