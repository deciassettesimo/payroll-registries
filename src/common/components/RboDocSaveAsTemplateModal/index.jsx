import { connect } from 'react-redux';

import RboDocSaveAsTemplateModal from './RboDocSaveAsTemplateModal';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default connect(
  mapStateToProps,
  actions,
)(RboDocSaveAsTemplateModal);
