import { REDUCER_KEY, ACTIONS } from './constants';

export const initialState = {
  isMounted: false,
  isVisible: false,
  isSaving: false,
  documentId: null,
  channels: {},
  name: null,
  errors: null,
};

function reducers(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.MOUNT:
      return { ...initialState, isMounted: true };

    case ACTIONS.UNMOUNT:
      return { ...initialState, isMounted: false };

    case ACTIONS.OPEN:
      return {
        ...state,
        isVisible: true,
        isSaving: false,
        documentId: action.payload.documentId,
        channels: action.payload.channels,
        name: null,
        errors: null,
      };

    case ACTIONS.CLOSE:
      return {
        ...state,
        isVisible: false,
        isSaving: false,
        documentId: null,
        channels: {},
        name: null,
        errors: null,
      };

    case ACTIONS.CHANGE:
      return { ...state, name: action.payload.name };

    case ACTIONS.SAVE_REQUEST:
      return { ...state, isSaving: true, errors: null };

    case ACTIONS.SAVE_SUCCESS:
      return { ...state, isSaving: false, errors: null };

    case ACTIONS.SAVE_FAIL:
      return { ...state, isSaving: false, errors: action.payload.errors };

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducers };
