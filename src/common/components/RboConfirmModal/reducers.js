import { REDUCER_KEY, ACTIONS } from './constants';

export const initialState = {
  isVisible: false,
  params: {},
};

function reducers(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.OPEN:
      return {
        ...state,
        isVisible: true,
        params: action.payload,
      };

    case ACTIONS.CLOSE:
      return {
        ...state,
        isVisible: false,
        params: {},
      };

    default:
      return state;
  }
}

export default { [REDUCER_KEY]: reducers };
