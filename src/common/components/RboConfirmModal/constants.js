import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'rboConfirmModal';

export const ACTIONS = keyMirrorWithPrefix(
  {
    OPEN: null,
    CONFIRM: null,
    CANCEL: null,
    CLOSE: null,
  },
  'RBO_CONFIRM_MODAL_',
);

export const FIELD_ID = keyMirrorWithPrefix(
  {
    BUTTON_CONFIRM: null,
    BUTTON_CANCEL: null,
  },
  'RBO_CONFIRM_MODAL_',
);
