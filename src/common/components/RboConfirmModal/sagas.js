import { put, select, takeEvery } from 'redux-saga/effects';

import { REDUCER_KEY, ACTIONS } from './constants';

const paramsSelector = state => state[REDUCER_KEY].params;

function* confirmSaga() {
  const params = yield select(paramsSelector);
  if (params.channels && params.channels.confirm) yield put(params.channels.confirm);
  yield put({ type: ACTIONS.CLOSE });
}

function* cancelSaga() {
  const params = yield select(paramsSelector);
  if (params.channels && params.channels.confirm) yield put(params.channels.cancel);
  yield put({ type: ACTIONS.CLOSE });
}

export default function* sagas() {
  yield takeEvery(ACTIONS.CONFIRM, confirmSaga);
  yield takeEvery(ACTIONS.CANCEL, cancelSaga);
}
