import { ACTIONS } from './constants';

export const openAction = params => ({
  type: ACTIONS.OPEN,
  payload: params,
});

export const confirmAction = () => ({
  type: ACTIONS.CONFIRM,
});

export const cancelAction = () => ({
  type: ACTIONS.CANCEL,
});
