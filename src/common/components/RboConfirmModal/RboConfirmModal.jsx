import React from 'react';
import PropTypes from 'prop-types';
import ModalWindow from '@rbo/rbo-components/lib/ModalWindow';
import { Block, Header } from '@rbo/rbo-components/lib/Styles';
import Form, { Row, Cell, Field } from '@rbo/rbo-components/lib/Form';
import Button from '@rbo/rbo-components/lib/Button';

import { FIELD_ID } from './constants';

const RboConfirmModal = props => {
  const { isVisible, labels, confirmButtonType, cancelButtonType, confirmAction, cancelAction } = props;

  if (!isVisible) return null;

  return (
    <ModalWindow isWithoutClosingIcon isClosingOnOutClick={false} isClosingOnEscPress={false} onClose={cancelAction}>
      <div style={{ width: 400 }}>
        {labels.title && (
          <Block borderBottomColor={Block.REFS.BORDER_COLORS.MARIGOLD}>
            <Header size={3}>{labels.title}</Header>
          </Block>
        )}
        <Block>
          {labels.message && <div style={{ whiteSpace: 'pre-line', marginBottom: 12 }}>{labels.message}</div>}
          <Form dimension={Form.REFS.DIMENSIONS.S} autoFocus={FIELD_ID.BUTTON_CONFIRM}>
            <Row>
              <Cell width="50%">
                <Field>
                  <Button type={confirmButtonType} display="block" id={FIELD_ID.BUTTON_CONFIRM} onClick={confirmAction}>
                    {labels.confirm}
                  </Button>
                </Field>
              </Cell>
              <Cell width="50%">
                <Field>
                  <Button type={cancelButtonType} display="block" id={FIELD_ID.BUTTON_CANCEL} onClick={cancelAction}>
                    {labels.cancel}
                  </Button>
                </Field>
              </Cell>
            </Row>
          </Form>
        </Block>
      </div>
    </ModalWindow>
  );
};

RboConfirmModal.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  labels: PropTypes.shape({
    title: PropTypes.string,
    message: PropTypes.string,
    confirm: PropTypes.string,
    cancel: PropTypes.string,
  }).isRequired,
  confirmButtonType: PropTypes.oneOf(Object.values(Button.REFS.TYPES)),
  cancelButtonType: PropTypes.oneOf(Object.values(Button.REFS.TYPES)),
  confirmAction: PropTypes.func.isRequired,
  cancelAction: PropTypes.func.isRequired,
};

RboConfirmModal.defaultProps = {
  confirmButtonType: Button.REFS.TYPES.BASE_PRIMARY,
  cancelButtonType: Button.REFS.TYPES.BASE_SECONDARY,
};

export default RboConfirmModal;
