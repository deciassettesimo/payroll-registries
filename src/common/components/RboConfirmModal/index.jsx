import { connect } from 'react-redux';

import RboConfirmModal from './RboConfirmModal';
import * as actions from './actions';
import mapStateToProps from './selectors';

export default connect(
  mapStateToProps,
  actions,
)(RboConfirmModal);
