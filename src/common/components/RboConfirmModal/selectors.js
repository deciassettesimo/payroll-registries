import { createSelector } from 'reselect';

import localization from 'localization';

import { LABELS } from './config';
import { REDUCER_KEY } from './constants';

const isVisibleSelector = state => state[REDUCER_KEY].isVisible;
const paramsSelector = state => state[REDUCER_KEY].params;

const labelsCreatedSelector = createSelector(
  paramsSelector,
  params => ({
    LOCALE: localization.getLocale(),
    title: params.title || null,
    message: params.message || null,
    confirm: params.confirm || localization.translate(LABELS.BUTTON_CONFIRM),
    cancel: params.cancel || localization.translate(LABELS.BUTTON_CANCEL),
  }),
);

const confirmButtonTypeCreatedSelector = createSelector(
  paramsSelector,
  params => params.confirmButtonType,
);
const cancelButtonTypeCreatedSelector = createSelector(
  paramsSelector,
  params => params.confirmButtonType,
);

const mapStateToProps = state => ({
  isVisible: isVisibleSelector(state),
  labels: labelsCreatedSelector(state),
  confirmButtonType: confirmButtonTypeCreatedSelector(state),
  cancelButtonType: cancelButtonTypeCreatedSelector(state),
});

export default mapStateToProps;
