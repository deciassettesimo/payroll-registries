import attachmentsAppReducer from 'apps/AttachmentsApp/reducers';
import detachmentsAppReducer from 'apps/DetachmentsApp/reducers';
import commonComponentsReducer from 'common/components/reducers';
import { LOCALES } from 'common/constants';

import { REDUCER_KEY, ACTIONS } from './constants';

const initialState = { locale: LOCALES.RU };

const setLocale = (state, { locale }) => ({ ...state, locale });

function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.SET_LOCALE:
      return setLocale(state, action.payload);
    default:
      return state;
  }
}

export default {
  [REDUCER_KEY]: reducer,
  ...commonComponentsReducer,
  ...attachmentsAppReducer,
  ...detachmentsAppReducer,
};
