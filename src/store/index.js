import createAppStore from 'utils/createAppStore';

import sagas from './sagas';
import reducers from './reducers';

const store = createAppStore(reducers, sagas);

export default store;
