import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

export const REDUCER_KEY = 'root';

export const ACTIONS = keyMirrorWithPrefix(
  {
    LOGOUT: null,
    SET_LOCALE: null,
  },
  'PAYROLL_REGISTRIES_ROOT_',
);
