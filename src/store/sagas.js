import { all, takeEvery } from 'redux-saga/effects';

import attachmentsAppSagas from 'apps/AttachmentsApp/sagas';
import detachmentsAppSagas from 'apps/DetachmentsApp/sagas';
import commonComponentsSagas from 'common/components/sagas';
import { logout } from 'platform';

import { ACTIONS } from './constants';

function* rootSagas() {
  yield takeEvery(ACTIONS.LOGOUT, logout);
}

export default function* sagas() {
  yield all([rootSagas(), commonComponentsSagas(), attachmentsAppSagas(), detachmentsAppSagas()]);
}
