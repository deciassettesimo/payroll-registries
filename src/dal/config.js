import { ERROR_KEY, LOCALE_KEY } from './constants';

export const LABELS = {
  [ERROR_KEY.DOC_SEND]: LOCALE_KEY.ERRORS.DOC_SEND,
};
