import localization from 'localization';

import { LABELS } from './config';

export function RBOError(key, response, data) {
  Error.call(this, response);
  this.name = 'RBOError';
  this.response = data ? { ...response, data } : response;
  this.title = LABELS[key] ? localization.translate(LABELS[key].TITLE) : key;
  this.message = LABELS[key] ? localization.translate(LABELS[key].MESSAGE) : key;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, this.constructor);
  } else {
    const { stack } = new Error();
    this.stack = stack;
  }
}

RBOError.prototype = Object.create(Error.prototype);

export const createRBOError = (error, data) => {
  const key = (data && data.error) || (error && error.response && error.response.data && error.response.data.error);

  return key ? new RBOError(key, error.response, data) : error;
};

export function RBOControlsError(response) {
  Error.call(this, response);
  this.name = 'RBOControlsError';
  this.response = response;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, this.constructor);
  } else {
    const { stack } = new Error();
    this.stack = stack;
  }
}

RBOControlsError.prototype = Object.create(Error.prototype);

export const checkRBOControlsError = response => {
  const data = (response && response.data) || {};
  if (data.errors && data.errors.filter(error => error.level === '3').length) {
    throw new RBOControlsError(response);
  }
  return response;
};
