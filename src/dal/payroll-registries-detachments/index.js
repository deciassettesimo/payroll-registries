import { call } from 'redux-saga/effects';
import { saveAs } from 'file-saver';

import { blobToBase64, blobToJSON, getFormattedDate } from 'utils';

import { createRBOError, checkRBOControlsError, RBOControlsError } from '../errors';

import api from './api';
import {
  normalizeGetDocDataFromServer,
  normalizePostDocDataFromServer,
  normalizePostDocDataToServer,
  normalizeGetTemplatesFromServer,
  normalizeGetListToServer,
  normalizeGetListFromServer,
  normalizeGetStatusesFromServer,
  normalizeImport1CFromServer,
  normalizeImport1CMonitoringFromServer,
  normalizeCSVImportFromServer,
} from './normalize';

export function* getDocData(id) {
  try {
    const result = yield call(api.getDocData, id);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* saveDocData(data) {
  try {
    const normalizedData = normalizePostDocDataToServer(data);
    const result = yield call(api.saveDocData, normalizedData);
    const checkedResult = checkRBOControlsError(result);
    return normalizePostDocDataFromServer((checkedResult && checkedResult.data) || {});
  } catch (error) {
    if (error instanceof RBOControlsError) {
      error.response.data = normalizePostDocDataFromServer(error.response.data);
    }
    throw createRBOError(error);
  }
}

export function* getDefaultData(orgId) {
  try {
    const result = yield call(api.getDefaultData, orgId);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getList(section, data) {
  try {
    const normalizedData = normalizeGetListToServer(data);
    const result = yield call(api.getList, section, normalizedData);
    return normalizeGetListFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* exportToExcel(section, data) {
  try {
    const normalizedData = normalizeGetListToServer(data);
    const result = yield call(api.exportToExcel, section || '/scroller', normalizedData);
    saveAs(
      result.data,
      `attaches_${section ? section.replace('/', '') : 'working'}_${getFormattedDate(
        getFormattedDate.today(),
        'DD-MM-YYYY_HH-mm-ss',
      )}.xlsx`,
    );
    return true;
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getStatuses() {
  try {
    const result = yield call(api.getStatuses);
    return normalizeGetStatusesFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getRepeatData(repeatedId) {
  try {
    const result = yield call(api.getRepeatData, repeatedId);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getDocErrors(data) {
  try {
    const normalizedData = normalizePostDocDataToServer(data);
    const result = yield call(api.getDocErrors, normalizedData);
    return normalizePostDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getDocStatus(id) {
  try {
    const result = yield call(api.getDocStatus, id);
    return normalizePostDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getDocHeaderErrors(id) {
  try {
    const result = yield call(api.getDocHeaderErrors, id);
    return normalizePostDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getEmployeeErrors(data) {
  try {
    const normalizedData = normalizePostDocDataToServer(data);
    const result = yield call(api.getEmployeeErrors, normalizedData);
    return normalizePostDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getRepeatWithRefusalsData(repeatedId) {
  try {
    const result = yield call(api.getRepeatWithRefusalsData, repeatedId);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* import1C(file, settings) {
  try {
    const content = yield call(() => new Promise(resolve => blobToBase64(file, r => resolve(r))));

    const result = yield call(api.import1C, content, settings);
    return normalizeImport1CFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* import1CMonitoring(taskId) {
  try {
    const result = yield call(api.import1CMonitoring, taskId);
    return normalizeImport1CMonitoringFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* CSVImport(file, settings) {
  try {
    const content = yield call(() => new Promise(resolve => blobToBase64(file, r => resolve(r))));

    const result = yield call(api.CSVImport, content, settings);
    return normalizeCSVImportFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* exportCSV(settings) {
  try {
    const result = yield call(api.exportCSV, settings);
    const { fromDate, toDate } = settings;
    saveAs(
      result.data,
      `detach_to_csv_${getFormattedDate(fromDate, 'DD-MM-YYYY')}_${getFormattedDate(toDate, 'DD-MM-YYYY')}.zip`,
    );
    return true;
  } catch (error) {
    const data = yield call(() => new Promise(resolve => blobToJSON(error.response.data, r => resolve(r))));
    throw createRBOError(error, data);
  }
}

export function* saveAsTemplate(id, name) {
  try {
    const result = yield call(api.saveAsTemplate, id, name);
    const checkedResult = checkRBOControlsError(result);
    return (checkedResult && checkedResult.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getTemplates(params) {
  try {
    const result = yield call(api.getTemplates, params);
    return normalizeGetTemplatesFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getTemplateData(id) {
  try {
    const result = yield call(api.getTemplateData, id);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* saveTemplateData(data) {
  try {
    const normalizedData = normalizePostDocDataToServer(data);
    const result = yield call(api.saveTemplateData, normalizedData);
    const checkedResult = checkRBOControlsError(result);
    return normalizePostDocDataFromServer((checkedResult && checkedResult.data) || {});
  } catch (error) {
    if (error instanceof RBOControlsError) {
      error.response.data = normalizePostDocDataFromServer(error.response.data);
    }
    throw createRBOError(error);
  }
}

export function* removeTemplate(id) {
  try {
    const result = yield call(api.removeTemplate, id);
    return (result && result.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getFromTemplateData(templateId) {
  try {
    const result = yield call(api.getFromTemplateData, templateId);
    return normalizeGetDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getTemplateErrors(data) {
  try {
    const normalizedData = normalizePostDocDataToServer(data);
    const result = yield call(api.getTemplateErrors, normalizedData);
    return normalizePostDocDataFromServer((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getBranches(organizations) {
  try {
    const result = yield call(api.getBranches, organizations);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}
