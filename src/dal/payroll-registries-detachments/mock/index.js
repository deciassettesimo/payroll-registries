import { jsonHeaders, logRequest } from '../../mock';

import { URLS } from '../constants';

import defaultValues from './default-values.json';
import documents from './documents.json';
import errors from './errors.json';
import list from './list.json';
import save from './save.json';
import statuses from './statuses.json';
import templates from './templates.json';

const getDefaultData = config => logRequest(config, [200, defaultValues, jsonHeaders]);

const getDocData = config => {
  const { url } = config;
  const id = url.split('/')[1];
  const item = documents.find(i => i.docInfo.id === id);
  const response = item ? [200, item, jsonHeaders] : [404, { error: 'documents.document_not_found' }, jsonHeaders];
  return logRequest(config, response);
};

const getDocErrors = config => logRequest(config, [200, errors, jsonHeaders]);

const saveDocData = config => logRequest(config, [200, save, jsonHeaders]);

const getStatuses = config => logRequest(config, [200, statuses, jsonHeaders]);

const getTemplates = config => logRequest(config, [200, templates, jsonHeaders]);

const getTemplateData = config => {
  const { url } = config;
  const id = url.split('/')[2];
  const item = documents.find(i => i.docInfo.id === id);
  const response = item ? [200, item, jsonHeaders] : [404, { error: 'documents.document_not_found' }, jsonHeaders];
  return logRequest(config, response);
};

const getTemplatesErrors = config => logRequest(config, [200, errors, jsonHeaders]);

const saveTemplateData = config => logRequest(config, [200, save, jsonHeaders]);

const getList = config => logRequest(config, [200, list, jsonHeaders]);

export default function(axiosMockInstance) {
  axiosMockInstance.onPost(URLS.GET_LIST.replace('{:section}', '')).reply(getList);
  axiosMockInstance.onGet(new RegExp(`^${URLS.GET_TEMPLATE_DATA.replace('{:id}', '')}[^/]+$`)).reply(getTemplateData);
  axiosMockInstance.onGet(URLS.GET_TEMPLATES).reply(getTemplates);
  axiosMockInstance.onGet(URLS.GET_STATUSES).reply(getStatuses);
  axiosMockInstance.onPost(URLS.GET_FROM_TEMPLATE_DATA).reply(getDefaultData);
  axiosMockInstance.onPost(URLS.GET_TEMPLATE_ERRORS).reply(getTemplatesErrors);
  axiosMockInstance.onPost(URLS.SAVE_TEMPLATE_DATA).reply(saveTemplateData);

  axiosMockInstance.onGet(URLS.GET_DEFAULT_DATA).reply(getDefaultData);
  axiosMockInstance.onGet(new RegExp(`^${URLS.GET_DOC_DATA.replace('{:id}', '')}[^/]+$`)).reply(getDocData);
  axiosMockInstance.onPost(URLS.GET_REPEAT_DATA).reply(getDefaultData);
  axiosMockInstance.onPost(URLS.GET_REPEAT_WITH_REFUSALS_DATA).reply(getDefaultData);
  axiosMockInstance.onPost(URLS.GET_DOC_ERRORS).reply(getDocErrors);
  axiosMockInstance.onPost(URLS.SAVE_DOC_DATA).reply(saveDocData);
}
