import keyMirrorWithPrefix from 'utils/keyMirrorWithPrefix';

const MAIN_PATH = 'detach-request';

export const URLS = {
  GET_DOC_DATA: `${MAIN_PATH}/{:id}`,
  SAVE_DOC_DATA: `${MAIN_PATH}`,
  GET_DEFAULT_DATA: `${MAIN_PATH}/defaultValues`,
  GET_LIST: `${MAIN_PATH}{:section}/query`,
  GET_STATUSES: `${MAIN_PATH}/statuses`,
  GET_REPEAT_DATA: `${MAIN_PATH}/copy`,
  EXPORT_TO_EXCEL: `${MAIN_PATH}/report{:section}`,
  GET_DOC_ERRORS: `${MAIN_PATH}/validation`,
  GET_DOC_STATUS: `${MAIN_PATH}/status/{:id}`,
  GET_DOC_HEADER_ERRORS: `${MAIN_PATH}/bank-message/{:id}`,
  GET_EMPLOYEE_ERRORS: `${MAIN_PATH}/employee/validation`,
  GET_REPEAT_WITH_REFUSALS_DATA: `${MAIN_PATH}/copyRejected`,
  IMPORT_1C: `${MAIN_PATH}/import/1C`,
  IMPORT_1C_MONITORING: `${MAIN_PATH}/import/1C/{:taskId}`,
  CSV_IMPORT: `${MAIN_PATH}/import/CSV`,
  EXPORT_CSV: `${MAIN_PATH}/report/CSV`,
  SAVE_AS_TEMPLATE: `${MAIN_PATH}/templates/saveByDocument`,
  GET_TEMPLATES: `${MAIN_PATH}/templates`,
  GET_TEMPLATE_DATA: `${MAIN_PATH}/templates/{:id}`,
  SAVE_TEMPLATE_DATA: `${MAIN_PATH}/templates`,
  REMOVE_TEMPLATE: `${MAIN_PATH}/templates/{:id}`,
  GET_FROM_TEMPLATE_DATA: `${MAIN_PATH}/templates/create`,
  GET_TEMPLATE_ERRORS: `${MAIN_PATH}/templates/validation`,
  GET_BRANCHES: `${MAIN_PATH}/branches`,
};

export const GET_LIST_SECTIONS = {
  WORKING: '',
  ARCHIVED: '/archived',
  REMOVED: '/deleted',
};

export const FIELD_ID = keyMirrorWithPrefix({
  ALLOWED_SM_ACTIONS: null,
  BRANCH_INFO_ID: null,
  BRANCH_INFO_NAME: null,
  CONFIRM_ACCEPTANCE: null,
  COPY_REJECTED_ENABLED: null,
  CUSTOMER_ID: null,
  CUSTOMER_INN: null,
  CUSTOMER_NAME: null,
  CUSTOMER_OGRN: null,
  DATE: null,
  DETACH_REQUEST_DATE: null,
  DETACH_REQUEST_NUMBER: null,
  EMPL_COUNT: null,
  EMPLOYEES_INFO: null,
  FROM_BANK_INFO_ACCEPT_DATE: null,
  FROM_BANK_INFO_BANK_MESSAGE: null,
  FROM_BANK_INFO_BANK_MESSAGE_AUTHOR: null,
  FROM_BANK_INFO_OPERATION_DATE: null,
  ID: null,
  NOTE: null,
  NUMBER: null,
  OFFICIAL_NAME: null,
  OFFICIAL_PHONE: null,
  STATUS: null,
  TEMPLATE_NAME: null,
});

export const EMPLOYEES_INFO_ITEM_FIELD_ID = keyMirrorWithPrefix(
  {
    ACCOUNT: null,
    EMPLOYEE_BIRTH_DATE: null,
    EMPLOYEE_NAME: null,
    EMPLOYEE_NUM: null,
    EMPLOYEE_PATRONYMIC: null,
    EMPLOYEE_POSITION: null,
    EMPLOYEE_PROCESSING_RESULT_CODE_1C: null,
    EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK: null,
    EMPLOYEE_PROCESSING_RESULT_STATUS: null,
    EMPLOYEE_SURNAME: null,
    ID: null,
    NPP: null,
  },
  'EMPLOYEES_INFO_ITEM_',
);

export { ALLOWED_SM_ACTION_ID } from '../constants';
