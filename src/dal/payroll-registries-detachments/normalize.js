import { v4 } from 'uuid';

import { IMPORT_TASK_STATUSES } from 'common/constants';
import { getStatus, getObjectKeyByValue, getCombinedErrorText, ServerDataNormalizer } from 'utils';

import { FIELD_ID, EMPLOYEES_INFO_ITEM_FIELD_ID } from './constants';

const branchInfo = {
  'branchInfo.id': FIELD_ID.BRANCH_INFO_ID,
  'branchInfo.name': FIELD_ID.BRANCH_INFO_NAME,
};

// prettier-ignore
const confirmAcceptance = {
  'confirmAcceptance': FIELD_ID.CONFIRM_ACCEPTANCE,
};

const customer = {
  'customer.INN': FIELD_ID.CUSTOMER_INN,
  'customer.OGRN': FIELD_ID.CUSTOMER_OGRN,
  'customer.id': FIELD_ID.CUSTOMER_ID,
  'customer.name': FIELD_ID.CUSTOMER_NAME,
};

const detachRequest = {
  'detachContract.date': FIELD_ID.DETACH_REQUEST_DATE,
  'detachContract.number': FIELD_ID.DETACH_REQUEST_NUMBER,
};

const docInfo = {
  'docInfo.allowedSmActions': FIELD_ID.ALLOWED_SM_ACTIONS,
  'docInfo.date': FIELD_ID.DATE,
  'docInfo.fromBankInfo.acceptDate': FIELD_ID.FROM_BANK_INFO_ACCEPT_DATE,
  'docInfo.fromBankInfo.bankMessage': FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE,
  'docInfo.fromBankInfo.bankMessageAuthor': FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE_AUTHOR,
  'docInfo.fromBankInfo.operationDate': FIELD_ID.FROM_BANK_INFO_OPERATION_DATE,
  'docInfo.id': FIELD_ID.ID,
  'docInfo.note': FIELD_ID.NOTE,
  'docInfo.number': FIELD_ID.NUMBER,
  'docInfo.status': FIELD_ID.STATUS,
  'docInfo.templateName': FIELD_ID.TEMPLATE_NAME,
};

// prettier-ignore
const emplCount = {
  'emplCount': FIELD_ID.EMPL_COUNT,
};

// prettier-ignore
const employeesInfo = {
  'employeesInfo': FIELD_ID.EMPLOYEES_INFO,
};

// prettier-ignore
const copyRejectedEnabled = {
  'copyRejectedEnabled': FIELD_ID.COPY_REJECTED_ENABLED,
};

const official = {
  'official.name': FIELD_ID.OFFICIAL_NAME,
  'official.phone': FIELD_ID.OFFICIAL_PHONE,
};

const docDataMap = {
  ...branchInfo,
  ...confirmAcceptance,
  ...customer,
  ...detachRequest,
  ...docInfo,
  ...emplCount,
  ...employeesInfo,
  ...copyRejectedEnabled,
  ...official,
};

// prettier-ignore
const saveDataMap = {
  'allowedSmActions': FIELD_ID.ALLOWED_SM_ACTIONS,
  'recordID': FIELD_ID.ID,
  'status': FIELD_ID.STATUS,
  'errors': 'errors',
};

/** EmployeesInfoItemMap */
const employeesInfoItemEmployee = {
  'employee.birthDate': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_BIRTH_DATE,
  'employee.employeeNum': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_NUM,
  'employee.name': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_NAME,
  'employee.patronymic': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PATRONYMIC,
  'employee.position': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_POSITION,
  'employee.surname': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_SURNAME,
  'processingResult.code1C': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C,
  'processingResult.messageFromBank': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK,
  'processingResult.status': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
};

// prettier-ignore
const employeesInfoItemOther = {
  'account': EMPLOYEES_INFO_ITEM_FIELD_ID.ACCOUNT,
  'id': EMPLOYEES_INFO_ITEM_FIELD_ID.ID,
  'npp': EMPLOYEES_INFO_ITEM_FIELD_ID.NPP,
};

const employeesInfoItemMap = {
  ...employeesInfoItemEmployee,
  ...employeesInfoItemOther,
};

// prettier-ignore
const templatesItemMap = {
  'recordID': FIELD_ID.ID,
  'name': FIELD_ID.TEMPLATE_NAME,
  'correspondentName': FIELD_ID.CUSTOMER_NAME,
};

const docDataNormalizer = new ServerDataNormalizer(docDataMap);
const employeesInfoItemNormalizer = new ServerDataNormalizer(employeesInfoItemMap);
const saveDataNormalizer = new ServerDataNormalizer(saveDataMap);
const templatesNormalizer = new ServerDataNormalizer(templatesItemMap);

export const normalizeEmployeeInfoDataFromServer = data => employeesInfoItemNormalizer.fromServer(data);

export const normalizeGetDocDataFromServer = data => {
  const normalizedData = docDataNormalizer.fromServer(data);
  const normalizedEmployeesInfo = normalizedData[FIELD_ID.EMPLOYEES_INFO].map(item =>
    normalizeEmployeeInfoDataFromServer(item),
  );

  return {
    ...normalizedData,
    [FIELD_ID.STATUS]: getStatus.idByServerValue(normalizedData[FIELD_ID.STATUS]),
    [FIELD_ID.EMPLOYEES_INFO]: normalizedEmployeesInfo,
  };
};

export const normalizePostDocDataToServer = data => {
  const preparedData = {
    ...data,
    [FIELD_ID.STATUS]: getStatus.serverValueById(data[FIELD_ID.STATUS]),
    [FIELD_ID.EMPLOYEES_INFO]: data[FIELD_ID.EMPLOYEES_INFO].map(item => employeesInfoItemNormalizer.toServer(item)),
  };
  return docDataNormalizer.toServer(preparedData);
};

export const normalizePostDocDataFromServer = data => {
  const normalizedData = saveDataNormalizer.fromServer(data);
  return {
    ...normalizedData,
    [FIELD_ID.STATUS]: getStatus.idByServerValue(normalizedData[FIELD_ID.STATUS]),
  };
};

export const normalizeGetListToServer = data => ({
  ...data,
  sort: {
    column: getObjectKeyByValue(docDataMap, data.sort.column),
    desc: data.sort.desc,
  },
  filter: {
    ...docDataNormalizer.toServer({
      ...data.filter,
      [FIELD_ID.STATUS]: data.filter[FIELD_ID.STATUS]
        ? data.filter[FIELD_ID.STATUS].map(status => getStatus.serverValueById(status))
        : null,
    }),
    visibleColumns: data.filter.visibleColumns
      ? data.filter.visibleColumns.map(column => getObjectKeyByValue(docDataMap, column))
      : null,
  },
});

export const normalizeGetListFromServer = data => data.map(item => normalizeGetDocDataFromServer(item));

export const normalizeGetTemplatesFromServer = data => data.map(item => templatesNormalizer.fromServer(item));

export const normalizeGetStatusesFromServer = data => data.map(item => getStatus.byServerValue(item));

export const normalizeCSVImportFromServer = data => ({
  employeesInfo: data
    .filter(item => item.employeeInfo)
    .map(item => normalizeEmployeeInfoDataFromServer(item.employeeInfo)),
  errors: data
    .filter(item => item.label)
    .map(item => ({
      id: v4(item.label),
      text: item.errors ? getCombinedErrorText(item.label, item.errors.map(error => ({ text: error })), 2) : item.label,
      level: '3',
    })),
});

export const normalizeImport1CFromServer = data => {
  if (data.message) return { error: data.message };
  return data;
};

export const normalizeImport1CMonitoringFromServer = data => {
  const statusKey = Object.keys(IMPORT_TASK_STATUSES).find(
    key => IMPORT_TASK_STATUSES[key].serverValue === data.result,
  );
  return {
    status: statusKey ? IMPORT_TASK_STATUSES[statusKey] : null,
    startTime: data.startTime,
    finishTime: data.finishTime,
    docId: data.docId,
    emplAmount: data.emplAmount,
    message: data.message,
  };
};
