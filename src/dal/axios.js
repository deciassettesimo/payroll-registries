import axios from 'axios';
import qs from 'qs';

import store from 'store';
import { ACTIONS } from 'store/constants';
import { normalizeQueryParams, validateDataSchemaCompliance } from 'utils';

import { API_CANCELED_MESSAGE } from './constants';

export const axiosInstance = axios.create({
  validateStatus: status => status >= 200 && status < 400,
  paramsSerializer: params => qs.stringify(normalizeQueryParams(params), { arrayFormat: 'repeat' }),
});

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const { response = {} } = error;
    if (response.status === 401) store.dispatch({ type: ACTIONS.LOGOUT });
    return Promise.reject(error);
  },
);

if (process.env.NODE_ENV === 'development') {
  axiosInstance.interceptors.request.use(request => {
    if (request.requestSchema) {
      validateDataSchemaCompliance(request.data, request.requestSchema, request.url);
    }
    return request;
  });

  axiosInstance.interceptors.response.use(response => {
    if (response.config && response.config.responseSchema) {
      validateDataSchemaCompliance(response.data, response.config.responseSchema, response.config.url);
    }
    return response;
  });
}

export const configureAxiosInstance = ({ apiAccessToken, apiIdToken, apiServiceBaseUrl }) => {
  axiosInstance.defaults.headers.common.Authorization = `Bearer ${apiAccessToken}`;
  axiosInstance.defaults.headers.common['Id-Token'] = apiIdToken;
  axiosInstance.defaults.baseURL = apiServiceBaseUrl;
};

export const getCancelToken = cancelToken => {
  if (cancelToken) cancelToken.cancel(API_CANCELED_MESSAGE);
  return axios.CancelToken.source();
};
