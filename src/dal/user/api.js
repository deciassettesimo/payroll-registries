/** Методы работы с данными пользователя
 */
import removeEmptyProperties from 'utils/removeEmptyProperties';

import { axiosInstance } from '../axios';
import { URLS } from './constants';

/**
 * Получение организаций и филиалов
 */
const getOrganizationsAndBranches = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_ORGANIZATIONS_AND_BRANCHES,
  });

/**
 * Получение настроек
 */
const getSettings = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_SETTINGS,
  });

/**
 * Сохранение настроек
 */
const saveSettings = data =>
  axiosInstance({
    method: 'put',
    url: URLS.SAVE_SETTINGS,
    data: removeEmptyProperties(data),
  });

export default {
  getOrganizationsAndBranches,
  getSettings,
  saveSettings,
};
