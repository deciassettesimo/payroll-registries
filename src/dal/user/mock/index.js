import { jsonHeaders, logRequest } from '../../mock';

import { URLS } from '../constants';

import organizationsAndBranches from './organizations-and-branches.json';
import settings from './settings.json';

const getOrganizationsAndBranches = config => logRequest(config, [200, organizationsAndBranches, jsonHeaders]);

const getSettings = config => logRequest(config, [200, settings, jsonHeaders]);

const saveSettings = config => logRequest(config, [200, config.data, jsonHeaders]);

export default function(axiosMockInstance) {
  axiosMockInstance.onGet(URLS.GET_ORGANIZATIONS_AND_BRANCHES).reply(getOrganizationsAndBranches);
  axiosMockInstance.onGet(URLS.GET_SETTINGS).reply(getSettings);
  axiosMockInstance.onPut(URLS.SAVE_SETTINGS).reply(saveSettings);
}
