const MAIN_PATH = 'user';

export const URLS = {
  GET_ORGANIZATIONS_AND_BRANCHES: `${MAIN_PATH}/organizations`,
  GET_SETTINGS: `${MAIN_PATH}/settings`,
  SAVE_SETTINGS: `${MAIN_PATH}/settings`,
};
