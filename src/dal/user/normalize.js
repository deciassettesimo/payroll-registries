export const normalizeGetOrganizationsAndBranches = data => ({
  organizations: data.orgs,
  branches: data.branches
    .map(item => item.id)
    .filter((v, i, a) => a.indexOf(v) === i)
    .map(id => data.branches.find(item => item.id === id))
    .map(item => ({
      ...item,
      organizations: data.links.filter(link => link.orgId === item.id).map(link => link.branchId),
    })),
});

export const normalizeGetSettings = data => data.settings;

export const normalizeSaveSettings = data => ({ settings: data });
