import { call } from 'redux-saga/effects';

import { createRBOError } from '../errors';

import api from './api';
import { normalizeGetOrganizationsAndBranches, normalizeGetSettings, normalizeSaveSettings } from './normalize';

export function* getOrganizationsAndBranches() {
  try {
    const result = yield call(api.getOrganizationsAndBranches);
    return normalizeGetOrganizationsAndBranches((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getSettings() {
  try {
    const result = yield call(api.getSettings);
    return normalizeGetSettings((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* saveSettings(data) {
  try {
    const result = yield call(api.saveSettings, normalizeSaveSettings(data));
    return normalizeGetSettings((result && result.data) || {});
  } catch (error) {
    throw createRBOError(error);
  }
}
