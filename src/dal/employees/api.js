/** Методы работы со справочником сотрудников
 * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=125898713
 */
import removeEmptyProperties from 'utils/removeEmptyProperties';

import { axiosInstance } from '../axios';

import { URLS } from './constants';
import listSchema from './schemas/list.json';

/**
 * 1.3 Получение данных справочника
 * @param data - настройки списка
 * @param cancelToken
 */
const getList = (data, cancelToken) =>
  axiosInstance({
    cancelToken,
    method: 'post',
    url: URLS.GET_LIST,
    data: removeEmptyProperties(data),
    responseSchema: listSchema,
  });

export default {
  getList,
};
