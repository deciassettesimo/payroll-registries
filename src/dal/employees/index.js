import { call } from 'redux-saga/effects';

import { createRBOError } from '../errors';
import { getCancelToken } from '../axios';

import api from './api';

const cancelTokens = {};

export function* getList(data, withCancelToken) {
  cancelTokens.getList = getCancelToken(cancelTokens.getList);
  try {
    const result = yield call(api.getList, data, withCancelToken ? cancelTokens.getList.token : null);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}
