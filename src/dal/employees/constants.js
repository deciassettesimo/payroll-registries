const MAIN_PATH = 'employee-spr';

export const URLS = {
  GET_LIST: `${MAIN_PATH}/query`,
};
