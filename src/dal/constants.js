export const API_CANCELED_MESSAGE = 'canceled';

export const ALLOWED_SM_ACTION_ID = {
  ARCHIVE: 'archive',
  RECALL: 'recall',
  REMOVE: 'delete',
  REMOVE_SIGN: 'signRemove',
  SAVE: 'save',
  SEND: 'send',
  SIGN: 'sign',
  UNARCHIVE: 'fromArchive',
};

export const ERROR_KEY = {
  DOC_SEND: 'document.document_send_error',
};

export const LOCALE_KEY = {
  ERRORS: {
    DOC_SEND: {
      TITLE: 'loc.sendError',
      MESSAGE: 'loc.documentCannotSentForProcessingToBank',
    },
  },
};
