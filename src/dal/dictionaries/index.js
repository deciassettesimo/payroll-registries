import { call } from 'redux-saga/effects';

import { checkRBOControlsError, createRBOError } from '../errors';
import { getCancelToken } from '../axios';

import api from './api';
import { normalizeGetCardBranchesCities, normalizeGetIdentityDocumentTypes } from './normalize';

const cancelTokens = {};

export function* getCardBranches() {
  try {
    const result = yield call(api.getCardBranches);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getCardBranchesCities() {
  try {
    const result = yield call(api.getCardBranchesCities);
    return normalizeGetCardBranchesCities((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getCountries(params, withCancelToken) {
  cancelTokens.getCountries = getCancelToken(cancelTokens.getCountries);
  try {
    const result = yield call(api.getCountries, params, withCancelToken ? cancelTokens.getCountries.token : null);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getIdentityDocumentTypes() {
  try {
    const result = yield call(api.getIdentityDocumentTypes);
    return normalizeGetIdentityDocumentTypes((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getLoadSchemes(params) {
  try {
    const result = yield call(api.getLoadSchemes, params);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getOfficials(params, withCancelToken) {
  cancelTokens.getOfficials = getCancelToken(cancelTokens.getOfficials);
  try {
    const result = yield call(api.getOfficials, params, withCancelToken ? cancelTokens.getOfficials.token : null);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* saveOfficial(data) {
  try {
    const result = yield call(api.saveOfficial, data);
    const checkedResult = checkRBOControlsError(result);
    return (checkedResult && checkedResult.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getTariffPlans(params) {
  try {
    const result = yield call(api.getTariffPlans, params);
    return (result && result.data) || [];
  } catch (error) {
    throw createRBOError(error);
  }
}
