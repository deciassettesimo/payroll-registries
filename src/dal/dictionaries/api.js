/** Методы работы со справочниками
 */
import removeEmptyProperties from 'utils/removeEmptyProperties';

import { axiosInstance } from '../axios';

import { URLS } from './constants';
import cardBranchesSchema from './schemas/card-branches.json';
import cardBranchesCitiesSchema from './schemas/card-branches-cities.json';
import countriesSchema from './schemas/countries.json';
import identityDocumentTypesSchema from './schemas/identity-document-types.json';
import loadSchemesSchema from './schemas/load-schemes.json';
import officialsSchema from './schemas/officials.json';
import tariffPlansSchema from './schemas/tariff-plans.json';

/**
 * Получение списка отделений выдачи карт (Зарплатный проект)
 */
const getCardBranches = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_CARD_BRANCHES,
    responseSchema: cardBranchesSchema,
  });

/**
 * Получение списка городов отделений выдачи карт (Зарплатный проект)
 */
const getCardBranchesCities = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_CARD_BRANCHES_CITIES,
    responseSchema: cardBranchesCitiesSchema,
  });

/**
 * Получение списка городов отделений выдачи карт (Зарплатный проект)
 */
const getCountries = ({ offset = 0, ...params }, cancelToken) =>
  axiosInstance({
    cancelToken,
    method: 'get',
    url: URLS.GET_COUNTRIES,
    responseSchema: countriesSchema,
    params: removeEmptyProperties({ offset, ...params }),
  });

/**
 * Получение списка документов, удостоверяющих личность (Зарплатный проект)
 */
const getIdentityDocumentTypes = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_IDENTITY_DOCUMENT_TYPES,
    responseSchema: identityDocumentTypesSchema,
  });

/**
 * Получение списка доступных схем загрузок (Зарплатный проект)
 * @param offset - смещение выдачи (для постраничного вывода)
 * @param params - параметры запроса (orgId, tpCode - Код тарифного плана )
 */
const getLoadSchemes = ({ offset = 0, ...params }) =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_LOAD_SCHEMES,
    responseSchema: loadSchemesSchema,
    params: removeEmptyProperties({ offset, ...params }),
  });

/**
 * Получение списка ответственных лиц
 * @param offset - смещение выдачи (для постраничного вывода)
 * @param offsetStep - кол-во items в выдаче (для постраничного вывода)
 * @param sort - сортировка по полю
 * @param desc - направление сортировки
 * @param params - параметры запроса (orgId, fio - поиск по ФИО, position, phone, fioLat, responsibility, fax)
 *
 * @param cancelToken - axios cancel token
 */
const getOfficials = ({ offset = 0, offsetStep = 31, sort = 'name', desc = false, ...params }, cancelToken) =>
  axiosInstance({
    cancelToken,
    method: 'get',
    url: URLS.GET_OFFICIALS,
    responseSchema: officialsSchema,
    params: { query: removeEmptyProperties({ offset, offsetStep, sort, desc, ...params }) },
  });

/**
 * Сохранение ответственного лица
 * @param data
 */
const saveOfficial = data =>
  axiosInstance({
    method: 'post',
    url: URLS.SAVE_OFFICIAL,
    data: removeEmptyProperties(data),
  });

/**
 * Получение списка доступных тарифных планов для организации (Зарплатный проект)
 * @param offset - смещение выдачи (для постраничного вывода)
 * @param params - параметры запроса (orgId, lsCode - Код схемы загрузки)
 */
const getTariffPlans = ({ offset = 0, ...params }) =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_TARIFF_PLANS,
    responseSchema: tariffPlansSchema,
    params: removeEmptyProperties({ offset, ...params }),
  });

export default {
  getCardBranches,
  getCardBranchesCities,
  getCountries,
  getIdentityDocumentTypes,
  getLoadSchemes,
  getOfficials,
  saveOfficial,
  getTariffPlans,
};
