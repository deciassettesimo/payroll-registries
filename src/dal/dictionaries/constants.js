const MAIN_PATH = 'dicts';

export const URLS = {
  GET_CARD_BRANCHES: `${MAIN_PATH}/cardBranch`,
  GET_CARD_BRANCHES_CITIES: `${MAIN_PATH}/cardBranchCity`,
  GET_COUNTRIES: `${MAIN_PATH}/countries`,
  GET_IDENTITY_DOCUMENT_TYPES: `${MAIN_PATH}/credentialDocType`,
  GET_LOAD_SCHEMES: `${MAIN_PATH}/loadScheme`,
  GET_OFFICIALS: `${MAIN_PATH}/officials`,
  SAVE_OFFICIAL: `${MAIN_PATH}/officials`,
  GET_TARIFF_PLANS: `${MAIN_PATH}/tariffPlan`,
};
