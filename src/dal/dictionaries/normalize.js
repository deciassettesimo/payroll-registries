import { v4 } from 'uuid';

export const normalizeGetCardBranchesCities = data =>
  data
    .filter(item => item)
    .map(item => ({
      id: v4(item),
      name: item,
    }));

export const normalizeGetIdentityDocumentTypes = data =>
  data
    .filter(item => item)
    .map(item => ({
      id: v4(`${item.code}${item.code1C}`),
      ...item,
    }));
