import { jsonHeaders, logRequest } from '../../mock';

import { URLS } from '../constants';

import cardBranches from './card-branches.json';
import cardBranchesCities from './card-branches-cities.json';
import loadSchemes from './load-schemes.json';
import officials from './officials.json';
import tariffPlans from './tariff-plans.json';

const getCardBranches = config => logRequest(config, [200, cardBranches, jsonHeaders]);

const getCardBranchesCities = config => logRequest(config, [200, cardBranchesCities, jsonHeaders]);

const getLoadSchemes = config => logRequest(config, [200, loadSchemes, jsonHeaders]);

const getOfficials = config => {
  const { params } = config;
  const searchValue = params.query.fio;
  const searchRegExp = new RegExp(`${searchValue}`, 'ig');
  const result = searchValue ? officials.filter(item => item.fio.search(searchRegExp) >= 0) : officials;
  return logRequest(config, [200, result, jsonHeaders]);
};

const getTariffPlans = config => logRequest(config, [200, tariffPlans, jsonHeaders]);

export default function(axiosMockInstance) {
  axiosMockInstance.onGet(URLS.GET_CARD_BRANCHES).reply(getCardBranches);
  axiosMockInstance.onGet(URLS.GET_CARD_BRANCHES_CITIES).reply(getCardBranchesCities);
  axiosMockInstance.onGet(URLS.GET_LOAD_SCHEMES).reply(getLoadSchemes);
  axiosMockInstance.onGet(URLS.GET_OFFICIALS).reply(getOfficials);
  axiosMockInstance.onGet(URLS.GET_TARIFF_PLANS).reply(getTariffPlans);
}
