import AxiosMockAdapter from 'axios-mock-adapter';

import { axiosInstance } from './axios';
import dictionaries from './dictionaries/mock';
import documents from './documents/mock';
import payrollRegistriesAttachments from './payroll-registries-attachments/mock';
import payrollRegistriesDetachments from './payroll-registries-detachments/mock';
import user from './user/mock';

export const configureAxiosMockAdapter = () => {
  const axiosMockInstance = new AxiosMockAdapter(axiosInstance, { delayResponse: 100 });

  dictionaries(axiosMockInstance);
  documents(axiosMockInstance);
  payrollRegistriesAttachments(axiosMockInstance);
  payrollRegistriesDetachments(axiosMockInstance);
  user(axiosMockInstance);
};

export const jsonHeaders = {
  'content-type': 'application/json',
};

const style1 = 'font-weight: normal; color: #888888;';
const style2 = 'font-weight: bold; color: #000000;';
const prefix = 'mock request';

export const logRequest = (config, response) => {
  if (process.env.NODE_ENV === 'development') {
    console.groupCollapsed('%c %s %c%s', style1, prefix, style2, `${config.method} ${config.url}`); /* eslint-disable-line */
    console.log('config', config); /* eslint-disable-line */
    console.log('response', response[1]); /* eslint-disable-line */
    console.groupEnd(); /* eslint-disable-line */
  }
  return response;
};
