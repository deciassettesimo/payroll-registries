/** Методы работы с документом РнП
 * https://confluence.raiffeisen.ru/pages/viewpage.action?pageId=127438141
 */
import removeEmptyProperties from 'utils/removeEmptyProperties';

import { axiosInstance } from '../axios';

import { URLS } from './constants';
import documentSchema from './schemas/document.json';
import listSchema from './schemas/list.json';

/**
 * 1.1 Получение РнП
 * @param id - id документа
 */
const getDocData = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_DOC_DATA.replace('{:id}', id),
    responseSchema: documentSchema,
  });

/**
 * 1.2 Сохранение РнП
 * @param data - данные документа
 */
const saveDocData = data =>
  axiosInstance({
    method: 'post',
    url: URLS.SAVE_DOC_DATA,
    requestSchema: documentSchema,
    data: removeEmptyProperties(data),
  });

/**
 * 1.3 Получение default values РнП
 * @param orgId - id организации
 */
const getDefaultData = orgId =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_DEFAULT_DATA,
    responseSchema: documentSchema,
    data: { orgId },
  });

/**
 * 1.4, 1.5, 1.6 Получение списка РнП в работе
 * @param section - REF GET_LIST_SECTIONS
 * @param data - настройки списка
 */
const getList = (section, data) =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_LIST.replace('{:section}', section),
    data: removeEmptyProperties(data),
    responseSchema: listSchema,
  });

/**
 * 1.7 Доступные статусы РнП
 */
const getStatuses = () =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_STATUSES,
  });

/**
 * 1.8 Копирование РнП
 * @param docId - id копируемого документа
 */
const getRepeatData = docId =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_REPEAT_DATA,
    responseSchema: documentSchema,
    data: { docId },
  });

/**
 * 1.9 1.10, 1.11 Экспорт списка РнП в Excel
 * @param section - REF GET_LIST_SECTIONS
 * @param data - настройки списка
 */
const exportToExcel = (section, data) =>
  axiosInstance({
    method: 'post',
    url: URLS.EXPORT_TO_EXCEL.replace('{:section}', section),
    data: removeEmptyProperties(data),
    responseType: 'blob',
  });

/**
 * 2.1 Проверка РнП
 * @param data - данные документа
 */
const getDocErrors = data =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_DOC_ERRORS,
    requestSchema: documentSchema,
    data: removeEmptyProperties(data),
  });

/**
 * 2.2 Получение статуса реестра
 * @param id - id документа
 */
const getDocStatus = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_DOC_STATUS.replace('{:id}', id),
  });

/**
 * 2.3 Получение контролей РнП
 * @param id - id документа
 */
const getDocHeaderErrors = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_DOC_HEADER_ERRORS.replace('{:id}', id),
  });

/**
 * 2.4 Проверка сотрудника
 * @param data - данные документа c одним сотрудником в employeesInfo
 */
const getEmployeeErrors = data =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_EMPLOYEE_ERRORS,
    requestSchema: documentSchema,
    data: removeEmptyProperties(data),
  });

/**
 * 2.5 Создание копии документа с копированием из исходного документа только строк, имеющих статус «Отвергнут Банком»
 * @param docId - id копируемого документа
 */
const getRepeatWithRefusalsData = docId =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_REPEAT_WITH_REFUSALS_DATA,
    responseSchema: documentSchema,
    data: { docId },
  });

/**
 * 2.14 Импорт реестра из 1С
 * @param content - content файла
 * @param settings - настройки: encoding, version, numberFromFile, dateFromFile, nppFromFile, controlDuplicate, numberControlDuplicate, dateControlDuplicate
 */
const import1C = (content, settings) =>
  axiosInstance({
    method: 'post',
    url: URLS.IMPORT_1C,
    data: { ...settings, content },
  });

/**
 * 2.15 Получение статуса импорта
 * @param taskId - id задания импорта
 */
const import1CMonitoring = taskId =>
  axiosInstance({
    method: 'get',
    url: URLS.IMPORT_1C_MONITORING.replace('{:taskId}', taskId),
  });

/**
 * 2.17 Импорт реестра из CSV
 * @param content - content файла
 * @param ignoreIncorrectSnils,
 * @param lastNpp,
 * @param settings - настройки: encoding: "utf-8", separator: ";", lastNpp: 0,
 */
const CSVImport = (content, { ignoreIncorrectSnils = false, lastNpp = 0, ...settings }) =>
  axiosInstance({
    method: 'post',
    url: URLS.CSV_IMPORT,
    data: { ignoreIncorrectSnils, lastNpp, ...settings, content },
  });

/**
 * 2.19 Экспорт реестра в CSV
 * @param settings - настройки: orgIds, fromDate, toDate, exportFileMask, encoding, separator,
 */
const exportCSV = settings =>
  axiosInstance({
    method: 'post',
    url: URLS.EXPORT_CSV,
    responseType: 'blob',
    data: settings,
  });

/**
 * 2.20 Экспорт реестра в 1C
 * @param settings - настройки: orgIds, fromDate, toDate, version, exportFileMask, encoding, exportWithoutNameSpace,
 */
const export1C = settings =>
  axiosInstance({
    method: 'post',
    url: URLS.EXPORT_1C,
    responseType: 'blob',
    data: settings,
  });

/**
 * 3.1 Создание шаблона на основе реестра
 * @param id - id документа для создания шаблона
 * @param name - имя шаблона
 */
const saveAsTemplate = (id, name = '') =>
  axiosInstance({
    method: 'post',
    url: URLS.SAVE_AS_TEMPLATE,
    data: { i: id, name },
  });

/**
 * 3.2 Получение списка заголовков шаблонов
 * @param offset - смещение выдачи (для постраничного вывода)
 * @param offsetStep - кол-во items в выдаче (для постраничного вывода)
 * @param params - параметры запроса (orgId - array)
 */
const getTemplates = ({ offset = 0, offsetStep = 240, ...params }) =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_TEMPLATES,
    params: { query: removeEmptyProperties({ offset, offsetStep, ...params }) },
  });

/**
 * 3.3 Получение шаблона
 * @param id - id шаблона
 */
const getTemplateData = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_TEMPLATE_DATA.replace('{:id}', id),
    responseSchema: documentSchema,
  });

/**
 * 3.4 Сохранение шаблона
 * @param data - данные документа
 */
const saveTemplateData = data =>
  axiosInstance({
    method: 'post',
    url: URLS.SAVE_TEMPLATE_DATA,
    requestSchema: documentSchema,
    data: removeEmptyProperties(data),
  });

/**
 * 3.5 Удаление шаблона
 * @param id - id шаблона
 */
const removeTemplate = id =>
  axiosInstance({
    method: 'delete',
    url: URLS.REMOVE_TEMPLATE.replace('{:id}', id),
  });

/**
 * 3.6 Создание документа по шаблону
 * @param id - id шаблона
 */
const getFromTemplateData = id =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_FROM_TEMPLATE_DATA,
    responseSchema: documentSchema,
    data: { templateId: id },
  });

/**
 * 3.7 Проверка шаблона
 * @param data - данные шаблона
 */
const getTemplateErrors = data =>
  axiosInstance({
    method: 'post',
    url: URLS.GET_TEMPLATE_ERRORS,
    requestSchema: documentSchema,
    data: removeEmptyProperties(data),
  });

export default {
  getDocData,
  saveDocData,
  getDefaultData,
  getList,
  getStatuses,
  getRepeatData,
  exportToExcel,
  getDocErrors,
  getDocStatus,
  getDocHeaderErrors,
  getEmployeeErrors,
  getRepeatWithRefusalsData,
  import1C,
  import1CMonitoring,
  CSVImport,
  exportCSV,
  export1C,
  saveAsTemplate,
  getTemplates,
  getTemplateData,
  saveTemplateData,
  removeTemplate,
  getFromTemplateData,
  getTemplateErrors,
};
