import { v4 } from 'uuid';

import { IMPORT_TASK_STATUSES } from 'common/constants';
import { getStatus, getObjectKeyByValue, getCombinedErrorText, ServerDataNormalizer } from 'utils';

import { FIELD_ID, EMPLOYEES_INFO_ITEM_FIELD_ID } from './constants';

const attachContract = {
  'attachContract.date': FIELD_ID.ATTACH_CONTRACT_DATE,
  'attachContract.number': FIELD_ID.ATTACH_CONTRACT_NUMBER,
};

const cardBranch = {
  'cardBranch.address': FIELD_ID.CARD_BRANCH_ADDRESS,
  'cardBranch.bic': FIELD_ID.CARD_BRANCH_BIC,
  'cardBranch.city': FIELD_ID.CARD_BRANCH_CITY,
  'cardBranch.id': FIELD_ID.CARD_BRANCH_ID,
  'cardBranch.name': FIELD_ID.CARD_BRANCH_NAME,
};

// prettier-ignore
const confirmAcceptance = {
  'confirmAcceptance': FIELD_ID.CONFIRM_ACCEPTANCE,
};

const customer = {
  'customer.INN': FIELD_ID.CUSTOMER_INN,
  'customer.OGRN': FIELD_ID.CUSTOMER_OGRN,
  'customer.id': FIELD_ID.CUSTOMER_ID,
  'customer.name': FIELD_ID.CUSTOMER_NAME,
};

const docInfo = {
  'docInfo.allowedSmActions': FIELD_ID.ALLOWED_SM_ACTIONS,
  'docInfo.date': FIELD_ID.DATE,
  'docInfo.fromBankInfo.acceptDate': FIELD_ID.FROM_BANK_INFO_ACCEPT_DATE,
  'docInfo.fromBankInfo.bankMessage': FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE,
  'docInfo.fromBankInfo.bankMessageAuthor': FIELD_ID.FROM_BANK_INFO_BANK_MESSAGE_AUTHOR,
  'docInfo.fromBankInfo.operationDate': FIELD_ID.FROM_BANK_INFO_OPERATION_DATE,
  'docInfo.id': FIELD_ID.ID,
  'docInfo.note': FIELD_ID.NOTE,
  'docInfo.number': FIELD_ID.NUMBER,
  'docInfo.status': FIELD_ID.STATUS,
  'docInfo.templateName': FIELD_ID.TEMPLATE_NAME,
};

// prettier-ignore
const emplCount = {
  'emplCount': FIELD_ID.EMPL_COUNT,
};

// prettier-ignore
const employeesInfo = {
  'employeesInfo': FIELD_ID.EMPLOYEES_INFO,
};

// prettier-ignore
const isCheck = {
  'isCheck': FIELD_ID.IS_CHECK,
};

// prettier-ignore
const copyRejectedEnabled = {
  'copyRejectedEnabled': FIELD_ID.COPY_REJECTED_ENABLED,
};

const loadScheme = {
  'loadScheme.name': FIELD_ID.LOAD_SCHEME_NAME,
  'loadScheme.number': FIELD_ID.LOAD_SCHEME_NUMBER,
};

const official = {
  'official.name': FIELD_ID.OFFICIAL_NAME,
  'official.phone': FIELD_ID.OFFICIAL_PHONE,
};

const tariffPlan = {
  'tariffPlan.name': FIELD_ID.TARIFF_PLAN_NAME,
  'tariffPlan.number': FIELD_ID.TARIFF_PLAN_NUMBER,
};

const docDataMap = {
  ...attachContract,
  ...cardBranch,
  ...confirmAcceptance,
  ...customer,
  ...docInfo,
  ...emplCount,
  ...employeesInfo,
  ...isCheck,
  ...copyRejectedEnabled,
  ...loadScheme,
  ...official,
  ...tariffPlan,
};

// prettier-ignore
const saveDataMap = {
  'allowedSmActions': FIELD_ID.ALLOWED_SM_ACTIONS,
  'recordID': FIELD_ID.ID,
  'status': FIELD_ID.STATUS,
  'errors': 'errors',
};

/** EmployeesInfoItemMap */
const employeesInfoItemAddressInfo = {
  'addressInfo.addressMatch': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_ADDRESS_MATCH,
  'addressInfo.livAddress.block': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BLOCK,
  'addressInfo.livAddress.building': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_BUILDING,
  'addressInfo.livAddress.city': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_CITY,
  'addressInfo.livAddress.country.code': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_CODE,
  'addressInfo.livAddress.country.name': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_COUNTRY_NAME,
  'addressInfo.livAddress.district': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_DISTRICT,
  'addressInfo.livAddress.flat': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_FLAT,
  'addressInfo.livAddress.index': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_INDEX,
  'addressInfo.livAddress.place': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_PLACE,
  'addressInfo.livAddress.state': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STATE,
  'addressInfo.livAddress.street': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_LIV_ADDRESS_STREET,
  'addressInfo.regAddress.block': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BLOCK,
  'addressInfo.regAddress.building': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_BUILDING,
  'addressInfo.regAddress.city': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_CITY,
  'addressInfo.regAddress.country.code': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_CODE,
  'addressInfo.regAddress.country.name': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_COUNTRY_NAME,
  'addressInfo.regAddress.district': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_DISTRICT,
  'addressInfo.regAddress.flat': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_FLAT,
  'addressInfo.regAddress.index': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_INDEX,
  'addressInfo.regAddress.place': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_PLACE,
  'addressInfo.regAddress.state': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STATE,
  'addressInfo.regAddress.street': EMPLOYEES_INFO_ITEM_FIELD_ID.ADDRESS_INFO_REG_ADDRESS_STREET,
};

const employeesInfoItemCardBranch = {
  'cardBranch.address': EMPLOYEES_INFO_ITEM_FIELD_ID.CARD_BRANCH_ADDRESS,
  'cardBranch.bic': EMPLOYEES_INFO_ITEM_FIELD_ID.CARD_BRANCH_BIC,
  'cardBranch.city': EMPLOYEES_INFO_ITEM_FIELD_ID.CARD_BRANCH_CITY,
  'cardBranch.id': EMPLOYEES_INFO_ITEM_FIELD_ID.CARD_BRANCH_ID,
  'cardBranch.name': EMPLOYEES_INFO_ITEM_FIELD_ID.CARD_BRANCH_NAME,
};

const employeesInfoItemCountry = {
  'country.code': EMPLOYEES_INFO_ITEM_FIELD_ID.COUNTRY_CODE,
  'country.name': EMPLOYEES_INFO_ITEM_FIELD_ID.COUNTRY_NAME,
};

const employeesInfoItemCitizenship = {
  'citizenship.code': EMPLOYEES_INFO_ITEM_FIELD_ID.CITIZENSHIP_CODE,
  'citizenship.name': EMPLOYEES_INFO_ITEM_FIELD_ID.CITIZENSHIP_NAME,
};

// prettier-ignore
const employeesInfoItemEmbossed = {
  'embossedFamily': EMPLOYEES_INFO_ITEM_FIELD_ID.EMBOSSED_FAMILY,
  'embossedName': EMPLOYEES_INFO_ITEM_FIELD_ID.EMBOSSED_NAME,
};

const employeesInfoItemEmployee = {
  'employee.birthDate': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_BIRTH_DATE,
  'employee.employeeNum': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_NUM,
  'employee.name': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_NAME,
  'employee.patronymic': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PATRONYMIC,
  'employee.position': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_POSITION,
  'employee.resident': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_RESIDENT,
  'employee.surname': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_SURNAME,
  'employeeContactInfo.email': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_CONTACT_INFO_EMAIL,
  'employeeContactInfo.homePhone': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_CONTACT_INFO_HOME_PHONE,
  'employeeContactInfo.mobilePhone': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_CONTACT_INFO_MOBILE_PHONE,
  'employeeContactInfo.workPhone': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_CONTACT_INFO_WORK_PHONE,
  'employeeDocument.documentType.code': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_CODE,
  'employeeDocument.documentType.typeDescription': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_TYPE_DESCRIPTION,
  'employeeDocument.issuedBy': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_BY,
  'employeeDocument.issuedCountry.code': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_CODE,
  'employeeDocument.issuedCountry.name': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_COUNTRY_NAME,
  'employeeDocument.issuedDate': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_ISSUED_DATE,
  'employeeDocument.number': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_NUMBER,
  'employeeDocument.series': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_SERIES,
  'employeeDocument.subdivisionCode': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_DOCUMENT_SUBDIVISION_CODE,
  'employeeProcessingResult.accountResult': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_ACCOUNTS,
  'employeeProcessingResult.processingResult.code1C': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_CODE_1C,
  'employeeProcessingResult.processingResult.messageFromBank':
    EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_MESSAGE_FROM_BANK,
  'employeeProcessingResult.processingResult.status': EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_PROCESSING_RESULT_STATUS,
};

const employeesInfoItemLoadScheme = {
  'loadScheme.name': EMPLOYEES_INFO_ITEM_FIELD_ID.LOAD_SCHEME_NAME,
  'loadScheme.number': EMPLOYEES_INFO_ITEM_FIELD_ID.LOAD_SCHEME_NUMBER,
};

const employeesInfoItemMigrationCard = {
  'migrationCard.dppDateEnd': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_DATE_END,
  'migrationCard.dppDateStart': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_DATE_START,
  'migrationCard.dppDocumentType.code': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_DOCUMENT_TYPE_CODE,
  'migrationCard.dppDocumentType.typeDescription':
    EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_DOCUMENT_TYPE_DESCRIPTION,
  'migrationCard.dppIssuedBy': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_ISSUED_BY,
  'migrationCard.dppIssuedDate': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_ISSUED_DATE,
  'migrationCard.dppNumber': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_NUMBER,
  'migrationCard.dppSeries': EMPLOYEES_INFO_ITEM_FIELD_ID.MIGRATION_CARD_DPP_SERIES,
};

const employeesInfoItemPermissionToStay = {
  'permissionToStay.dppDateEnd': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_END,
  'permissionToStay.dppDateStart': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_DATE_START,
  'permissionToStay.dppDocumentType.code': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_DOCUMENT_TYPE_CODE,
  'permissionToStay.dppDocumentType.typeDescription':
    EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_DOCUMENT_TYPE_DESCRIPTION,
  'permissionToStay.dppIssuedBy': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_BY,
  'permissionToStay.dppIssuedDate': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_ISSUED_DATE,
  'permissionToStay.dppNumber': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_NUMBER,
  'permissionToStay.dppSeries': EMPLOYEES_INFO_ITEM_FIELD_ID.PERMISSION_TO_STAY_DPP_SERIES,
};

const employeesInfoItemTariffPlan = {
  'tariffPlan.name': EMPLOYEES_INFO_ITEM_FIELD_ID.TARIFF_PLAN_NAME,
  'tariffPlan.number': EMPLOYEES_INFO_ITEM_FIELD_ID.TARIFF_PLAN_NUMBER,
};

const employeesInfoItemVisa = {
  'visa.dppDateEnd': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_DATE_END,
  'visa.dppDateStart': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_DATE_START,
  'visa.dppDocumentType.code': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_DOCUMENT_TYPE_CODE,
  'visa.dppDocumentType.typeDescription': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_DOCUMENT_TYPE_DESCRIPTION,
  'visa.dppIssuedBy': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_ISSUED_BY,
  'visa.dppIssuedDate': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_ISSUED_DATE,
  'visa.dppNumber': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_NUMBER,
  'visa.dppSeries': EMPLOYEES_INFO_ITEM_FIELD_ID.VISA_DPP_SERIES,
};

// prettier-ignore
const employeesInfoItemOther = {
  'gender': EMPLOYEES_INFO_ITEM_FIELD_ID.GENDER,
  'id': EMPLOYEES_INFO_ITEM_FIELD_ID.ID,
  'inn': EMPLOYEES_INFO_ITEM_FIELD_ID.INN,
  'married': EMPLOYEES_INFO_ITEM_FIELD_ID.MARRIED,
  'npp': EMPLOYEES_INFO_ITEM_FIELD_ID.NPP,
  'placeOfBirth': EMPLOYEES_INFO_ITEM_FIELD_ID.PLACE_OF_BIRTH,
  'snils': EMPLOYEES_INFO_ITEM_FIELD_ID.SNILS,
};

const employeesInfoItemMap = {
  ...employeesInfoItemAddressInfo,
  ...employeesInfoItemCardBranch,
  ...employeesInfoItemCitizenship,
  ...employeesInfoItemCountry,
  ...employeesInfoItemEmbossed,
  ...employeesInfoItemEmployee,
  ...employeesInfoItemLoadScheme,
  ...employeesInfoItemMigrationCard,
  ...employeesInfoItemPermissionToStay,
  ...employeesInfoItemTariffPlan,
  ...employeesInfoItemVisa,
  ...employeesInfoItemOther,
};

export const employeesInfoItemEmployeeResidentValues = {
  fromServer: value => {
    switch (value) {
      case true:
        return '1';
      case false:
        return '0';
      default:
        return null;
    }
  },
  toServer: value => {
    switch (value) {
      case '1':
        return true;
      case '0':
        return false;
      default:
        return null;
    }
  },
};

// prettier-ignore
const templatesItemMap = {
  'recordID': FIELD_ID.ID,
  'name': FIELD_ID.TEMPLATE_NAME,
  'correspondentName': FIELD_ID.CUSTOMER_NAME,
};

const docDataNormalizer = new ServerDataNormalizer(docDataMap);
const employeesInfoItemNormalizer = new ServerDataNormalizer(employeesInfoItemMap);
const saveDataNormalizer = new ServerDataNormalizer(saveDataMap);
const templatesNormalizer = new ServerDataNormalizer(templatesItemMap);

export const normalizeEmployeeInfoDataFromServer = data => {
  const normalizedEmployeesInfoItem = employeesInfoItemNormalizer.fromServer(data);
  return {
    ...normalizedEmployeesInfoItem,
    [EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_RESIDENT]: employeesInfoItemEmployeeResidentValues.fromServer(
      normalizedEmployeesInfoItem[EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_RESIDENT],
    ),
  };
};

export const normalizeGetDocDataFromServer = data => {
  const normalizedData = docDataNormalizer.fromServer(data);
  const normalizedEmployeesInfo = normalizedData[FIELD_ID.EMPLOYEES_INFO].map(item =>
    normalizeEmployeeInfoDataFromServer(item),
  );

  return {
    ...normalizedData,
    [FIELD_ID.STATUS]: getStatus.idByServerValue(normalizedData[FIELD_ID.STATUS]),
    [FIELD_ID.EMPLOYEES_INFO]: normalizedEmployeesInfo,
  };
};

export const normalizePostDocDataToServer = data => {
  const preparedData = {
    ...data,
    [FIELD_ID.STATUS]: getStatus.serverValueById(data[FIELD_ID.STATUS]),
    [FIELD_ID.EMPLOYEES_INFO]: data[FIELD_ID.EMPLOYEES_INFO].map(item =>
      employeesInfoItemNormalizer.toServer({
        ...item,
        [EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_RESIDENT]: employeesInfoItemEmployeeResidentValues.toServer(
          item[EMPLOYEES_INFO_ITEM_FIELD_ID.EMPLOYEE_RESIDENT],
        ),
      }),
    ),
  };
  return docDataNormalizer.toServer(preparedData);
};

export const normalizePostDocDataFromServer = data => {
  const normalizedData = saveDataNormalizer.fromServer(data);
  return {
    ...normalizedData,
    [FIELD_ID.STATUS]: getStatus.idByServerValue(normalizedData[FIELD_ID.STATUS]),
  };
};

export const normalizeGetListToServer = data => ({
  ...data,
  sort: {
    column: getObjectKeyByValue(docDataMap, data.sort.column),
    desc: data.sort.desc,
  },
  filter: {
    ...docDataNormalizer.toServer({
      ...data.filter,
      [FIELD_ID.STATUS]: data.filter[FIELD_ID.STATUS]
        ? data.filter[FIELD_ID.STATUS].map(status => getStatus.serverValueById(status))
        : null,
    }),
    visibleColumns: data.filter.visibleColumns
      ? data.filter.visibleColumns.map(column => getObjectKeyByValue(docDataMap, column))
      : null,
  },
});

export const normalizeGetListFromServer = data => data.map(item => normalizeGetDocDataFromServer(item));

export const normalizeGetTemplatesFromServer = data => data.map(item => templatesNormalizer.fromServer(item));

export const normalizeGetStatusesFromServer = data => data.map(item => getStatus.byServerValue(item));

export const normalizeCSVImportFromServer = data => ({
  employeesInfo: data
    .filter(item => item.employeeInfo)
    .map(item => normalizeEmployeeInfoDataFromServer(item.employeeInfo)),
  errors: data
    .filter(item => item.label)
    .map(item => ({
      id: v4(item.label),
      text: item.errors ? getCombinedErrorText(item.label, item.errors.map(error => ({ text: error })), 2) : item.label,
      level: '3',
    })),
});

export const normalizeImport1CFromServer = data => {
  if (data.message) return { error: data.message };
  return data;
};

export const normalizeImport1CMonitoringFromServer = data => {
  const statusKey = Object.keys(IMPORT_TASK_STATUSES).find(
    key => IMPORT_TASK_STATUSES[key].serverValue === data.result,
  );
  return {
    status: statusKey ? IMPORT_TASK_STATUSES[statusKey] : null,
    startTime: data.startTime,
    finishTime: data.finishTime,
    docId: data.docId,
    emplAmount: data.emplAmount,
    message: data.message,
  };
};
