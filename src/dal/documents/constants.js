const MAIN_PATH = 'documents';

export const URLS = {
  REMOVE: `${MAIN_PATH}`,
  ARCHIVE: `${MAIN_PATH}/archivation`,
  UNARCHIVE: `${MAIN_PATH}/archivation`,
  SEND: `${MAIN_PATH}/send`,
  GET_STATE_HISTORY: `${MAIN_PATH}/stateHistory`,
  GET_SIGNS_CHECK: `${MAIN_PATH}/signsCheck`,
};
