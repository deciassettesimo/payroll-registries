import { call } from 'redux-saga/effects';

import { createRBOError } from '../errors';

import api from './api';
import { normalizeGetStateHistoryFromServer, normalizeGetSignsCheckFromServer } from './normalize';

export function* remove(ids) {
  try {
    const result = yield call(api.remove, ids);
    return (result && result.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* archive(ids) {
  try {
    const result = yield call(api.archive, ids);
    return (result && result.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* unarchive(ids) {
  try {
    const result = yield call(api.unarchive, ids);
    return (result && result.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* send(ids) {
  try {
    const result = yield call(api.send, ids);
    return (result && result.data) || {};
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getStateHistory(id) {
  try {
    const result = yield call(api.getStateHistory, id);
    return normalizeGetStateHistoryFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* getSignsCheck(id) {
  try {
    const result = yield call(api.getSignsCheck, id);
    return normalizeGetSignsCheckFromServer((result && result.data) || []);
  } catch (error) {
    throw createRBOError(error);
  }
}
