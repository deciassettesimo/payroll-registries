import { getStatus } from 'utils';

export const normalizeGetStateHistoryFromServer = data =>
  data.map(item => ({
    ...item,
    endState: getStatus.idByServerValue(item.endState),
  }));

export const normalizeGetSignsCheckFromServer = data => {
  if (Array.isArray(data) && data[0]) return data[0].signs;
  return [];
};
