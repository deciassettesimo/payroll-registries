/** Общие методы работы с документами
 */
import { axiosInstance } from '../axios';

import { URLS } from './constants';

/**
 * 1.1 Удалить
 * @param ids - ids документов
 */
const remove = ids =>
  axiosInstance({
    method: 'delete',
    url: URLS.REMOVE,
    params: { docId: ids },
  });

/**
 * 1.2 Архивировать
 * @param ids - ids документов
 */
const archive = ids =>
  axiosInstance({
    method: 'post',
    url: URLS.ARCHIVE,
    data: { docId: ids },
  });

/**
 * 1.3 Возвратить из архива
 * @param ids - ids документов
 */
const unarchive = ids =>
  axiosInstance({
    method: 'delete',
    url: URLS.UNARCHIVE,
    params: { docId: ids },
  });

/**
 * 1.4 Отправить в Банк
 * @param ids - ids документов
 */
const send = ids =>
  axiosInstance({
    method: 'post',
    url: URLS.SEND,
    data: { docId: ids },
  });

/**
 * 1.5 История статусов
 * @param id - id документа
 */
const getStateHistory = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_STATE_HISTORY,
    params: { docId: id },
  });

/**
 * 1.6 Проверить подпись
 * @param id - id документа
 */
const getSignsCheck = id =>
  axiosInstance({
    method: 'get',
    url: URLS.GET_SIGNS_CHECK,
    params: { docId: id },
  });

export default {
  remove,
  archive,
  unarchive,
  send,
  getStateHistory,
  getSignsCheck,
};
