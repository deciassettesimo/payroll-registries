import { jsonHeaders, logRequest } from '../../mock';

import { URLS } from '../constants';

import signsCheck from './signs-check.json';
import stateHistory from './state-history.json';

const remove = config => logRequest(config, [200, {}, jsonHeaders]);

const archive = config => logRequest(config, [200, {}, jsonHeaders]);

const unarchive = config => logRequest(config, [200, {}, jsonHeaders]);

const send = config => logRequest(config, [200, {}, jsonHeaders]);

const getStateHistory = config => logRequest(config, [200, stateHistory, jsonHeaders]);

const getSignsCheck = config => logRequest(config, [200, signsCheck, jsonHeaders]);

export default function(axiosMockInstance) {
  axiosMockInstance.onDelete(URLS.REMOVE).reply(remove);
  axiosMockInstance.onPost(URLS.ARCHIVE).reply(archive);
  axiosMockInstance.onDelete(URLS.UNARCHIVE).reply(unarchive);
  axiosMockInstance.onPost(URLS.SEND).reply(send);
  axiosMockInstance.onGet(URLS.GET_STATE_HISTORY).reply(getStateHistory);
  axiosMockInstance.onGet(URLS.GET_SIGNS_CHECK).reply(getSignsCheck);
}
