import { call } from 'redux-saga/effects';
import { saveAs } from 'file-saver';

import getFormattedDate from 'utils/getFormattedDate';

import { createRBOError } from '../errors';

import api from './api';

export function* download(params) {
  try {
    const { docId, signId, signDate } = params;
    const result = yield call(api.download, { docId, signId });
    saveAs(result.data, `signature_${signDate && `_${getFormattedDate(signDate, 'DD-MM-YYYY_HH-mm')}`}.zip`);
    return true;
  } catch (error) {
    throw createRBOError(error);
  }
}

export function* print(params) {
  try {
    const { docId, signId, signDate } = params;
    const result = yield call(api.print, { docId, signId });
    saveAs(result.data, `signature_${signDate && `_${getFormattedDate(signDate, 'DD-MM-YYYY_HH-mm')}`}.pdf`);
    return true;
  } catch (error) {
    throw createRBOError(error);
  }
}
