/** Общие методы работы с подписями
 */
import { axiosInstance } from '../axios';

import { URLS } from './constants';

/**
 * Скачивание подписи
 * @param params - (docId - id документа, signId - id подписи)
 */
const download = params =>
  axiosInstance({
    method: 'get',
    url: URLS.DOWNLOAD,
    responseType: 'blob',
    params: { ...params, format: 'zip' },
  });

/**
 * Печать подписи
 * @param params - (docId - id документа, signId - id подписи)
 */
const print = params =>
  axiosInstance({
    method: 'get',
    url: URLS.PRINT,
    responseType: 'blob',
    params: { ...params, format: 'pdf' },
  });

export default {
  download,
  print,
};
