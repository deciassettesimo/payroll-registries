import qs from 'qs';
import uuid from 'uuid';
import { settings } from './settings';

const storagePrefix = 'payroll_registries';

function setAuthParams() {
  window.localStorage.setItem(`${storagePrefix}_state`, uuid());
  window.localStorage.setItem(`${storagePrefix}_nonce`, uuid());
  window.localStorage.setItem(`${storagePrefix}_scope`, 'openid profile email phone');
}

function getAuthParams() {
  const state = window.localStorage.getItem(`${storagePrefix}_state`);
  const nonce = window.localStorage.getItem(`${storagePrefix}_nonce`);
  const scope = window.localStorage.getItem(`${storagePrefix}_scope`);
  return { state, nonce, scope };
}

function clearAuthParams() {
  window.localStorage.removeItem(`${storagePrefix}_state`);
  window.localStorage.removeItem(`${storagePrefix}_nonce`);
  window.localStorage.removeItem(`${storagePrefix}_scope`);
}

function setTokens(params) {
  const { access_token, id_token } = qs.parse(params);
  window.sessionStorage.setItem(`${storagePrefix}_access_token`, access_token);
  window.sessionStorage.setItem(`${storagePrefix}_id_token`, id_token);
}

function getTokens() {
  const accessToken = window.sessionStorage.getItem(`${storagePrefix}_access_token`);
  const idToken = window.sessionStorage.getItem(`${storagePrefix}_id_token`);
  return { accessToken, idToken };
}

function clearTokens() {
  window.sessionStorage.removeItem(`${storagePrefix}_access_token`);
  window.sessionStorage.removeItem(`${storagePrefix}_id_token`);
}

function getTokenData(token) {
  if (!token) return {};
  try {
    const escaped = token
      .split('.')[1]
      .replace(/-/g, '+')
      .replace(/_/g, '/');
    return JSON.parse(
      decodeURIComponent(
        atob(escaped)
          .split('')
          .map(c => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
          .join(''),
      ),
    );
  } catch (error) {
    return null;
  }
}

export function redirectToAuthorize() {
  clearTokens();
  setAuthParams();
  const {
    SERVICE_AUTH_URL,
    SERVICE_AUTH_CLIENT_ID,
    SERVICE_AUTH_REDIRECT_URI,
    SERVICE_AUTH_AUTHORIZE_METHOD,
  } = settings;
  const { state, nonce, scope } = getAuthParams();
  const query = {
    client_id: SERVICE_AUTH_CLIENT_ID,
    redirect_uri: SERVICE_AUTH_REDIRECT_URI,
    response_type: 'id_token token',
    state,
    nonce,
    scope,
  };

  window.location.replace(`${SERVICE_AUTH_URL}${SERVICE_AUTH_AUTHORIZE_METHOD}?${qs.stringify(query)}`);
}

function redirectToHome() {
  window.location.replace(settings.SERVICE_AUTH_REDIRECT_URI);
}

export function checkAuth() {
  return new Promise((resolve, reject) => {
    const { accessToken, idToken } = getTokens();
    if (accessToken && idToken) {
      resolve({ accessToken, idToken });
    } else {
      const authParams = getAuthParams();
      const hashString = window.location.hash.replace('#', '');
      const hashData = qs.parse(hashString);
      const userDetails = getTokenData(hashData.id_token);
      if (
        authParams &&
        authParams.state &&
        authParams.nonce &&
        userDetails &&
        hashData.state === authParams.state &&
        userDetails.nonce === authParams.nonce
      ) {
        setTokens(hashString);
        clearAuthParams();
        reject();
        redirectToHome();
      } else {
        reject();
        redirectToAuthorize();
      }
    }
  });
}
