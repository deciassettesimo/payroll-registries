const serviceAuthUrl = () => {
  switch (process.env.REACT_APP_STAND) {
    case 'bss5': return 'https://sso.test.rbp.raiffeisen.ru/';
    case 'bss15': return 'https://auth.test.rbo.raiffeisen.ru/';
    case 'bss7': case 'bss6': default: return 'https://auth.dev.rbo.raiffeisen.ru/';
  }
};

const serviceAuthClientId = () => {
  switch (process.env.REACT_APP_STAND) {
    case 'bss5': case 'bss15': return 'rbo';
    case 'bss7': return 'elbrus';
    case 'bss6': default: return 'rbo2';
  }
};

const serviceBaseUrl = () => {
  switch (process.env.REACT_APP_STAND) {
    case 'bss5': return 'https://back.test.rbp.raiffeisen.ru/rbo-rest-server/';
    case 'bss15': return 'https://app.test.rbo.raiffeisen.ru/rbo-rest-server/';
    case 'bss7': return 'https://app.dev.rbo.raiffeisen.ru/rbo-rest-server/';
    case 'bss6': default: return 'https://s-msk-t-rbp0048.raiffeisen.ru/rbo-rest-server/';
  }
};

export const settings = {
  SERVICE_AUTH_URL: serviceAuthUrl(),
  SERVICE_AUTH_CLIENT_ID: serviceAuthClientId(),
  SERVICE_BASE_URL: serviceBaseUrl(),
  SERVICE_AUTH_REDIRECT_URI: `http://localhost:${process.env.REACT_APP_PORT}/`,
  SERVICE_AUTH_AUTHORIZE_METHOD: 'authorize',
  LOCALE: process.env.REACT_APP_LOCALE,
};

