import { redirectToAuthorize } from './auth';

const style1 = 'font-weight: normal; color: #888888;';
const style2 = 'font-weight: bold; color: #000000;';
const prefix = 'platform action';

export const logout = () => {
  console.log('%c %s %c%s', style1, prefix, style2, 'LOGOUT');
  redirectToAuthorize();
};

export const notification = params => {
  console.groupCollapsed('%c %s %c%s', style1, prefix, style2, 'NOTIFICATION');
  console.log('params', params);
  console.groupEnd();

  if (params.type === 'confirm') params.success.callback();
};

export const sign = params => {
  console.groupCollapsed('%c %s %c%s', style1, prefix, style2, 'SIGN');
  console.log('params', params);
  console.groupEnd();

  params.callbacks.success();
};

export const removeSign = params => {
  console.groupCollapsed('%c %s %c%s', style1, prefix, style2, 'REMOVE_SIGN');
  console.log('params', params);
  console.groupEnd();

  params.callbacks.success();
};

export const saved = param => {
  console.log('%c %s %c%s', style1, prefix, style2, 'SAVED', param);
};
