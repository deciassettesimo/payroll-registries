/* eslint-disable */
import i18nRaw from './i18n';
import ru from './ref-ru.json';
import en from './ref-en.json';

const STRICT_I18N_MODE = true;
const localesList = { ru, en };
let locale = null;

export const setLocale = param => {
  locale = param;
};

export const getLocale = () => {
  return locale;
};

export const translate = (params, variables) => {
  if (typeof params === 'string') {
    return i18n(params, variables);
  }
  if (typeof params === 'object') {
    return i18nMap(params, variables);
  }
  return null;
};

/**
 * Функция интернационализации, уже содержит в себе информацию о словарях и локали
 * @param {string} key идентификатор фразы для перевода
 * @param {object} [variables] переменные для интерполяции
 * @returns {string}
 */
export const i18n = (key, variables = {}) => {
  if (!locale) {
    console.error('Не подключён язык');
    return '';
  }

  try {
    return i18nRaw(locale, localesList, key, variables);
  } catch (error) {
    if (STRICT_I18N_MODE) {
      throw error;
    } else {
      console.error(error);
      return '';
    }
  }
};

let i18nMapCache = new Map();
let cachedLang = null;
/**
 * Проходит по объекту и преобразовавает все ключи в слова, может принимать переменные, которые используются для интерполяции, мемоизирована
 * @param {object} map карта ключей
 * @param {object} [variables] переменные для интерполяции
 * @returns {object} переведённая карта
 */
export function i18nMap(map, variables) {
  if (typeof map !== 'object' || map === null) {
    return null;
  }

  if (locale !== cachedLang) {
    i18nMapCache = new Map();
    cachedLang = locale;
  }

  if (i18nMapCache.has(map)) {
    const savedData = i18nMapCache.get(map);

    if (savedData.has(variables)) {
      return savedData.get(variables);
    }
  } else {
    i18nMapCache.set(map, new Map());
  }

  const result = Object.keys(map).reduce(
    (acc, key) => {
      const levelMap = { ...acc };

      if (typeof map[key] === 'string') {
        levelMap[key] = i18n(map[key], variables || {});
      } else {
        levelMap[key] = i18nMap(map[key], variables);
      }

      return levelMap;
    },
    {},
  );

  i18nMapCache.get(map).set(variables, result);
  return result;
}
