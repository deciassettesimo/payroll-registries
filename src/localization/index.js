import store from 'store';
import { ACTIONS } from 'store/constants';

import { setLocale, getLocale, translate } from './translate';

const localization = { getLocale, translate };

export const configureLocalization = ({ locale }) => {
  store.dispatch({ type: ACTIONS.SET_LOCALE, payload: { locale } });
  setLocale(locale);
};

export default localization;
