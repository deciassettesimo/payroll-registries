/* eslint-disable */
import invariant from 'invariant';

const pipesMap = {};
const globalPipes = {};

const pluralLangMap = {
  ru: ruPlural,
  en: enPlural,
  default: enPlural
};

/**
 * Функция позволяет локализовывать фразы под необходимый язык, мемоизирована,
 * имеет возможность склонения существительных в зависимости от числительных, использовать значения по-умолчанию
 *
 * Пример:
 * ```
 const testPhrases = {
   ru: {
     simple: 'Привет, товарищ!',
     interpolate: 'Привет, {name}! Я тебя видел в {time} вчера.',
     default: 'Привет, {name|default[товарищ]}!',
     then: 'Привет, {num|then[№ ]}{num}!',
     plural: 'Привет, {countString} {count|plural[подруга, подруги, подруг]}!',
     mixed: 'Привет, {countString|default[одна]} {count|default[1]|plural[подруга, подруги, подруг]}!'
     globalDefault: 'Привет, {count} {count|plural[подруга, подруги, подруг]}!{count|globalDefault[Я не знаю, сколько вас]}'
   }
 };

 i18n('ru', testPhrases, 'simple'); // ==> 'Привет, товарищ!'
 i18n('ru', testPhrases, 'interpolate', { name: 'друг', time: '8:00' }); // ==> 'Привет, друг! Я тебя видел в 8:00 вчера.'
 i18n('ru', testPhrases, 'default', { name: 'друг' }); // ==> 'Привет, друг!'
 i18n('ru', testPhrases, 'then', { num: 1 }); // ==> 'Привет, № 1!'
 i18n('ru', testPhrases, 'plural', { count: 3, countString: 'три' }); // ==> 'Привет, три подруги!'
 i18n('ru', testPhrases, 'plural', { count: 100, countString: 'сто' }); // ==> 'Привет, сто подруг!'
 i18n('ru', testPhrases, 'mixed'); // ==> 'Привет, одна подруга!'
 i18n('ru', testPhrases, 'globalDefault'); // ==> 'Я не знаю, сколько вас'
 ```
 * @param {'ru'|'en'} language ключ языка
 * @param {Object} phrases карта фраз
 * @param {string} key ключ фразы
 * @param {*} [variables] переменные для интерполяции
 * @returns {string}
 */
const i18n = (() => {
  const cache = {};

  return (language, phrases, key, variables = {}) => {
    const langPhases = phrases[language];
    invariant(Boolean(langPhases), `i18n: Can't find ${language} pack`);

    const phrase = langPhases[key];
    invariant(typeof phrase === 'string', `i18n: Can't find phrase with key ${key}`);

    const variablesHash = Object.keys(variables).sort().map(variableKey => `${variableKey}:${variables[variableKey]}`).join(',');
    const hash = `${language}:${key}:${variablesHash}`;

    if (cache[hash]) {
      return cache[hash];
    }

    let phraseMeta = {};
    let computedPhrase = phrase.replace(/\{.+?\}/g, (token) => {
      const { value, meta } = parseToken(token, variables, language);

      phraseMeta = meta;
      return value;
    });

    computedPhrase = Object.values(globalPipes).reduce((acc, pipe) => {
      if (pipe.after && typeof pipe.after === 'function') {
        return pipe.after(acc, phraseMeta);
      }

      return acc;
    }, computedPhrase);

    cache[hash] = computedPhrase;

    return cache[hash];
  };
})();

export default i18n;

/**
 * Дополнить функцию интернационализации дополнительным обработчиком или изменить существующий
 * @param {string} name Имя обработчика
 * @param {function|object} pipe Обработчик
 * @param {object} [options] Опции
 */
export const injectPipe = (name, pipe, { isGlobal } = {}) => {
  if (isGlobal) {
    invariant(name.includes('global') || name.includes('Global'), `i18n: global pipe with name ${name} must include "global" in name`);
    globalPipes[name] = { ...pipe };
  } else {
    pipesMap[name] = pipe;
  }
};


function parseToken(token, variables, language) {
  const trimToken = token.replace(/\{|\}/g, '').trim();
  const pipes = trimToken.split('|').map(pipe => pipe.trim());
  const variableName = pipes.shift();
  const value = variables[variableName];

  const result = pipes.reduce((acc, pipeString) => {
    const { pipe, pipeContent } = parsePipe(pipeString);

    return pipe({
      content: pipeContent,
      variableName,
      language
    }, acc);
  }, { value, meta: {} });

  if (!result.value && result.value !== 0) {
    result.value = '';
  }

  return result;
}

function parsePipe(content) {
  const [, pipeName, pipeContent] = content.match(/(\w+) *\[(.*?)\]/);
  const pipe = pipesMap[pipeName] || (globalPipes[pipeName] && globalPipes[pipeName].pipe);

  invariant(Boolean(pipe), `i18n: Can't find ${pipeName} pipe`);

  return {
    pipe,
    pipeContent
  };
}

// injecting pipes:
injectPipe('default', defaultPipe);
injectPipe('plural', pluralPipe);
injectPipe('then', thenPipe);
injectPipe('globalDefault', {
  pipe: globalDefaultPipe,
  after: globalReplacer
}, { isGlobal: true });
injectPipe('globalThen', {
  pipe: globalThenPipe,
  after: globalReplacer
}, { isGlobal: true });

function globalReplacer(computedPhrase, phraseMeta) {
  if (phraseMeta.replaceAllWithText !== undefined) {
    return phraseMeta.replaceAllWithText;
  }

  return computedPhrase;
}

function globalDefaultPipe({ content }, data) {
  const { value } = data;

  if (!value && value !== 0) {
    return {
      ...data,
      value,
      meta: {
        ...data.meta,
        replaceAllWithText: content
      }
    };
  }

  return {
    ...data,
    value: ''
  };
}

function globalThenPipe({ content }, data) {
  const { value } = data;

  if (value) {
    return {
      ...data,
      value,
      meta: {
        ...data.meta,
        replaceAllWithText: content
      }
    };
  }

  return {
    ...data,
    value: ''
  };
}

function defaultPipe({ content, variableName }, data) {
  const { value } = data;
  invariant(Boolean(content), `i18n: If you use default pipe, you need set defaultValue in square brackets, like this "{${variableName}|default[some value]}"`);

  return {
    ...data,
    value: !value && value !== 0 ? content : value
  };
}

function thenPipe({ content, variableName }, data) {
  const { value } = data;
  invariant(Boolean(content), `i18n: If you use then pipe, you need set value for true condition in square brackets, like this "{${variableName}|then[some value]}"`);

  return {
    ...data,
    value: !value && value !== 0 ? '' : content
  };
}

function pluralPipe({ content, variableName, language }, data) {
  const { value } = data;
  invariant(Boolean(content), `i18n: If you use plural pipe, you need set list of plurals in square brackets, like this "{${variableName}|plural[apple, apples]}"`);

  const plurals = content.split(',').map(plural => plural.trim());
  const pluralizeFunction = pluralLangMap[language] || pluralLangMap.default;

  return {
    ...data,
    value: pluralizeFunction(Number(value), plurals)
  };
}

function ruPlural(val, [one, few, many, other]) {
  if (val % 10 === 1 && val % 100 !== 11) {
    return one;
  }

  if (checkValueInRange(val % 10, 2, 4) && !checkValueInRange(val % 100, 12, 14)) {
    return few;
  }

  if (val === 0 || checkValueInRange(val % 10, 5, 9) || checkValueInRange(val % 100, 1, 14)) {
    return many;
  }

  return other || many || few || one;
}

function enPlural(val, [one, other]) {
  if (val === 1) {
    return one;
  }

  return other || one;
}

function checkValueInRange(value, min, max) {
  return value >= min && value <= max;
}
