import moment from 'moment';

/**
 * Форматирование даты
 * @param {string} date - дата в формате ISO
 * @param {string} format - формат
 * @returns {string} - отформатированная дата
 */
export default function getFormattedDate(date, format = 'DD.MM.YYYY') {
  return date && moment(date).isValid() ? moment(date).format(format) : '';
}

getFormattedDate.withTime = date => getFormattedDate(date, 'DD.MM.YYYY HH:mm:ss');

/**
 * Получение текущей даты в формате ISO
 * @param {string} param - (start, end) - начало или конец дня
 * @returns {string} - текущая дата в формате ISO
 */
getFormattedDate.today = param => {
  const today = moment();
  if (param === 'start') return today.startOf('day').format();
  if (param === 'end') return today.endOf('day').format();
  return today;
};
