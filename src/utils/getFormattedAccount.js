export default function getFormattedAccount(account) {
  if (!account) return '';
  const value = account
    .toString()
    .replace(/\D/g, '')
    .substring(0, 20);

  const result = [];
  value.split('').forEach((digit, i) => {
    result.push(digit);
    if (i !== value.length - 1 && (i === 4 || i === 7 || i === 8)) result.push('.');
  });
  return result.join('');
}
