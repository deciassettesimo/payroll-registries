export default function getRoutePath(routes, id) {
  const route = routes.find(item => item.id === id);
  return route ? route.path : null;
}
