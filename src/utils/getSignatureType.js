import { SIGNATURES_TYPES } from 'common/constants';
import localization from 'localization';

export default function getSignatureType(id) {
  return Object.keys(SIGNATURES_TYPES).find(key => SIGNATURES_TYPES[key].id === id);
}

getSignatureType.titleById = id => {
  const signatureType = getSignatureType(id);
  return signatureType ? localization.translate(SIGNATURES_TYPES[signatureType].titleKey) : '';
};
