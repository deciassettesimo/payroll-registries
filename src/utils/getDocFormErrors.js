export default function getDocFormErrors(docErrors, docErrorsMap, levels) {
  const result = {};
  docErrors
    .filter(error => levels.includes(error.level))
    .forEach(error => {
      const fieldsIds = docErrorsMap[error.id];
      if (fieldsIds) {
        fieldsIds.forEach(fieldId => {
          result[fieldId] = true;
        });
      }
    });
  return result;
}
