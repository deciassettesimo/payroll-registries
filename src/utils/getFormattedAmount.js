export default function getFormattedAmount(value, frictionLength = 2) {
  if (typeof value !== 'number') return '';
  const stringValue = value.toString();

  const minus = stringValue.indexOf('-') < 0 ? '' : '-';
  const preparedValue = stringValue
    .replace(',', '.')
    .replace(/[^\d\\.]/g, '')
    .replace(/^0+/, '0')
    .replace(/^0([\d\\.]+)$/, '$1');
  const dotIndex = preparedValue.indexOf('.');
  const integerValue = preparedValue.substring(0, dotIndex < 0 ? preparedValue.length : dotIndex);
  const decimalValue = dotIndex < 0 ? '' : preparedValue.substring(dotIndex + 1).substring(0, frictionLength);

  const integerResultValueArray = [];
  integerValue
    .split('')
    .reverse()
    .forEach((symbol, index) => {
      if (index && !(index % 3)) integerResultValueArray.unshift(' ');
      integerResultValueArray.unshift(symbol);
    });
  const integerResultValue = integerResultValueArray.join('');

  const emptyDecimal = Array(frictionLength)
    .fill('0')
    .join('');
  const decimalResultValue = `${decimalValue}${emptyDecimal}`.substring(0, frictionLength);

  if (!integerResultValue.length) return `${minus}0.${decimalResultValue}`;
  return `${minus}${integerResultValue}.${decimalResultValue}`;
}
