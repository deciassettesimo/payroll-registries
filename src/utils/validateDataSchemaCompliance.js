/**
 * Валидация данных при помощи JSON schema
 * Пример:
 * {
 *    type: "object",
 *    properties: {
 *      "id": {
 *        type: "string"
 *        required: true // если поле обязательно для заполнения
 *      },
 *      "requisites": {
 *        "type": "array",
 *        "items": {
 *          "$ref": "#/definitions/requisites" // ссылка на объект definitions
 *        }
 *      }
 *    }
 * }
 *
 * Валидирует данные: тип, обязательность заполнения, лишние поля (не описанные в схеме)
 * Валидирует схему на наличие полей "type" и если type === object должно быть поле "properties"
 */
class DataSchemaValidator {
  static isExists(value) {
    return value !== null && value !== undefined;
  }

  static getType(value) {
    return DataSchemaValidator.isExists(value)
      ? value.constructor
          .toString()
          .slice(9, -20)
          .toLowerCase()
      : value;
  }

  static getValueTypeError(path, receivedType, expectedType) {
    return `Failed value: Invalid value '${path.join('.')}' of type "${receivedType}", expected type "${expectedType}"`;
  }

  static logError(error) {
    console.warn(error); /* eslint-disable-line */
  }

  constructor(schema, url) {
    // эквивалентные типы, при сравнении приравниваются
    this.equivalentsTypes = [
      ['object'],
      ['array'],
      ['string', 'blob', 'UUID', 'date'],
      ['number', 'integer', 'float'],
      ['boolean'],
    ];
    this.url = url;
    this.definitions = schema.definitions;
    this.schema = this.parseSchema(schema);
    this.validateErrors = [];
    this.schemaErrors = [];
    this.structureDataErrors = [];
  }

  run(data) {
    this.validateErrors = [];
    this.schemaErrors = [];
    this.structureDataErrors = [];
    this.data = data;

    this.validateTypeAndStructureSchema(this.schema, data);
    this.structureDataValidate(this.schema, data);

    if (this.schemaErrors.length) {
      DataSchemaValidator.logError(
        `----- ${this.url}: [${this.schemaErrors.length}] JSON SCHEMA STRUCTURE ERROR -----`,
      );
      DataSchemaValidator.logError(this.schemaErrors);
    }
    if (this.validateErrors.length) {
      DataSchemaValidator.logError(`----- ${this.url}: [${this.validateErrors.length}] MATCH TYPES ERROR -----`);
      DataSchemaValidator.logError(this.validateErrors);
    }
    if (this.structureDataErrors.length) {
      DataSchemaValidator.logError(
        `----- ${this.url}: [${this.structureDataErrors.length}] DATA STRUCTURE ERROR -----`,
      );
      DataSchemaValidator.logError(this.structureDataErrors);
    }
  }

  validateTypeAndStructureSchema(schema, data, path = []) {
    const targetPath = path;
    const schemaType = schema.type;
    const dataType = DataSchemaValidator.getType(data);
    const isRequired = schema.required;

    if (schemaType === 'object' && dataType === schemaType) {
      const validateData = { ...data };
      const { properties } = schema;
      const schemaProperties = properties ? Object.keys(properties) : [];
      // валидируем структуру схемы
      this.getSchemaStructureErrorObject(targetPath, properties);

      schemaProperties.forEach(key => {
        const validateDataValue = validateData[key];
        const schemaOfProperty = properties[key];

        this.validateTypeAndStructureSchema(schemaOfProperty, validateDataValue, [...targetPath, key]);
      });
    }

    if (schemaType === 'array' && dataType === schemaType) {
      const validateData = [...data];
      const { items } = schema;
      // валидируем структуру схемы
      this.getSchemaStructureErrorArray(targetPath, items);

      validateData.forEach((item, index) => {
        const itemSchema = DataSchemaValidator.getType(items) === 'object' ? items : items[0];

        this.validateTypeAndStructureSchema(itemSchema, item, [...targetPath, `[${index}]`]);
      });
    }

    // проверяем наличие типа в схеме
    this.getSchemaTypeError(targetPath, schemaType);

    // проверяем на обязательность наличия значения в данных
    if (isRequired && !DataSchemaValidator.isExists(data)) {
      this.validateErrors.push(`Failed value: field '${targetPath.join('.')}' is required but value is ${data}`);
    }

    // сопоставляем тип данных и тип описанный в схеме
    const result = DataSchemaValidator.getValueTypeError(targetPath, dataType, schemaType);

    if (dataType && !this.compareTypes(dataType, schemaType)) {
      this.validateErrors.push(result);
    }
  }

  structureDataValidate(schema, data, path = []) {
    const targetPath = path;
    const dataType = DataSchemaValidator.getType(data);

    // валидируем структуру данных
    if (!DataSchemaValidator.isExists(schema)) {
      this.structureDataErrors.push(
        `Failed structure of data: schema does not have property "${targetPath.join('.')}"`,
      );
    }

    if (dataType === 'object') {
      const validateData = { ...data };
      const validateDataKeys = Object.keys(validateData);

      validateDataKeys.forEach(key => {
        const validateDataValue = validateData[key];
        const properties = schema && schema.properties;
        const schemaProperty = properties ? properties[key] : null;

        this.structureDataValidate(schemaProperty, validateDataValue, [...targetPath, key]);
      });
    }

    if (dataType === 'array') {
      const validateData = [...data];
      const items = schema && schema.items;

      if (items) {
        validateData.forEach((item, index) => {
          const itemSchema = DataSchemaValidator.getType(items) === 'object' ? items : items[0];

          this.structureDataValidate(itemSchema, item, [...targetPath, `[${index}]`]);
        });
      }
    }
  }

  parseSchema(schema) {
    const { type } = schema;

    /**
     * свойство 'allof' возникает в результате автогенерации схемы из доменной модели
     * если аналитик не исправил структуру - пытаемся поправить ее тут
     */
    if (schema.properties) {
      const rootPropertiesKeys = Object.keys(schema.properties);
      rootPropertiesKeys.forEach(rootPropertyKey => {
        const innerProperty = schema.properties[rootPropertyKey];

        if (DataSchemaValidator.getType(innerProperty) === 'object') {
          const innerPropertyKeys = Object.keys(innerProperty);
          innerPropertyKeys.forEach(innerPropertyKey => {
            if (innerPropertyKey === 'allof') {
              schema.properties = innerProperty[innerPropertyKey][0]; /* eslint-disable-line */
            }
          });
        }
      });
    }

    let parsedSchema = this.findRefs(schema);

    if (type === 'object') {
      const { properties } = parsedSchema;

      if (properties.properties) {
        parsedSchema = properties;
      }
    }

    return parsedSchema;
  }

  findRefs = object => {
    let data = { ...object };
    let keys = Object.keys(data);

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      const value = data[key];

      // если $ref то заменяем это поле данными из ссылки и проходимся по всем полям снова
      if (key === '$ref') {
        const definitionKeyRef = value.replace('#/definitions/', '');
        const { $ref, ...otherData } = data;

        data = { ...otherData, ...this.definitions[definitionKeyRef] };
        keys = Object.keys(data);
        i = 0;
      }

      if (DataSchemaValidator.getType(value) === 'object') {
        data[key] = this.findRefs(value);
      }
    }

    return data;
  };

  compareTypes(typeA, typeB) {
    return !!this.equivalentsTypes.filter(
      equivalentsTypes => equivalentsTypes.includes(typeA) && equivalentsTypes.includes(typeB),
    ).length;
  }

  getSchemaTypeError(path, type) {
    const allowedTypes = this.equivalentsTypes.reduce((result, types) => result.concat(types), []);

    if (!type) {
      this.schemaErrors.push(`Failed type of schema: in property '${path.join('.')}' field type is not exists`);
    } else if (!allowedTypes.includes(type)) {
      this.schemaErrors.push(
        `Failed type of schema: in property '${path.join('.')}' type "${type}" does not have an equivalent type`,
      );
    }
  }

  getSchemaStructureErrorObject(path, properties) {
    const keys = properties ? Object.keys(properties) : [];

    if (!keys.length) {
      this.schemaErrors.push(
        `Failed structure of schema: value '${
          path.length ? path.join('.') : '$schema'
        }' have type "object" but "properties" ${properties ? 'is empty object' : 'is not exists'}`,
      );
    }
  }

  getSchemaStructureErrorArray(path, items) {
    const keys = items ? Object.keys(items) : [];

    if (!keys.length) {
      this.schemaErrors.push(
        `Failed structure of schema: value '${path.join('.')}' have type "array" but "items" ${
          items ? 'is empty object' : 'is not exists'
        }`,
      );
    }
  }
}

export default function validateDataSchemaCompliance(data = {}, schema = {}, url = '') {
  const validator = new DataSchemaValidator(schema, url);
  validator.run(data);
}
