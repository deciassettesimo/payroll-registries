export default function mapDataToStructure(structure, data) {
  return structure.reduce(
    (tabs, tab) =>
      tab.sections
        ? {
            ...tabs,
            [tab.id]: tab.sections.reduce(
              (sections, section) =>
                section.fields
                  ? {
                      ...sections,
                      [section.id]: section.fields.reduce(
                        (fields, field) => (data[field] ? { ...fields, [field]: data[field] } : fields),
                        {},
                      ),
                    }
                  : sections,
              {},
            ),
          }
        : tabs,
    {},
  );
}
