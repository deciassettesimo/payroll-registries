export default function composeObjects(keys, data) {
  return keys.reduce(
    (result, key) => ({
      ...result,
      [key]: Object.keys(data).reduce((dataResult, dataKey) => ({ ...dataResult, [dataKey]: data[dataKey][key] }), {}),
    }),
    {},
  );
}
