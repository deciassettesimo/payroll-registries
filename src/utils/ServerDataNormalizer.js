import { fromJS, Map } from 'immutable';

/**
 * Принимает карту соответсвий серверных ключей к клиентскими
 *
 * Возвращает объект с двумя методами нормализации "fromServer" и "toServer"
 *
 * mapKeys {Object} - словарь ключей { serverKey: 'clientKey' }
 * можно изменять вложенность и наименование вложенных полей, например:
 *
 * пример метода fromServer :
 *
 * mapKeys = { 'document.number': 'docNumber' }
 * dataFromServer = { document: { number: 123 } }
 * return { docNumber: 123 }
 *
 * mapKeys = { 'docNote': 'document.note' }
 * dataFromServer = { docNote: 'hello world' }
 * return { document: { note: 'hello world' } }
 *
 * dataFromServer {Object} - объект данных пришедший с сервера, который нужно нормализовать
 *
 * метод "toServer" меняет ключи на исходные
 */

export default class ServerDataNormalizer {
  constructor(mapKeys) {
    this.mapKeys = mapKeys;
  }

  fromServer(dataFromServer) {
    const data = fromJS(dataFromServer);
    let result = Map();
    const immutableMap = Map(this.mapKeys);
    const serverKeys = Object.keys(this.mapKeys);
    const serverKeyPaths = serverKeys.map(key => key.split('.'));

    serverKeyPaths.forEach(serverKeyPath => {
      const value = data.getIn(serverKeyPath, null);

      if (value !== null) {
        const clientKey = immutableMap.get(serverKeyPath.join('.'));
        const clientKeyPath = clientKey.split('.');

        result = result.deleteIn(serverKeyPath).setIn(clientKeyPath, value);
      }
    });
    return result.toJS();
  }

  toServer(clientData) {
    const data = fromJS(clientData);
    let result = Map();

    // переворачиваем mapKeys: key <-> value
    const serverKeys = Object.keys(this.mapKeys);
    const revertedMapKeys = {};
    serverKeys.forEach(serverKey => {
      revertedMapKeys[this.mapKeys[serverKey]] = serverKey;
    });
    const revertedMapKeysImmutable = Map(revertedMapKeys);

    const clientKeys = Object.keys(revertedMapKeys);
    const clientKeyPaths = clientKeys.map(key => key.split('.'));

    clientKeyPaths.forEach(clientKeyPath => {
      const value = data.getIn(clientKeyPath, null);

      if (value !== null) {
        const serverKey = revertedMapKeysImmutable.get(clientKeyPath.join('.'));
        const serverKeyPath = serverKey.split('.');

        result = result.deleteIn(clientKeyPaths).setIn(serverKeyPath, value);
      }
    });

    return result.toJS();
  }
}
