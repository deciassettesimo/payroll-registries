import { DOC_STATUS_ID } from 'common/constants';

export default function getIsDocNeedGetErrors(status) {
  return [
    DOC_STATUS_ID.CREATED,
    DOC_STATUS_ID.CREATED_WITH_WARNINGS,
    DOC_STATUS_ID.IMPORTED,
    DOC_STATUS_ID.INVALID_CONTROLS,
  ].includes(status);
}
