export default function blobToJSON(blob, callback) {
  const reader = new FileReader(); /* eslint-disable-line */
  reader.readAsText(blob);
  reader.onloadend = () => {
    callback(reader.result ? JSON.parse(reader.result) : '');
  };
}
