import { put } from 'redux-saga/effects';

import { API_CANCELED_MESSAGE } from 'dal/constants';

export default function* dispatchErrorAction({ error, types, payload }) {
  if (Array.isArray(types) && types.length) {
    if (error.message === API_CANCELED_MESSAGE && types[1]) yield put({ type: types[1], error, payload });
    else yield put({ type: types[0], error, payload });
  }
}
