export default function getFilterForQuery(filter) {
  return {
    id: filter.id,
    isPreset: filter.isPreset || false,
    isChanged: filter.isChanged || false,
    isNew: filter.isNew || false,
    params: filter.params.map(item => ({ id: item.id, value: item.value })),
  };
}
