export default function windowRefresh() {
  let event;

  if (typeof Event === 'function') {
    event = new Event('resize'); /* eslint-disable-line */
  } else {
    event = document.createEvent('Event');
    event.initEvent('resize', true, true);
  }

  window.dispatchEvent(event);
}
