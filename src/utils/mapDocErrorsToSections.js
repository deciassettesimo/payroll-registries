import getCombinedErrorText from './getCombinedErrorText';

/**
 * Маппинг секций и ошибок документа NEW ARCHITECTURE!
 * @param {array} docSections - секции документа
 * @param {array} docErrors - ошибки документа
 * @param {object} docErrorsMap - маппинг ошибок к полям документа
 * @param {string} activeDocField - активное поле документа (для того, чтобы сделать активными ошибки и секцию)
 * @param {string} activeError - активная ошибка (для того, чтобы сделать активными ошибки и секцию)
 * @returns {array}
 */
export default function mapDocErrorsToSections(docSections, docErrors, docErrorsMap, activeDocField, activeError) {
  if (!docSections) return null;
  const mappingDocErrorsToFields = docErrors.map(error => ({
    ...error,
    text: error.errors ? getCombinedErrorText(error.text, error.errors, 2) : error.text,
    fieldsIds: docErrorsMap[error.id] || [],
    isActive:
      (docErrorsMap[error.id] && !!docErrorsMap[error.id].find(i => i === activeDocField)) || error.id === activeError,
  }));
  const activeDocError = mappingDocErrorsToFields.find(e => e.isActive);
  return docSections.map(section => ({
    ...section,
    isActive:
      (activeDocField && !!section.fields.includes(activeDocField)) ||
      (activeDocError && !!section.fields.find(field => activeDocError.fieldsIds.includes(field))),
    errors: mappingDocErrorsToFields
      .filter(error => !!section.fields.find(field => error.fieldsIds.find(e => field === e)))
      .sort((a, b) => {
        if (a.level === b.level) {
          return section.fields.indexOf(a.fieldsIds[0]) > section.fields.indexOf(b.fieldsIds[0]) ? 1 : -1;
        }
        return a.level < b.level ? 1 : -1;
      }),
  }));
}
