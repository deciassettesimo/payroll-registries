export default function getFilterFromQuery(filter) {
  if (!filter) return null;
  return {
    id: filter.id || null,
    isPreset: filter.isPreset === 'true',
    isChanged: filter.isChanged === 'true',
    isNew: filter.isNew === 'true',
    params: filter.params.map(item => ({ id: item.id, value: item.value })),
  };
}
