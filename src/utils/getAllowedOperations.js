import { OPERATION_ID, DOC_STATUS_ID } from 'common/constants';
import { ALLOWED_SM_ACTION_ID } from 'dal/constants';

/**
 * Фильтрация доступных операций для документа
 * @param {array} operations - все операции
 * @param {string} status - статус документа
 * @param {object} allowed - доступные операции с сервера
 * @param {bool} isTemplate
 * @returns {array}
 */
export default function getAllowedOperations({ operations, status, allowed, isTemplate }) {
  return operations.filter(operation => {
    switch (operation.id) {
      case OPERATION_ID.BACK:
        return true;
      case OPERATION_ID.EXPORT:
      case OPERATION_ID.STATE_HISTORY:
      case OPERATION_ID.PRINT:
      case OPERATION_ID.REPEAT:
      case OPERATION_ID.SAVE_AS_TEMPLATE:
        return !isTemplate && status !== DOC_STATUS_ID.NEW;
      case OPERATION_ID.ARCHIVE:
      case OPERATION_ID.RECALL:
      case OPERATION_ID.SEND:
      case OPERATION_ID.SIGN:
        return !isTemplate && status !== DOC_STATUS_ID.NEW && allowed[operation.id];
      case OPERATION_ID.REMOVE:
        return status !== DOC_STATUS_ID.NEW && allowed[ALLOWED_SM_ACTION_ID.REMOVE];
      case OPERATION_ID.REMOVE_SIGN:
        return status !== DOC_STATUS_ID.NEW && allowed[ALLOWED_SM_ACTION_ID.REMOVE_SIGN];
      case OPERATION_ID.UNARCHIVE:
        return !isTemplate && status !== DOC_STATUS_ID.NEW && allowed[ALLOWED_SM_ACTION_ID.UNARCHIVE];
      case OPERATION_ID.SAVE:
        return status === DOC_STATUS_ID.NEW || allowed[operation.id];
      case OPERATION_ID.SIGN_AND_SEND:
        return (
          !isTemplate &&
          (status === DOC_STATUS_ID.NEW || allowed[ALLOWED_SM_ACTION_ID.SAVE] || allowed[ALLOWED_SM_ACTION_ID.SIGN])
        );
      case OPERATION_ID.VISA:
        return !isTemplate && status !== DOC_STATUS_ID.NEW && allowed[ALLOWED_SM_ACTION_ID.SIGN];
      case OPERATION_ID.CHECK_SIGN:
        return (
          !isTemplate &&
          [
            DOC_STATUS_ID.ACCEPTED,
            DOC_STATUS_ID.ACCEPTED_BY_ABS,
            DOC_STATUS_ID.ACCEPTED_BY_CC,
            DOC_STATUS_ID.ACCEPTED_FOR_PROCESSING,
            DOC_STATUS_ID.APPROVED_BY_CC,
            DOC_STATUS_ID.CHECKED,
            DOC_STATUS_ID.CHECKED_REQUIRE_CLARIFICATIONS,
            DOC_STATUS_ID.CLOSED,
            DOC_STATUS_ID.DECLINED,
            DOC_STATUS_ID.DECLINED_BY_BANK,
            DOC_STATUS_ID.DELIVERED,
            DOC_STATUS_ID.EXECUTED,
            DOC_STATUS_ID.INVALID_PROPS,
            DOC_STATUS_ID.INVALID_SIGN,
            DOC_STATUS_ID.PARTLY_EXECUTED,
            DOC_STATUS_ID.PARTLY_SIGNED,
            DOC_STATUS_ID.PROCESSED,
            DOC_STATUS_ID.PROCESSED_BY_CC,
            DOC_STATUS_ID.QUEUED,
            DOC_STATUS_ID.RECALLED,
            DOC_STATUS_ID.REFUSED_BY_CC,
            DOC_STATUS_ID.SIGNED,
            DOC_STATUS_ID.UNLOADED,
          ].includes(status)
        );
      case OPERATION_ID.REPEAT_WITH_REFUSALS:
        return !isTemplate && [DOC_STATUS_ID.DECLINED_BY_BANK, DOC_STATUS_ID.PARTLY_EXECUTED].includes(status);
      case OPERATION_ID.CREATE_FROM_TEMPLATE:
        return isTemplate;
      default:
        return false;
    }
  });
}
