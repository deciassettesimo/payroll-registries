export default function blobToBase64(blob, callback) {
  const reader = new FileReader(); /* eslint-disable-line */
  reader.readAsDataURL(blob);
  reader.onloadend = () => {
    callback(reader.result ? reader.result.split(',')[1] : '');
  };
}
