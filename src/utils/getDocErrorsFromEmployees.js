import { EMPLOYEES_CONTROLS } from 'common/constants';

import getStatus from './getStatus';

export default function getDocumentErrorsFromEmployees(employees, employeeStatusFieldId) {
  const result = [];
  const employeesWithWarnings = !!employees.filter(item => {
    const employeeStatus = getStatus.employee.byServerValue(item[employeeStatusFieldId]);
    return employeeStatus ? getStatus.employee.isWarning(employeeStatus.id) : false;
  }).length;
  if (employeesWithWarnings) {
    result.push(EMPLOYEES_CONTROLS.WARNINGS);
  }
  const employeesWithErrors = !!employees.filter(item => {
    const employeeStatus = getStatus.employee.byServerValue(item[employeeStatusFieldId]);
    return employeeStatus ? getStatus.employee.isError(employeeStatus.id) : false;
  }).length;
  if (employeesWithErrors) {
    result.push(EMPLOYEES_CONTROLS.ERRORS);
  }
  return result;
}
