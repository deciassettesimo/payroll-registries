import {
  DOC_STATUSES,
  DOC_STATUS_ID,
  DOC_STATUS_SERVER_VALUE,
  EMPLOYEE_STATUSES,
  EMPLOYEE_STATUS_ID,
} from 'common/constants';
import localization from 'localization';

export default function getStatus(id) {
  return Object.keys(DOC_STATUSES).find(key => DOC_STATUSES[key].id === id);
}

getStatus.byServerValue = serverValue => {
  const status = Object.keys(DOC_STATUSES).find(key => DOC_STATUSES[key].serverValue === serverValue);
  return status ? DOC_STATUSES[status] : null;
};

getStatus.idByServerValue = serverValue => {
  const status = getStatus.byServerValue(serverValue);
  return status ? status.id : null;
};

getStatus.serverValueById = id => {
  const status = getStatus(id);
  return status ? DOC_STATUSES[status].serverValue : null;
};

getStatus.titleById = id => {
  const status = getStatus(id);
  return status ? localization.translate(DOC_STATUSES[status].titleKey) : '';
};

getStatus.isSuccess = id =>
  [
    DOC_STATUS_ID.ACCEPTED,
    DOC_STATUS_ID.PROCESSED,
    DOC_STATUS_ID.EXECUTED,
    DOC_STATUS_ID.ACCEPTED_FOR_PROCESSING,
    DOC_STATUS_ID.APPROVED_OR_PROCESSED,
    DOC_STATUS_ID.ACCEPTED_BY_CC,
    DOC_STATUS_ID.ACCEPTED_BY_ABS,
    DOC_STATUS_ID.PROCESSED_BY_CC,
  ].includes(id);

getStatus.isSuccess.byServerValue = serverValue =>
  [
    DOC_STATUS_SERVER_VALUE.ACCEPTED,
    DOC_STATUS_SERVER_VALUE.PROCESSED,
    DOC_STATUS_SERVER_VALUE.EXECUTED,
    DOC_STATUS_SERVER_VALUE.ACCEPTED_FOR_PROCESSING,
    DOC_STATUS_SERVER_VALUE.APPROVED_OR_PROCESSED,
    DOC_STATUS_SERVER_VALUE.ACCEPTED_BY_CC,
    DOC_STATUS_SERVER_VALUE.ACCEPTED_BY_ABS,
    DOC_STATUS_SERVER_VALUE.PROCESSED_BY_CC,
  ].includes(serverValue);

getStatus.isWarning = id => [DOC_STATUS_ID.CREATED_WITH_WARNINGS].includes(id);

getStatus.isWarning.byServerValue = serverValue =>
  [DOC_STATUS_SERVER_VALUE.CREATED_WITH_WARNINGS].includes(serverValue);

getStatus.isError = id =>
  [
    DOC_STATUS_ID.INVALID_CONTROLS,
    DOC_STATUS_ID.INVALID_PROPS,
    DOC_STATUS_ID.INVALID_SIGN,
    DOC_STATUS_ID.DECLINED,
    DOC_STATUS_ID.DECLINED_BY_BANK,
    DOC_STATUS_ID.DECLINED_BY_ABS,
    DOC_STATUS_ID.RECALLED,
    DOC_STATUS_ID.REFUSED_BY_CC,
  ].includes(id);

getStatus.isError.byServerValue = serverValue =>
  [
    DOC_STATUS_SERVER_VALUE.INVALID_CONTROLS,
    DOC_STATUS_SERVER_VALUE.INVALID_PROPS,
    DOC_STATUS_SERVER_VALUE.INVALID_SIGN,
    DOC_STATUS_SERVER_VALUE.DECLINED,
    DOC_STATUS_SERVER_VALUE.DECLINED_BY_BANK,
    DOC_STATUS_SERVER_VALUE.DECLINED_BY_ABS,
    DOC_STATUS_SERVER_VALUE.RECALLED,
    DOC_STATUS_SERVER_VALUE.REFUSED_BY_CC,
  ].includes(serverValue);

getStatus.employee = {
  byServerValue: serverValue => {
    const status = Object.keys(EMPLOYEE_STATUSES).find(key => EMPLOYEE_STATUSES[key].serverValue === serverValue);
    return status ? EMPLOYEE_STATUSES[status] : null;
  },
  isSuccess: id => [EMPLOYEE_STATUS_ID.EXECUTED].includes(id),
  isWarning: id => [EMPLOYEE_STATUS_ID.CREATED_WITH_WARNINGS].includes(id),
  isError: id =>
    [
      EMPLOYEE_STATUS_ID.INVALID_CONTROLS,
      EMPLOYEE_STATUS_ID.ERROR_REQUIRE_CLARIFICATIONS,
      EMPLOYEE_STATUS_ID.DECLINED_BY_BANK,
    ].includes(id),
};
