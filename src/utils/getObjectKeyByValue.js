export default function getObjectKeyByValue(obj, value) {
  return Object.keys(obj).find(key => obj[key] === value);
}
