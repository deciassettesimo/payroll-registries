import { FROM_BANK_INFO_TYPES } from 'common/constants';

import getFormattedDate from './getFormattedDate';

export default function getFormattedFromBankInfoValue(type, value, ref) {
  switch (type) {
    case FROM_BANK_INFO_TYPES.DATE:
      return getFormattedDate(value);
    case FROM_BANK_INFO_TYPES.REF:
      return ref[value];
    case FROM_BANK_INFO_TYPES.STRING:
    default:
      return value;
  }
}
