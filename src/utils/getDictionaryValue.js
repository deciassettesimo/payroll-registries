export default function getDictionaryValue(value, dictionary, inKey, outKey = null) {
  if (!dictionary || !dictionary.items) return null;
  const selectedItem = dictionary.items.find(item => item[inKey] === value);
  if (outKey) return selectedItem ? selectedItem[outKey] : null;
  return selectedItem || null;
}
