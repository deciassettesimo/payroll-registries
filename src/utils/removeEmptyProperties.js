export default function removeEmptyProperties(params, removeEmptyArraysFlag) {
  if (!(params instanceof Object)) return params;
  const result = params;
  Object.keys(params).forEach(key => {
    if (result[key] === undefined || result[key] === null || result[key] === '') {
      delete result[key];
    } else if (result[key] instanceof Array) {
      result[key] = result[key].map(item => removeEmptyProperties(item, removeEmptyArraysFlag));
      if (removeEmptyArraysFlag && !result[key].length) delete result[key];
    } else if (result[key] instanceof Object) {
      result[key] = removeEmptyProperties(result[key], removeEmptyArraysFlag);
      if (!Object.keys(result[key]).length) delete result[key];
    }
  });
  return result;
}
