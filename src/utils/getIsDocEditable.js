import { DOC_STATUS_ID } from 'common/constants';

export default function getIsDocEditable(status, isArchived, isTemplate) {
  if (isTemplate) return true;
  if (isArchived) return false;
  return [
    DOC_STATUS_ID.CREATED,
    DOC_STATUS_ID.CREATED_WITH_WARNINGS,
    DOC_STATUS_ID.IMPORTED,
    DOC_STATUS_ID.INVALID_CONTROLS,
    DOC_STATUS_ID.NEW,
  ].includes(status);
}
