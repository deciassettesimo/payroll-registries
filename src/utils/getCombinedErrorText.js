export default function getCombinedErrorText(text, childrenErrors, level) {
  let result = `${text}:`;
  const spaces = new Array(level).join(' ');
  childrenErrors.forEach(item => {
    if (item.errors) result = `${result}\n${spaces}- ${getCombinedErrorText(item.text, item.errors, level + 2)}`;
    else result = `${result}\n${spaces}- ${item.text}`;
  });
  return result;
}
