export default function escapeRegExp(text) {
  return text ? text.replace(/[-[\]{}()*+?.,\\^$|#]/gi, '\\$&') : '';
}
