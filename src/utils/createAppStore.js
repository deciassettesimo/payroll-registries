import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

export default function createAppStore(reducers, sagas) {
  const middleware = [];
  const enhancers = [];

  const sagaMiddleware = createSagaMiddleware();
  middleware.push(sagaMiddleware);

  if (process.env.NODE_ENV === 'development') {
    middleware.push(
      createLogger({
        level: 'log',
        collapsed: true,
        timestamp: false,
        colors: {
          title: ({ type }) => {
            if (/_REQUEST$/.test(type)) return '#0474c1';
            if (/_SUCCESS$/.test(type)) return '#4f901b';
            if (/_CANCELED$/.test(type)) return '#fe6729';
            if (/_FAIL$/.test(type)) return '#ca2e21';
            return '#681b5b';
          },
        },
      }),
    );

    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__; /* eslint-disable-line */
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers,
  );

  const store = createStore(combineReducers(reducers), composedEnhancers);

  sagaMiddleware.run(sagas);

  return store;
}
