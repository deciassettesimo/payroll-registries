export default function getScrollTopByDataId(elementDataId) {
  try {
    const elementOffsetTop = document.querySelector(`[data-id="${elementDataId}"]`).getBoundingClientRect().top;
    return elementOffsetTop + window.pageYOffset;
  } catch (error) {
    return 0;
  }
}
