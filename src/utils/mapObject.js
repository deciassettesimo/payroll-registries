export default function mapObject(func, object) {
  return Object.keys(object)
    .map(key => ({ key, value: func(object[key], key) }))
    .reduce((result, item) => ({ ...result, [item.key]: item.value }), {});
}
