export default function getFieldElementSelectorByIds(fieldIds) {
  const foundFields = fieldIds
    .filter(fieldId => !!document.querySelector(`#${fieldId}`) || !!document.querySelector(`[name=${fieldId}]`))
    .filter(
      fieldId =>
        !document.querySelector(`#${fieldId}[disabled=disabled]`) ||
        !document.querySelector(`[name=${fieldId}][disabled=disabled]`),
    );
  if (foundFields.length) {
    return document.querySelector(`#${foundFields[0]}`) || document.querySelector(`[name=${foundFields[0]}]`);
  }
  return null;
}
