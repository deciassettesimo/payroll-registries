const normalizeListFilters = (filters = [], conditions) => {
  const conditionsIds = conditions.map(condition => condition.id);
  return filters.filter(
    item => item.params.filter(condition => conditionsIds.includes(condition.id)).length === item.params.length,
  );
};

export default function normalizeListSettings(settings = {}, initialSettings, initialColumns, conditions) {
  return {
    filters: normalizeListFilters(settings.filters, conditions),
    visibility: settings.visibility || initialSettings.visibility,
    sorting:
      settings.sorting && initialColumns.find(collumn => collumn.id === settings.sorting.id)
        ? settings.sorting
        : initialSettings.sorting,
    pagination: settings.pagination || initialSettings.pagination,
    grouped: settings.grouped || initialSettings.grouped,
    width: (settingsWidth => {
      const result = {};
      initialColumns.forEach(item => {
        result[item.id] = settingsWidth[item.id] || initialSettings.width[item.id];
      });
      return result;
    })(settings.width || {}),
  };
}
