export default function normalizeQueryParams(params) {
  if (params.query && typeof params.query === 'object') {
    return { ...params, query: JSON.stringify(params.query) };
  }
  return params;
}
